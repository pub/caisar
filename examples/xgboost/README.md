# XGBoost example

The following command verifies an XGBoost model, exported in JSON format, on a
dataset, in CSV format, using the prover CVC5.

```
caisar verify-xgboost ../../lib/xgboost/example/california.json ../../lib/xgboost/example/california.csv --prover CVC5
```

The current verification consists in checking that every input that has each
component between elements of the dataset will have a prediction between the
minimum and maximum prediction of the dataset.

```
∀ e.
 (∀ i. ∃ e1 e2. e1,e2 ∈ dataset ⇒ e1[i] ≤ e[i] ≤ e2[i]) =>
  ∃ e1 e2. e1,e2 ∈ dataset ⇒ prediction(e1) ≤ predication(e) ≤ prediction(e2)
```
