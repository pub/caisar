import numpy as np


# Given a np array row, return a linear combination of the output
def f(x: np.ndarray) -> np.ndarray:
    return np.array((x[0] - x[1] - x[2]))


if __name__ == "__main__":
    arr1 = np.random.normal(size=(10000, 3))
    arr2 = np.apply_along_axis(f, 1, arr1)
    arr3 = np.random.normal(size=(10000, 3))
    arr4 = np.apply_along_axis(f, 1, arr1)
    np.save(file="data.npy", arr=arr1)
    np.save(file="target.npy", arr=arr2)
    np.save(file="test_data.npy", arr=arr3)
    np.save(file="test_target.npy", arr=arr4)
