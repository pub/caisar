theory MNIST

  use ieee_float.Float64
  use caisar.types.Float64WithBounds as Feature
  use caisar.types.IntWithBounds as Label
  use caisar.model.Model
  
  use caisar.dataset.CSV
  use caisar.robust.ClassRobustCSV

  constant model_filename: string
  constant dataset_filename: string

  constant label_bounds: Label.bounds =
    Label.{ lower = 0; upper = 9 }
  
  constant feature_bounds: Feature.bounds =
    Feature.{ lower = (0.0:t); upper = (1.0:t) }

  goal robustness:
    let nn = read_model model_filename in
    let dataset = read_dataset dataset_filename in
    let eps = (0.0100000000000000002081668171172168513294309377670288085937500000:t) in
    robust feature_bounds label_bounds nn dataset eps

end
