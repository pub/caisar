#!/usr/bin/env python3
"""
Modified by the AISER team, Software Safety and Security Laboratory, CEA-List.

This file is part of CAISAR.
This file is used for integrating Marabou, via its Python interface, in CAISAR.

Top contributors (to current version):
    - Andrew Wu

This file is part of the Marabou project.
Copyright (c) 2017-2021 by the authors listed in the file AUTHORS
in the top-level source directory) and their institutional affiliations.
All rights reserved. See the file COPYING in the top-level source
directory for licensing information.
"""

import argparse
import os
import pathlib
import shutil
import subprocess
import sys
import tempfile
from importlib.metadata import version

from maraboupy import Marabou, MarabouCore  # type: ignore


class WrongNetFormat(Exception):
    def __init__(self, networkPath):
        self.message = (
            f"Network {networkPath} has an unrecognized extension."
            f"The network must be in .pb, .nnet or .onnx format."
        )
        super().__init__(self.message)


sys.path.insert(0, os.path.join(str(pathlib.Path(__file__).parent.absolute()), "../"))


def maraboupy_version(marabou_binary: str):
    result = subprocess.run([marabou_binary, "--version"], capture_output=True, text=True)
    if result.returncode != 0:
        print(f"Error running {marabou_binary} --version")
        sys.exit(1)

    maraboupy_version = version("maraboupy")
    output = result.stdout.strip()
    if maraboupy_version in output:
        return f"maraboupy {maraboupy_version}"

    return sys.exit(1)


def arguments():
    parser = argparse.ArgumentParser(description="Thin wrapper around Maraboupy executable")
    parser.add_argument(
        "network",
        type=str,
        nargs="?",
        default=None,
        help="The network file name, the extension can be only .pb, .nnet, and .onnx",
    )
    parser.add_argument("prop", type=str, nargs="?", default=None, help="The property file name")
    parser.add_argument("-t", "--timeout", type=int, default=10, help="Timeout in seconds")
    parser.add_argument("--temp-dir", type=str, default="/tmp/", help="Temporary directory")
    marabou_path = shutil.which("Marabou")
    parser.add_argument(
        "--marabou-binary",
        type=str,
        default=marabou_path,
        help="The path to Marabou binary",
    )
    parser.add_argument(
        "--version",
        action="store_true",
        help="Output a version string if Maraboupy is present and exit",
    )
    parser.set_defaults(version=False)

    return parser


def main():
    args, unknown = arguments().parse_known_args()

    marabou_binary = args.marabou_binary
    if not os.access(marabou_binary, os.X_OK):
        sys.exit('"{}" does not exist or is not executable'.format(marabou_binary))
    else:
        if args.version:
            print(f"{maraboupy_version(marabou_binary)}")
        else:
            assert args.network is not None
            assert args.prop is not None

            networkPath = args.network
            suffix = networkPath.split(".")[-1]
            if suffix == "nnet":
                network = Marabou.read_nnet(networkPath)
            elif suffix == "pb":
                network = Marabou.read_tf(networkPath)
            elif suffix == "onnx":
                network = Marabou.read_onnx(networkPath)
            else:
                raise WrongNetFormat(networkPath)

            query = network.getInputQuery()
            MarabouCore.loadProperty(query, args.prop)
            temp = tempfile.NamedTemporaryFile(dir=args.temp_dir, delete=False)
            name = temp.name
            timeout = args.timeout
            MarabouCore.saveQuery(query, name)

            print("Running Marabou with the following arguments: ", unknown)
            subprocess.run(
                [marabou_binary] + ["--input-query={}".format(name)] + ["--timeout={}".format(timeout)] + unknown
            )
            os.remove(name)


if __name__ == "__main__":
    main()
