#!/usr/bin/env python3
###########################################################################
#                                                                         #
#  This file is part of CAISAR.                                           #
#                                                                         #
#  Copyright (C) 2025                                                     #
#    CEA (Commissariat à l'énergie atomique et aux énergies               #
#         alternatives)                                                   #
#                                                                         #
#  You can redistribute it and/or modify it under the terms of the GNU    #
#  Lesser General Public License as published by the Free Software        #
#  Foundation, version 2.1.                                               #
#                                                                         #
#  It is distributed in the hope that it will be useful,                  #
#  but WITHOUT ANY WARRANTY; without even the implied warranty of         #
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the           #
#  GNU Lesser General Public License for more details.                    #
#                                                                         #
#  See the GNU Lesser General Public License version 2.1                  #
#  for more details (enclosed in the file licenses/LGPLv2.1).             #
#                                                                         #
###########################################################################

import sys
import importlib
import importlib.util
from importlib import import_module

module_name = str(sys.argv[1])

spec = importlib.util.find_spec(module_name)

if spec is None:
    # [module_name] cannot be found: most likely, the PYTHONPATH has not been
    # correctly set up.
    exit(1)

# [module_name] has been found. However, this is not sufficient to conclude that
# its interpretation/execution will not fail, e.g., due to unmet dependencies.
try:
    import_module(module_name)
except ModuleNotFoundError:
    exit(1)

print("dummy-version")
exit(0)
