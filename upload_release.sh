#!/usr/bin/env bash
# set -xe

usage () { echo "Usage: ./upload_release.sh -g GITLAB_TOKEN -t TAG -d DESCRIPTION -p PKG -u PACKAGE_URL."; }

while getopts ":g:t:d:p:u:" opt; do
  case $opt in
    g) GITLAB_TOKEN="$OPTARG"
    ;;
    t) TAG="$OPTARG"
    ;;
    d) DESCRIPTION="$OPTARG"
    ;;
    p) PKG="$OPTARG"
    ;;
    u) PACKAGE_URL="$OPTARG"
    ;;
    \?) echo "Invalid option -$OPTARG. " >&2
    exit 1
    ;;
    :) echo "Missing option argument for -$OPTARG" >&2; exit 1;;
  esac
done

if [ -z "$GITLAB_TOKEN" ] || [ -z "$TAG" ] || [ -z "$DESCRIPTION" ] || [ -z "$PKG" ]|| [ -z "$PACKAGE_URL" ] ; then
    echo "Missing one of the following arguments: -g, -t, -d, -p, -u. $(usage)" >&2
        exit 1
fi

curl --header 'Content-Type: application/json' \
--header "PRIVATE-TOKEN: $GITLAB_TOKEN" \
--request POST "https://git.frama-c.com/api/v4/projects/1082/releases" \
--data  '{"name": "Release '$TAG'","tag_name": "'$TAG'", "description": "'"$DESCRIPTION"'", "milestones":[], "ref":"master", "assets": {"links": [{"name":"'$PKG'","url":"'$PACKAGE_URL'", "link_type":"other"}]}}'
