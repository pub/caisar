all:
	dune build --root=. @install caisar.opam

all-ci:
	dune build -j2 --root=. @install caisar.opam

install:
	dune install

uninstall:
	dune uninstall

lint:
	dune fmt -j2

test:
	dune runtest

test-ci:
	dune build -j2 @runtest @ci

promote:
	dune promote --root=.

docker:
	$(MAKE) -C docker all

clean:
	dune clean

doc:
	$(MAKE) -C doc html latexpdf

view-doc: doc
	x-www-browser doc/_build/html/index.html

###################################
# opam package creation and release
###################################

PROJECT_ID=1082
VERSION=$(shell cat VERSION)
MAJOR=$(shell echo $(VERSION) | cut -d'.' -f1)
MINOR=$(shell echo $(VERSION) | cut -d'.' -f2)
REV=$(shell echo $(VERSION) | cut -d'.' -f3)
TAG=$(MAJOR).$(MINOR)
PKG="caisar-$(TAG).tbz"
PACKAGE_URL="https://git.frama-c.com/api/v4/projects/$(PROJECT_ID)/packages/generic/caisar/$(TAG)/$(PKG)"
DESCRIPTION="$(shell sed -n -e "p;n;:next;/^##/Q;p;n;b next" CHANGES.md | perl -pe 's/\n/\\n/')"

release:
	@echo "Proceed to release CAISAR with release version $(TAG)? (y/n)?"
	@read yesno; test "$$yesno" = y
	@bash update_headers.sh
	@echo -n $(DESCRIPTION)
	@echo "Is the CHANGES.md correct for $(TAG) (y/n)?"
	@read yesno; test "$$yesno" = y
	dune-release tag $(TAG)
	dune-release distrib --skip-build --skip-lint -t $(TAG) -V $(TAG)
	curl --header "PRIVATE-TOKEN: $(GITLAB_TOKEN)" \
	--upload-file _build/$(PKG) \
	$(PACKAGE_URL)
	echo $(PACKAGE_URL) > _build/asset-$(TAG).url
	git push origin $(TAG)
	@bash upload_release.sh -g $(GITLAB_TOKEN) -t $(TAG) -d $(DESCRIPTION) -p $(PKG) -u $(PACKAGE_URL)
	dune-release opam pkg
	dune-release opam submit

.PHONY: release clean doc view-doc promote test docker uninstall install all
