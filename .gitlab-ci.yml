default:
  interruptible: true
  tags: [ nix-v2 ]

stages:
  - prepare
  - build
  - tests
  - lint
  - doc
  - make_public

env:
  stage: prepare
  script:
    - export VERSION=$(cat VERSION)
    - export MAJOR=$(echo $VERSION | cut -d'.' -f1)
    - export MINOR=$(echo $VERSION | cut -d'.' -f2)
    - echo "VERSION=$VERSION" >> build.env
    - echo "MAJOR=$MAJOR" >> build.env
    - echo "MINOR=$MINOR" >> build.env
    - echo "TAG=$MAJOR.$MINOR" >> build.env
    - cat build.env
  artifacts:
    reports:
      dotenv: build.env

build:
  stage: build
  script:
    - nix --extra-experimental-features "nix-command flakes" develop --command make

build_ocaml4:
  stage: build
  script:
    - nix --extra-experimental-features "nix-command flakes" build .#caisar414

build_ocaml5:
  stage: build
  script:
    - nix --extra-experimental-features "nix-command flakes" build .#caisar51

# Optional and manual full rebuild using a clean docker image
full_rebuild_and_test:
  stage: build
  image: ocaml/opam:ubuntu-22.04-ocaml-4.14
  cache:
    key: $CI_COMMIT_REF_SLUG
    paths:
      - _opam
  script:
    - if [ ! -d _opam ]; then echo "no local switch in the CI cache, we setup a new switch"; opam switch create --yes --no-install . ocaml-base-compiler.4.14.1; fi
    - opam switch 4.14
    - eval $(opam env --switch=4.14 --set-switch)
    - opam switch
    - sudo apt-get update
    - sudo apt-get install -y protobuf-compiler python3 python3-pip cmake texlive-full
    - opam repository add remote https://opam.ocaml.org
    - opam depext --yes ocplib-endian base fmt alt-ergo.2.4.0
    - opam install --jobs 2 . --deps-only --with-test --yes
    - python3 -m pip install onnx
    - make all-ci
    - make test-ci
  tags:
    - docker
  when: manual

tests:
  stage: tests
  script:
    - nix --extra-experimental-features "nix-command flakes" build
    - nix --extra-experimental-features "nix-command flakes" build -L .#checks.x86_64-linux.default

caisarpy-tests:
  stage: tests
  script:
    - NIXPKGS_ALLOW_UNFREE=1 nix --extra-experimental-features "nix-command flakes" build -L --impure .#checks.x86_64-linux.pythonBindings

lint:
  stage: lint
  script:
    - nix --extra-experimental-features "nix-command flakes" build -L .#checks.x86_64-linux.lint


## Manual generation of the documentation

documentation:
  stage: doc
  script:
    - nix --extra-experimental-features "nix-command flakes" build
  artifacts:
   paths:
     - result/share/doc/ocaml4.14.1-caisar-${VERSION}/html
     - result/share/doc/ocaml4.14.1-caisar-${VERSION}/latex
  when: manual

################################################################################
### PUBLIC

# make_public stage is used to push the current master branch of CAISAR to
# public repositories.

# make_public targets the public repository git.frama-c.com/pub/caisar.
# For that, it uses the 'caisar to caisar-public' deploy key of the public
# repository. The latter is the public part of the key, whereas the private one
# is stored in the environment variable $CAISAR_PUBLIC_SSH_PRIVATE_KEY of the
# private repository.

make_public:
  stage: make_public
  image: ocaml/opam@sha256:013a26ccbaa8344b63274e335e2492c432cec1c3526b9ba888ab151abb2b4c25
  script:
    - echo "$CAISAR_PUBLIC_SSH_PRIVATE_KEY" | base64 -d > ci/caisar-public/id_ed25519
    - chmod 400 ci/caisar-public/id_ed25519
    - GIT_SSH=$PWD/ci/caisar-public/ssh.sh git push --atomic git@git.frama-c.com:pub/caisar.git origin/master:refs/heads/master $TAG
  tags:
    - docker
  rules:
    - if: $CI_PIPELINE_SOURCE == "schedule"
