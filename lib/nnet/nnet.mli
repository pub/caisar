(**************************************************************************)
(*                                                                        *)
(*  This file is part of CAISAR.                                          *)
(*                                                                        *)
(*  Copyright (C) 2025                                                    *)
(*    CEA (Commissariat à l'énergie atomique et aux énergies              *)
(*         alternatives)                                                  *)
(*                                                                        *)
(*  You can redistribute it and/or modify it under the terms of the GNU   *)
(*  Lesser General Public License as published by the Free Software       *)
(*  Foundation, version 2.1.                                              *)
(*                                                                        *)
(*  It is distributed in the hope that it will be useful,                 *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of        *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          *)
(*  GNU Lesser General Public License for more details.                   *)
(*                                                                        *)
(*  See the GNU Lesser General Public License version 2.1                 *)
(*  for more details (enclosed in the file licenses/LGPLv2.1).            *)
(*                                                                        *)
(**************************************************************************)

(** Module to parse neural networks written in the NNet format (see the
    specification at https://github.com/sisl/NNet). *)

type t = private {
  n_layers : int;  (** Number of layers. *)
  n_inputs : int;  (** Number of inputs. *)
  n_outputs : int;  (** Number of outputs. *)
  max_layer_size : int;  (** Maximum layer size. *)
  layer_sizes : int list;  (** Size of each layer. *)
  min_input_values : float list option;  (** Minimum values of inputs. *)
  max_input_values : float list option;  (** Maximum values of inputs. *)
  mean_values : (float list * float) option;
    (** Mean values of inputs and one value for all outputs. *)
  range_values : (float list * float) option;
    (** Range values of inputs and one value for all outputs. *)
  weights_biases : float list list;
    (** All weights and biases of NNet model. *)
  nir : Nir.Ngraph.t;
}
(** NNet model metadata. *)

val parse : ?permissive:bool -> string -> (t, string) Result.t
(** Parse an NNet file.

    @param permissive
      [false] by default. When set, parsing does not fail on non available
      information, which are set to [None] instead. *)

val to_nir : t -> Nir.Ngraph.t
(** Convert a well-formed NNet into a Nir. *)
