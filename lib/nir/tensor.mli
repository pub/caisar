(**************************************************************************)
(*                                                                        *)
(*  This file is part of CAISAR.                                          *)
(*                                                                        *)
(*  Copyright (C) 2025                                                    *)
(*    CEA (Commissariat à l'énergie atomique et aux énergies              *)
(*         alternatives)                                                  *)
(*                                                                        *)
(*  You can redistribute it and/or modify it under the terms of the GNU   *)
(*  Lesser General Public License as published by the Free Software       *)
(*  Foundation, version 2.1.                                              *)
(*                                                                        *)
(*  It is distributed in the hope that it will be useful,                 *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of        *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          *)
(*  GNU Lesser General Public License for more details.                   *)
(*                                                                        *)
(*  See the GNU Lesser General Public License version 2.1                 *)
(*  for more details (enclosed in the file licenses/LGPLv2.1).            *)
(*                                                                        *)
(**************************************************************************)

(** {1 Immutable tensor module} *)

(** Tensors are multidimensional arrays used to represent numerical such as a
    neural network paramters.

    This library relies on Bigarray.Genarray to instanciante tensors. *)

(** [get t idx] returns the value in tensor [t] stored at coordinates [idx].
    Throw an error if the coordinate is invalid.*)

(** [set_idx t idx v] sets value [v] for tensor [t] at [idx]. Throw an error if
    the coordinate is invalid.*)

type ('a, 'b) t

val of_tensor : ('a, 'b, Bigarray.c_layout) Bigarray.Genarray.t -> ('a, 'b) t
val to_tensor : ('a, 'b) t -> ('a, 'b, Bigarray.c_layout) Bigarray.Genarray.t

val create_1_float : float -> (float, Bigarray.float64_elt) t
(** [create_1_float f] returns an unidimentional tensor with one floating point
    value [f]. *)

val create_1_int64 : int64 -> (int64, Bigarray.int64_elt) t
(** [create_1_int64 i] returns an unidimentional tensor with one int64 value
    [i]. *)

val create_const_float : Shape.t -> float -> (float, Bigarray.float64_elt) t
(** [create_const_float shape v] returns a tensor of shape [shape] where each
    value is initialized to [v]. *)

val shape : ('a, 'b) t -> Shape.t

val flatten : ('a, 'b) t -> 'a list
(** [flatten t] returns all values stored in [t] as a flat list. *)

val of_array1 :
  Shape.t -> ('a, 'b, Bigarray.c_layout) Bigarray.Array1.t -> ('a, 'b) t
(* [of_array1 sh a] takes an 1-dimensional array [a] and a shape [sh] and
   returns a tensor with shape [sh] and the corresponding value. *)

val reshape :
  Shape.t ->
  ('a, 'b, Bigarray.c_layout) Bigarray.Genarray.t ->
  ('a, 'b, Bigarray.c_layout) Bigarray.Genarray.t

val get : ('a, 'b) t -> int array -> 'a
(** [get t sh] returns the value stored at coordinates [sh] in [t].

    @raise Invalid_argument
      if [sh] does not exactly match the shape of [t], or if [sh] is
      out-of-bounds. *)
