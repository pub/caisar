(**************************************************************************)
(*                                                                        *)
(*  This file is part of CAISAR.                                          *)
(*                                                                        *)
(*  Copyright (C) 2025                                                    *)
(*    CEA (Commissariat à l'énergie atomique et aux énergies              *)
(*         alternatives)                                                  *)
(*                                                                        *)
(*  You can redistribute it and/or modify it under the terms of the GNU   *)
(*  Lesser General Public License as published by the Free Software       *)
(*  Foundation, version 2.1.                                              *)
(*                                                                        *)
(*  It is distributed in the hope that it will be useful,                 *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of        *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          *)
(*  GNU Lesser General Public License for more details.                   *)
(*                                                                        *)
(*  See the GNU Lesser General Public License version 2.1                 *)
(*  for more details (enclosed in the file licenses/LGPLv2.1).            *)
(*                                                                        *)
(**************************************************************************)

type t =
  | Float of (float, Bigarray.float64_elt) Tensor.t
  | Int64 of (int64, Bigarray.int64_elt) Tensor.t

val create_1_float : float -> t
val create_1_int64 : int64 -> t

val create_const_float : Shape.t -> float -> t
(** [create_const_float shape v] returns a tensor of shape [shape] where each
    value is initialized to [v]. *)

val of_float_array : ?shape:Shape.t -> float array -> t
(** [of_float_array a shape] returns a Tensor with data contained in [l] and
    shape [shape]. If no shape is given, the resulting tensor shape is
    1-dimensional, equals to the length of [l].*)

val of_int64_array : ?shape:Shape.t -> int64 array -> t
(** [of_int64_array a shape] returns a Tensor with data contained in [l] and
    shape [shape]. If no shape is given, the resulting tensor shape is
    1-dimensional, equals to the length of [l].*)

val shape : t -> Shape.t
