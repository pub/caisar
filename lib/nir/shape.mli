(**************************************************************************)
(*                                                                        *)
(*  This file is part of CAISAR.                                          *)
(*                                                                        *)
(*  Copyright (C) 2025                                                    *)
(*    CEA (Commissariat à l'énergie atomique et aux énergies              *)
(*         alternatives)                                                  *)
(*                                                                        *)
(*  You can redistribute it and/or modify it under the terms of the GNU   *)
(*  Lesser General Public License as published by the Free Software       *)
(*  Foundation, version 2.1.                                              *)
(*                                                                        *)
(*  It is distributed in the hope that it will be useful,                 *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of        *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          *)
(*  GNU Lesser General Public License for more details.                   *)
(*                                                                        *)
(*  See the GNU Lesser General Public License version 2.1                 *)
(*  for more details (enclosed in the file licenses/LGPLv2.1).            *)
(*                                                                        *)
(**************************************************************************)

type t [@@deriving show, ord, eq]

val to_array : t -> int array
val of_array : int array -> t
val to_list : t -> int list
val of_list : int list -> t
val rank : t -> int
val size : t -> int
val get : t -> int -> int
val set : t -> int -> int -> t
val row_major : t -> int array -> int
val unrow_major : t -> int -> int array
val to_array_unsafe : t -> int array

val remove_row : t -> int -> t
(** [remove_row \[|d0;...;dk|\] i] is the shape [|d0;...;d(i-1);d(i+1),...,dk|]. *)
