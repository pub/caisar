(**************************************************************************)
(*                                                                        *)
(*  This file is part of CAISAR.                                          *)
(*                                                                        *)
(*  Copyright (C) 2025                                                    *)
(*    CEA (Commissariat à l'énergie atomique et aux énergies              *)
(*         alternatives)                                                  *)
(*                                                                        *)
(*  You can redistribute it and/or modify it under the terms of the GNU   *)
(*  Lesser General Public License as published by the Free Software       *)
(*  Foundation, version 2.1.                                              *)
(*                                                                        *)
(*  It is distributed in the hope that it will be useful,                 *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of        *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          *)
(*  GNU Lesser General Public License for more details.                   *)
(*                                                                        *)
(*  See the GNU Lesser General Public License version 2.1                 *)
(*  for more details (enclosed in the file licenses/LGPLv2.1).            *)
(*                                                                        *)
(**************************************************************************)

(** {1 Neural Intermediate Representation (NIR)} *)

(** NIR is a graph describing a machine learning model control flow.

    A graph is described starting from its output node. *)

type t

val pp : t Fmt.t
val pp_debug : t Fmt.t

val create : Node.t -> t
(** Create a network from its output node.t *)

val output : t -> Node.t
(** Output node.t of the network *)

val nodes : t -> Node.t list
(** Output nodes of the network *)

val input_shape : t -> Shape.t
(** Input shape of the network *)

val succs : t -> Node.t -> Node.t list
(** successors of a node.t *)

val iter_vertex : (Node.t -> unit) -> t -> unit
val iter_succ : (Node.t -> unit) -> t -> Node.t -> unit
val grapheasy : t -> string

(** Respect some OcamlGraph signature *)
module GFloat : sig
  type nonrec t = t

  module V = Node

  module E : sig
    type t = V.t * V.t

    val src : t -> V.t
    val dst : t -> V.t
  end

  val iter_vertex : (V.t -> unit) -> t -> unit
  val iter_succ : (V.t -> unit) -> t -> V.t -> unit
  val iter_edges_e : (E.t -> unit) -> t -> unit
end

module Dot : sig
  val fprint_graph : Format.formatter -> GFloat.t -> unit
  val output_graph : out_channel -> GFloat.t -> unit
end
