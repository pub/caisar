(**************************************************************************)
(*                                                                        *)
(*  This file is part of CAISAR.                                          *)
(*                                                                        *)
(*  Copyright (C) 2025                                                    *)
(*    CEA (Commissariat à l'énergie atomique et aux énergies              *)
(*         alternatives)                                                  *)
(*                                                                        *)
(*  You can redistribute it and/or modify it under the terms of the GNU   *)
(*  Lesser General Public License as published by the Free Software       *)
(*  Foundation, version 2.1.                                              *)
(*                                                                        *)
(*  It is distributed in the hope that it will be useful,                 *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of        *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          *)
(*  GNU Lesser General Public License for more details.                   *)
(*                                                                        *)
(*  See the GNU Lesser General Public License version 2.1                 *)
(*  for more details (enclosed in the file licenses/LGPLv2.1).            *)
(*                                                                        *)
(**************************************************************************)

(** {1 Nodes descriptions} *)

(** A node is composed of

    - a unique [id] of type int
    - a node description of type [descr]

    [descr] describes several operations. When an operation shares the same name
    as an ONNX operation, it follows the standard defined in the ONNX IR v8 and
    ONNX Opset v13 standards, described here:
    https://onnx.ai/onnx/operators/index.html.

    Nodes only require their inputs: it is assumed that a node only returns one
    value. *)

open Base

type ty =
  | Float
  | Int64
[@@deriving show]

type descr =
  | Constant of { data : Gentensor.t }
      (** A constant tensor, used to store non-varying parameters during
          inference. *)
  | Add of {
      input1 : t;
      input2 : t;
    }
  | Sub of {
      input1 : t;
      input2 : t;
    }
  | Mul of {
      input1 : t;
      input2 : t;
    }
  | Div of {
      input1 : t;
      input2 : t;
    }
  | Matmul of {
      input1 : t;
      input2 : t;
    }
  | Gemm of {
      inputA : t;
      inputB : t;
      inputC : t option;
      alpha : float;
      beta : float;
      transA : int;
      transB : int;
    }
  | LogSoftmax
  | ReLu of { input : t }
  | Transpose of {
      input : t;
        (** Called "data" in ONNX documentation :
            https://onnx.ai/onnx/operators/onnx__Transpose.html .*)
      perm : int list; (* if perm = [] then perm = [rank-1; rank-2; ...; 1; 0] *)
    }
  | Squeeze of {
      data : t;
      axes : t option; (* Expects a int64 . *)
    }
  | MaxPool
  | Conv
  | Reshape of {
      input : t;
      shape : t; (* Expects a int64 *)
    }
  | Flatten of {
      input : t;
      axis : int;
    }
  | Identity of { input : t }
  | Input of { shape : Shape.t }
  | RW_Linearized_ReLu
  | Concat of {
      inputs : t list;
      axis : Base.int;
    }
  | Gather of {
      input : t;
      indices : t;
      axis : int;
    }
  | ReduceSum of {
      input : t;
      axes : t option;
      keepdims : int;
      noop_with_empty_axes : int;
    }
  | GatherND of {
      data : t;
      indices : t;
      batch_dims : int;
    }
  | RandomNormal of {
      dtype : int;
      mean : float;
      scale : float;
      seed : float;
      shape : int array;
    }
  | Abs of { input : t }
  | Log of { input : t }
  | Sign of { input : t }
  | ArgMax of {
      input : t;
      axis : int;
      keepdims : bool;
    }
  | Pow of {
      input1 : t;
      input2 : t;
    }
[@@deriving show]

and t = private {
  id : int;
  descr : descr;
  shape : Shape.t;
  ty : ty;  (** Describes the shape of the result of the node computation. *)
}

val equal : t -> t -> bool

include Base.Hashtbl.Key.S with type t := t
include Base.Comparator.S with type t := t

val create : descr -> t
(** [create descr] returns a value of type node with proper indexing and the
    shape according to the ONNX semantic. *)

val gather_int : ?encode:bool -> t -> int -> t
(** [gather_int n i] gathers the [i]th element of the node [n]. *)

val map : (t -> t) -> t -> t
(** [map f n] replace the direct inputs [i] of n by [f i] *)

val map_rec : (t -> t) -> t -> t
(** [map_rec f n] replace top-bottom the nodes [i] accessible from [n] by [f i] *)

val replace_input : (unit -> t) -> t -> t
(** [replace_input f n] replace the input in [n] by [f ()] *)

val preds : t -> t list
(** Direct predecessors of a t *)

val iter_rec : (t -> unit) -> t -> unit
(** Iterate on the predecessors of a t and itself. Repect topological order. *)

val compute_shape : t -> Shape.t
val ( + ) : t -> t -> t
val ( * ) : t -> t -> t
val ( - ) : t -> t -> t
val mul_float : t -> float -> t
val div_float : ?encode:bool -> t -> float -> t
val concat_0 : t list -> t
val reshape : Shape.t -> t -> t

val sum_list : ?shp:Shape.t -> t list -> t
(** [sum_list shp ns] is a node corresponding to the sum of the nodes in [ns].
    If [ns] is empty, this returns a tensor of shape [shp] filled with 0s. By
    default, [shp] is a single float. *)

val partial_dot_product : ?shp:Shape.t -> t array -> t array -> int -> int -> t
(** [partial_dot_product shp arr1 arr2 first last] where
    [arr1 = \[\|n11, n12, ..., n1k1\|\]] and
    [arr2 = \[\|n21, n22, ..., n2k2\|\]] is a node corresponding to
    [(n1first * n2first) + (n1first + 1 * n2first + 1) + ... + (n1last - 1 * n2last - 1)]
    if this exists. It is assumed that [arr1] and [arr2] contain tensors with
    same shape. Edge cases include:

    - if [last > length n1] or [last > length n2], then fails
    - if [last >= first], then returns a tensor where all values are initialized
      to 0. The shape of this tensor is determined using the following order:

    + if [length arr1 <> 0] then use the shape of [arr1.(0)]
    + if [length arr2 <> 0] then use the shape of [arr2.(0)]
    + if [shp <> None], then use [shp]
    + otherwise, fails *)

val transpose : int list -> int array -> int array
(** [transpose perm id] is the position of the component at position [id] when
    the permutation [perm] is applied as the result of a tensor transposition.
    For instance, if [id =
    \[\|10; 20; 30\|\]] and [\|perm = \[2;0;1\]\|],
    then [transpose perm id] will equal [\[\|30; 10; 20\|\]]. The empty
    permutation is represented by [\[\]] and is interpreted as
    [\[r-1; r-2; ...; 1; 0\]] (following
    <https://onnx.ai/onnx/operators/onnx__Transpose.html#transpose-23>);
    otherwise it is assumed that [perm] is a permutation of
    [\[0; 1; ...; r-1\]]. *)

val untranspose : int list -> int array -> int array
(** [untranspose] is the reverse of [transpose] so that
    [untranspose perm @@ transpose perm id] equals [id]. *)

val flatten : Shape.t -> int -> int array -> int array
(** [flatten shp axis id] computes the position of the component at position
    [id] when the flattening on [axis] is performed over shape [shp]. Following
    the definition of [Flatten]
    <https://onnx.ai/onnx/operators/onnx__Flatten.html#flatten-23>, [axis] can
    be negative in which case it computes from the end. For instance, if the
    shape is [10 * 10 * 10 * 10 * 10] (rank [5]), [axis = 3], and
    [id = \[\|1; 2; 3; 4; 5\|\]], then the result will be [\[\|123; 45\|\]]
    (computed as [\[\| 3 + (10 * ( 2 + 10 * 1)); 5 + 10 * 4) \|\]]). *)

val unflatten : Shape.t -> int -> int array -> int array
(** [unflatten] is the reverse of [flatten] so that
    [unflatten sh axis @@ flatten sh axis id] equals [id] (for correct inputs
    [id]). *)
