(**************************************************************************)
(*                                                                        *)
(*  This file is part of CAISAR.                                          *)
(*                                                                        *)
(*  Copyright (C) 2025                                                    *)
(*    CEA (Commissariat à l'énergie atomique et aux énergies              *)
(*         alternatives)                                                  *)
(*                                                                        *)
(*  You can redistribute it and/or modify it under the terms of the GNU   *)
(*  Lesser General Public License as published by the Free Software       *)
(*  Foundation, version 2.1.                                              *)
(*                                                                        *)
(*  It is distributed in the hope that it will be useful,                 *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of        *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          *)
(*  GNU Lesser General Public License for more details.                   *)
(*                                                                        *)
(*  See the GNU Lesser General Public License version 2.1                 *)
(*  for more details (enclosed in the file licenses/LGPLv2.1).            *)
(*                                                                        *)
(**************************************************************************)

let () = Printexc.record_backtrace true

let predict xg filename =
  let inputs = Caisar_xgboost.Input.of_filename xg filename in
  List.iter
    (fun features ->
      let sum1 = Caisar_xgboost.Predict.predict xg features in
      let sum2 =
        Caisar_xgboost.Tree.predict (Caisar_xgboost.Tree.convert xg) features
      in
      if sum1 = sum2
      then Format.printf "%0.2f\n" sum1
      else Format.printf "ERROR: %0.8f <> %0.8f\n" sum1 sum2)
    inputs

let () =
  let yojson = Yojson.Safe.from_file Sys.argv.(1) in
  match Caisar_xgboost.Parser.of_yojson yojson with
  | Error exn -> Format.eprintf "Error: %s" exn
  | Ok ok -> predict ok Sys.argv.(2)
