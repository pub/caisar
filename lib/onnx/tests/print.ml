(**************************************************************************)
(*                                                                        *)
(*  This file is part of CAISAR.                                          *)
(*                                                                        *)
(*  Copyright (C) 2025                                                    *)
(*    CEA (Commissariat à l'énergie atomique et aux énergies              *)
(*         alternatives)                                                  *)
(*                                                                        *)
(*  You can redistribute it and/or modify it under the terms of the GNU   *)
(*  Lesser General Public License as published by the Free Software       *)
(*  Foundation, version 2.1.                                              *)
(*                                                                        *)
(*  It is distributed in the hope that it will be useful,                 *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of        *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          *)
(*  GNU Lesser General Public License for more details.                   *)
(*                                                                        *)
(*  See the GNU Lesser General Public License version 2.1                 *)
(*  for more details (enclosed in the file licenses/LGPLv2.1).            *)
(*                                                                        *)
(**************************************************************************)

let file = "../../../examples/acasxu/nets/onnx/ACASXU_1_1.onnx"
let () = Format.printf "%b@." (Sys.file_exists file)

let () =
  try Unix.mkdir "out" 0o700 with Unix.Unix_error (Unix.EEXIST, _, _) -> ()

let temporary_file = "out/test.onnx"

let () =
  match Onnx.Reader.from_file file with
  | Error s -> print_endline s
  | Ok { nir = Error s; _ } -> print_endline s
  | Ok { nir = Ok g; _ } -> (
    print_endline "ok";
    Onnx.Writer.to_file g temporary_file;
    match Onnx.Reader.from_file temporary_file with
    | Error s -> print_endline s
    | Ok { nir = Error s; _ } -> print_endline s
    | Ok { nir = Ok _; _ } -> print_endline "ok")

let () =
  let pid =
    Unix.create_process "python3"
      [| "python3"; "../../../tests/bin/inspect_onnx.py" |]
      Unix.stdin Unix.stdout Unix.stdout
  in
  ignore (Unix.waitpid [] pid)
