(**************************************************************************)
(*                                                                        *)
(*  This file is part of CAISAR.                                          *)
(*                                                                        *)
(*  Copyright (C) 2023                                                    *)
(*    CEA (Commissariat à l'énergie atomique et aux énergies              *)
(*         alternatives)                                                  *)
(*                                                                        *)
(*  You can redistribute it and/or modify it under the terms of the GNU   *)
(*  Lesser General Public License as published by the Free Software       *)
(*  Foundation, version 2.1.                                              *)
(*                                                                        *)
(*  It is distributed in the hope that it will be useful,                 *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of        *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          *)
(*  GNU Lesser General Public License for more details.                   *)
(*                                                                        *)
(*  See the GNU Lesser General Public License version 2.1                 *)
(*  for more details (enclosed in the file licenses/LGPLv2.1).            *)
(*                                                                        *)
(**************************************************************************)

module IR = Nir

type sv = float array (* nb ins *)

type class_descriptor = {
  name : string;
  nb_svs : int;
}

type kernel_type =
  | Linear (* kernel function is [<x,x'>] *)
  | Poly of {
      gamma : float;
      degree : float;
      coef : float;
    }
(* kernel function is [(g * <x,x'> + r)^d] *)
(* | Rbf of float --- not implemented yet. *)

type t = {
  nb_ins : int;  (** Number of inputs *)
  nb_classes : int;  (** Number of classes *)
  name_and_nb_sv_of_class : class_descriptor array;
    (** [name_and_nb_sv_of_class.(cl)] is a description of class [cl]. *)
  dual_coefs : float array array;
    (** [dual_coefs.(cl).(i)] is the dual coef associated with the [i]th support
        vector ([cl] is between [0] and [nb_classes-2] and [i] is between [0]
        and [nb_svs-1]). *)
  support_vectors : sv array array;
    (** [support_vectors.(cl).(i)] is the [i]th support vector of [cl] ([i] is
        between [0] and [name_and_nb_sv_of_class.(cl).nb_svs - 1]. *)
  intercept : float array;
    (** [intercept.(i)] is the [i]th intercept. There is one intercept for each
        pair of classes (so there are [nb_classes * (nb_classes-1) / 2] of
        them). The positions [i] and [i'] of pairs [(j,l)] and [(j',l')] are
        such that [i < i'] iff [j < j'] or both [j = j'] and [l < l']. In other
        words, the positions for the following pairs are:

        - [(0,1)] -> [0]
        - [(0,2)] -> [1]
        - ...
        - [(0,nb_classes-1)] -> [nb_classes-2]
        - [(1,2)] -> [nb_classes-1]
        - [(1,3)] -> [nb_classes]
        - ...
        - [(nb_classes-2),(nb_classes-1)] ->
          [nb_classes * (nb_classes-1) / 2 - 1]. *)
  k : kernel_type;  (** [k] is the type of kernel of this SVM. *)
}
(** Each SVM record is defined by a number (>1) of inputs or "features" and a
    number of outputs or "classes". Each class has a name and a number (>=1) of
    Support Vectors (SV).

    Each support vector is defined as a vector of floats, one float for each
    input.

    Additional information in the SVM record includes:

    - the dual coefficients (a vector of floats of cardinality nb_SVs *
      (nb_classes - 1);

    - the intercept (a vector of floats of cardinality nb_classes *
      (nb_classes-1));

    - the kernel type, which is either Linear, RBF (with a gamma parameter), or
      Polynomial (with degree coefficients). *)

val parse : string -> (t, string) Result.t
(** Parses an OVO file according to the format implemented here:
    https://github.com/abstract-machine-learning/data-collection/blob/master/trainers/classifier_mapper.py#L21

    The input format is: ovo_file ::== header type_of_kernel classes_description
    dual_coef support_vector intercept header ::== 'ovo' nb_ins=INT
    nb_classes=INT type_of_kernel ::== 'linear' | 'rbf' gamma=FLOAT |
    ('polynomial'|'poly') ('gamma' gamma=FLOAT)? degree=FLOAT coef=FLOAT
    classes_description ::== | (classname1=STRING nb_sv_of_class1=INT
    classname0=STRING nb_sv_of_class0=INT) | (classname=STRING
    nb_sv_of_class=INT)^(nb_classes) dual_coef ::== (FLOAT)^(total_nb_sv *
    (nb_classes - 1)) support_vector ::== (FLOAT)^(nb_ins * nb_classes)
    intercept ::== (FLOAT)^(nb_classes * (nb_classes - 1) / 2)

    Notes:

    * The classes are described in increasing order (class 0, class 1, class 2,
    etc.) *except* if there are exactly two classes.

    * When the polynomial kernel 'gamma' parameter is not specified (as in the
    implementation mentionned above), it is assumed to be 'auto' as specified in
    the scikit-learn implementation, i.e., it is computed as [1.0 /. nb_ins].
    The default (preferred) method seems to now be 'scale' instead (since
    version 0.22 of scikit-learn, cf.
    https://github.com/scikit-learn/scikit-learn/issues/12741); however, 'scale'
    requires access to the training data which is not available here; in
    general, it's just better to save the 'gamma' value in the ovo file (still,
    keeping the default option for compatibility reasons). *)

val to_nn : t -> IR.Ngraph.t
