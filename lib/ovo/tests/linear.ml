(**************************************************************************)
(*                                                                        *)
(*  This file is part of CAISAR.                                          *)
(*                                                                        *)
(*  Copyright (C) 2025                                                    *)
(*    CEA (Commissariat à l'énergie atomique et aux énergies              *)
(*         alternatives)                                                  *)
(*                                                                        *)
(*  You can redistribute it and/or modify it under the terms of the GNU   *)
(*  Lesser General Public License as published by the Free Software       *)
(*  Foundation, version 2.1.                                              *)
(*                                                                        *)
(*  It is distributed in the hope that it will be useful,                 *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of        *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          *)
(*  GNU Lesser General Public License for more details.                   *)
(*                                                                        *)
(*  See the GNU Lesser General Public License version 2.1                 *)
(*  for more details (enclosed in the file licenses/LGPLv2.1).            *)
(*                                                                        *)
(**************************************************************************)

(* Testing the linear implementation of ovo.

   (1) Run python script `gen_linear_ovo.py` that creates a linear ovo, stores
   the ovo, generates a sequence of inputs, computes their class, and stores
   them.contents

   (2) Read the ovo file and translates it into an ONNX.

   (3) Run python script `check_onnx.py` that reads the onnx file, runs the onnx
   file on all inputs, checks that the class computed by the onnx matches that
   of the stored input. *)

let pid = Unix.getpid ()
let dir = "out_" ^ Int.to_string pid

let () =
  try Unix.mkdir dir 0o700 with Unix.Unix_error (Unix.EEXIST, _, _) -> ()

let ovo_filename = dir ^ "/file.ovo"
let onnx_filename = dir ^ "/file.onnx"
let test_filename = dir ^ "/tests"

(* 1 *)
let () =
  let pid =
    Unix.create_process "python3"
      [|
        "python3";
        "../../../tests/bin/gen_linear_ovo.py";
        ovo_filename;
        test_filename;
      |]
      Unix.stdin Unix.stdout Unix.stdout
  in
  ignore (Unix.waitpid [] pid)

(* 2 *)
let () =
  match Ovo.parse ovo_filename with
  | Ok ovo ->
    let ir = Ovo.to_nn ovo in
    Onnx.Writer.to_file ir onnx_filename
  | _ -> failwith "Error in writing OVO to ONNX"

(* 3 *)
let () =
  let pid =
    Unix.create_process "python3"
      [|
        "python3";
        "../../../tests/bin/check_onnx.py";
        onnx_filename;
        test_filename;
      |]
      Unix.stdin Unix.stdout Unix.stdout
  in
  ignore (Unix.waitpid [] pid)
