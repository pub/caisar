(**************************************************************************)
(*                                                                        *)
(*  This file is part of CAISAR.                                          *)
(*                                                                        *)
(*  Copyright (C) 2025                                                    *)
(*    CEA (Commissariat à l'énergie atomique et aux énergies              *)
(*         alternatives)                                                  *)
(*                                                                        *)
(*  You can redistribute it and/or modify it under the terms of the GNU   *)
(*  Lesser General Public License as published by the Free Software       *)
(*  Foundation, version 2.1.                                              *)
(*                                                                        *)
(*  It is distributed in the hope that it will be useful,                 *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of        *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          *)
(*  GNU Lesser General Public License for more details.                   *)
(*                                                                        *)
(*  See the GNU Lesser General Public License version 2.1                 *)
(*  for more details (enclosed in the file licenses/LGPLv2.1).            *)
(*                                                                        *)
(**************************************************************************)

open Base
open Result
module Format = Stdlib.Format
module Sys = Stdlib.Sys
module Filename = Stdlib.Filename
module Fun = Stdlib.Fun

(* Links: *
   https://datascience.stackexchange.com/questions/18374/predicting-probability-from-scikit-learn-svc-decision-function-with-decision-fun
   explains how the ovo (one-versus-one) procedure works *
   https://scikit-learn.org/stable/modules/svm.html#multi-class-classification a
   description of the SVMs. *
   https://github.com/abstract-machine-learning/saver#classifier-format. In a
   description of the input format (notice that there are broken links). *)

(* BASIC PARSING TOOLS *)
type parser = {
  input : Csv.in_channel;
  mutable tokens : string list;
}

let ovo_format_error s =
  Error (Format.sprintf "OVO format error: %s condition not satisfied." s)

let create_parser inc =
  { input = Csv.of_channel ~separator:' ' inc; tokens = [] }

let rec (peek_token : parser -> string) =
 fun p ->
  (* returns (without consuming) the top token *)
  match p.tokens with
  | "" :: tl ->
    (* can have empty strings if there are trailing spaces at the end of a
       line *)
    p.tokens <- tl;
    peek_token p
  | hd :: _ -> hd
  | [] ->
    let sp = Csv.next p.input in
    p.tokens <- sp;
    peek_token p

let read_token p =
  let _ = peek_token p in
  match p.tokens with
  | hd :: tl ->
    p.tokens <- tl;
    Ok hd
  | _ -> Error "EOF"

let read_int ?(msg = "") p =
  (* returns and consumes the top token as an int *)
  read_token p >>= fun str ->
  try Ok (Int.of_string str)
  with Failure _ ->
    ovo_format_error (Format.sprintf "(%s) not an int (%s)" str msg)

let read_float ?(msg = "") p =
  (* returns and consumes the top token as a float *)
  read_token p >>= fun str ->
  try Ok (Float.of_string str)
  with Failure _ ->
    ovo_format_error (Format.sprintf "(%s) not a float (%s)" str msg)

let read_keyword p k =
  (* returns and consumes the top token as the specified string *)
  read_token p >>= fun tok ->
  if String.equal tok k
  then Ok ()
  else ovo_format_error (Format.sprintf "expected keyword (%s) was (%s)" k tok)

let read_float_array parser msg size =
  (* returns and consumes an array of floats of specified size *)
  let rec fill_array nb arr =
    if nb = size
    then Ok arr
    else
      read_float ~msg parser >>= fun f ->
      arr.(nb) <- f;
      fill_array (nb + 1) arr
  in
  fill_array 0 (Array.create ~len:size 0.0)

let read_2_dim_float parser msg size1 param_size2 =
  (* returns and consumes a 2D array of floats of specified size param_size2
     indicates the size as a function of the index of [0;size1-1]. *)
  let rec fill_mat nb mat =
    if nb = size1
    then Ok mat
    else
      read_float_array parser msg (param_size2 size1) >>= fun line ->
      mat.(nb) <- line;
      fill_mat (nb + 1) mat
  in
  fill_mat 0 (Array.create ~len:size1 [||])

let (read_3_dim_float :
      parser ->
      string ->
      int ->
      (int -> int) ->
      (int -> int) ->
      (float array array array, string) Result.t) =
 (* returns and consumes a 3D array of floats of specified size param_size2 and
    param_size3 indicate the size as a function of the higher index. *)
 fun parser msg size1 param_size2 param_size3 ->
  let rec fill_3d nb tensor =
    if nb = size1
    then Ok tensor
    else
      read_2_dim_float parser msg (param_size2 nb) param_size3 >>= fun mat ->
      tensor.(nb) <- mat;
      fill_3d (nb + 1) tensor
  in
  fill_3d 0 (Array.create ~len:size1 [| [||] |])

let check_eof parser =
  (* verifies that the end of the channel has been reached. *)
  try
    let _ = peek_token parser in
    ovo_format_error "File not finished"
  with End_of_file -> Ok ()

(* OVO DATA STRUCTURE *)
type sv = float array

type class_descriptor = {
  name : string;
  nb_svs : int;
}

type kernel_type =
  | Linear
  | Poly of {
      gamma : float;
      degree : float;
      coef : float;
    }

type t = {
  nb_ins : int;
  nb_classes : int;
  name_and_nb_sv_of_class : class_descriptor array;
  dual_coefs : float array array;
  support_vectors : sv array array;
  intercept : float array;
  k : kernel_type;
}

(* PARSING METHODS PER SE *)

let parse_header parser =
  (* reads 'ovo' nb_ins nb_classes and returns Ok(nb_ins, nb_classes) *)
  let open Result in
  read_keyword parser "ovo" >>= fun _ ->
  read_int ~msg:"nb_ins" parser >>= fun nb_ins ->
  read_int ~msg:"nb_classes" parser >>= fun nb_classes -> Ok (nb_ins, nb_classes)

let parse_classes_description parser nb_classes =
  (* Reads "name_of_class number_of_sv" [nb_classes] times *)
  let rec fill_descriptions nb descriptions =
    (* fills the array of descriptions *)
    if nb = nb_classes
    then Ok descriptions
    else
      read_token parser >>= fun name ->
      read_int ~msg:"nb SVs of class" parser >>= fun nb_svs ->
      descriptions.(nb) <- { name; nb_svs };
      fill_descriptions (nb + 1) descriptions
  in
  let swap_first_two_descriptions descriptions =
    let desc0 = descriptions.(0) in
    let desc1 = descriptions.(1) in
    descriptions.(0) <- desc1;
    descriptions.(1) <- desc0
  in
  Array.init nb_classes ~f:(fun _ -> { name = ""; nb_svs = -1 })
  |> fill_descriptions 0
  >>= fun descriptions ->
  if nb_classes = 2 then swap_first_two_descriptions descriptions;
  Ok descriptions

let parse_support_vectors parser nb_ins name_and_nb_sv_of_class =
  (* Parses the SVs. This assumes that the SV are enumerated as follows:
     first_param_of_first_sv ... last_param_of_first_sv ...
     first_param_of_last_sv ... last_param_of_last_sv

     (no need to have end_of_line separators) where each param is a float. The
     SVs are assumed to be ordered, with the SVs associated with the first class
     first, then the SVs associated with the second class, etc. *)
  read_3_dim_float parser "SV parameters"
    (Array.length name_and_nb_sv_of_class)
    (fun c -> name_and_nb_sv_of_class.(c).nb_svs)
    (fun _ -> nb_ins)

let parse_kernel_type parser nb_ins =
  (* parses the description of the kernel function *)
  read_token parser >>= fun tok ->
  if String.equal tok "linear"
  then Ok Linear
  else if String.equal tok "poly" || String.equal tok "polynomial"
  then
    let first = peek_token parser in
    (* is there a neat way to factorise this if/then/else? *)
    if String.equal first "gamma"
    then
      read_token parser >>= fun _ ->
      read_float_array parser "poly kernel parameters" 3 >>= function
      | [| gamma; degree; coef |] -> Ok (Poly { gamma; degree; coef })
      | _ -> assert false (* Should have 3 parameters *)
    else
      read_float_array parser "poly kernel parameters" 2 >>= function
      | [| degree; coef |] ->
        Ok (Poly { gamma = 1.0 /. Float.of_int nb_ins; degree; coef })
      | _ -> assert false (* Should have 2 parameters *)
  else ovo_format_error "kernel"

let (nb_svs : class_descriptor array -> int) =
 fun name_and_nb_sv_of_class ->
  (* calculates the number of Support Vectors *)
  Array.fold_right name_and_nb_sv_of_class ~init:0 ~f:(fun cdesc sum ->
    sum + cdesc.nb_svs)

let parse_dual_coefs parser name_and_nb_sv_of_class =
  (* parses the dual coefficients *)
  let nb_classes = Array.length name_and_nb_sv_of_class in
  let nb_svs = nb_svs name_and_nb_sv_of_class in
  read_2_dim_float parser "dual coefficients" (nb_classes - 1) (fun _ -> nb_svs)

let parse_intercept parser nb_classes =
  (* parses the intercept *)
  read_float_array parser "dual coefficients" (nb_classes * (nb_classes - 1) / 2)

let parse parser =
  (* Parses the OVO. Look at the description of the input language in the mli
     file. *)
  let open Result in
  parse_header parser >>= fun (nb_ins, nb_classes) ->
  parse_kernel_type parser nb_ins >>= fun k ->
  parse_classes_description parser nb_classes >>= fun name_and_nb_sv_of_class ->
  parse_dual_coefs parser name_and_nb_sv_of_class >>= fun dual_coefs ->
  parse_support_vectors parser nb_ins name_and_nb_sv_of_class
  >>= fun support_vectors ->
  parse_intercept parser nb_classes >>= fun intercept ->
  check_eof parser >>= fun () ->
  Ok
    {
      nb_ins;
      nb_classes;
      name_and_nb_sv_of_class;
      dual_coefs;
      support_vectors;
      intercept;
      k;
    }

let parse inc =
  let parser = create_parser inc in
  match parse parser with Error e -> failwith e | x -> x

let parse filename =
  let in_channel = Stdlib.open_in filename in
  Fun.protect
    ~finally:(fun () -> Stdlib.close_in in_channel)
    (fun () -> parse in_channel)

(* ACCESSES *)
let svs ovo =
  (* [svs ovo] are the support vectors of [ovo]. *)
  let rec aux acc class_number =
    if class_number = ovo.nb_classes
    then acc
    else
      aux
        (List.concat
           [
             ovo.support_vectors.(class_number) |> Array.to_list |> List.rev;
             acc;
           ])
        (class_number + 1)
  in
  aux [] 0 |> List.rev

let compute_start_end ovo =
  (* returns two arrays [start_arr] and [end_arr] such that the SVs associated
     with class [c] are those between position [start_arr.(c)] (inclusive) and
     [end_arr.(c)] (exclusive). *)
  let start_arr = Array.create ~len:ovo.nb_classes 0 in
  let end_arr = Array.create ~len:ovo.nb_classes 0 in
  let rec initialise_tables c index =
    if c = ovo.nb_classes
    then ()
    else (
      start_arr.(c) <- index;
      let new_index = index + ovo.name_and_nb_sv_of_class.(c).nb_svs in
      end_arr.(c) <- new_index;
      initialise_tables (c + 1) new_index)
  in
  initialise_tables 0 0;
  (start_arr, end_arr)

(*
 * Help methods for IR.  Could be inserted in nier_simple.
 *)

module IR = Nir

let float_array_constant arr =
  Array.init (Array.length arr) ~f:(fun index ->
    IR.Node.create
    @@ IR.Node.Constant { data = IR.Gentensor.create_1_float arr.(index) })

let float_matrix_constant mat =
  Array.init (Array.length mat) ~f:(fun index ->
    float_array_constant @@ mat.(index))

let unfold_1d_tensor node =
  (* Takes a 1d node and returns an array that points to each value in the
     node *)
  let array_shape = IR.Shape.to_array @@ IR.Node.compute_shape node in
  if Array.length array_shape = 0
  then
    Caisar_logging.Logging.code_error ~src:Caisar_logging.Logging.src_ovo
      (fun m -> m "Calling [unfold_1d_tensor] with a 0d array")
  else
    let size = array_shape.(0) in
    Array.init size ~f:(fun index -> IR.Node.gather_int node index)

(* Transformation OVO -> Nier *)

(* exception Not_implemented_yet *)

let build_kernel ovo input_node =
  (* [build_kernel ovo input_node] returns an array whose ith element is the
     kernel value of [input_node] for the [i]th support vector of [ovo]. In
     other words, the [i]th element is the dot_product between the [i]th SV and
     [input_node]. *)
  let svs = Array.of_list (svs ovo) in
  let svs = float_matrix_constant svs in
  let copy_input = unfold_1d_tensor input_node in
  let product =
    Array.map svs ~f:(fun sv ->
      Nir.Node.partial_dot_product sv copy_input 0 (Array.length sv))
  in
  match ovo.k with
  | Linear ->
    (* for each sv [x], calculates [<x,x'>] where [x'] is the input vector *)
    product
  | Poly { gamma; degree; coef } ->
    (* for each sv [x], calculates [(g * <x,x'> + r)^d] where [x'] is the input
       vector *)
    (* TODO: create methods (array_of_tensor + constant)  *)
    (* TODO: create methods (array_of_tensor * constant)  *)
    (* TODO: create methods (array_of_tensor ^ constant)  *)
    let constant_node v =
      IR.Node.create
      @@ IR.Node.Constant { data = IR.Gentensor.create_1_float v }
    in
    let constant_gamma = constant_node gamma in
    let constant_degree = constant_node degree in
    let constant_coef = constant_node coef in
    product
    |> Array.map ~f:(fun v -> Nir.Node.(v * constant_gamma))
    |> Array.map ~f:(fun v -> Nir.Node.(v + constant_coef))
    |> Array.map ~f:(fun v ->
         Nir.Node.create @@ IR.Node.Pow { input1 = v; input2 = constant_degree })

let one_v_one_scores ovo kernel dual_coefs intercept =
  (* returns a list of triples ([c1],[c2],[score]) where [score] is the neural
     network node corresponding [c1] against [c2] (i.e., [c1] wins if
     eval([score])>0 and [c2] wins otherwise). *)
  let start_arr, end_arr = compute_start_end ovo in
  let specific_score c1 c2 intercept_index =
    let score1 =
      IR.Node.partial_dot_product dual_coefs.(c1) kernel start_arr.(c2)
        end_arr.(c2)
    in
    let score2 =
      IR.Node.partial_dot_product
        dual_coefs.(c2 - 1)
        kernel start_arr.(c1) end_arr.(c1)
    in
    let result = IR.Node.(score1 + score2 + intercept.(intercept_index)) in
    (c1, c2, result)
  in
  let rec aux c1 c2 intercept_index result =
    if c1 = ovo.nb_classes
    then result
    else if c2 = ovo.nb_classes
    then aux (c1 + 1) (c1 + 2) intercept_index result
    else
      let score = specific_score c1 c2 intercept_index in
      aux c1 (c2 + 1) (intercept_index + 1) (score :: result)
  in
  aux 0 1 0 []

let compute_points ovo scores =
  (* [compute_points ovo scores] creates the parts of the network that computes
     the points of each class for [ovo]. Input [scores] is a list of triple
     [(c1,c2,score)] where [score] indicates the result of comparing [c1] with
     [c2], i.e., [score > 0] iff [c1] wins. The outcome of this routine is an
     array [points] where [points.(cl)] represents the number of wins for class
     [cl]. *)
  let zero_node =
    IR.Node.create
    @@ IR.Node.Constant { data = IR.Gentensor.create_1_float 0.0 }
  in
  let scores_of_class cl =
    (* [scores_of_class cl] returns the list of "personal records" of [cl]
       represented as pairs [(is_first, score)] where [score] is a score
       involving class [cl] and [is_first] is true if [cl] is the first player
       in the score (i.e., [score] represents a win for [cl] iff [score > 0]).
       Scores that have been filtered out do not involve [cl]. *)
    scores
    |> List.filter_map ~f:(fun (i, j, score) ->
         if i = cl
         then Some (true, score)
         else if j = cl
         then Some (false, score)
         else None)
  in
  let turn_prec_into_points prec =
    (* [turn_prec_into_points prec] turns a list of personal records into a list
       of [0 / +1 / -1] points, *)
    prec
    |> List.map ~f:(fun (is_first, score) ->
         if is_first
         then IR.Node.create @@ IR.Node.Sign { input = score }
         else
           IR.Node.create
           @@ IR.Node.Sign { input = IR.Node.(zero_node - score) })
  in
  Array.init ovo.nb_classes ~f:(fun cl ->
    let pscores = scores_of_class cl in
    let points = turn_prec_into_points pscores in
    Nir.Node.sum_list points)

let to_nn ovo =
  let input_node =
    IR.Node.create
      (IR.Node.Input { shape = IR.Shape.of_array [| ovo.nb_ins |] })
  in
  let kernel = build_kernel ovo input_node in
  let dual_coefs = float_matrix_constant ovo.dual_coefs in
  let intercept = float_array_constant ovo.intercept in
  let scores = one_v_one_scores ovo kernel dual_coefs intercept in
  let points = compute_points ovo scores in
  let concatpoints =
    IR.Node.create @@ IR.Node.Concat { inputs = Array.to_list points; axis = 0 }
  in
  IR.Ngraph.create
    (IR.Node.create
    @@ IR.Node.ArgMax { input = concatpoints; axis = 0; keepdims = false })

(* eof *)
