"""Python file containing custom functions, that will be used by AIMOS.

This functions are relative to the MNIST dataset and models.
"""

import numpy as np


def load_mnist(path: str) -> tuple[list[np.ndarray], list[str]]:
    """Load the MNIST dataset.

    Args:
        path: Path to the dataset.

    Returns:
        Properly loaded inputs.
    """
    inp = np.genfromtxt(path, delimiter=",")
    inputs, inputs_name = [], []

    if inp.ndim == 1:
        inputs.append(inp[1:])
        inputs_name.append("0")
    else:
        for idx, row in enumerate(inp):
            inputs.append(row[1:])
            inputs_name.append(str(idx))

    return inputs, inputs_name
