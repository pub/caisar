"""Python file containing custom transformations, that will be used by AIMOS.

This transformation is relative to the ACAS Xu use case.
"""

import numpy as np
from aimos.core_transformations import identity


def rotation_input(inp):
    """Applies plane rotation to ACAS inputs.

    Args:
        inp: Input to rotate.

    Returns:
        A numpy array containing the rotated input.
    """

    def rotate_plane(output):
        output[1] = -output[1]
        output[2] = -output[2]
        return output

    return np.apply_along_axis(rotate_plane, -1, inp)


def rotation_output(inp):
    """Applies plane rotation to ACAS outputs.

    Args:
        inp: Output to rotate.

    Returns:
        A numpy array containing the rotated input.
    """

    def rotate_output(advisory):
        advisory[1], advisory[2] = advisory[2], advisory[1]
        advisory[3], advisory[4] = advisory[4], advisory[3]
        return advisory

    return np.apply_along_axis(rotate_output, -1, inp)


def reluplex_rotation():
    """Callable rotation for Reluplex use case.

    Returns:
        A tuple containing the rotation function on the inputs and the rotation function on the
        outputs.
    """

    return rotation_input, rotation_output
