(**************************************************************************)
(*                                                                        *)
(*  This file is part of CAISAR.                                          *)
(*                                                                        *)
(*  Copyright (C) 2025                                                    *)
(*    CEA (Commissariat à l'énergie atomique et aux énergies              *)
(*         alternatives)                                                  *)
(*                                                                        *)
(*  You can redistribute it and/or modify it under the terms of the GNU   *)
(*  Lesser General Public License as published by the Free Software       *)
(*  Foundation, version 2.1.                                              *)
(*                                                                        *)
(*  It is distributed in the hope that it will be useful,                 *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of        *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          *)
(*  GNU Lesser General Public License for more details.                   *)
(*                                                                        *)
(*  See the GNU Lesser General Public License version 2.1                 *)
(*  for more details (enclosed in the file licenses/LGPLv2.1).            *)
(*                                                                        *)
(**************************************************************************)

module C = Configurator.V1

module Version = struct
  type t = {
    major : string;
    minor : string;
    ext : string;
  }

  let get configurator =
    let out_VERSION =
      let out = C.Process.run configurator "cat" [ "VERSION" ] in
      if out.exit_code <> 0 then C.die "Cannot read VERSION.";
      out.stdout
    in
    let re_version = Str.regexp {|\([0-9]+\)\.\([0-9]\)\(.*\)|} in
    let major, minor, ext =
      if Str.string_match re_version out_VERSION 0
      then
        ( Str.matched_group 1 out_VERSION,
          Str.matched_group 2 out_VERSION,
          try Str.matched_group 3 out_VERSION with Not_found -> "" )
      else C.die "Cannot read VERSION."
    in
    { major; minor; ext }

  let pp_sed fmt version =
    Format.fprintf fmt "s|@VERSION@|%s.%s%s|g" version.major version.minor
      version.ext
end

let configure configurator =
  let version = Version.get configurator in
  let config_sed = open_out "config.sed" in
  let fmt = Format.formatter_of_out_channel config_sed in
  Format.fprintf fmt "@[%a@]@." Version.pp_sed version;
  close_out config_sed

let () = C.main ~name:"caisar_config" configure
