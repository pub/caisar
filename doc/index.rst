.. CAISAR documentation master file, created by
   sphinx-quickstart on Thu Jun 16 11:14:48 2022.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

.. _index:

The CAISAR Platform
===================

.. image:: _static/media/caisar_logo.png
   :scale: 50 %
   :alt: The CAISAR logo
   :align: center

:Authors:
  Michele Alberti,
  François Bobot,
  Julien Girard

:Version: |version|, January 2025
:Copyright: 2020--2024 CEA (Commissariat à l'énergie atomique et aux énergies alternatives)

.. _Confiance.ai: https://www.confiance.ai/
.. _DeepGreen: https://www.deepgreen.ai/

This work has been partly supported by the `Confiance.ai`_ program and the `DeepGreen`_ project.

.. toctree::
   :maxdepth: 2
   :numbered:

   foreword
   installation
   usage
   examples
   interpretation
   contributing
   provers
   genindex
   CAISAR website <http://www.caisar-platform.com>

.. * :ref:`genindex`
.. * :ref:`search`
.. * :ref:`modindex`
