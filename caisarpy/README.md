# CAISARpy

A Python API to launch CAISAR inside of python scripts.
The API is currently highly unstable and may change between versions. Feedbacks
welcome!

## Features

* automated installation of CAISAR
* launching CAISAR verification queries on predefined specifications (available under the `template`
  directory)

## Installation

### Locally

To install CAISAR python API, we recommend using the [poetry](https://python-poetry.org) build system.

At the root of caisarpy, `poetry install` to install the correct dependencies.

To check that the API works correctly, run the unit tests:

`poetry shell && python3 -m unittest tests.test_suite`

### Via pip

`caisarpy` will be released on `pip` in the near future.

## Usage

See the `tests/` folder for example use cases.
