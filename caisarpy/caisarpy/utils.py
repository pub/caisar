import os
import shlex
import subprocess
from io import TextIOWrapper

# Generic utilities

def run_command(config, args, cwd=None, timeout=None):
    prefix = config.caisar_install_dir.resolve()
    env = os.environ.copy()
    env["PATH"] = f"{prefix / 'bin'}:{env['PATH']}"
    env["OCAMLPATH"] = f"{prefix / 'lib'}"

    print(args)

    res = subprocess.run(
        args=args,
        cwd=cwd,
        stdout=subprocess.PIPE,
        stderr=subprocess.STDOUT,
        encoding="utf-8",
        env=env,
        timeout=timeout,
    )
    res.check_returncode()
    return res


def args_to_string(args: list[str]):
    return " ".join(map(lambda a: shlex.quote(str(a)), args))


def print_process_error(f: TextIOWrapper, e):
    if isinstance(e, subprocess.CalledProcessError):
        f.write(f"Process failed with code {e.returncode}\n")
    elif isinstance(e, subprocess.TimeoutExpired):
        f.write(f"Process timeout after {e.timeout} seconds\n")
    f.write(f"Command: {args_to_string(e.cmd)}\n")
    f.write(f"Output:\n{e.stdout}\n\n")
