from pathlib import Path
from argparse import Namespace
import shutil
import caisarpy.terminal as terminal
import caisarpy.utils as utils


# Default configuration

DEFAULT = Namespace(
    caisar=None,
    caisar_src_dir=Path("../../"),
    caisar_install_dir=Path("install"),
    templates_dir=Path("templates"),
)


class Configuration(Namespace):
    """
    The configuration of caisarpy, CAISAR's Python interface.
    Takes care of registering CAISAR source and installation directory,
    and to launch the installation if necessary.

    """

    def __init__(
        self,
        caisar=DEFAULT.caisar,
        caisar_src_dir=DEFAULT.caisar_src_dir,
        caisar_install_dir=DEFAULT.caisar_install_dir,
        templates_dir=DEFAULT.templates_dir,
    ):
        self.caisar = caisar
        self.caisar_src_dir = caisar_src_dir
        self.caisar_install_dir = caisar_install_dir
        self.templates_dir = templates_dir

    def is_caisar_installed(self):
        caisar = self.caisar_install_dir / "bin" / "caisar"
        return caisar.is_file()

    @staticmethod
    def of_args(args: Namespace):
        assert hasattr(args, "caisar")
        assert hasattr(args, "caisar_src_dir")
        assert hasattr(args, "caisar_install_dir")
        assert hasattr(args, "templates_dir")
        return Configuration(
            args.caisar,
            args.caisar_src_dir,
            args.caisar_install_dir,
            args.templates_dir,
        )

    def build_and_install_caisar(self):
        """CAISAR compilation and installation, including dependencies."""
        # TODO: expects the user to have opam, make and dune on their system.
        # Investigate on fetching a compiled version of CAISAR instead.

        terminal.print_title("Installing dependencies")

        caisar_opam_file = Path(self.caisar_src_dir / "caisar.opam")
        pkgs = [caisar_opam_file.resolve()]
        [
            utils.run_command(
                self, args=["opam", "install", "--deps-only", "--yes", pkg]
            )
            for pkg in pkgs
        ]
        terminal.print_title("Building CAISAR")
        utils.run_command(self, args=["make"], cwd=self.caisar_src_dir.resolve())

        terminal.print_title("Installing CAISAR")
        utils.run_command(
            self,
            args=[
                "dune",
                "install",
                f"--prefix={self.caisar_install_dir.resolve()}",
            ],
        )

    def caisar_binary(self) -> Path:
        """Return the path of CAISAR's binary, or install it if is not
        available."""

        caisar_alt = shutil.which("caisar")
        self.caisar = self.caisar_install_dir / "bin" / "caisar"

        if not self.is_caisar_installed() and (caisar_alt is None):
            self.build_and_install_caisar()
            return self.caisar
        else:
            if self.is_caisar_installed():
                print(f"CAISAR installation found at {self.caisar}.")
                pass
            elif caisar_alt is not None:
                print(f"Pre-existing installation found at {caisar_alt}.")
                self.caisar = Path(caisar_alt)
            return self.caisar
