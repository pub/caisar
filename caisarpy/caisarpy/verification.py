"""Verification queries handling.
"""
from collections import namedtuple
from pathlib import Path
import re
import subprocess
import sys
from typing import List, Optional

from caisarpy.config import Configuration
import caisarpy.utils as utils


class Template:
    """Interfacing with existing WhyML templates."""

    def __init__(self, config: Configuration):
        self.templates_paths = list(
            filter(lambda x: x.suffix == ".why", config.templates_dir.iterdir())
        )

    def list_templates(self):
        return self.templates_paths


class CsvRobustnessTemplate(Template):
    def __init__(
        self,
        config: Configuration,
        model: Path,
        csv: Path,
        eps: float,
        min_label: int,
        max_label: int,
    ):
        super().__init__(config)
        self.path = Path(config.templates_dir / "csv_robustness.why")
        self.defs = [
            f"model_filename:{str(model)}",
            f"csv_filename:{str(csv)}",
            f"eps:{format(eps, '.64f')}",
            f"min_label:{min_label}",
            f"max_label:{max_label}",
        ]


Prover = namedtuple("Prover", ["name", "version", "altern"])


class Provers:
    def __init__(self, config: Configuration):
        def _parse_autodetect_output(line):
            pattern = re.compile(
                r"(?P<name>[a-zA-Z-0-9_]+)\s*(?P<version>[a-zA-Z-_\d.+]+)?\s*(?:\((?P<altern>[^)]+)\))?$"
            )
            match = pattern.match(line)

            if match:
                return Prover(**(match.groupdict()))

            return None

        def _run_autodetect(config: Configuration):
            try:
                print(config)
                config.caisar_binary()
                print(config)
                res = utils.run_command(
                    config, args=[config.caisar, "config", "--detect"]
                )
                print(res.stdout)
                return res
            except subprocess.CalledProcessError as e:
                utils.print_process_error(sys.stdout, e)
                sys.exit(1)

        res = _run_autodetect(config)

        lines = res.stdout.split("\n")
        self.provers = [_parse_autodetect_output(line) for line in lines]

    def available_provers(self):
        return self.provers

    def select_prover(
        self,
        provers: List[Optional[Prover]],
        name: str,
        altern: Optional[str],
    ) -> Optional[Prover]:
        return next(
            (
                prover
                for prover in provers
                if prover and prover.name == name and prover.altern == altern
            ),
            None,
        )


class VerificationQuery:
    """
    In charge of building and launching a verification query to CAISAR.
    For now, only handles launching specific queries on existing patterns.
    """

    def __init__(self, config: Configuration):
        self.avail_templates = Template(config)

    def _run_verify(
        self,
        config: Configuration,
        prover: str,
        prover_altern: Optional[str],
        files: List[Path],
        defines=[],
        verbose=0,
    ):
        config.caisar_binary()

        args = [
            config.caisar,
            "verify",
            "--prover",
            prover,
        ]

        if prover_altern:
            args.extend(["--prover-altern", prover_altern])

        args.extend([f"--define={str(define)}" for define in defines])
        args.extend([str(file.resolve()) for file in files])

        if verbose:
            args.extend([f"-{'v' * verbose}"])

        res = utils.run_command(config, args=args)
        print(res.stdout)
        return res

    def verify_csv_robustness(
        self,
        config: Configuration,
        prover: Prover,
        model: Path,
        csv: Path,
        eps: float,
        min_label: int,
        max_label: int,
    ):
        csv_template = CsvRobustnessTemplate(
            config, model, csv, eps, min_label, max_label
        )

        self._run_verify(
            config=config,
            prover=prover.name,
            prover_altern=prover.altern,
            files=[csv_template.path],
            defines=csv_template.defs,
            verbose=2,
        )
