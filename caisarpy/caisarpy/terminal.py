from typing import Optional
from enum import Enum
import termcolor


class Spacing(Enum):
    both = 1
    pre = 2
    post = 3


def pretty_print(text: str, color, spacing: Optional[Spacing] = None):
    if spacing == Spacing.both:
        text = f"\n{text}\n"
    elif spacing == Spacing.pre:
        text = f"\n{text}"
    elif spacing == Spacing.post:
        text = f"{text}\n"

    termcolor.cprint(f"{text}", color)


# Title
def print_title(text: str):
    pretty_print(text, "green", spacing=Spacing.both)


# Info
def print_info(text: str, spacing: Spacing = None):
    pretty_print(text, "blue", spacing)


# Error
def print_error(text: str, spacing: Spacing = None):
    pretty_print(text, "red", spacing)


# Warning
def print_warning(text: str, spacing: Spacing = None):
    pretty_print(text, "yellow", spacing)


def print_bold(text: str):
    return termcolor.colored(text, attrs=["bold"])
