#!/usr/bin/env python3

import argparse
from pathlib import Path
import sys

from caisarpy.config import Configuration, DEFAULT
import caisarpy.terminal as terminal
import caisarpy.verification


if __name__ == "__main__":
    parent_parser = argparse.ArgumentParser(
        add_help=False, allow_abbrev=False, argument_default=argparse.SUPPRESS
    )

    parent_parser.add_argument(
        "--caisar", metavar="FILE", type=Path, help="CAISAR binary"
    )

    parent_parser.add_argument(
        "--caisar-src",
        metavar="DIR",
        type=Path,
        help="CAISAR source directory",
        default=DEFAULT.caisar_src_dir,
    )

    parent_parser.add_argument(
        "--caisar-install",
        metavar="DIR",
        type=Path,
        help="CAISAR install directory",
        default=DEFAULT.caisar_install_dir,
    )

    parent_parser.add_argument("-v", "--verbose", action="count", default=0)

    main_parser = argparse.ArgumentParser(
        parents=[parent_parser],
        formatter_class=argparse.ArgumentDefaultsHelpFormatter,
        description="Python interface for CAISAR",
    )

    subparsers = main_parser.add_subparsers(
        dest="command", help="CAISAR commands", required=True
    )

    subparser_config = subparsers.add_parser(
        parents=[parent_parser],
        name="provers",
        formatter_class=argparse.ArgumentDefaultsHelpFormatter,
        help="list of (autodetected) provers",
    )

    subparser_verify = subparsers.add_parser(
        parents=[parent_parser],
        name="verify",
        formatter_class=argparse.ArgumentDefaultsHelpFormatter,
        help="property verification",
    )

    subparser_verify.add_argument(
        "FILE",
        nargs="+",
        type=Path,
        help="file(s) WhyML to verify",
    )

    subparser_verify.add_argument(
        "-p",
        "--prover",
        dest="prover",
        metavar="PROVER",
        nargs=1,
        type=str,
        help="prover to use",
    )

    subparser_verify.add_argument(
        "--prover-altern",
        dest="prover_altern",
        metavar="CONFIG",
        nargs=1,
        type=str,
        help="alternative prover configuration",
    )

    args = main_parser.parse_args(namespace=DEFAULT)

    config = Configuration.of_args(args)
    if args.command == "provers":
        terminal.print_title("Autodetecting provers")
        caisarpy.verification._run_autodetect(config=config)
    elif args.command == "verify":
        terminal.print_title("Verifying WhyML file(s)")
        print(args)
        caisarpy.verification._run_verify(
            config=config,
            prover=args.prover[0],  # single prover
            prover_altern=args.prover_altern[0],  # single prover alternative
            files=args.FILE,
            verbose=args.verbose,
        )
    else:  # Should not happen as argparse should have checked arguments
        terminal.print_error(f"Unknown command: {args.command}")
        sys.exit(1)
