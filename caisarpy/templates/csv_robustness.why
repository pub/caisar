(** Classification Robustness of a Dataset in CSV format *)

theory CSV_ROBUSTNESS

  use int.Int
  use ieee_float.Float64
  use caisar.types.Float64WithBounds as Feature
  use caisar.types.IntWithBounds as Label
  use caisar.model.Model
  use caisar.dataset.CSV
  use caisar.robust.ClassRobustCSV

  constant model_filename: string
  constant csv_filename: string
  constant eps: t

  constant min_feature_value: Feature.t = 0.0
  constant max_feature_value: Feature.t = 1.0

  constant min_label : Label.t
  constant max_label : Label.t

  constant label_bounds: Label.bounds =
    Label.{ lower = min_label; upper = max_label }

  constant feature_bounds: Feature.bounds =
    Feature.{ lower = min_feature_value; upper = max_feature_value }

  goal robustness:
    let nn = read_model model_filename in
    let dataset = read_dataset csv_filename in
    robust feature_bounds label_bounds nn dataset eps

end
