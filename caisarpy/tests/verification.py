from pathlib import Path
import unittest

from caisarpy.verification import VerificationQuery
from caisarpy.verification import Provers
from caisarpy.verification import Prover


class TestVerification(unittest.TestCase):
    def setUp(self):
        from caisarpy.config import Configuration

        self.default_config = Configuration()
        self.default_config.caisar_src_dir = Path("../")

    def test_prover_selection(self):

        provers = Provers(self.default_config)
        alt_ergo = provers.select_prover(provers.available_provers(), "Alt-Ergo", None)
        self.assertEqual(alt_ergo, Prover("Alt-Ergo", None, None))

    def test_verify_csv(self):
        provers = Provers(self.default_config)
        marabou = provers.select_prover(
            provers.available_provers(), "Marabou", None
        )
        assert marabou is not None
        query = VerificationQuery(self.default_config)
        query.verify_csv_robustness(
            self.default_config,
            prover=marabou,
            model=Path("../../examples/mnist/nets/MNIST_256_2.onnx"),
            csv=Path("../../examples/mnist/csv/single_image.csv"),
            eps=0.5,
            min_label=0,
            max_label=9,
        )
