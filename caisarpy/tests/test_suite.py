import unittest
from tests.loading import TestImports
from tests.verification import TestVerification

if __name__ == "__main__":
    imports = unittest.defaultTestLoader.loadTestsFromTestCase(TestImports)
    verifs = unittest.defaultTestLoader.loadTestsFromTestCase(TestVerification)

    suite = unittest.TestSuite([imports, verifs])
    runner = unittest.TextTestRunner()
    result = runner.run(suite)
    if result:
        exit(0)
    else:
        exit(1)
