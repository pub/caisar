from pathlib import Path
import unittest


class TestImports(unittest.TestCase):
    def setUp(self):
        from caisarpy.config import Configuration

        self.default_config = Configuration()
        self.default_config.caisar_src_dir = Path("../")

    def test_utils_imports(self):
        import caisarpy.utils

    def test_verification_imports(self):
        from caisarpy.verification import Template
        from caisarpy.verification import Provers
        from caisarpy.verification import Prover

        templates = Template(self.default_config)
        self.assertEqual(
            templates.list_templates(), [Path("templates/csv_robustness.why")]
        )


class TestSolversDetected(unittest.TestCase):
    def setUp(self):
        from caisarpy.config import Configuration
        from caisarpy.verification import Template
        from caisarpy.verification import Provers

        self.default_config = Configuration()
        self.default_config.caisar_src_dir = Path("../")
        self.provers = Provers(self.default_config)

    def test_altergo_imports(self):
        from caisarpy.verification import Prover
        self.assertTrue(
            list.__contains__(
                self.provers.available_provers(),
                Prover(name="Alt-Ergo", version=None, altern=None),
            )
        )
    def test_cvc5_imports(self):
        from caisarpy.verification import Prover
        self.assertTrue(
            list.__contains__(
                self.provers.available_provers(),
                Prover(name="CVC5", version="1.2.0", altern=None),
            )
        )
