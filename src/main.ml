(**************************************************************************)
(*                                                                        *)
(*  This file is part of CAISAR.                                          *)
(*                                                                        *)
(*  Copyright (C) 2025                                                    *)
(*    CEA (Commissariat à l'énergie atomique et aux énergies              *)
(*         alternatives)                                                  *)
(*                                                                        *)
(*  You can redistribute it and/or modify it under the terms of the GNU   *)
(*  Lesser General Public License as published by the Free Software       *)
(*  Foundation, version 2.1.                                              *)
(*                                                                        *)
(*  It is distributed in the hope that it will be useful,                 *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of        *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          *)
(*  GNU Lesser General Public License for more details.                   *)
(*                                                                        *)
(*  See the GNU Lesser General Public License version 2.1                 *)
(*  for more details (enclosed in the file licenses/LGPLv2.1).            *)
(*                                                                        *)
(**************************************************************************)

open Base
open Cmdliner

let caisar = "caisar"

let () =
  Relop.register_simplify_relop ();
  Relop.register_vars_on_lhs_of_relop ()

let () =
  Pyrat.init ();
  Marabou.init ();
  Vnnlib.init ()

(* -------------------------------------------------------------------------- *)
(* --- Logs                                                                   *)
(* -------------------------------------------------------------------------- *)

let log_tags =
  let all_tags = Logging.all_srcs () in
  let doc =
    Fmt.str "Logging tags. Available $(docv)s: %s."
      (Arg.doc_alts
         (List.map
            ~f:(fun src ->
              Fmt.str "%s (%s)" (Logs.Src.name src) (Logs.Src.doc src))
            all_tags))
  in
  let enum_log_tags =
    Arg.enum (List.map ~f:(fun c -> (Logs.Src.name c, c)) all_tags)
  in
  Arg.(
    value & opt_all enum_log_tags []
    & info [ "log-tag"; "ltag" ] ~doc ~docv:"TAG")

let setup_logs =
  Term.(
    const Logging.setup $ Fmt_cli.style_renderer () $ Logs_cli.level ()
    $ log_tags)

(* -------------------------------------------------------------------------- *)
(* --- Commands                                                               *)
(* -------------------------------------------------------------------------- *)

let config detect () =
  if detect
  then (
    Logs.debug (fun m -> m "Automatic detection");
    let config = Autodetect.autodetection () in
    let provers = Why3.Whyconf.get_provers config in
    if not (Why3.Whyconf.Mprover.is_empty provers)
    then
      Logs.app (fun m ->
        m "@[<v>%a@]"
          Why3.(
            Pp.print_iter2 Whyconf.Mprover.iter Pp.newline Pp.nothing
              Whyconf.print_prover Pp.nothing)
          provers))

let log_theory_answer =
  Why3.Wstdlib.Mstr.iter (fun theory_name task_answers ->
    Logs.info (fun m ->
      m "@[Verification results for theory '%s'@]" theory_name);
    List.iter task_answers ~f:(fun answer ->
      let id, prover_answer, additional_info =
        let open Verification_types.Answer in
        match answer with
        | {
         problem_answer =
           GenericProblemAnswer { id; prover_answer; additional_info; _ };
         _;
        } ->
          (id, prover_answer, additional_info)
        | {
         problem_answer =
           LegacyDatasetAnswer { id; prover_answer; additional_info; _ };
         _;
        } ->
          (id, prover_answer, additional_info)
      in
      Logs.app (fun m ->
        m "@[Goal %a:@ %a%a@]" Why3.Pretty.print_pr id
          Why3.Call_provers.print_prover_answer prover_answer
          Fmt.(option ~none:nop (any " " ++ string))
          additional_info)))

let verify ?format ~loadpath ?memlimit ?timelimit ?dataset prover ?prover_altern
  ?definitions ?theories ?goals ?onnx_out_dir files =
  let open Verification_types in
  let problems =
    List.map files ~f:(Problem.of_file ?definitions ?goals ?theories)
  in
  let queries =
    List.map problems ~f:(fun problem ->
      Query.make ?prover_altern ~loadpath ?memlimit ?timelimit ?onnx_out_dir
        ?dataset prover problem)
  in
  let theory_answers = List.map queries ~f:(Verification.verify ?format) in
  List.iter theory_answers ~f:log_theory_answer;
  theory_answers

let record_json_output answer file =
  let out_channel = Stdlib.open_out file in
  Yojson.Safe.to_channel out_channel
    (Verification_types.Answer.to_yojson answer);
  Logs.info (fun m -> m "@[Results recorded in file '%s'@]" file);
  Stdlib.close_out out_channel

let record_verification_result verification_result file =
  Why3.Wstdlib.Mstr.iter
    (fun _th_name (answers : Verification.verification_result list) ->
      let rec aux answers file =
        match answers with
        | answer :: tl ->
          record_json_output answer file;
          aux tl file
        | [] -> ()
      in
      aux answers file)
    verification_result

let verify_json ?memlimit ?timelimit ?outfile json =
  let query =
    Verification_types.Query.of_json ?memlimit ?timelimit ?outfile json
  in
  let verification_results = Verification.verify query in
  let outfile =
    match query.output_file with
    | None ->
      Logging.code_error ~src:Logs.default (fun m ->
        m "No output file specified to record verification results")
    | Some outfile -> outfile
  in
  record_verification_result verification_results outfile

let verify_xgboost ?memlimit ?timelimit xgboost dataset prover =
  let open Verification_types.Query in
  let memlimit = Option.map memlimit ~f:memlimit_of_string in
  let timelimit = Option.map timelimit ~f:timelimit_of_string in
  Convert_xgboost.verify ?memlimit ?timelimit ~xgboost ~dataset ~prover ()

let exec_cmd cmdname cmd =
  Logs.debug (fun m -> m "Execution of command '%s'" cmdname);
  cmd ()

(* -------------------------------------------------------------------------- *)
(* --- Command line                                                           *)
(* -------------------------------------------------------------------------- *)

let memlimit =
  let doc =
    "Memory limit. $(docv) is intended in megabytes (MB) by default, but \
     gigabytes (GB) and terabytes (TB) may also be specified instead."
  in
  Arg.(
    value & opt (some string) None & info [ "m"; "memlimit" ] ~doc ~docv:"MEM")

let timelimit =
  let doc =
    "Time limit. $(docv) is intended in seconds (s) by default, but minutes \
     (m) and hours (h) may also be specified instead."
  in
  Arg.(
    value & opt (some string) None & info [ "t"; "timelimit" ] ~doc ~docv:"TIME")

let prover =
  let all_provers = Prover.list_available () in
  let doc =
    Fmt.str "Prover to use. Support is provided for the following $(docv)s: %a."
      (Fmt.list ~sep:Fmt.comma Fmt.string)
      (List.map ~f:Prover.to_string all_provers)
  in
  let provers =
    Arg.enum (List.map ~f:(fun p -> (Prover.to_string p, p)) all_provers)
  in
  Arg.(
    required
    & opt (some provers) None
    & info [ "p"; "prover" ] ~doc ~docv:"PROVER")

let verify_cmd =
  let cmdname = "verify" in
  let info =
    let doc = "Property verification using external provers." in
    Cmd.info cmdname ~sdocs:Manpage.s_common_options ~exits:Cmd.Exit.defaults
      ~doc
      ~man:[ `S Manpage.s_description; `P doc ]
  in
  let loadpath =
    let doc = "Additional loadpath." in
    Arg.(value & opt_all string [ "." ] & info [ "L"; "loadpath" ] ~doc)
  in
  let format =
    let doc = "File format. Deprecated, will be removed in version 4.0." in
    Arg.(value & opt (some string) None & info [ "format" ] ~doc)
  in
  let prover_altern =
    let doc = "Alternative prover configuration." in
    Arg.(value & opt (some string) None & info [ "prover-altern" ] ~doc)
  in
  let dataset =
    let doc =
      "Dataset $(docv) (CSV format only). Deprecated, directly specify the \
       dataset in the file or with the --define option. Will be removed in \
       version 4.0."
    in
    Arg.(value & opt (some file) None & info [ "dataset" ] ~doc ~docv:"FILE")
  in
  let onnx_out_dir =
    let doc =
      "Write all NIR from current verification query as ONNX file in $(docv)."
    in
    Arg.(
      value
      & opt (some string) None
      & info [ "onnx-out-dir" ] ~doc ~docv:"DIRECTORY")
  in
  let definitions =
    let doc = "Define a toplevel constant declaration with the given value." in
    Arg.(
      value
      & opt_all (pair ~sep:':' string string) []
      & info [ "D"; "define" ] ~doc ~docv:"constant:value")
  in
  let theories =
    let doc = "Verify all goals in $(docv)." in
    let docv = "theory" in
    Arg.(value & opt_all string [] & info [ "T"; "theory" ] ~doc ~docv)
  in
  let goals =
    let doc =
      "Verify only $(docv). When no theory is provided, verify goal,...,goal \
       from all theories."
    in
    let docv = "[theory]:goal,...,goal" in
    let conv_goals =
      let non_empty_list str =
        match String.split str ~on:',' with
        | [ "" ] -> Error "empty list of goals"
        | goals -> Ok goals
      in
      let printer = Fmt.list ~sep:Fmt.comma Fmt.string in
      (non_empty_list, printer)
    in
    Arg.(
      value
      & opt_all (pair ~sep:':' string (conv' conv_goals)) []
      & info [ "g"; "goal" ] ~doc ~docv)
  in
  let files =
    let doc = "File(s) to verify." in
    let file_or_stdin = Arg.conv' Verification_types.File.(of_string, pretty) in
    Arg.(non_empty & pos_all file_or_stdin [] & info [] ~doc ~docv:"FILE")
  in
  let verify_term =
    let verify format loadpath memlimit timelimit prover prover_altern dataset
      definitions theories goals onnx_out_dir files () =
      ignore
        (verify ?format ~loadpath ?memlimit ?timelimit ?dataset prover
           ?prover_altern ~definitions ~theories ~goals ?onnx_out_dir files)
    in
    Term.(
      const (fun _ -> exec_cmd cmdname)
      $ setup_logs
      $ (const verify $ format $ loadpath $ memlimit $ timelimit $ prover
       $ prover_altern $ dataset $ definitions $ theories $ goals $ onnx_out_dir
       $ files))
  in
  Cmd.v info verify_term

let verify_json_cmd =
  let cmdname = "verify-json" in
  let info =
    let doc =
      "Property verification using external provers via a JSON configuration \
       file."
    in
    Cmd.info cmdname ~sdocs:Manpage.s_common_options ~exits:Cmd.Exit.defaults
      ~doc
      ~man:[ `S Manpage.s_description; `P doc ]
  in
  let outfile =
    let doc = "JSON file containing the results of the verification." in
    Arg.(
      value & opt (some string) None & info [ "o"; "output" ] ~doc ~docv:"FILE")
  in
  let json =
    let doc = "JSON file." in
    Arg.(
      required & pos ~rev:true 0 (some file) None & info [] ~doc ~docv:"FILE")
  in
  let verify_json_term =
    let verify_json memlimit timelimit outfile json () =
      verify_json ?memlimit ?timelimit ?outfile json
    in
    Term.(
      const (fun _ -> exec_cmd cmdname)
      $ setup_logs
      $ (const verify_json $ memlimit $ timelimit $ outfile $ json))
  in
  Cmd.v info verify_json_term

let verify_xgboost_cmd =
  let cmdname = "verify-xgboost" in
  let info =
    let doc = "EXPERIMENTAL: Property verification of XGBoost file." in
    Cmd.info cmdname ~sdocs:Manpage.s_common_options ~exits:Cmd.Exit.defaults
      ~doc
      ~man:[ `S Manpage.s_description; `P doc ]
  in
  let xgboost =
    let doc = "XGBoost model file in JSON format." in
    Arg.(required & pos 0 (some file) None & info [] ~doc ~docv:"FILE")
  in
  let dataset =
    let doc = "Dataset file (CSV, or SVM)." in
    Arg.(required & pos 1 (some file) None & info [] ~doc ~docv:"FILE")
  in
  let verify_xgboost_term =
    let verify_xgboost memlimit timelimit xgboost dataset prover () =
      verify_xgboost ?memlimit ?timelimit xgboost dataset prover
    in
    Term.(
      const (fun _ -> exec_cmd cmdname)
      $ setup_logs
      $ (const verify_xgboost $ memlimit $ timelimit $ xgboost $ dataset
       $ prover))
  in
  Cmd.v info verify_xgboost_term

let config_cmd =
  let cmdname = "config" in
  let info =
    let doc = Fmt.str "%s configuration." caisar in
    let exits = Cmd.Exit.defaults in
    let man =
      [
        `S Manpage.s_description;
        `P (Fmt.str "Handle the configuration of %s." caisar);
      ]
    in
    Cmd.info cmdname ~sdocs:Manpage.s_common_options ~exits ~doc ~man
  in
  let config_term =
    let detect =
      let doc = "Detect solvers in \\$PATH." in
      Arg.(value & flag & info [ "d"; "detect" ] ~doc)
    in
    Term.(
      ret
        (const (fun detect _ ->
           if not detect
           then `Help (`Pager, Some cmdname)
           else
             (* TODO: Do not only check for [detect], and enable it by default,
                as soon as other options are available. *)
             `Ok (exec_cmd cmdname (fun () -> config true ())))
        $ detect $ setup_logs))
  in
  Cmd.v info config_term

let default = Term.(ret (const (fun _ -> `Help (`Pager, None)) $ const ()))

let default_info =
  let doc =
    "A platform for characterizing the safety and robustness of artificial \
     intelligence based software."
  in
  let sdocs = Manpage.s_common_options in
  let man =
    [
      `S Manpage.s_common_options;
      `P "These options are common to all commands.";
      `S "MORE HELP";
      `P "Use `$(mname) $(i,COMMAND) --help' for help on a single command.";
      `S Manpage.s_bugs;
      `P "Submit bug reports to https://git.frama-c.com/pub/caisar/issues";
    ]
  in
  let version = Config.version in
  let exits = Cmd.Exit.defaults in
  Cmd.info caisar ~version ~doc ~sdocs ~exits ~man

let () =
  Logging.protect_main (fun () ->
    Cmd.group ~default default_info
      [ config_cmd; verify_cmd; verify_json_cmd; verify_xgboost_cmd ]
    |> Cmd.eval ~catch:false |> Stdlib.exit)
