(**************************************************************************)
(*                                                                        *)
(*  This file is part of CAISAR.                                          *)
(*                                                                        *)
(*  Copyright (C) 2025                                                    *)
(*    CEA (Commissariat à l'énergie atomique et aux énergies              *)
(*         alternatives)                                                  *)
(*                                                                        *)
(*  You can redistribute it and/or modify it under the terms of the GNU   *)
(*  Lesser General Public License as published by the Free Software       *)
(*  Foundation, version 2.1.                                              *)
(*                                                                        *)
(*  It is distributed in the hope that it will be useful,                 *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of        *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          *)
(*  GNU Lesser General Public License for more details.                   *)
(*                                                                        *)
(*  See the GNU Lesser General Public License version 2.1                 *)
(*  for more details (enclosed in the file licenses/LGPLv2.1).            *)
(*                                                                        *)
(**************************************************************************)

open Why3
open Base

let src = Logs.Src.create "SAVer" ~doc:"SAVer prover"

(* SAVer command line parameters. *)
let re_svm = Re__.Core.(compile (str "%{svm}"))
let re_dataset = Re__.Core.(compile (str "%{dataset}"))
let re_abstraction = Re__.Core.(compile (str "%{abstraction}"))
let re_perturbation = Re__.Core.(compile (str "%{perturbation}"))
let re_perturbation_param = Re__.Core.(compile (str "%{perturbation_param}"))

type perturbation = From_file of string

let string_of_perturbation = function From_file _ -> "from_file"

let string_of_perturbation_param = function
  | From_file filename -> Unix.realpath filename

let build_command config_prover ?(abstraction = "hybrid") ~svm ~dataset
  perturbation =
  let command = Whyconf.get_complete_command ~with_steps:false config_prover in
  let params =
    [
      (re_svm, Unix.realpath svm);
      (re_dataset, Unix.realpath dataset);
      (re_perturbation, string_of_perturbation perturbation);
      (re_perturbation_param, string_of_perturbation_param perturbation);
      (re_abstraction, abstraction);
    ]
  in
  List.fold params ~init:command ~f:(fun cmd (param, by) ->
    Re__.Replace.replace_string param ~by cmd)

type answer = {
  prover_answer : Call_provers.prover_answer;
  nb_total : int;
  nb_proved : int;
}

(* SAVer's output is made of 7 columns separated by (multiple) space(s) of the
   following form:

   [SUMMARY] integer float float integer integer integer

   We are interested in recovering the integers. The following regexp matches
   groups of one or more digits by means of '(\\d+)'. The latter is used 4 times
   for the 4 columns of integers we are interested in. The 1st group reports the
   number of total data points in the dataset, 2nd group reports the number of
   correct data points, 3rd group reports the number of robust data points, 4th
   group reports the number of conditionally robust (ie, correct and robust)
   data points. *)
let re_saver_output =
  Re__.Pcre.regexp
    "\\[SUMMARY\\]\\s*(\\d+)\\s*[0-9.]+\\s*[0-9.]+\\s*(\\d)+\\s*(\\d+)\\s*(\\d+)"

(* The dataset size is the first group of digits matched by
   [re_saver_output]. *)
let re_group_number_dataset_size = 1

let build_answer prover_result =
  match prover_result.Call_provers.pr_answer with
  | Call_provers.Unknown _ -> (
    let pr_output = prover_result.pr_output in
    match Re__.Core.exec_opt re_saver_output pr_output with
    | Some re_group ->
      let nb_total =
        Int.of_string
          (Re__.Core.Group.get re_group re_group_number_dataset_size)
      in
      let nb_proved =
        let re_group_number = 4 (* cond. robust: correct and robust *) in
        Int.of_string (Re__.Core.Group.get re_group re_group_number)
      in
      let prover_answer =
        if nb_total = nb_proved
        then Call_provers.Valid
        else Call_provers.Unknown ""
      in
      { prover_answer; nb_total; nb_proved }
    | None ->
      Logging.code_error ~src (fun m ->
        m "Cannot interpret the output provided by SAVer"))
  | unexpected_pr_answer ->
    (* Any other answer than HighFailure should never happen as we do not define
       anything in SAVer's driver. *)
    Logging.code_error ~src (fun m ->
      m "Unexpected SAVer prover answer '%a'"
        Why3.Call_provers.print_prover_answer unexpected_pr_answer)

let call_prover limit config config_prover ?(abstraction = Language.Hybrid) ~svm
  ~dataset ~perturbations () =
  let command =
    let abstraction = Language.string_of_svm_abstraction abstraction in
    let dataset =
      let filename = Stdlib.Filename.temp_file "dataset" "saver" in
      Csv.save filename dataset;
      filename
    in
    let perturbation =
      let filename = Stdlib.Filename.temp_file "perturbations" "saver" in
      let perturbations =
        List.concat_map perturbations ~f:(fun (lower, upper) ->
          [ Float.to_string lower; Float.to_string upper ])
        |> List.return
      in
      Csv.save ~separator:' ' filename perturbations;
      filename
    in
    build_command config_prover ~abstraction ~svm ~dataset
      (From_file perturbation)
  in
  let prover_call =
    let res_parser =
      {
        Call_provers.prp_regexps =
          [ ("NeverMatch", Call_provers.Failure "Should not happen in CAISAR") ];
        prp_timeregexps = [];
        prp_stepregexps = [];
        prp_exitcodes = [];
        prp_model_parser = Model_parser.lookup_model_parser "no_model";
      }
    in
    Call_provers.call_on_buffer ~command ~config ~limit ~res_parser
      ~filename:" " ~get_model:None ~gen_new_file:false (Buffer.create 10)
  in
  let prover_result = Call_provers.wait_on_call prover_call in
  build_answer prover_result
