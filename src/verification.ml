(**************************************************************************)
(*                                                                        *)
(*  This file is part of CAISAR.                                          *)
(*                                                                        *)
(*  Copyright (C) 2025                                                    *)
(*    CEA (Commissariat à l'énergie atomique et aux énergies              *)
(*         alternatives)                                                  *)
(*                                                                        *)
(*  You can redistribute it and/or modify it under the terms of the GNU   *)
(*  Lesser General Public License as published by the Free Software       *)
(*  Foundation, version 2.1.                                              *)
(*                                                                        *)
(*  It is distributed in the hope that it will be useful,                 *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of        *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          *)
(*  GNU Lesser General Public License for more details.                   *)
(*                                                                        *)
(*  See the GNU Lesser General Public License version 2.1                 *)
(*  for more details (enclosed in the file licenses/LGPLv2.1).            *)
(*                                                                        *)
(**************************************************************************)

open Why3
open Base

type verification_query = Verification_types.Query.t
type verification_result = Verification_types.Answer.t

let () =
  Language.register_nnet_support ();
  Language.register_onnx_support ();
  Language.register_ovo_support ()

let create_env loadpath =
  let config = Autodetect.autodetection () in
  let config =
    let main = Whyconf.get_main config in
    let dft_timelimit = 20.0 (* 20 seconds *) in
    let dft_memlimit = 4000 (* 4 GB *) in
    let main =
      Whyconf.(
        set_limits main dft_timelimit dft_memlimit (running_provers_max main))
    in
    Whyconf.set_main config main
  in
  let stdlib = Dirs.Sites.stdlib in
  ( Env.create_env
      (loadpath @ stdlib @ Whyconf.loadpath (Whyconf.get_main config)),
    config )

let write_nir_as_onnx onnx_out_dir =
  Language.iter_nn (fun ls nn ->
    match nn.nn_format with
    | ONNX (Some nn_nir) | NNet (Some nn_nir) -> (
      try
        if not (Stdlib.Sys.file_exists onnx_out_dir)
        then Stdlib.Sys.mkdir onnx_out_dir 0o755;
        let filename =
          Fmt.str "%s%s%a.nir.onnx" onnx_out_dir Stdlib.Filename.dir_sep
            Pretty.print_ls ls
        in
        Onnx.Writer.to_file nn_nir filename;
        Logs.debug ~src:Logging.src_nir (fun m ->
          m "@[Wrote NIR as ONNX in file '%s'@]" filename)
      with Sys_error msg ->
        Logging.user_error (fun m ->
          m "@[Cannot write NIR as ONNX in folder '%s': '%s'@]" onnx_out_dir msg)
      )
    | _ ->
      Logs.warn (fun m ->
        m "@[No available NIR to write as ONNX for logic symbol '%a'@]"
          Pretty.print_ls ls))

let write_ovo_as_onnx onnx_out_dir =
  Language.iter_svm (fun ls svm ->
    try
      if not (Stdlib.Sys.file_exists onnx_out_dir)
      then Stdlib.Sys.mkdir onnx_out_dir 0o755;
      let filename =
        Fmt.str "%s%s%a.ovo.onnx" onnx_out_dir Stdlib.Filename.dir_sep
          Pretty.print_ls ls
      in
      Onnx.Writer.to_file svm.svm_nir filename;
      Logs.debug ~src:Logging.src_nir (fun m ->
        m "@[Wrote SVM as ONNX in file '%s'@]" filename)
    with Sys_error msg ->
      Logging.user_error (fun m ->
        m "@[Cannot write SVM as ONNX in folder '%s': '%s'@]" onnx_out_dir msg))

let answer_aimos limit config env config_prover dataset task =
  let id = Task.task_goal task in
  let predicate =
    let on_model ls =
      Option.value_or_thunk
        ~default:(fun () ->
          Logging.user_error (fun m ->
            m "No neural network model found as logic symbol %a" Pretty.print_ls
              ls))
        (Language.lookup_loaded_nets ls)
    in
    let on_dataset _ls =
      match dataset with
      | None -> Logging.user_error (fun m -> m "No dataset provided")
      | Some filename ->
        if String.is_suffix filename ~suffix:".csv"
        then filename
        else
          Logging.user_error (fun m ->
            m "File '%s' has an unsupported extension" filename)
    in
    Dataset.interpret_predicate env ~on_model ~on_dataset task
  in
  let prover_answer = Aimos.call_prover limit config config_prover predicate in
  Verification_types.Answer.LegacyDatasetAnswer
    {
      id;
      prover_answer;
      percentage_valid = None;
      dataset_results = [];
      additional_info = None;
    }

let re_nnet_or_onnx = Re__.Core.(compile (str "%{nnet-onnx}"))
let re_config = Re__.Core.(compile (str "%{config}"))
let config_site = List.hd_exn Dirs.Sites.config

let build_command config_prover ~nn_filename =
  let command = Whyconf.get_complete_command ~with_steps:false config_prover in
  let command =
    Re__.Replace.replace_string re_nnet_or_onnx
      ~by:(Unix.realpath nn_filename)
      command
  in
  Re__.Replace.replace_string re_config ~by:config_site command

let call_prover_on_task limit config command driver task =
  Logs.debug ~src:Logging.src_prover_spec (fun m ->
    let print fmt task = ignore (Driver.print_task_prepared driver fmt task) in
    m "Prover-tailored specification:@.%a" print task);
  let prover_call =
    Driver.prove_task_prepared ~command ~config ~limit driver task
  in
  let prover_result = Call_provers.wait_on_call prover_call in
  prover_result.pr_answer

let combine_prover_answers answers =
  List.fold_left answers ~init:Call_provers.Valid ~f:(fun acc r ->
    match (acc, r) with
    | Call_provers.Invalid, _ | _, Call_provers.Invalid -> Invalid
    | Timeout, _ | _, Timeout -> Timeout
    | OutOfMemory, _ | _, OutOfMemory -> OutOfMemory
    | StepLimitExceeded, _ | _, StepLimitExceeded -> StepLimitExceeded
    | Failure s, _ | _, Failure s -> Failure s
    | HighFailure, _ | _, HighFailure -> HighFailure
    | Unknown s, _ | _, Unknown s -> Unknown s
    | Valid, Valid -> Valid)

let answer_saver limit config env config_prover ~proof_strategy task =
  let tasks = proof_strategy task in
  let id = Task.task_goal task in
  let answers =
    List.map tasks ~f:(fun task ->
      let svm, abstraction =
        match Task.on_meta_excl Meta.meta_svm_filename task with
        | None -> Logging.user_error (fun m -> m "No SVM model found")
        | Some [ MAstr svm_filename; MAstr svm_abstraction ] ->
          (svm_filename, Language.svm_abstraction_of_string svm_abstraction)
        | Some _ -> assert false (* By construction of the meta. *)
      in
      let vars =
        match Trans.apply Utils.input_terms task with
        | Utils.Vars mls -> Why3.Term.Mls.keys mls
        | Others ->
          Logging.user_error (fun m -> m "Cannot determine input variables")
      in
      let dataset : Csv.t =
        let features = Trans.apply (Utils.input_features env ~vars) task in
        let features =
          List.map vars ~f:(fun v ->
            match Why3.Term.Mls.find_opt v features with
            | None ->
              Logging.user_error (fun m ->
                m "Cannot find feature for input variable '%a'"
                  Why3.Pretty.print_ls v)
            | Some feature -> Float.to_string feature)
        in
        let label = Trans.apply (Utils.output_label env) task in
        let label =
          match label with
          | None ->
            Logging.user_error (fun m -> m "Cannot find (classification) label")
          | Some label -> Int.to_string label
        in
        let vector = label :: features in
        let header = [ Fmt.str "# 1 %d" (List.length features) ] in
        [ header; vector ]
      in
      let perturbations =
        let perturbation =
          Trans.apply (Trans.seq [ Inlining.trivial; Relop.simplify env ]) task
          |> Trans.apply (Utils.input_perturbations env ~vars)
        in
        List.map vars ~f:(fun v ->
          match Why3.Term.Mls.find_opt v perturbation with
          | None ->
            Logging.user_error (fun m ->
              m "Cannot find input perturbation for variable '%a'"
                Why3.Pretty.print_ls v)
          | Some (None, _ | _, None) ->
            Logging.user_error (fun m ->
              m "Partial input perturbation for variable '%a'"
                Why3.Pretty.print_ls v)
          | Some (Some lower, Some upper) -> (lower, upper))
      in
      let saver_answer =
        Saver.call_prover limit config config_prover ?abstraction ~svm ~dataset
          ~perturbations ()
      in
      saver_answer.prover_answer)
  in
  let prover_answer = combine_prover_answers answers in
  Verification_types.Answer.LegacyDatasetAnswer
    {
      id;
      prover_answer;
      percentage_valid = None;
      dataset_results = [];
      additional_info = None;
    }

let answer_generic limit config prover config_prover driver ~proof_strategy task
    =
  let id = Task.task_goal task in
  let tasks = proof_strategy task in
  let answers =
    List.concat_map tasks ~f:(fun task ->
      let task = Driver.prepare_task driver task in
      let nn_filename =
        match
          ( Task.on_meta_excl Meta.meta_nn_filename task,
            Task.on_meta_excl Meta.meta_svm_filename task )
        with
        | None, None ->
          Logging.user_error (fun m ->
            m "No neural network nor SVM model found in task.")
        | Some [ MAstr nn_filename ], None | None, Some [ MAstr nn_filename; _ ]
          ->
          nn_filename
        | Some _, _ | _, Some _ ->
          assert false (* By construction of the meta. *)
      in
      let tasks =
        (* Turn [task] into a list (ie, conjunction) of disjunctions of
           tasks. *)
        match prover with
        | Prover.Marabou | Maraboupy -> Trans.apply Split.split_all task
        | Pyrat | Nnenum | ABCrown -> Trans.apply Split.split_premises task
        | _ -> [ task ]
      in
      let command = build_command config_prover ~nn_filename in
      List.map tasks ~f:(call_prover_on_task limit config command driver))
  in
  let prover_answer = combine_prover_answers answers in
  let additional_info = None in
  Verification_types.Answer.GenericProblemAnswer
    { prover_answer; additional_info; id }

let call_prover ~cwd ~limit config env prover config_prover driver ?dataset
  definitions task =
  match prover with
  | Prover.Saver ->
    let task = Interpreter.interpret_task ~cwd env ~definitions task in
    let proof_strategy = Proof_strategy.apply_svm_prover_strategy in
    answer_saver limit config env config_prover ~proof_strategy task
  | Aimos -> answer_aimos limit config env config_prover dataset task
  | Marabou | Maraboupy | Pyrat | Nnenum | ABCrown ->
    let task = Interpreter.interpret_task ~cwd env ~definitions task in
    let proof_strategy = Proof_strategy.apply_native_nn_prover_strategy env in
    answer_generic limit config prover config_prover driver ~proof_strategy task
  | CVC5 | AltErgo | Z3 ->
    let task = Interpreter.interpret_task ~cwd env ~definitions task in
    let proof_strategy = Proof_strategy.apply_classic_prover_strategy env in
    answer_generic limit config prover config_prover driver ~proof_strategy task

let open_file ?format env file =
  match file with
  | Verification_types.File.Stdin ->
    ( Unix.getcwd (),
      Env.(read_channel ?format base_language env "stdin" Stdlib.stdin) )
  | File file ->
    let mlw_files, _ = Env.(read_file ?format base_language env file) in
    (Stdlib.Filename.dirname file, mlw_files)

let tasks_of_theory ~theories ~goals theory =
  let tasks = Task.split_theory theory None None in
  let theory_id = theory.th_name.id_string in
  match (theories, goals) with
  | [], [] -> tasks
  | theories, goals ->
    if List.exists theories ~f:(String.equal theory_id)
    then tasks
    else
      let goals_of_theory =
        List.fold goals ~init:[]
          ~f:(fun goals_of_theory (theory_name, goal_names) ->
          (if String.equal theory_name "" || String.equal theory_name theory_id
           then goal_names
           else [])
          @ goals_of_theory)
      in
      List.filter tasks ~f:(fun task ->
        let task_goal = Task.task_goal task in
        let task_goal_id = task_goal.pr_name.id_string in
        List.exists goals_of_theory ~f:(String.equal task_goal_id))

let verify ?format query =
  let debug = Logging.(is_debug_level src_prover_call) in
  (if debug then Debug.(set_flag (lookup_flag "call_prover")));
  let env, config = create_env query.Verification_types.Query.loadpath in
  let main = Whyconf.get_main config in
  let limit =
    let memlimit =
      Option.value query.memory_limit ~default:(Whyconf.memlimit main)
    in
    let timelimit =
      Option.value_map query.time_limit ~f:Float.of_int
        ~default:(Whyconf.timelimit main)
    in
    let def = Call_provers.empty_limit in
    {
      Call_provers.limit_time = timelimit;
      Call_provers.limit_steps = def.limit_steps;
      Call_provers.limit_mem = memlimit;
    }
  in
  let dataset = query.dataset in
  let config_prover =
    let altern =
      let is_legacy_dataset_query = Option.is_some query.dataset in
      let default =
        if Prover.has_vnnlib_support query.prover && is_legacy_dataset_query
        then "VNNLIB"
        else "" (* Select the default one, w/o an alternative. *)
      in
      Option.value query.prover_altern ~default
    in
    let prover = Prover.to_string query.prover in
    try
      Whyconf.(filter_one_prover config (mk_filter_prover ~altern prover))
    with
    | Whyconf.ProverNotFound _ ->
      Logging.user_error (fun m ->
        m "No prover corresponds to %s%a" prover
          (if String.equal altern ""
           then Fmt.nop
           else Fmt.(any " " ++ parens string))
          altern)
    | Whyconf.ProverAmbiguity _ ->
      Logging.user_error (fun m ->
        m "More than one prover corresponds to %s%a" prover
          (if String.equal altern ""
           then Fmt.nop
           else Fmt.(any " " ++ parens string))
          altern)
  in
  let driver_str = snd config_prover.driver in
  let driver =
    Re__.Replace.replace_string re_config ~by:config_site driver_str
  in
  let driver =
    if String.equal driver driver_str
    then Driver.load_driver_for_prover main env config_prover
    else
      Driver.load_driver_file_and_extras main env ~extra_dir:None driver
        config_prover.extra_drivers
  in
  let filepath, definitions, goals, theories =
    match query.problem with
    | GenericProblem { filepath; definitions; goals; theories } ->
      (filepath, definitions, goals, theories)
  in
  let cwd, mstr_theory = open_file ?format env filepath in
  let verification_result =
    Wstdlib.Mstr.map
      (fun theory ->
        let tasks = tasks_of_theory ~theories ~goals theory in
        List.map
          ~f:(fun task ->
            let answer =
              call_prover ~cwd ~limit main env query.prover config_prover driver
                ?dataset definitions task
            in
            Verification_types.Answer.make ~query answer)
          tasks)
      mstr_theory
  in
  Option.iter ~f:write_nir_as_onnx query.onnx_out_dir;
  Option.iter ~f:write_ovo_as_onnx query.onnx_out_dir;
  verification_result
