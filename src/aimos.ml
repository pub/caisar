(**************************************************************************)
(*                                                                        *)
(*  This file is part of CAISAR.                                          *)
(*                                                                        *)
(*  Copyright (C) 2025                                                    *)
(*    CEA (Commissariat à l'énergie atomique et aux énergies              *)
(*         alternatives)                                                  *)
(*                                                                        *)
(*  You can redistribute it and/or modify it under the terms of the GNU   *)
(*  Lesser General Public License as published by the Free Software       *)
(*  Foundation, version 2.1.                                              *)
(*                                                                        *)
(*  It is distributed in the hope that it will be useful,                 *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of        *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          *)
(*  GNU Lesser General Public License for more details.                   *)
(*                                                                        *)
(*  See the GNU Lesser General Public License version 2.1                 *)
(*  for more details (enclosed in the file licenses/LGPLv2.1).            *)
(*                                                                        *)
(**************************************************************************)

open Why3
open Base

(* How does the integration with AIMOS work?

   AIMOS can be configured via a YAML configuration file. The main idea behind
   the integration is to generate a proper configuration file, holding all the
   required information, that AIMOS will be able to parse.

   Mainly, AIMOS needs to know where the model is located on the file system,
   where are the inputs, what transformation must be applied to them and
   eventually the range of said transformation. All this information must be
   provided in the configuration file.

   When a user requires AIMOS for checking a property, CAISAR will generate (in
   `/tmp/`) a configuration file respecting the specification provided by the
   user, which will then be passed to AIMOS (cf. `caisar-detection-data.conf`),
   which will then return the result of the test.

   For now, AIMOS can be used with the predicates `meta_robust`,
   `meta_robust_min` and `meta_robust_diff` defined in
   `stdlib/caisar/caisar.mlw`.

   After AIMOS runs, its result is retrieved and matched with a regex, to only
   get the value of the test. This value is compared against the threshold
   defined in the specification. If the comparison is successful (i.e. the value
   returned by AIMOS is greater than or equal to the threshold). This
   computation is done by CAISAR itself, not AIMOS. *)

let src = Logs.Src.create "AIMOS" ~doc:"AIMOS verifier"
let re_aimos_file = Re__.Core.(compile (str "%{aimos_file}"))
let re_aimos_output = Re__.Pcre.regexp ".*,.*,\\s*(\\d+.\\d+)"

module YamlConfig = struct
  (* Using a module to wrap the following types is necessary for
     ppx_deriving_yaml to work, as it is incompatible with Base. *)

  open Stdlib

  type transformation = {
    name : string;
    fn_range : Dataset.amplitude;
  }

  let transformation_to_yaml transformation =
    let name = transformation.name in
    let fn_range = transformation.fn_range in
    match fn_range with
    | Some range_record ->
      `O
        [
          ("name", `String name);
          ("fn_range", `String (Dataset.string_of_amplitude_params range_record));
        ]
    | None -> `O [ ("name", `String name) ]

  type transformations = transformation list [@@deriving to_yaml]

  type aimos_options = {
    plot : bool;
    inputs_path : string;
    transformations : transformations;
    custom_t_path : string;
  }
  [@@deriving to_yaml]

  type aimos_defaults = {
    models_path : string;
    out_mode : string;
  }
  [@@deriving to_yaml]

  type aimos_customs = {
    custom_global_load : string option;
    custom_f_path : string;
  }
  [@@deriving to_yaml]

  type aimos_model = {
    defaults : aimos_defaults;
    customs : aimos_customs;
  }
  [@@deriving to_yaml]

  type aimos_models = aimos_model list [@@deriving to_yaml]

  let aimos_models_to_yaml aimos_models =
    `A [ aimos_model_to_yaml (List.nth aimos_models 0) ]

  type aimos_config = {
    options : aimos_options;
    models : aimos_models;
  }
  [@@deriving to_yaml]
end

include YamlConfig

let relativize_wrt_config_file ~config_file filename =
  Fpath.relativize ~root:(Fpath.v config_file) (Fpath.v filename)
  |> Option.value_map ~default:filename ~f:Fpath.to_string

let write_config inputs_path models_path (perturbation : string)
  custom_global_load amplitude out_mode =
  let config_file = Stdlib.Filename.temp_file "aimos-" ".yml" in
  let custom_t_path =
    Misc.lookup_file Dirs.Sites.config "aimos_custom_transformations.py"
    |> relativize_wrt_config_file ~config_file
  in
  let custom_f_path =
    Misc.lookup_file Dirs.Sites.config "aimos_custom_functions.py"
    |> relativize_wrt_config_file ~config_file
  in
  let models_path = relativize_wrt_config_file ~config_file models_path in
  let transformations =
    let transformation = { name = perturbation; fn_range = amplitude } in
    [ transformation ]
  in
  let options = { plot = false; inputs_path; transformations; custom_t_path } in
  let out_mode =
    match out_mode with
    | None | Some "classif" -> "classif"
    | Some out_mode -> out_mode
  in
  let custom_global_load =
    match custom_global_load with "" -> None | _ -> Some custom_global_load
  in
  let defaults = { models_path; out_mode } in
  let customs = { custom_global_load; custom_f_path } in
  let aimos_model = { defaults; customs } in
  let models = [ aimos_model ] in
  let aimos_config = { options; models } in
  let full_config = aimos_config_to_yaml aimos_config in
  Yaml_unix.to_file_exn (Fpath.v config_file) full_config;
  config_file

let build_command config_prover
  (predicate : (Language.nn_shape, string) Dataset.predicate) =
  let inputs_path = Unix.realpath predicate.dataset in
  let models_path = Unix.realpath predicate.model.filename in
  let perturbation, amplitude, load_function =
    match predicate.property with
    | Dataset.MetaRobust (_, aimos_params)
    | Dataset.MetaRobustMin (_, aimos_params)
    | Dataset.MetaRobustDiff (_, aimos_params) ->
      (aimos_params.perturbation, aimos_params.amplitude, aimos_params.g_load_fn)
    | _ -> Logging.user_error (fun m -> m "Unsupported property")
  in
  let out_mode =
    match predicate.property with
    | Dataset.MetaRobust (_, _) -> None
    | Dataset.MetaRobustMin (_, _) -> Some "classif_min"
    | Dataset.MetaRobustDiff (_, _) -> Some "diff"
    | _ -> Logging.user_error (fun m -> m "Unsupported property")
  in
  let aimos_config =
    write_config inputs_path models_path perturbation load_function amplitude
      out_mode
  in
  Logs.debug ~src (fun m ->
    m "AIMOS configuration:@.%s" (Stdio.In_channel.read_all aimos_config));
  let command = Whyconf.get_complete_command ~with_steps:false config_prover in
  Re__.Replace.replace_string re_aimos_file ~by:aimos_config command

let build_answer predicate_kind prover_result =
  let threshold =
    match predicate_kind with
    | Dataset.MetaRobust (f, _)
    | Dataset.MetaRobustMin (f, _)
    | Dataset.MetaRobustDiff (f, _) ->
      Float.of_string (Dataset.string_of_threshold f)
    | _ -> Logging.user_error (fun m -> m "Unsupported property")
  in
  match prover_result.Call_provers.pr_answer with
  | Call_provers.Unknown _ -> (
    let pr_output = prover_result.pr_output in
    match Re__.Core.exec_opt re_aimos_output pr_output with
    | Some re_group ->
      let res_aimos = Float.of_string (Re__.Core.Group.get re_group 1) in
      let prover_answer =
        if Float.( >= ) res_aimos threshold
        then Call_provers.Valid
        else Call_provers.Invalid
      in
      prover_answer
    | None ->
      Logging.code_error ~src (fun m ->
        m "Cannot interpret the output provided by AIMOS"))
  | unexpected_pr_answer ->
    (* Any other answer than HighFailure should never happen as we do not define
       anything in AIMOS's driver. *)
    Logging.code_error ~src (fun m ->
      m "Unexpected AIMOS prover answer '%a'"
        Why3.Call_provers.print_prover_answer unexpected_pr_answer)

let call_prover limit config config_prover
  (predicate : (Language.nn_shape, string) Dataset.predicate) =
  let command = build_command config_prover predicate in
  let prover_call =
    let res_parser =
      {
        Call_provers.prp_regexps =
          [ ("NeverMatch", Call_provers.Failure "Should not happen in CAISAR") ];
        prp_timeregexps = [];
        prp_stepregexps = [];
        prp_exitcodes = [];
        prp_model_parser = Model_parser.lookup_model_parser "no_model";
      }
    in
    Call_provers.call_on_buffer ~command ~config ~limit ~res_parser
      ~filename:" " ~get_model:None ~gen_new_file:false (Buffer.create 10)
  in
  let prover_result = Call_provers.wait_on_call prover_call in
  build_answer predicate.property prover_result
