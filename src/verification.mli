(**************************************************************************)
(*                                                                        *)
(*  This file is part of CAISAR.                                          *)
(*                                                                        *)
(*  Copyright (C) 2025                                                    *)
(*    CEA (Commissariat à l'énergie atomique et aux énergies              *)
(*         alternatives)                                                  *)
(*                                                                        *)
(*  You can redistribute it and/or modify it under the terms of the GNU   *)
(*  Lesser General Public License as published by the Free Software       *)
(*  Foundation, version 2.1.                                              *)
(*                                                                        *)
(*  It is distributed in the hope that it will be useful,                 *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of        *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          *)
(*  GNU Lesser General Public License for more details.                   *)
(*                                                                        *)
(*  See the GNU Lesser General Public License version 2.1                 *)
(*  for more details (enclosed in the file licenses/LGPLv2.1).            *)
(*                                                                        *)
(**************************************************************************)

type verification_query = Verification_types.Query.t
type verification_result = Verification_types.Answer.t

val verify :
  ?format:string ->
  verification_query ->
  verification_result list Why3.Wstdlib.Mstr.t
(** [verify query problem file] starts a verification on the provided [problem]
    according to the [query]. It returns a verification result.

    @param query is a value of type {!Verification_types.Query.t}.
    @param problem is a value of type {!Verification_types.Query.Problem.t}.
    @return
      for each theory, an [answer] for each goal of the theory, respecting the
      order of appearance. *)

val create_env : string list -> Why3.Env.env * Why3.Whyconf.config
