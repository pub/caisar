(**************************************************************************)
(*                                                                        *)
(*  This file is part of CAISAR.                                          *)
(*                                                                        *)
(*  Copyright (C) 2025                                                    *)
(*    CEA (Commissariat à l'énergie atomique et aux énergies              *)
(*         alternatives)                                                  *)
(*                                                                        *)
(*  You can redistribute it and/or modify it under the terms of the GNU   *)
(*  Lesser General Public License as published by the Free Software       *)
(*  Foundation, version 2.1.                                              *)
(*                                                                        *)
(*  It is distributed in the hope that it will be useful,                 *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of        *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          *)
(*  GNU Lesser General Public License for more details.                   *)
(*                                                                        *)
(*  See the GNU Lesser General Public License version 2.1                 *)
(*  for more details (enclosed in the file licenses/LGPLv2.1).            *)
(*                                                                        *)
(**************************************************************************)

open Base

(* -------------------------------------------------------------------------- *)
(* --- Conversions from/to float                                              *)
(* -------------------------------------------------------------------------- *)

let float_of_real_constant rc =
  let is_neg, rc =
    Why3.(BigInt.lt rc.Number.rl_real.rv_sig BigInt.zero, Number.abs_real rc)
  in
  let rc_str = Fmt.str "%a" Why3.Number.(print_real_constant full_support) rc in
  let f = Float.of_string rc_str in
  if is_neg then Float.neg f else f

let real_constant_of_float value =
  assert (Float.is_finite value);
  let m, e = Float.frexp value in
  (* put into the form m * 2^e, where m is an integer *)
  let m, e = (Int.of_float (Float.ldexp m 53), e - 53) in
  Why3.Constant.ConstReal
    {
      rl_kind = RLitDec 0;
      rl_real =
        Why3.Number.real_value ~pow2:(Why3.BigInt.of_int e)
          (Why3.BigInt.of_int m);
    }

let term_of_float env f =
  let th = Symbols.Float64.create env in
  Why3.Term.t_const (real_constant_of_float f) th.ty

(* -------------------------------------------------------------------------- *)
(* --- Information about input specification                                  *)
(* -------------------------------------------------------------------------- *)

(* Collects input variables and their position inside input vectors. Such
   process is memoized wrt lsymbols corresponding to input vectors. *)

(* Terms forming vectors in input to models. *)
type input_terms =
  | Vars of
      (int Why3.Term.Mls.t
      [@printer
        fun fmt mls ->
          Why3.(
            Pp.print_iter2 Term.Mls.iter Pp.newline Pp.comma Pretty.print_ls
              Pp.int fmt mls)])
    (* A map from input variable lsymbols to corresponding positions in input
       vectors. *)
  | Others (* Generic terms. *)
[@@deriving show]

let input_terms : input_terms Why3.Trans.trans =
  let exception NotInputVariable in
  let hls = Why3.Term.Hls.create 13 in
  let add index mls = function
    | { Why3.Term.t_node = Tapp (ls, []); _ } -> Why3.Term.Mls.add ls index mls
    | _ -> raise NotInputVariable
  in
  let rec do_collect mls t =
    match t.Why3.Term.t_node with
    | Why3.Term.Tapp
        ( ls1 (* @@ *),
          [
            { t_node = Tapp (_ (* model *), _); _ };
            { t_node = Tapp (ls2 (* input vector *), tl (* input vars *)); _ };
          ] )
      when String.equal ls1.ls_name.id_string (Why3.Ident.op_infix "@@") -> (
      match Language.lookup_vector ls2 with
      | Some vector_length ->
        assert (vector_length = List.length tl);
        if Why3.Term.Hls.mem hls ls2
        then mls
        else List.foldi ~init:mls ~f:add tl
      | None -> mls)
    | _ -> Why3.Term.t_fold do_collect mls t
  in
  Why3.Trans.fold_decl
    (fun decl mls ->
      match mls with
      | Vars mls -> (
        try Vars (Why3.Decl.decl_fold do_collect mls decl)
        with NotInputVariable -> Others)
      | Others -> Others)
    (Vars Why3.Term.Mls.empty)

(* Collects input feature values (these are typically coming from a data point
   of a data set).

   Such process is done by visiting the task and retrieving the real constants
   [rc] in expressions of the form [_ <= x .- rc] or [x .- rc <= _]. *)

type features =
  (Float.t Why3.Term.Mls.t
  [@printer
    fun fmt mls ->
      Why3.(
        Pp.print_iter2 Term.Mls.iter Pp.newline Pp.comma Pretty.print_ls
          Pp.float fmt mls)])
[@@deriving show]

let collect_features env vars =
  let th_f64 = Symbols.Float64.create env in
  let rec do_collect features t =
    match t.Why3.Term.t_node with
    | Why3.Term.Tapp
        ( ls_le (* less-equal *),
          [
            _ (* eps *);
            ( {
                t_node =
                  Why3.Term.Tapp
                    ( _ (* typically, .- *),
                      [
                        { t_node = Tapp (ls_var (* input var *), []); _ };
                        { t_node = Tconst (ConstReal rc_feature_value); _ };
                      ] );
                _;
              }
            | {
                t_node =
                  Why3.Term.Tapp
                    ( _ (* typically, sub *),
                      [
                        _ (* mode *);
                        { t_node = Tapp (ls_var (* input var *), []); _ };
                        { t_node = Tconst (ConstReal rc_feature_value); _ };
                      ] );
                _;
              } );
          ] )
      when Why3.Term.ls_equal ls_le th_f64.le
           && List.mem vars ls_var ~equal:Why3.Term.ls_equal ->
      let feature_value = float_of_real_constant rc_feature_value in
      Why3.Term.Mls.add ls_var feature_value features
    | Why3.Term.Tapp
        ( ls_le (* less-equal *),
          [
            ( {
                t_node =
                  Why3.Term.Tapp
                    ( _ (* typically, .- *),
                      [
                        { t_node = Tapp (ls_var (* input var *), []); _ };
                        { t_node = Tconst (ConstReal rc_feature_value); _ };
                      ] );
                _;
              }
            | {
                t_node =
                  Why3.Term.Tapp
                    ( _ (* typically, sub *),
                      [
                        _ (* mode*);
                        { t_node = Tapp (ls_var (* input var *), []); _ };
                        { t_node = Tconst (ConstReal rc_feature_value); _ };
                      ] );
                _;
              } );
            _ (* eps *);
          ] )
      when Why3.Term.ls_equal ls_le th_f64.le
           && List.mem vars ls_var ~equal:Why3.Term.ls_equal ->
      let feature_value = float_of_real_constant rc_feature_value in
      Why3.Term.Mls.add ls_var feature_value features
    | _ -> Why3.Term.t_fold do_collect features t
  in
  Why3.Trans.fold_decl
    (fun decl features -> Why3.Decl.decl_fold do_collect features decl)
    Why3.Term.Mls.empty

let input_features env ~vars : features Why3.Trans.trans =
  Why3.Trans.bind (collect_features env vars) (fun features ->
    Why3.Trans.return features)

(* Collects input feature perturbations as lower and upper bounds.

   Such process is done by visiting the task and retrieving the real constants
   [rc] in expressions of the form [rc <= x] and [x <= rc] respectively. If more
   than one lower (resp. upper) bound is present, the max (resp. min) is
   considered.

   Assumption: [inline_trivial] and [simplify_formula] transformations have been
   performed beforehand. *)

type interval = float option * float option [@@deriving show]

type perturbation =
  (interval Why3.Term.Mls.t
  [@printer
    fun fmt mls ->
      Why3.(
        Pp.print_iter2 Term.Mls.iter Pp.newline Pp.comma Pretty.print_ls
          pp_interval fmt mls)])
[@@deriving show]

let collect_perturbation env vars =
  let th_f64 = Symbols.Float64.create env in
  let rec do_collect perturbation t =
    match t.Why3.Term.t_node with
    | Why3.Term.Tapp
        ( ls_le (* less-equal *),
          [
            { t_node = Tconst (ConstReal rc_lower_bound); _ } (* lower bound *);
            { t_node = Tapp (ls_var (* input var *), []); _ };
          ] )
      when Why3.Term.ls_equal ls_le th_f64.le
           && List.mem vars ls_var ~equal:Why3.Term.ls_equal ->
      let lower_bound = float_of_real_constant rc_lower_bound in
      let interval =
        match Why3.Term.Mls.find_opt ls_var perturbation with
        | None -> (Some lower_bound, None)
        | Some (current_lower_bound, current_upper_bound) ->
          let lower_bound =
            Option.value_map current_lower_bound ~default:lower_bound
              ~f:(Float.max lower_bound)
          in
          (Some lower_bound, current_upper_bound)
      in
      Why3.Term.Mls.add ls_var interval perturbation
    | Why3.Term.Tapp
        ( ls_le (* less-equal *),
          [
            { t_node = Tapp (ls_var (* input var *), []); _ };
            { t_node = Tconst (ConstReal rc_upper_bound); _ } (* upper bound *);
          ] )
      when Why3.Term.ls_equal ls_le th_f64.le
           && List.mem vars ls_var ~equal:Why3.Term.ls_equal ->
      let upper_bound = float_of_real_constant rc_upper_bound in
      let interval =
        match Why3.Term.Mls.find_opt ls_var perturbation with
        | None -> (Some upper_bound, None)
        | Some (current_lower_bound, current_upper_bound) ->
          let upper_bound =
            Option.value_map current_upper_bound ~default:upper_bound
              ~f:(Float.min upper_bound)
          in
          (current_lower_bound, Some upper_bound)
      in
      Why3.Term.Mls.add ls_var interval perturbation
    | _ -> Why3.Term.t_fold do_collect perturbation t
  in
  Why3.Trans.fold_decl
    (fun decl perturbation -> Why3.Decl.decl_fold do_collect perturbation decl)
    Why3.Term.Mls.empty

let input_perturbations env ~vars : perturbation Why3.Trans.trans =
  Why3.Trans.bind (collect_perturbation env vars) (fun perturbation ->
    Why3.Trans.return perturbation)

(* Collects the output (classification) label (this is typically associated to
   the feature values coming from a data point of a data set).

   Such process is done by visiting the task and retrieving the vector index
   (typically, appearing inside lsymbol [_]) that appears the most in
   expressions of the form [(model @@ vector)[i1] <= (model @@ vector)[i2]]. *)

type label = int [@@deriving show]

let collect_label env =
  let th_f64 = Symbols.Float64.create env in
  let rec do_collect label t =
    match t.Why3.Term.t_node with
    | Why3.Term.Tapp
        ( ls_le (* less-equal *),
          [
            {
              t_node =
                Why3.Term.Tapp
                  ( ls_get1 (* [_] *),
                    [
                      ({ t_node = Tapp (ls_atat1 (* @@ *), _); _ } as _t1);
                      ({ t_node = Tconst (ConstInt index1); _ } as _t2);
                    ] );
              _;
            };
            {
              t_node =
                Why3.Term.Tapp
                  ( ls_get2 (* [_] *),
                    [
                      ({ t_node = Tapp (ls_atat2 (* @@ *), _); _ } as _t3);
                      ({ t_node = Tconst (ConstInt index2); _ } as _t4);
                    ] );
              _;
            };
          ] )
      when Why3.Term.ls_equal ls_le th_f64.le
           && String.equal ls_get1.ls_name.id_string (Why3.Ident.op_get "")
           && String.equal ls_atat1.ls_name.id_string (Why3.Ident.op_infix "@@")
           && String.equal ls_get2.ls_name.id_string (Why3.Ident.op_get "")
           && String.equal ls_atat2.ls_name.id_string (Why3.Ident.op_infix "@@")
      -> (
      let index2 = Why3.Number.to_small_integer index2 in
      match label with
      | None -> Some index2
      | Some label ->
        let index1 = Why3.Number.to_small_integer index1 in
        if label = index2 || label = index1
        then if label = index2 then Some label else Some index1
        else
          Logging.user_error (fun m ->
            m "Cannot determine (classification) label"))
    | _ -> Why3.Term.t_fold do_collect label t
  in
  Why3.Trans.fold
    (fun task_hd label ->
      match task_hd.Why3.Task.task_decl.td_node with
      | Why3.Theory.Decl ({ d_node = Dprop (Pgoal, _, _); _ } as decl) ->
        Why3.Decl.decl_fold do_collect label decl
      | _ -> label)
    None

let output_label env : label option Why3.Trans.trans =
  Why3.Trans.bind (collect_label env) (fun label -> Why3.Trans.return label)
