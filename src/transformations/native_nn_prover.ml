(**************************************************************************)
(*                                                                        *)
(*  This file is part of CAISAR.                                          *)
(*                                                                        *)
(*  Copyright (C) 2025                                                    *)
(*    CEA (Commissariat à l'énergie atomique et aux énergies              *)
(*         alternatives)                                                  *)
(*                                                                        *)
(*  You can redistribute it and/or modify it under the terms of the GNU   *)
(*  Lesser General Public License as published by the Free Software       *)
(*  Foundation, version 2.1.                                              *)
(*                                                                        *)
(*  It is distributed in the hope that it will be useful,                 *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of        *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          *)
(*  GNU Lesser General Public License for more details.                   *)
(*                                                                        *)
(*  See the GNU Lesser General Public License version 2.1                 *)
(*  for more details (enclosed in the file licenses/LGPLv2.1).            *)
(*                                                                        *)
(**************************************************************************)

open Base

let src = Logs.Src.create "NN-Gen" ~doc:"Generation of neural networks"
let src_term = Logs.Src.create "NN-Gen-Term" ~doc:"Generation of new formula"

let src_show =
  Logs.Src.create "NN-Gen-Show" ~doc:"Show generated neural networks"

type new_output = {
  index : int;
  term : Why3.Term.term;
}

exception UnknownLogicSymbol

let tempfile =
  let c = ref (-1) in
  fun () ->
    match Sys.getenv "CAISAR_ONNX_OUTPUT_DIR" with
    | Some dir ->
      (* deterministic *)
      Int.incr c;
      Stdlib.Filename.concat dir (Fmt.str "caisar_%i.onnx" !c)
    | None -> Stdlib.Filename.temp_file "caisar" ".onnx"

let match_nn_app th_model term =
  match term.Why3.Term.t_node with
  | Tapp
      ( ls_get (* [ ] *),
        [
          {
            t_node =
              Why3.Term.Tapp
                ( ls_atat (* @@ *),
                  [
                    { t_node = Tapp (ls_nn (* nn *), _); _ };
                    {
                      t_node = Tapp (ls (* input vector *), tl (* input vars *));
                      _;
                    };
                  ] );
            _;
          };
          { t_node = Tconst (ConstInt c); _ };
        ] )
    when String.equal ls_get.ls_name.id_string (Why3.Ident.op_get "")
         && Why3.Term.ls_equal ls_atat th_model.Symbols.Model.atat -> (
    match (Language.lookup_nn ls_nn, Language.lookup_vector ls) with
    | Some ({ nn_nb_inputs; _ } as nn), Some vector_length ->
      assert (nn_nb_inputs = vector_length && vector_length = List.length tl);
      let c = Why3.Number.to_small_integer c in
      Some (nn, tl, c)
    | _, _ -> None)
  | _ -> None

let create_new_nn env input_vars outputs : string =
  let module IR = Nir in
  let th_f64 = Symbols.Float64.create env in
  let th_model = Symbols.Model.create env in
  let len_input = Why3.Term.Mls.cardinal input_vars in
  let input =
    IR.Node.create (Input { shape = IR.Shape.of_list [ len_input ] })
  in
  assert (Why3.Term.Mls.for_all (fun _ i -> 0 <= i && i < len_input) input_vars);
  let get_input =
    Why3.Term.Hls.memo 10 (fun ls ->
      let i = Why3.Term.Mls.find_exn UnknownLogicSymbol ls input_vars in
      IR.Node.gather_int input i)
  in
  let cache = Why3.Term.Hterm.create 17 in
  (* Instantiate the input of [old_nn] with the [converted_arg] term transformed
     into a node. *)
  let convert_nn ?loc old_nn converted_arg =
    if old_nn.Language.nn_nb_inputs
       <> Nir.Shape.size converted_arg.Nir.Node.shape
    then
      Logging.user_error ?loc (fun m ->
        m "Neural network applied with the wrong number of arguments");
    let old_nn_nir =
      let nir_opt =
        match old_nn.Language.nn_format with
        | NNet nir_opt -> nir_opt
        | ONNX nir_opt -> nir_opt
      in
      match nir_opt with
      | None ->
        Logging.code_error ~src (fun m ->
          m "No NIR available for NN '%s'" old_nn.nn_filename)
      | Some g -> g
    in
    (* Create the graph to replace the old input of the nn *)
    let input () =
      let node = converted_arg in
      Nir.Node.reshape (Nir.Ngraph.input_shape old_nn_nir) node
    in
    let out =
      IR.Node.replace_input input (Nir.Ngraph.output old_nn_nir)
      |> IR.Node.reshape (Nir.Shape.of_array [| old_nn.nn_nb_outputs |])
    in
    out
  in
  let rec convert_term term =
    match Why3.Term.Hterm.find_opt cache term with
    | None ->
      let n = convert_term_aux term in
      Why3.Term.Hterm.add cache term n;
      n
    | Some n -> n
  and convert_term_aux term : Nir.Node.t =
    let loc = term.Why3.Term.t_loc in
    if false
       && not
            (Why3.Ty.ty_equal (Option.value_exn term.Why3.Term.t_ty) th_f64.ty)
    then
      Logging.user_error ?loc:term.t_loc (fun m ->
        m "Cannot convert non Float64 term %a" Why3.Pretty.print_term term);
    match term.Why3.Term.t_node with
    | Tapp (ls_get (* [ ] *), [ t1; { t_node = Tconst (ConstInt c); _ } ])
      when String.equal ls_get.ls_name.id_string (Why3.Ident.op_get "") ->
      let c = Why3.Number.to_small_integer c in
      let t1 = convert_term t1 in
      if Nir.Shape.size t1.Nir.Node.shape <= c
      then
        Logging.user_error ?loc (fun m ->
          m "Vector accessed outside its bounds");
      Nir.Node.gather_int t1 c
    | Tapp (ls_atat (* @@ *), [ { t_node = Tapp (ls_nn (* nn *), _); _ }; t2 ])
      when Why3.Term.ls_equal ls_atat th_model.Symbols.Model.atat -> (
      match Language.lookup_nn ls_nn with
      | Some nn -> convert_nn ?loc nn (convert_term t2)
      | _ ->
        Logging.code_error ~src (fun m ->
          m "Neural network application without fixed NN: %a"
            Why3.Pretty.print_term term))
    | Tapp (ls (* input vector *), tl (* input vars *))
      when Language.mem_vector ls ->
      let inputs = List.map tl ~f:convert_term in
      IR.Node.create (Concat { inputs; axis = 0 })
    | Tconst (Why3.Constant.ConstReal r) ->
      IR.Node.create
        (Constant
           {
             data =
               Nir.Gentensor.create_1_float (Utils.float_of_real_constant r);
           })
    | Tapp (ls, []) -> get_input ls
    | Tapp (ls, [ _; a; b ]) when Why3.Term.ls_equal ls th_f64.add ->
      IR.Node.create (Add { input1 = convert_term a; input2 = convert_term b })
    | Tapp (ls, [ _; a; b ]) when Why3.Term.ls_equal ls th_f64.sub ->
      IR.Node.create (Sub { input1 = convert_term a; input2 = convert_term b })
    | Tapp (ls, [ _; a; b ]) when Why3.Term.ls_equal ls th_f64.mul ->
      IR.Node.create (Mul { input1 = convert_term a; input2 = convert_term b })
    | Tapp (ls, [ _; a; b ]) when Why3.Term.ls_equal ls th_f64.div -> (
      match b.t_node with
      | Tconst (Why3.Constant.ConstReal r) ->
        let f = Utils.float_of_real_constant r in
        Nir.Node.div_float (convert_term a) f
      | _ ->
        IR.Node.create
          (Div { input1 = convert_term a; input2 = convert_term b }))
    | Tapp (ls, [ a ]) when Why3.Term.ls_equal ls th_f64.neg ->
      Nir.Node.mul_float (convert_term a) (-1.)
    | Tconst _
    | Tapp (_, _)
    | Tif (_, _, _)
    | Tlet (_, _)
    | Tbinop (_, _, _)
    | Tcase (_, _)
    | Tnot _ | Ttrue | Tfalse ->
      Logging.not_implemented_yet (fun m ->
        m "Why3 term to IR: %a" Why3.Pretty.print_term term)
    | Tvar _ | Teps _ | Tquant (_, _) ->
      Logging.not_implemented_yet (fun m ->
        m "Why3 term to IR (impossible?): %a" Why3.Pretty.print_term term)
  in
  (* Start actual term conversion. *)
  let outputs =
    List.rev_map outputs ~f:(fun { index; term } -> (index, convert_term term))
    |> List.sort ~compare:(fun (i, _) (j, _) -> Int.compare i j)
    |> List.mapi ~f:(fun i (j, n) ->
         assert (i = j);
         n)
  in
  let output = IR.Node.concat_0 outputs in
  assert (
    IR.Shape.equal output.shape (IR.Shape.of_array [| List.length outputs |]));
  let nn = IR.Ngraph.create output in
  Logs.debug ~src:src_show (fun f -> f "@.%s@." (IR.Ngraph.grapheasy nn));
  Logs.debug ~src (fun f -> f "@[<v>%a@]@." IR.Ngraph.pp_debug nn);
  let filename = tempfile () in
  Onnx.Writer.to_file nn filename;
  filename

let check_if_new_nn_needed env input_vars outputs =
  let th_model = Symbols.Model.create env in
  let exception New_NN_Needed in
  try
    (* outputs must be direct application, inputs must be lsymbols *)
    let outputs =
      List.map outputs ~f:(fun (term, ls) ->
        match match_nn_app th_model term with
        | None -> raise New_NN_Needed
        | Some (nn, tl, c) ->
          let tl =
            List.map tl ~f:(function
              | { t_node = Why3.Term.Tapp (ls, []); _ } -> ls
              | _ -> raise New_NN_Needed)
          in
          (ls, nn, tl, c))
    in
    match outputs with
    | [] -> raise New_NN_Needed
    | (_, nn, tl, _) :: _ ->
      (* with the same nn and arguments *)
      if not
           (List.for_all outputs ~f:(fun (_, nn', tl', _) ->
              String.equal nn'.Language.nn_filename nn.Language.nn_filename
              && List.equal Why3.Term.ls_equal tl tl'))
      then raise New_NN_Needed;
      assert (Why3.Term.Sls.equal (Why3.Term.Sls.of_list tl) input_vars);
      let outputs =
        List.map outputs ~f:(fun (ls, _, _, index) -> (index, ls))
      in
      let compare (a, _) (b, _) = Int.compare a b in
      let outputs = List.sort outputs ~compare in
      assert (List.is_sorted_strictly outputs ~compare);
      let indexed_input_vars =
        List.mapi tl ~f:(fun i x -> (x, i)) |> Why3.Term.Mls.of_list
      in
      (nn.nn_filename, indexed_input_vars, outputs)
  with New_NN_Needed ->
    let _, indexed_input_vars =
      Why3.Term.Mls.mapi_fold (fun _ _ i -> (i + 1, i)) input_vars 0
    in
    let outputs1 =
      List.mapi outputs ~f:(fun index (term, _) -> { index; term })
    in
    let outputs2 = List.mapi outputs ~f:(fun index (_, ls) -> (index, ls)) in
    (create_new_nn env indexed_input_vars outputs1, indexed_input_vars, outputs2)

(* Choose the term pattern for starting the conversion to ONNX. *)
let has_start_pattern env term =
  let th_f = Symbols.Float64.create env in
  match term.Why3.Term.t_node with
  | Tapp (ls_get (* [ ] *), _)
    when String.equal ls_get.ls_name.id_string (Why3.Ident.op_get "") ->
    true
  | Tapp ({ ls_value = Some ty; _ }, []) ->
    (* input symbol *) Why3.Ty.ty_equal ty th_f.ty
  | Tapp (ls_app, _) ->
    List.mem
      [ th_f.add; th_f.sub; th_f.mul; th_f.div ]
      ~equal:Why3.Term.ls_equal ls_app
  | _ -> false

(* Abstract terms that contains neural network applications. *)
let abstract_nn_term env =
  let th_float64 = Symbols.Float64.create env in
  let simplify_as_output_vars term =
    let rec do_simplify output_vars term =
      if has_start_pattern env term
      then
        let output_vars, ls_output_var =
          match Why3.Term.Mterm.find_opt term output_vars with
          | None ->
            let ls =
              Why3.Term.create_fsymbol (Why3.Ident.id_fresh "y") []
                th_float64.ty
            in
            let output_vars = Why3.Term.Mterm.add term ls output_vars in
            (output_vars, ls)
          | Some ls -> (output_vars, ls)
        in
        let output_var = Why3.Term.fs_app ls_output_var [] th_float64.ty in
        (output_vars, output_var)
      else Why3.Term.t_map_fold do_simplify output_vars term
    in
    let output_vars, term = do_simplify Why3.Term.Mterm.empty term in
    (output_vars, term)
  in
  Why3.Trans.fold_map
    (fun task_hd (input_vars, task) ->
      let tdecl = task_hd.task_decl in
      match tdecl.td_node with
      | Decl { d_node = Dparam ls; _ } when Language.mem_nn ls ->
        (* All neural networks are removed. *)
        (input_vars, task)
      | Meta (meta, _) when Why3.Theory.meta_equal meta Meta.meta_nn_filename ->
        (* All neural networks meta are removed. *)
        (input_vars, task)
      | Decl
          {
            d_node =
              Dparam
                ({
                   ls_args = [];
                   ls_constr = 0;
                   ls_proj = false;
                   ls_value = Some ty;
                   _;
                 } as ls);
            _;
          }
        when Why3.Ty.ty_equal ty th_float64.ty ->
        let task = Why3.Task.add_tdecl task tdecl in
        let input_vars = Why3.Term.Sls.add ls input_vars in
        (input_vars, task)
      | Decl { d_node = Dprop (Pgoal, pr, goal); _ } ->
        let output_vars, goal = simplify_as_output_vars goal in
        Logs.debug ~src:src_term (fun f ->
          f "new goal:%a" Why3.Pretty.print_term goal);
        let new_outputs = Why3.Term.Mterm.bindings output_vars in
        (* Collect all lsymbols appearing in terms simplified as output
           variables: at the moment, vector selection on a neural network
           application to an input vector (ie, (nn @@ v)[_]). *)
        let used_ls =
          List.fold
            ~f:(fun acc (new_output, _) ->
              Why3.Term.t_s_fold
                (fun acc _ -> acc)
                (fun acc ls -> Why3.Term.Sls.add ls acc)
                acc new_output)
            ~init:Why3.Term.Sls.empty new_outputs
        in
        let input_vars = Why3.Term.Sls.inter input_vars used_ls in
        (* Create new ONNX and add the corresponding meta. *)
        let nn_filename, indexed_input_vars, output_vars =
          check_if_new_nn_needed env input_vars new_outputs
        in
        (* Add meta for input variable declarations. *)
        let task =
          Why3.Term.Mls.fold_left
            (fun task ls index ->
              Why3.Task.add_meta task Meta.meta_input [ MAls ls; MAint index ])
            task indexed_input_vars
        in

        (* Add output variable declarations and corresponding meta. *)
        let task =
          List.fold_left
            ~f:(fun task (index, output_var) ->
              let task =
                Why3.Task.add_meta task Meta.meta_output
                  [ MAls output_var; MAint index ]
              in
              Why3.Task.add_param_decl task output_var)
            ~init:task output_vars
        in
        let task =
          Why3.Task.add_meta task Meta.meta_nn_filename [ MAstr nn_filename ]
        in
        let decl =
          let pr = Why3.Decl.create_prsymbol (Why3.Ident.id_clone pr.pr_name) in
          Why3.Decl.create_prop_decl Pgoal pr goal
        in
        (input_vars, Why3.Task.add_decl task decl)
      | Decl { d_node = Dprop (_, _, _); _ } ->
        (* TODO: check no nn applications *)
        (input_vars, Why3.Task.add_tdecl task tdecl)
      | _ -> (input_vars, Why3.Task.add_tdecl task tdecl))
    Why3.Term.Sls.empty None

let trans = Why3.Env.Wenv.memoize 10 abstract_nn_term
