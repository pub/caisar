(**************************************************************************)
(*                                                                        *)
(*  This file is part of CAISAR.                                          *)
(*                                                                        *)
(*  Copyright (C) 2025                                                    *)
(*    CEA (Commissariat à l'énergie atomique et aux énergies              *)
(*         alternatives)                                                  *)
(*                                                                        *)
(*  You can redistribute it and/or modify it under the terms of the GNU   *)
(*  Lesser General Public License as published by the Free Software       *)
(*  Foundation, version 2.1.                                              *)
(*                                                                        *)
(*  It is distributed in the hope that it will be useful,                 *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of        *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          *)
(*  GNU Lesser General Public License for more details.                   *)
(*                                                                        *)
(*  See the GNU Lesser General Public License version 2.1                 *)
(*  for more details (enclosed in the file licenses/LGPLv2.1).            *)
(*                                                                        *)
(**************************************************************************)

open Base

let split_generic filter_prop_kind =
  Why3.Trans.decl_l
    (fun d ->
      match d.d_node with
      | Dprop (k, pr, t) when filter_prop_kind k ->
        let l =
          match k with
          | Paxiom | Plemma -> Why3.Split_goal.split_neg_full t
          | Pgoal -> Why3.Split_goal.split_pos_full t
        in
        List.map l ~f:(fun t -> [ Why3.Decl.create_prop_decl k pr t ])
      | _ -> [ [ d ] ])
    None

let split_premises = split_generic (function Paxiom -> true | _ -> false)
let split_all = split_generic (fun _ -> true)

let split_goal_full_stop_on_quant =
  let trans_add_stop_split =
    let rec add_stop_split_on_quant t =
      match t.Why3.Term.t_node with
      | Tquant _ -> Why3.Term.t_attr_add Why3.Term.stop_split t
      | _ -> Why3.Term.t_map add_stop_split_on_quant t
    in
    Why3.Trans.goal (fun pr t ->
      let t = add_stop_split_on_quant t in
      [ Why3.Decl.create_prop_decl Pgoal pr t ])
  in
  Why3.Trans.bind trans_add_stop_split (fun task ->
    Why3.Trans.return (Why3.Trans.apply Why3.Split_goal.split_goal_full task))
