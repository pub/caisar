(**************************************************************************)
(*                                                                        *)
(*  This file is part of CAISAR.                                          *)
(*                                                                        *)
(*  Copyright (C) 2025                                                    *)
(*    CEA (Commissariat à l'énergie atomique et aux énergies              *)
(*         alternatives)                                                  *)
(*                                                                        *)
(*  You can redistribute it and/or modify it under the terms of the GNU   *)
(*  Lesser General Public License as published by the Free Software       *)
(*  Foundation, version 2.1.                                              *)
(*                                                                        *)
(*  It is distributed in the hope that it will be useful,                 *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of        *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          *)
(*  GNU Lesser General Public License for more details.                   *)
(*                                                                        *)
(*  See the GNU Lesser General Public License version 2.1                 *)
(*  for more details (enclosed in the file licenses/LGPLv2.1).            *)
(*                                                                        *)
(**************************************************************************)

val split_premises : Why3.Task.task Why3.Trans.tlist
(** Split disjunctions appearing in axioms. *)

val split_all : Why3.Task.task Why3.Trans.tlist
(** Split disjunctions appearing in premises, and conjunctions appearing in
    goals. *)

val split_goal_full_stop_on_quant : Why3.Task.task Why3.Trans.tlist
(** Like [Why3.Split_goal.split_goal_full] but stop split on quantified
    (sub)formulas. *)
