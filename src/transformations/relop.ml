(**************************************************************************)
(*                                                                        *)
(*  This file is part of CAISAR.                                          *)
(*                                                                        *)
(*  Copyright (C) 2025                                                    *)
(*    CEA (Commissariat à l'énergie atomique et aux énergies              *)
(*         alternatives)                                                  *)
(*                                                                        *)
(*  You can redistribute it and/or modify it under the terms of the GNU   *)
(*  Lesser General Public License as published by the Free Software       *)
(*  Foundation, version 2.1.                                              *)
(*                                                                        *)
(*  It is distributed in the hope that it will be useful,                 *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of        *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          *)
(*  GNU Lesser General Public License for more details.                   *)
(*                                                                        *)
(*  See the GNU Lesser General Public License version 2.1                 *)
(*  for more details (enclosed in the file licenses/LGPLv2.1).            *)
(*                                                                        *)
(**************************************************************************)

open Why3
open Base

(* -------------------------------------------------------------------------- *)
(* --- Simplify Relops                                                        *)
(* -------------------------------------------------------------------------- *)

let make_rewriter env =
  let th_f64 = Symbols.Float64.create env in
  let relop_f64 = [ th_f64.le; th_f64.lt; th_f64.ge; th_f64.gt ] in
  let aop_f64 =
    [ th_f64.add; th_f64.sub; th_f64.mul; th_f64.div; th_f64.neg ]
  in
  let aop_f64_of_ls ls_op =
    if Term.ls_equal ls_op th_f64.sub
    then Float.( - )
    else if Term.ls_equal ls_op th_f64.add
    then Float.( + )
    else if Term.ls_equal ls_op th_f64.mul
    then Float.( * )
    else if Term.ls_equal ls_op th_f64.div
    then Float.( / )
    else assert false
  in
  let inv_aop_f64_of_ls ls_op =
    if Term.ls_equal ls_op th_f64.sub
    then Float.( + )
    else if Term.ls_equal ls_op th_f64.add
    then Float.( - )
    else if Term.ls_equal ls_op th_f64.mul
    then Float.( / )
    else if Term.ls_equal ls_op th_f64.div
    then Float.( * )
    else assert false
  in
  let rec rewriter t =
    let t = Term.t_map rewriter t in
    match t.t_node with
    | Tapp (ls, [ { t_node = Tconst (ConstReal rc); _ } ])
      when Term.ls_equal ls th_f64.neg ->
      (* [.- rc] => [-rc] *)
      let rc = Number.neg_real rc in
      Term.t_const (ConstReal rc) th_f64.ty
    | Tapp
        ( ls_op,
          [
            _ (* mode *);
            { t_node = Tconst (ConstReal rc1); _ };
            { t_node = Tconst (ConstReal rc2); _ };
          ] )
      when List.exists ~f:(Term.ls_equal ls_op) aop_f64 ->
      (* [rc1 ls_op rc2] => [res_rc], where [res_rc] is the actual arithmetic
         result of computing [ls_op] on [rc1] and [rc2]. *)
      let rc1_float = Utils.float_of_real_constant rc1 in
      let rc2_float = Utils.float_of_real_constant rc2 in
      let res_rc_float = (aop_f64_of_ls ls_op) rc1_float rc2_float in
      let res_rc = Utils.real_constant_of_float res_rc_float in
      Term.t_const res_rc th_f64.ty
    | Tapp
        ( ls_rel,
          [
            { t_node = Tconst (ConstReal rc1); _ };
            {
              t_node =
                Tapp
                  ( ls_op,
                    [ _ (* mode *); t'; { t_node = Tconst (ConstReal rc2); _ } ]
                  );
              _;
            };
          ] )
      when List.exists ~f:(Term.ls_equal ls_rel) relop_f64
           && List.exists ~f:(Term.ls_equal ls_op) aop_f64 ->
      (* [rc1 ls_rel t' ls_op rc2] => [(rc1 inv_ls_op rc2) ls_rel t'], where
         [inv_ls_op] is the inverse operation of the one provided by [ls_op]. *)
      let rc1_float = Utils.float_of_real_constant rc1 in
      let rc2_float = Utils.float_of_real_constant rc2 in
      let res_rc_float = (inv_aop_f64_of_ls ls_op) rc1_float rc2_float in
      let res_rc_t = Utils.term_of_float env res_rc_float in
      let t = Term.t_app_infer ls_rel [ res_rc_t; t' ] in
      rewriter t
    | Tapp
        ( ls_rel,
          [
            {
              t_node =
                Tapp
                  ( ls_op,
                    [ _ (* mode *); t'; { t_node = Tconst (ConstReal rc2); _ } ]
                  );
              _;
            };
            { t_node = Tconst (ConstReal rc1); _ };
          ] )
      when List.exists ~f:(Term.ls_equal ls_rel) relop_f64
           && List.exists ~f:(Term.ls_equal ls_op) aop_f64 ->
      (* [t' ls_op rc2 ls_rel rc1] => [t' ls_rel (rc1 inv_ls_op rc2)], where
         [inv_ls_op] is the inverse operation of the one provided by [ls_op]. *)
      let rc1_float = Utils.float_of_real_constant rc1 in
      let rc2_float = Utils.float_of_real_constant rc2 in
      let res_rc_float = (inv_aop_f64_of_ls ls_op) rc1_float rc2_float in
      let res_rc_t = Utils.term_of_float env res_rc_float in
      let t = Term.t_app_infer ls_rel [ t'; res_rc_t ] in
      rewriter t
    | _ -> t
  in
  (rewriter, None)

let simplify env =
  let rewriter, task = make_rewriter env in
  Trans.rewrite rewriter task

let register_simplify_relop () =
  Trans.register_env_transform
    ~desc:"Simplify relop expressions involving IEEE float64 values."
    "simplify_relop" simplify

(* -------------------------------------------------------------------------- *)
(* --- Variables on LHS                                                       *)
(* -------------------------------------------------------------------------- *)

let make_rewriter env =
  let th_64 = Symbols.Float64.create env in
  let th_real = Symbols.Real.create env in
  let rec rewriter t =
    let t = Term.t_map rewriter t in
    match t.t_node with
    | Tapp
        ( ls,
          [
            ({ t_node = Tconst _; _ } as const);
            ({ t_node = Tapp (_, []); _ } as var);
          ] ) ->
      let tt = [ var; const ] in
      let ls_rel =
        if Term.ls_equal ls th_64.le
        then th_64.ge
        else if Term.ls_equal ls th_64.ge
        then th_64.le
        else if Term.ls_equal ls th_64.lt
        then th_64.gt
        else if Term.ls_equal ls th_64.gt
        then th_64.lt
        else ls
      in
      let ls_rel =
        if Term.ls_equal ls th_real.le
        then th_real.ge
        else if Term.ls_equal ls th_real.ge
        then th_real.le
        else if Term.ls_equal ls th_real.lt
        then th_real.gt
        else if Term.ls_equal ls th_real.gt
        then th_real.lt
        else ls_rel
      in
      if Term.ls_equal ls_rel ls then t else Term.ps_app ls_rel tt
    | _ -> t
  in
  let task =
    List.fold
      [ th_64.le; th_64.lt; th_64.ge; th_64.gt ]
      ~init:(Task.add_ty_decl None th_64.tysymbol)
      ~f:Task.add_param_decl
  in
  let task =
    List.fold
      [ th_real.le; th_real.lt; th_real.ge; th_real.gt ]
      ~init:(Task.add_ty_decl task th_real.tysymbol)
      ~f:Task.add_param_decl
  in
  (rewriter, task)

let vars_on_lhs env =
  let rewriter, task = make_rewriter env in
  Trans.rewrite rewriter task

let register_vars_on_lhs_of_relop () =
  Trans.register_env_transform
    ~desc:
      "Move variables on the left-hand-side of relop symbols in expressions \
       involving IEEE float64 values."
    "vars_on_lhs_of_relop" vars_on_lhs
