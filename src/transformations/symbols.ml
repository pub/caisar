(**************************************************************************)
(*                                                                        *)
(*  This file is part of CAISAR.                                          *)
(*                                                                        *)
(*  Copyright (C) 2025                                                    *)
(*    CEA (Commissariat à l'énergie atomique et aux énergies              *)
(*         alternatives)                                                  *)
(*                                                                        *)
(*  You can redistribute it and/or modify it under the terms of the GNU   *)
(*  Lesser General Public License as published by the Free Software       *)
(*  Foundation, version 2.1.                                              *)
(*                                                                        *)
(*  It is distributed in the hope that it will be useful,                 *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of        *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          *)
(*  GNU Lesser General Public License for more details.                   *)
(*                                                                        *)
(*  See the GNU Lesser General Public License version 2.1                 *)
(*  for more details (enclosed in the file licenses/LGPLv2.1).            *)
(*                                                                        *)
(**************************************************************************)

(* -------------------------------------------------------------------------- *)
(* --- Why3 Theories                                                          *)
(* -------------------------------------------------------------------------- *)

module Float64 = struct
  type t = {
    ty : Why3.Ty.ty;
    tysymbol : Why3.Ty.tysymbol;
    add : Why3.Term.lsymbol; (* Float64.add *)
    sub : Why3.Term.lsymbol; (* Float64.sub *)
    mul : Why3.Term.lsymbol; (* Float64.mul *)
    div : Why3.Term.lsymbol; (* Float64.div *)
    neg : Why3.Term.lsymbol; (* Float64.neg *)
    le : Why3.Term.lsymbol; (* Float64.le *)
    ge : Why3.Term.lsymbol; (* Float64.ge *)
    lt : Why3.Term.lsymbol; (* Float64.lt *)
    gt : Why3.Term.lsymbol; (* Float64.gt *)
    infix : infix;
  }

  and infix = {
    plus : Why3.Term.lsymbol; (* Float64.( .+ ) *)
    minus : Why3.Term.lsymbol; (* Float64.( .- ) *)
    times : Why3.Term.lsymbol; (* Float64.( .* ) *)
    divide : Why3.Term.lsymbol; (* Float64.( ./ ) *)
    negation : Why3.Term.lsymbol; (* Float64.( .-_ ) *)
  }

  let create_t env =
    let th = Why3.Env.read_theory env [ "ieee_float" ] "Float64" in
    let tysymbol = Why3.Theory.ns_find_ts th.th_export [ "t" ] in
    let ty = Why3.Ty.ty_app tysymbol [] in
    let add = Why3.Theory.ns_find_ls th.th_export [ "add" ] in
    let sub = Why3.Theory.ns_find_ls th.th_export [ "sub" ] in
    let mul = Why3.Theory.ns_find_ls th.th_export [ "mul" ] in
    let div = Why3.Theory.ns_find_ls th.th_export [ "div" ] in
    let neg = Why3.Theory.ns_find_ls th.th_export [ "neg" ] in
    let le = Why3.Theory.ns_find_ls th.th_export [ "le" ] in
    let ge = Why3.Theory.ns_find_ls th.th_export [ "ge" ] in
    let lt = Why3.Theory.ns_find_ls th.th_export [ "lt" ] in
    let gt = Why3.Theory.ns_find_ls th.th_export [ "gt" ] in
    let infix =
      let plus =
        Why3.Theory.ns_find_ls th.th_export [ Why3.Ident.op_infix ".+" ]
      in
      let minus =
        Why3.Theory.ns_find_ls th.th_export [ Why3.Ident.op_infix ".-" ]
      in
      let times =
        Why3.Theory.ns_find_ls th.th_export [ Why3.Ident.op_infix ".*" ]
      in
      let divide =
        Why3.Theory.ns_find_ls th.th_export [ Why3.Ident.op_infix "./" ]
      in
      let negation =
        Why3.Theory.ns_find_ls th.th_export [ Why3.Ident.op_prefix ".-" ]
      in
      { plus; minus; times; divide; negation }
    in
    { ty; tysymbol; add; sub; mul; div; neg; le; ge; lt; gt; infix }

  let create = Why3.Env.Wenv.memoize 10 (fun env -> create_t env)
end

module Real = struct
  type t = {
    ty : Why3.Ty.ty;
    tysymbol : Why3.Ty.tysymbol;
    add : Why3.Term.lsymbol; (* Real.add *)
    sub : Why3.Term.lsymbol; (* Real.sub *)
    mul : Why3.Term.lsymbol; (* Real.mul *)
    div : Why3.Term.lsymbol; (* Real.div *)
    neg : Why3.Term.lsymbol; (* Real.neg *)
    le : Why3.Term.lsymbol; (* Real.le *)
    ge : Why3.Term.lsymbol; (* Real.ge *)
    lt : Why3.Term.lsymbol; (* Real.lt *)
    gt : Why3.Term.lsymbol; (* Real.gt *)
  }

  let create_t env =
    let th = Why3.Env.read_theory env [ "real" ] "Real" in
    let tysymbol = Why3.Ty.ts_real in
    let ty = Why3.Ty.ty_app tysymbol [] in
    let add = Why3.Theory.ns_find_ls th.th_export [ Why3.Ident.op_infix "+" ] in
    let sub = Why3.Theory.ns_find_ls th.th_export [ Why3.Ident.op_infix "-" ] in
    let mul = Why3.Theory.ns_find_ls th.th_export [ Why3.Ident.op_infix "*" ] in
    let div = Why3.Theory.ns_find_ls th.th_export [ Why3.Ident.op_infix "/" ] in
    let neg =
      Why3.Theory.ns_find_ls th.th_export [ Why3.Ident.op_prefix "-" ]
    in
    let le = Why3.Theory.ns_find_ls th.th_export [ Why3.Ident.op_infix "<=" ] in
    let ge = Why3.Theory.ns_find_ls th.th_export [ Why3.Ident.op_infix ">=" ] in
    let lt = Why3.Theory.ns_find_ls th.th_export [ Why3.Ident.op_infix "<" ] in
    let gt = Why3.Theory.ns_find_ls th.th_export [ Why3.Ident.op_infix ">" ] in
    { ty; tysymbol; add; sub; mul; div; neg; le; ge; lt; gt }

  let create = Why3.Env.Wenv.memoize 10 (fun env -> create_t env)
end

(* -------------------------------------------------------------------------- *)
(* --- CAISAR Theories                                                        *)
(* -------------------------------------------------------------------------- *)

module Model = struct
  type t = { atat : Why3.Term.lsymbol (* Model.( @@ ) *) }

  let create_t env =
    let th = Why3.Env.read_theory env [ "caisar"; "model" ] "Model" in
    let atat =
      Why3.Theory.ns_find_ls th.th_export [ Why3.Ident.op_infix "@@" ]
    in
    { atat }

  let create = Why3.Env.Wenv.memoize 10 (fun env -> create_t env)
end
