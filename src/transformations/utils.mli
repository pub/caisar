(**************************************************************************)
(*                                                                        *)
(*  This file is part of CAISAR.                                          *)
(*                                                                        *)
(*  Copyright (C) 2025                                                    *)
(*    CEA (Commissariat à l'énergie atomique et aux énergies              *)
(*         alternatives)                                                  *)
(*                                                                        *)
(*  You can redistribute it and/or modify it under the terms of the GNU   *)
(*  Lesser General Public License as published by the Free Software       *)
(*  Foundation, version 2.1.                                              *)
(*                                                                        *)
(*  It is distributed in the hope that it will be useful,                 *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of        *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          *)
(*  GNU Lesser General Public License for more details.                   *)
(*                                                                        *)
(*  See the GNU Lesser General Public License version 2.1                 *)
(*  for more details (enclosed in the file licenses/LGPLv2.1).            *)
(*                                                                        *)
(**************************************************************************)

(* -------------------------------------------------------------------------- *)
(** {2 Conversions from/to float} *)
(* -------------------------------------------------------------------------- *)

val float_of_real_constant : Why3.Number.real_constant -> float
val real_constant_of_float : float -> Why3.Constant.constant
val term_of_float : Why3.Env.env -> float -> Why3.Term.term

(* -------------------------------------------------------------------------- *)
(** {2 Information about input specification} *)
(* -------------------------------------------------------------------------- *)

(** Terms forming vectors in input to models. *)
type input_terms =
  | Vars of int Why3.Term.Mls.t
      (** A map from input variable lsymbols to corresponding positions in input
          vectors. *)
  | Others  (** Generic terms. *)
[@@deriving show]

type features = Float.t Why3.Term.Mls.t [@@deriving show]
(** Input feature values. *)

type perturbation = interval Why3.Term.Mls.t [@@deriving show]

and interval = float option * float option [@@deriving show]
(** Perturbation interval for each input variable: lower and uppper bounds. *)

type label = int [@@deriving show]
(** Output label. *)

val input_terms : input_terms Why3.Trans.trans

val input_features :
  Why3.Env.env -> vars:Why3.Term.lsymbol list -> features Why3.Trans.trans

val input_perturbations :
  Why3.Env.env -> vars:Why3.Term.lsymbol list -> perturbation Why3.Trans.trans

val output_label : Why3.Env.env -> label option Why3.Trans.trans
