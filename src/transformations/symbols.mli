(**************************************************************************)
(*                                                                        *)
(*  This file is part of CAISAR.                                          *)
(*                                                                        *)
(*  Copyright (C) 2025                                                    *)
(*    CEA (Commissariat à l'énergie atomique et aux énergies              *)
(*         alternatives)                                                  *)
(*                                                                        *)
(*  You can redistribute it and/or modify it under the terms of the GNU   *)
(*  Lesser General Public License as published by the Free Software       *)
(*  Foundation, version 2.1.                                              *)
(*                                                                        *)
(*  It is distributed in the hope that it will be useful,                 *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of        *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          *)
(*  GNU Lesser General Public License for more details.                   *)
(*                                                                        *)
(*  See the GNU Lesser General Public License version 2.1                 *)
(*  for more details (enclosed in the file licenses/LGPLv2.1).            *)
(*                                                                        *)
(**************************************************************************)

(* -------------------------------------------------------------------------- *)
(** {2 Why3 Theories} *)
(* -------------------------------------------------------------------------- *)

module Float64 : sig
  type t = private {
    ty : Why3.Ty.ty;
    tysymbol : Why3.Ty.tysymbol;
    add : Why3.Term.lsymbol;  (** Float64.add *)
    sub : Why3.Term.lsymbol;  (** Float64.sub *)
    mul : Why3.Term.lsymbol;  (** Float64.mul *)
    div : Why3.Term.lsymbol;  (** Float64.div *)
    neg : Why3.Term.lsymbol;  (** Float64.neg *)
    le : Why3.Term.lsymbol;  (** Float64.le *)
    ge : Why3.Term.lsymbol;  (** Float64.ge *)
    lt : Why3.Term.lsymbol;  (** Float64.lt *)
    gt : Why3.Term.lsymbol;  (** Float64.gt *)
    infix : infix;
  }

  and infix = private {
    plus : Why3.Term.lsymbol;  (** Float64.( .+ ) *)
    minus : Why3.Term.lsymbol;  (** Float64.( .- ) *)
    times : Why3.Term.lsymbol;  (** Float64.( .* ) *)
    divide : Why3.Term.lsymbol;  (** Float64.( ./ ) *)
    negation : Why3.Term.lsymbol;  (** Float64.( .-_ ) *)
  }

  val create : Why3.Env.env -> t
end

module Real : sig
  type t = private {
    ty : Why3.Ty.ty;
    tysymbol : Why3.Ty.tysymbol;
    add : Why3.Term.lsymbol;  (** Real.add *)
    sub : Why3.Term.lsymbol;  (** Real.sub *)
    mul : Why3.Term.lsymbol;  (** Real.mul *)
    div : Why3.Term.lsymbol;  (** Real.div *)
    neg : Why3.Term.lsymbol;  (** Real.neg *)
    le : Why3.Term.lsymbol;  (** Real.le *)
    ge : Why3.Term.lsymbol;  (** Real.ge *)
    lt : Why3.Term.lsymbol;  (** Real.lt *)
    gt : Why3.Term.lsymbol;  (** Real.gt *)
  }

  val create : Why3.Env.env -> t
end

(* -------------------------------------------------------------------------- *)
(** {2 CAISAR Theories} *)
(* -------------------------------------------------------------------------- *)

module Model : sig
  type t = private { atat : Why3.Term.lsymbol  (** NN.( \@\@ ) *) }

  val create : Why3.Env.env -> t
end
