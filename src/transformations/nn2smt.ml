(**************************************************************************)
(*                                                                        *)
(*  This file is part of CAISAR.                                          *)
(*                                                                        *)
(*  Copyright (C) 2025                                                    *)
(*    CEA (Commissariat à l'énergie atomique et aux énergies              *)
(*         alternatives)                                                  *)
(*                                                                        *)
(*  You can redistribute it and/or modify it under the terms of the GNU   *)
(*  Lesser General Public License as published by the Free Software       *)
(*  Foundation, version 2.1.                                              *)
(*                                                                        *)
(*  It is distributed in the hope that it will be useful,                 *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of        *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          *)
(*  GNU Lesser General Public License for more details.                   *)
(*                                                                        *)
(*  See the GNU Lesser General Public License version 2.1                 *)
(*  for more details (enclosed in the file licenses/LGPLv2.1).            *)
(*                                                                        *)
(**************************************************************************)

open Base
module IR = Nir.Ngraph

let src =
  Logs.Src.create "NN2SMT" ~doc:"Encoding of neural networks into SMT-LIB"

(* Term describing the sum of two variables v1 and v2. *)
let sum v1 v2 ~env =
  let th = Symbols.Float64.create env in
  Why3.Term.t_app_infer th.infix.plus [ v1; v2 ]

(* Term describing the sum of a sequence of terms. *)
let sum_seq ~env = Sequence.reduce_exn ~f:(fun x y -> sum x y ~env)

(* Term describing the division of two variables v1 and v2. *)
let div v1 v2 ~env =
  let th = Symbols.Float64.create env in
  Why3.Term.t_app_infer th.infix.divide [ v1; v2 ]

(* Term describing the multiplication of two variables v1 and v2. *)
let mul v1 v2 ~env =
  let th = Symbols.Float64.create env in
  Why3.Term.t_app_infer th.infix.times [ v1; v2 ]

(* Term describing the ReLU operation. *)
let relu ~env expr =
  let relu_s =
    let nn = Why3.Env.read_theory env [ "caisar"; "caisar" ] "NN" in
    Why3.Theory.ns_find_ls nn.th_export [ "relu" ]
  in
  Why3.Term.t_app_infer relu_s [ expr ]

let terms_of_nir m d node index ty_inputs env input_vars input_terms =
  (* Retrieves or creates the logic symbol associated with the [index]th
     component of the output tensor of [node]. This method ensures that all the
     components that this one depends on are created recursively. The logic
     symbols are stored in map [m] and the declarations are saved in [d]. *)
  let rec terms_of_nir_rec node index =
    let mi =
      Option.value ~default:(Map.empty (module Int)) @@ Map.find !m node
    in
    match Map.find mi index with
    | Some ls -> ls
    | None ->
      let ls, decl = terms_of_nir_create node index in
      Queue.enqueue d (Why3.Theory.create_decl decl);
      Queue.enqueue d
        (Why3.Theory.create_meta Why3.Inlining.meta [ Why3.Theory.MAls ls ]);
      let mi = Map.add_exn mi ~key:index ~data:ls in
      m := Map.set !m ~key:node ~data:mi;
      ls
  and t_app_of_nir node index =
    let ls = terms_of_nir_rec node index in
    Why3.Term.fs_app ls
      (if List.is_empty input_vars then [] else input_terms)
      ty_inputs
  and terms_of_nir_create (node : Nir.Node.t) index =
    (* Creates the logic symbol associated with the [index]th component of the
       output tensor of [node], and returns the declaration that defines this
       symbol. *)
    let preid = Why3.Ident.id_fresh (Fmt.str "n%i_%i" node.id index) in
    let ls =
      Why3.Term.create_fsymbol preid
        (List.map input_vars ~f:(fun _ -> ty_inputs))
        ty_inputs
    in
    let v_term =
      (* TODO: axiomatize the resulting term using [let d =
         Decl.create_prop_decl Paxiom ps t] *)
      match node.descr with
      | Constant { data = Float ba } ->
        let id = Nir.Shape.unrow_major node.shape index in
        Utils.term_of_float env (Nir.Tensor.get ba id)
      | Matmul { input1; input2 } ->
        (* [|... ; _; n |], [| ...; n; _ |] *)
        let id = Nir.Shape.unrow_major node.shape index in
        let broadcast shape id_dst =
          let len = Nir.Shape.rank shape in
          let id_src = Array.create ~len 0 in
          for i = 0 to len - 3 do
            id_src.(i) <- id_dst.(i)
          done;
          id_src
        in
        let id1 = broadcast input1.shape id in
        id1.(Array.length id1 - 2) <- id.(Array.length id - 2);
        let id2 = broadcast input2.shape id in
        id2.(Array.length id2 - 1) <- id.(Array.length id - 1);
        let acc =
          Sequence.init
            (Nir.Shape.get input1.shape (Nir.Shape.rank input1.shape - 1))
            ~f:(fun i ->
              (* id1 = [...; _; i ] id2 = [...; i; _ ] *)
              id1.(Array.length id1 - 1) <- i;
              id2.(Array.length id2 - 2) <- i;
              let i1 = Nir.Shape.row_major input1.shape id1 in
              let i2 = Nir.Shape.row_major input2.shape id2 in
              let a1 = t_app_of_nir input1 i1 in
              let a2 = t_app_of_nir input2 i2 in
              mul a1 a2 ~env)
        in
        let t = sum_seq ~env acc in
        t
      | Input _ -> List.nth_exn input_terms index
      | Add { input1; input2 } | Mul { input1; input2 } | Div { input1; input2 }
        -> (
        (* Binary operators *)
        let t1 = t_app_of_nir input1 index in
        let t2 = t_app_of_nir input2 index in
        match node.descr with
        | Add _ -> sum t1 t2 ~env
        | Mul _ -> mul t1 t2 ~env
        | Div _ -> div t1 t2 ~env
        | _ -> assert false)
      | ReLu { input } ->
        let t = t_app_of_nir input index in
        relu ~env t
      | Transpose { input = input'; perm = perm' } ->
        (* assumes length perm is the rank of input *)
        let id = Nir.Shape.unrow_major node.shape index in
        let id' = Nir.Node.untranspose perm' id in
        let index' = Nir.Shape.row_major input'.shape id' in
        t_app_of_nir input' index'
      | Flatten { input = input'; axis } ->
        let id = Nir.Shape.unrow_major node.shape index in
        let id' = Nir.Node.unflatten input'.shape axis id in
        let index' = Nir.Shape.row_major input'.shape id' in
        t_app_of_nir input' index'
      | Reshape { input = input'; _ } ->
        (* Note: the ONNX documentation is not very informative, but it seems
           clear that the interpretation is that the component at position
           [index] should also be at position [index] in the reshaped tensor,
           when [index] is computed using [Shape.row_major]. *)
        t_app_of_nir input' index
      | Gemm { inputA; inputB; inputC; alpha; beta; transA; transB } -> (
        (* alpha Sum_{k in 0...k_dim} (A(i,k) * B(k,j)) + beta C(i,j) *)
        let id = Nir.Shape.unrow_major node.shape index in
        let i, j = (id.(0), id.(1)) in
        let k_dim =
          (Nir.Shape.to_array inputA.shape).(if transA = 1 then 0 else 1)
        in
        let broadcast x trans (input : Nir.Node.t) =
          (* Returns the sequence of logic symbols A(i,0), A(i,1), ...,
             A(i,k_dim-1) except that: - A is a parameter [input] (in practice,
             either [inputA] or [inputB]) - i is a parameter [x] (it is [i] for
             [inputA] and [j] for [inputB]) - (i,k) is replaced with (k,i) if
             [trans] is true. *)
          Sequence.range ~start:`inclusive ~stop:`exclusive 0
            k_dim (* seq of k *)
          |> Sequence.map
               ~f:
                 (if (* seq of [|k;x|] or [|x;k|] *)
                     trans
                  then fun k -> [| x; k |]
                  else fun k -> [| k; x |])
          |> Sequence.map ~f:(fun id ->
               let index = Nir.Shape.row_major input.shape id in
               t_app_of_nir input index)
          (* seq of A(k,x) or A(x,k) *)
        in
        let seq_of_a_ki = broadcast i (not (transA = 1)) inputA in
        let seq_of_b_ki = broadcast j (transB = 1) inputB in
        let alpha = Utils.term_of_float env alpha in
        let interm_result =
          Sequence.zip seq_of_a_ki seq_of_b_ki
          |> Sequence.map ~f:(fun (t1, t2) -> mul t1 t2 ~env)
          |> sum_seq ~env |> mul alpha ~env
        in
        match inputC with
        | None -> interm_result
        | Some inputC ->
          let beta = Utils.term_of_float env beta in
          let cid =
            if 1 = Nir.Shape.rank inputC.shape
            then
              (* sometimes, a (1,N,) tensor is replaced with a (N,) tensor *)
              if i = 1 then [| j |] else [| i |]
            else [| i; j |]
          in
          let cindex = Nir.Shape.row_major inputC.shape cid in
          let c = t_app_of_nir inputC cindex in
          sum interm_result (mul beta c ~env) ~env)
      | _ ->
        Logging.not_implemented_yet (fun f ->
          f "ONNX operator %a is not implemented for nn2smt transformation"
            Nir.Node.pp node)
    in
    (ls, Why3.Decl.(create_logic_decl [ make_ls_defn ls input_vars v_term ]))
  in
  terms_of_nir_rec node index

module MTermL = Why3.Extmap.Make (struct
  module T = struct
    type t = Why3.Term.term

    let compare = Why3.Term.t_compare
  end

  type t = T.t list [@@deriving ord]
end)

let app_terms_of_nir_output m d (nn : Language.nn) env index tl =
  match nn.nn_format with
  | ONNX None | NNet None ->
    Logging.code_error ~src (fun f -> f "No ONNX to convert")
  | ONNX (Some g) | NNet (Some g) ->
    let vtl = List.fold tl ~init:Why3.Term.Mvs.empty ~f:Why3.Term.t_freevars in
    let m' = ref (MTermL.find_def (Map.empty (module Nir.Node)) tl !m) in
    let t =
      if Why3.Term.Mvs.is_empty vtl
      then
        (* use global constant *)
        let ls = terms_of_nir m' d (IR.output g) index nn.nn_ty_elt env [] tl in
        Why3.Term.fs_app ls [] nn.nn_ty_elt
      else
        (* use global functions *)
        let input_vars =
          List.init
            ~f:(fun i ->
              let preid = Why3.Ident.id_fresh (Fmt.str "i%i" i) in
              let vsymbol = Why3.Term.create_vsymbol preid nn.nn_ty_elt in
              vsymbol)
            (List.length tl)
        in
        let input_terms = List.map input_vars ~f:Why3.Term.t_var in
        let ls =
          terms_of_nir m' d (IR.output g) index nn.nn_ty_elt env input_vars
            input_terms
        in
        Why3.Term.fs_app ls tl nn.nn_ty_elt
    in
    m := MTermL.add tl !m' !m;
    t

(* Create logic symbols for input variables and replace model applications by
   control flow terms. *)
let actual_nn_flow env =
  let rec substitute_nn_ls m d term =
    match term.Why3.Term.t_node with
    | Tapp
        ( ls_get (* [ ] *),
          [
            {
              t_node =
                Why3.Term.Tapp
                  ( ls_atat (* @@ *),
                    [
                      { t_node = Tapp (ls_nn (* nn *), _); _ };
                      {
                        t_node =
                          Tapp (ls (* input vector *), tl (* input vars *));
                        _;
                      };
                    ] );
              _;
            };
            { t_node = Tconst (ConstInt index); _ };
          ] )
      when String.equal ls_get.ls_name.id_string (Why3.Ident.op_get "")
           && String.equal ls_atat.ls_name.id_string (Why3.Ident.op_infix "@@")
      -> (
      match (Language.lookup_nn ls_nn, Language.lookup_vector ls) with
      | Some ({ nn_nb_inputs; _ } as nn), Some vector_length ->
        assert (nn_nb_inputs = vector_length && vector_length = List.length tl);
        app_terms_of_nir_output m d nn env
          (Why3.Number.to_small_integer index)
          tl
      | _, _ ->
        Logging.code_error ~src (fun m ->
          m "Neural network application without fixed NN or arguments: %a"
            Why3.Pretty.print_term term))
    | _ -> Why3.Term.t_map (substitute_nn_ls m d) term
  in

  let m = MTermL.empty in
  let task_initial =
    let nn = Why3.Env.read_theory env [ "caisar"; "caisar" ] "NN" in
    Why3.Task.use_export None nn
  in
  Why3.Trans.fold_map
    (fun task_hd (m, task) ->
      match task_hd.task_decl.td_node with
      | Use _ | Clone _ | Meta _ ->
        (m, Why3.Task.add_tdecl task task_hd.task_decl)
      | Decl decl ->
        let m = ref m in
        let d = Queue.create () in
        let decl =
          Why3.Decl.decl_map
            (fun term ->
              let term = substitute_nn_ls m d term in
              term)
            decl
        in
        let task = Queue.fold d ~init:task ~f:Why3.Task.add_tdecl in
        (!m, Why3.Task.add_decl task decl))
    m task_initial

let trans env = Why3.Trans.seq [ actual_nn_flow env ]
