(**************************************************************************)
(*                                                                        *)
(*  This file is part of CAISAR.                                          *)
(*                                                                        *)
(*  Copyright (C) 2025                                                    *)
(*    CEA (Commissariat à l'énergie atomique et aux énergies              *)
(*         alternatives)                                                  *)
(*                                                                        *)
(*  You can redistribute it and/or modify it under the terms of the GNU   *)
(*  Lesser General Public License as published by the Free Software       *)
(*  Foundation, version 2.1.                                              *)
(*                                                                        *)
(*  It is distributed in the hope that it will be useful,                 *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of        *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          *)
(*  GNU Lesser General Public License for more details.                   *)
(*                                                                        *)
(*  See the GNU Lesser General Public License version 2.1                 *)
(*  for more details (enclosed in the file licenses/LGPLv2.1).            *)
(*                                                                        *)
(**************************************************************************)

open Why3
open Base

let filter_binaries =
  let d = lazy (Sys.getenv "CAISAR_AUTODETECT_BIN") in
  fun path ->
    match Lazy.force d with
    | Some d -> String.equal (Stdlib.Filename.dirname path) d
    | None -> true

let autodetection () =
  let debug = Logging.(is_debug_level src_autodetect) in
  if debug then Debug.set_flag Autodetection.debug;
  let caisar_conf =
    Misc.lookup_file Dirs.Sites.config "caisar-detection-data.conf"
  in
  let data = Autodetection.Prover_autodetection_data.from_file caisar_conf in
  let provers = Autodetection.find_provers data in
  let provers =
    List.filter_map provers ~f:(fun (path, name, version) ->
      Option.some_if (filter_binaries path)
        {
          Autodetection.Partial.name;
          path;
          version;
          shortcut = None;
          manual = false;
        })
  in
  let config = Whyconf.init_config (Some Stdlib.Filename.null) in
  let provers = Autodetection.compute_builtin_prover provers config data in
  Whyconf.set_provers config provers
