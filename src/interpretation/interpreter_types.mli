(**************************************************************************)
(*                                                                        *)
(*  This file is part of CAISAR.                                          *)
(*                                                                        *)
(*  Copyright (C) 2025                                                    *)
(*    CEA (Commissariat à l'énergie atomique et aux énergies              *)
(*         alternatives)                                                  *)
(*                                                                        *)
(*  You can redistribute it and/or modify it under the terms of the GNU   *)
(*  Lesser General Public License as published by the Free Software       *)
(*  Foundation, version 2.1.                                              *)
(*                                                                        *)
(*  It is distributed in the hope that it will be useful,                 *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of        *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          *)
(*  GNU Lesser General Public License for more details.                   *)
(*                                                                        *)
(*  See the GNU Lesser General Public License version 2.1                 *)
(*  for more details (enclosed in the file licenses/LGPLv2.1).            *)
(*                                                                        *)
(**************************************************************************)

module IRE = Interpreter_reduction_engine

type nn_format =
  | NNet
  | ONNX
[@@deriving show]

type model =
  | NN of Why3.Term.lsymbol * nn_format
  | SVM of Why3.Term.lsymbol
[@@deriving show]

type dataset = DS_csv of Csv.t [@@deriving show]
type data = D_csv of string list [@@deriving show]

type interpreter_op =
  | Model of model
  | Dataset of Why3.Term.lsymbol * dataset
  | Data of data
  | Vector of Why3.Term.lsymbol
[@@deriving show]

type interpreter_op_hls = interpreter_op Why3.Term.Hls.t [@@deriving show]
type ls_htbl_interpreter_op = (interpreter_op, Why3.Term.lsymbol) Base.Hashtbl.t

type interpreter_env = private {
  op_of_ls : interpreter_op_hls;
  ls_of_op : ls_htbl_interpreter_op;
  env : Why3.Env.env;
  cwd : string;
}

val op_of_ls : interpreter_env IRE.engine -> Why3.Term.lsymbol -> interpreter_op

val op_of_term :
  interpreter_env IRE.engine ->
  Why3.Term.term ->
  (interpreter_op * Why3.Term.term list) option

val term_of_op :
  ?args:(Why3.Term.term * Why3.Ty.ty) list ->
  interpreter_env IRE.engine ->
  interpreter_op ->
  Why3.Ty.ty option ->
  Why3.Term.term

val interpreter_env : cwd:string -> Why3.Env.env -> interpreter_env
