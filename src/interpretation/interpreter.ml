(**************************************************************************)
(*                                                                        *)
(*  This file is part of CAISAR.                                          *)
(*                                                                        *)
(*  Copyright (C) 2025                                                    *)
(*    CEA (Commissariat à l'énergie atomique et aux énergies              *)
(*         alternatives)                                                  *)
(*                                                                        *)
(*  You can redistribute it and/or modify it under the terms of the GNU   *)
(*  Lesser General Public License as published by the Free Software       *)
(*  Foundation, version 2.1.                                              *)
(*                                                                        *)
(*  It is distributed in the hope that it will be useful,                 *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of        *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          *)
(*  GNU Lesser General Public License for more details.                   *)
(*                                                                        *)
(*  See the GNU Lesser General Public License version 2.1                 *)
(*  for more details (enclosed in the file licenses/LGPLv2.1).            *)
(*                                                                        *)
(**************************************************************************)

module IRE = Interpreter_reduction_engine
module ITypes = Interpreter_types
open Base

let builtin_of_constant env known_map (name, value) =
  let decls =
    Why3.Ident.Mid.fold_left
      (fun acc id ls ->
        if String.equal id.Why3.Ident.id_string name then ls :: acc else acc)
      [] known_map
  in
  match decls with
  | [] ->
    Logging.user_error (fun m ->
      m "'%s' is not a declared toplevel constant" name)
  | _ :: _ :: _ ->
    Logging.user_error (fun m ->
      m "'%s' corresponds to multiple declared toplevel constants" name)
  | [ { Why3.Decl.d_node = Dparam ls; _ } ] ->
    let cst =
      match ls.Why3.Term.ls_value with
      | None -> (
        match value with
        | "true" -> Why3.Term.t_true
        | "false" -> Why3.Term.t_false
        | _ ->
          Logging.user_error (fun m ->
            m "'%s' expects 'true' or 'false', got '%s' instead" name value))
      | Some ty when Why3.Ty.ty_equal ty Why3.Ty.ty_bool -> (
        match value with
        | "true" -> Why3.Term.t_bool_true
        | "false" -> Why3.Term.t_bool_false
        | _ ->
          Logging.user_error (fun m ->
            m "'%s' expects 'true' or 'false', got '%s' instead" name value))
      | Some ty
        when Why3.Ty.ty_equal ty Why3.Ty.ty_int
             || Why3.Ty.ty_equal ty Why3.Ty.ty_real
             || Why3.Ty.ty_equal ty (Symbols.Float64.create env).ty ->
        let lb = Lexing.from_string value in
        Why3.Loc.set_file
          (Fmt.str "constant '%s' to be bound to '%s'" value name)
          lb;
        let parsed = Why3.Lexer.parse_term lb in
        let cst =
          match parsed.term_desc with
          | Why3.Ptree.Tconst cst -> cst
          | _ ->
            Logging.user_error (fun m ->
              m "'%s' expects a numerical constant, got '%s' instead" name value)
        in
        Why3.Term.t_const cst ty
      | Some ty when Why3.Ty.ty_equal ty Why3.Ty.ty_str ->
        let cst = Why3.Constant.ConstStr value in
        Why3.Term.t_const cst ty
      | Some ty ->
        Logging.not_implemented_yet (fun m ->
          m
            "'%s' has type '%a' but only toplevel constants of type bool, int, \
             real and string can be defined"
            name Why3.Pretty.print_ty ty)
    in
    (ls, fun _ _ _ _ -> IRE.eval_term cst)
  | _ ->
    Logging.user_error (fun m ->
      m "'%s' does not appear to be a declared toplevel constant" name)

type bounded_quant_result = {
  new_quant : Why3.Term.vsymbol list;
  substitutions : Why3.Term.term Why3.Term.Mvs.t;
  remaining_implication : Why3.Term.term;
}

let rec bounded_quant_aux engine vs ~cond : bounded_quant_result option =
  match vs.Why3.Term.vs_ty with
  | {
   ty_node = Tyapp ({ ts_name = { id_string = "vector"; _ }; _ }, ty :: _);
   _;
  } -> (
    match cond.Why3.Term.t_node with
    | Tapp
        ( { ls_name = { id_string = "has_length"; _ }; _ },
          [
            ({ t_node = Tvar vs1; _ } as _t1);
            ({ t_node = Tconst (ConstInt n); _ } as _t2);
          ] ) ->
      if not (Why3.Term.vs_equal vs vs1)
      then None
      else
        let n = Why3.Number.to_small_integer n in
        let new_quant =
          List.init n ~f:(fun _ ->
            let preid = Why3.Ident.id_fresh "x" in
            Why3.Term.create_vsymbol preid ty)
        in
        let args = List.map new_quant ~f:(fun vs -> (Why3.Term.t_var vs, ty)) in
        let op =
          let { ITypes.env; _ } = IRE.user_env engine in
          ITypes.Vector (Language.create_vector env n)
        in
        let substitutions =
          Why3.Term.Mvs.singleton vs
            (ITypes.term_of_op ~args engine op (Some vs.vs_ty))
        in

        Some
          { new_quant; substitutions; remaining_implication = Why3.Term.t_true }
    | Tapp ({ ls_name = { id_string = "has_length"; _ }; _ }, _) -> None
    | Tbinop (Tand, a, b) -> (
      match bounded_quant_aux engine vs ~cond:a with
      | None -> (
        match bounded_quant_aux engine vs ~cond:b with
        | None -> None
        | Some { new_quant; substitutions; remaining_implication } ->
          Some
            {
              new_quant;
              substitutions;
              remaining_implication =
                Why3.Term.t_and_simp a remaining_implication;
            })
      | Some { new_quant; substitutions; remaining_implication } ->
        Some
          {
            new_quant;
            substitutions;
            remaining_implication = Why3.Term.t_and_simp remaining_implication b;
          })
    | _ ->
      Logging.user_error ?loc:vs.vs_name.id_loc (fun m ->
        m
          "Expecting 'has_length' predicate after universal quantifier on \
           vector '%a'"
          Why3.Pretty.print_vs vs))
  | _ -> None

(* Only one vector for now *)
let rec bounded_quant engine vsl ~cond : bounded_quant_result option =
  match vsl with
  | ({
       Why3.Term.vs_ty =
         {
           ty_node = Tyapp ({ ts_name = { id_string = "vector"; _ }; _ }, _);
           _;
         };
       _;
     } as vs)
    :: l -> (
    match bounded_quant_aux engine vs ~cond with
    | None -> bounded_quant engine l ~cond
    | Some r1 -> (
      match bounded_quant engine l ~cond:r1.remaining_implication with
      | None -> Some { r1 with new_quant = r1.new_quant @ l }
      | Some r2 ->
        Some
          {
            new_quant = r1.new_quant @ r2.new_quant;
            substitutions =
              Why3.Term.Mvs.set_union r1.substitutions r2.substitutions;
            remaining_implication = r2.remaining_implication;
          }))
  | v :: l -> (
    match bounded_quant engine l ~cond with
    | None -> None
    | Some r -> Some { r with new_quant = v :: r.new_quant })
  | [] -> None

let bounded_quant engine vsl ~cond : IRE.bounded_quant_result option =
  Option.map (bounded_quant engine vsl ~cond) ~f:(fun r ->
    {
      IRE.new_quant = r.new_quant;
      substitutions = [ r.substitutions ];
      remaining_implication = r.remaining_implication;
    })

let declare_language_lsymbols interpreter_env task =
  (* Declare [Language] logic symbols only. *)
  Why3.Term.Hls.fold
    (fun ls _ task ->
      (* Add meta corresponding to logic symbol. *)
      let task = Language.add_meta_nn task ls in
      let task = Language.add_meta_svm task ls in
      let task = Language.add_meta_dataset_csv task ls in
      (* Add actual logic symbol declaration. *)
      let decl = Why3.Decl.create_param_decl ls in
      Why3.Task.add_decl task decl)
    interpreter_env.ITypes.op_of_ls task

let interpret_task ~cwd ?(definitions = []) env task =
  let known_map = Why3.Task.task_known task in
  let interpreter_env = ITypes.interpreter_env ~cwd env in
  let params =
    {
      IRE.compute_defs = true;
      compute_builtin = true;
      compute_def_set = Why3.Term.Sls.empty;
      compute_max_quantifier_domain = Int.max_value;
    }
  in
  let builtins = List.map ~f:(builtin_of_constant env known_map) definitions in
  let engine =
    IRE.create ~bounded_quant ~builtins params env known_map interpreter_env
      Interpreter_theory.builtins
  in
  let g, f = (Why3.Task.task_goal task, Why3.Task.task_goal_fmla task) in
  let f = IRE.normalize ~limit:Int.max_value engine Why3.Term.Mvs.empty f in
  Logs.debug ~src:Logging.src_interpret_goal (fun m ->
    m "Interpreted formula for goal '%a':@.%a@.%a" Why3.Pretty.print_pr g
      Why3.Pretty.print_term f ITypes.pp_interpreter_op_hls
      interpreter_env.op_of_ls);
  let _, task = Why3.Task.task_separate_goal task in
  let task = declare_language_lsymbols interpreter_env task in
  let task = Why3.Task.(add_prop_decl task Pgoal g f) in
  task
