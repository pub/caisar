(**************************************************************************)
(*                                                                        *)
(*  This file is part of CAISAR.                                          *)
(*                                                                        *)
(*  Copyright (C) 2025                                                    *)
(*    CEA (Commissariat à l'énergie atomique et aux énergies              *)
(*         alternatives)                                                  *)
(*                                                                        *)
(*  You can redistribute it and/or modify it under the terms of the GNU   *)
(*  Lesser General Public License as published by the Free Software       *)
(*  Foundation, version 2.1.                                              *)
(*                                                                        *)
(*  It is distributed in the hope that it will be useful,                 *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of        *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          *)
(*  GNU Lesser General Public License for more details.                   *)
(*                                                                        *)
(*  See the GNU Lesser General Public License version 2.1                 *)
(*  for more details (enclosed in the file licenses/LGPLv2.1).            *)
(*                                                                        *)
(**************************************************************************)

module IRE = Interpreter_reduction_engine
module ITypes = Interpreter_types
open Base

let fail_on_unexpected_argument ls =
  Logging.code_error ~src:Logging.src_interpret_goal (fun m ->
    m "Unexpected argument(s) for '%a'" Why3.Pretty.print_ls ls)

(* -------------------------------------------------------------------------- *)
(* --- Vector Builtins                                                        *)
(* -------------------------------------------------------------------------- *)

module Vector = struct
  let (get : _ IRE.builtin) =
   fun engine ls vl ty ->
    let th_model = Symbols.Model.create (IRE.user_env engine).ITypes.env in
    match vl with
    | [
     Term
       ({
          t_node = Tapp (ls_atat (* @@ *), [ { t_node = Tapp (ls, _); _ }; _ ]);
          _;
        } as _t1);
     Term ({ t_node = Tconst (ConstInt i); _ } as t2);
    ]
      when Why3.Term.ls_equal ls_atat th_model.atat -> (
      let i = Why3.Number.to_small_integer i in
      if i < 0
      then
        Logging.user_error ?loc:t2.t_loc (fun m ->
          m "Index constant %d is negative" i);
      match ITypes.op_of_ls engine ls with
      | Model (NN (nn, _)) ->
        let nn =
          match Language.lookup_nn nn with
          | None ->
            Logging.code_error ~src:Logging.src_interpret_goal (fun m ->
              m "Cannot find neural network model from lsymbol %a"
                Why3.Pretty.print_ls nn)
          | Some nn -> nn
        in
        if nn.nn_nb_outputs < i
        then
          Logging.user_error ?loc:t2.t_loc (fun m ->
            m "Index constant %d is out-of-bounds [0,%d]" i
              (nn.nn_nb_outputs - 1))
        else IRE.reconstruct_term ()
      | Model (SVM svm) ->
        let svm =
          match Language.lookup_svm svm with
          | None ->
            Logging.code_error ~src:Logging.src_interpret_goal (fun m ->
              m "Cannot find SVM model from lsymbol %a" Why3.Pretty.print_ls svm)
          | Some svm -> svm
        in
        if svm.svm_nb_outputs < i
        then
          Logging.user_error ?loc:t2.t_loc (fun m ->
            m "Index constant %d is out-of-bounds [0,%d]" i
              (svm.svm_nb_outputs - 1))
        else IRE.reconstruct_term ()
      | interpreter_op ->
        Logging.code_error ~src:Logging.src_interpret_goal (fun m ->
          m "Unexpected interpreter operation %a" ITypes.pp_interpreter_op
            interpreter_op))
    | [ Term t1; Term ({ t_node = Tconst (ConstInt i); _ } as t2) ] -> (
      let i = Why3.Number.to_small_integer i in
      if i < 0
      then
        Logging.user_error ?loc:t2.t_loc (fun m ->
          m "Index constant %d is negative" i);
      match ITypes.op_of_term engine t1 with
      | Some (Dataset (_, DS_csv csv), _) ->
        let row = List.nth_exn csv i in
        let label, features =
          match row with
          | [] | [ _ ] -> assert false
          | label :: features -> (label, features)
        in
        let ty_features =
          match ty with
          | Some { ty_node = Tyapp (_, [ _; ty ]); _ } -> Some ty
          | _ -> assert false
        in
        let t_features, t_label =
          ( ITypes.term_of_op engine (Data (D_csv features)) ty_features,
            Why3.(Term.t_int_const (BigInt.of_int (Int.of_string label))) )
        in
        IRE.value_term (Why3.Term.t_tuple [ t_label; t_features ])
      | Some (Vector v, tl1) ->
        let n = Option.value_exn (Language.lookup_vector v) in
        if List.length tl1 <> n
        then
          Logging.code_error ~src:Logging.src_interpret_goal (fun m ->
            m
              "Mismatch between (container) vector length and number of \
               (contained) input variables.");
        if i < n
        then IRE.value_term (List.nth_exn tl1 i)
        else
          Logging.user_error ?loc:t1.t_loc (fun m ->
            m "Index constant %d is out-of-bounds [0,%d]" i (n - 1))
      | Some (interpreter_op, _) ->
        Logging.code_error ~src:Logging.src_interpret_goal (fun m ->
          m "Unexpected interpreter operation %a" ITypes.pp_interpreter_op
            interpreter_op)
      | None -> IRE.reconstruct_term ())
    | [ Term _t1; Term _t2 ] -> IRE.reconstruct_term ()
    | _ ->
      Logging.code_error ~src:Logging.src_interpret_goal (fun m ->
        m "Unexpected argument(s) for '%a': %a" Why3.Pretty.print_ls ls
          (Fmt.list ~sep:Fmt.comma IRE.pp_value)
          vl)

  let length : _ IRE.builtin =
   fun engine ls vl _ty ->
    match vl with
    | [ Term { t_node = Tapp (ls, []); _ } ] -> (
      match ITypes.op_of_ls engine ls with
      | Dataset (_, DS_csv csv) ->
        IRE.value_int (Why3.BigInt.of_int (Csv.lines csv))
      | Data (D_csv data) ->
        IRE.value_int (Why3.BigInt.of_int (List.length data))
      | Vector _ | Model _ -> assert false)
    | [ Term { t_node = Tapp (ls, tl); _ } ] -> (
      match ITypes.op_of_ls engine ls with
      | Vector v ->
        let n = Option.value_exn (Language.lookup_vector v) in
        assert (List.length tl = n);
        IRE.value_int (Why3.BigInt.of_int n)
      | Dataset _ | Data _ | Model _ -> assert false)
    | [ Term _t ] -> IRE.reconstruct_term ()
    | _ -> fail_on_unexpected_argument ls

  let sub : _ IRE.builtin =
   fun engine ls vl ty ->
    match vl with
    | [
     Term ({ t_node = Tapp (ls1, tl1); _ } as t1);
     Term ({ t_node = Tapp (ls2, _); _ } as _t2);
    ] -> (
      match ITypes.(op_of_ls engine ls1, op_of_ls engine ls2) with
      | Vector v, Data (D_csv data) ->
        let n = Option.value_exn (Language.lookup_vector v) in
        assert (n = List.length data);
        let ty_cst =
          match ty with
          | Some { ty_node = Tyapp (_, [ ty ]); _ } -> ty
          | _ -> assert false
        in
        let csts =
          List.map data ~f:(fun d ->
            let cst = Utils.real_constant_of_float (Float.of_string d) in
            Why3.Term.t_const cst ty_cst)
        in
        let { ITypes.env; _ } = IRE.user_env engine in
        let args =
          let minus =
            (* TODO: generalize wrt the type of constants [csts]. *)
            let th = Symbols.Float64.create env in
            th.infix.minus
          in
          List.map2_exn tl1 csts ~f:(fun tl c ->
            (Why3.Term.t_app_infer minus [ tl; c ], ty_cst))
        in
        let op = ITypes.Vector (Language.create_vector env n) in
        IRE.value_term (ITypes.term_of_op ~args engine op ty)
      | Vector _, Vector _ ->
        Logging.user_error ?loc:t1.t_loc (fun m ->
          m "Invalid substraction operation involving two abstract vectors")
      | _ ->
        Logging.user_error ?loc:t1.t_loc (fun m ->
          m "Invalid subtraction operation"))
    | [ Term _t1; Term _t2 ] -> IRE.reconstruct_term ()
    | _ -> fail_on_unexpected_argument ls

  let add : _ IRE.builtin =
   fun engine ls vl ty ->
    match vl with
    | [
     Term ({ t_node = Tapp (ls1, tl1); _ } as t1);
     Term ({ t_node = Tapp (ls2, _); _ } as _t2);
    ] -> (
      match ITypes.(op_of_ls engine ls1, op_of_ls engine ls2) with
      | Data (D_csv data), Vector v | Vector v, Data (D_csv data) ->
        let n = Option.value_exn (Language.lookup_vector v) in
        assert (n = List.length data);
        let ty_cst =
          match ty with
          | Some { ty_node = Tyapp (_, [ ty ]); _ } -> ty
          | _ -> assert false
        in
        let csts =
          List.map data ~f:(fun d ->
            let cst = Utils.real_constant_of_float (Float.of_string d) in
            Why3.Term.t_const cst ty_cst)
        in
        let { ITypes.env; _ } = IRE.user_env engine in
        let args =
          let plus =
            (* TODO: generalize wrt the type of constants [csts]. *)
            let th = Symbols.Float64.create env in
            th.infix.plus
          in
          List.map2_exn tl1 csts ~f:(fun tl c ->
            (Why3.Term.t_app_infer plus [ tl; c ], ty_cst))
        in
        let op = ITypes.Vector (Language.create_vector env n) in
        IRE.value_term (ITypes.term_of_op ~args engine op ty)
      | Vector _, Vector _ ->
        Logging.user_error ?loc:t1.t_loc (fun m ->
          m "%a: Invalid addition operation involving two abstract vectors"
            (Fmt.option Why3.Loc.pp_position)
            t1.t_loc)
      | _ ->
        Logging.user_error ?loc:t1.t_loc (fun m ->
          m "Invalid addition operation involving two non-vectors"))
    | [ Term _t1; Term _t2 ] -> IRE.reconstruct_term ()
    | _ -> fail_on_unexpected_argument ls

  let mapi : _ IRE.builtin =
   fun engine ls vl ty ->
    match vl with
    | [
     Term ({ t_node = Tapp (ls1, tl1); _ } as _t1);
     Term ({ t_node = Teps _tb; _ } as t2);
    ] -> (
      assert (Why3.Term.t_is_lambda t2);
      match ITypes.op_of_ls engine ls1 with
      | Vector v ->
        let n = Option.value_exn (Language.lookup_vector v) in
        assert (List.length tl1 = n);
        let args =
          List.mapi tl1 ~f:(fun idx t ->
            let idx = Why3.Term.t_int_const (Why3.BigInt.of_int idx) in
            (Why3.Term.t_func_app_beta_l t2 [ idx; t ], Option.value_exn t.t_ty))
        in
        let op =
          let { ITypes.env; _ } = IRE.user_env engine in
          ITypes.Vector (Language.create_vector env n)
        in
        IRE.eval_term (ITypes.term_of_op ~args engine op ty)
      | Dataset (_, DS_csv csv) ->
        IRE.value_int (Why3.BigInt.of_int (Csv.lines csv))
      | Data _ | Model _ -> assert false)
    | [ Term _t1; Term _t2 ] -> IRE.reconstruct_term ()
    | _ -> fail_on_unexpected_argument ls

  let builtins : _ IRE.built_in_theories =
    ( [ "caisar"; "types" ],
      "Vector",
      [],
      [
        ([ Why3.Ident.op_get "" ] (* ([]) *), None, get);
        ([ Why3.Ident.op_infix "-" ], None, sub);
        ([ Why3.Ident.op_infix "+" ], None, add);
        ([ "length" ], None, length);
        ([ "mapi" ], None, mapi);
      ] )
end

(* -------------------------------------------------------------------------- *)
(* --- Model Builtins                                                      *)
(* -------------------------------------------------------------------------- *)

module Model = struct
  let read_model : _ IRE.builtin =
   fun engine ls vl ty ->
    match vl with
    | [ Term { t_node = Tconst (ConstStr model_filename); t_loc; _ } ] ->
      let { ITypes.env; cwd; _ } = IRE.user_env engine in
      let op =
        let model_ext = Stdlib.Filename.extension model_filename in
        let mk_nn nn_format nn_type =
          let nn_filename = Stdlib.Filename.concat cwd model_filename in
          let nn = Language.create_nn env nn_format nn_filename in
          ITypes.Model (NN (nn, nn_type))
        in
        match String.lowercase model_ext with
        | ".nnet" -> mk_nn `NNet ITypes.NNet
        | ".onnx" -> mk_nn `ONNX ITypes.ONNX
        | ".ovo" ->
          let svm_filename = Stdlib.Filename.concat cwd model_filename in
          ITypes.Model (SVM (Language.create_svm env svm_filename))
        | _ ->
          Logging.user_error ?loc:t_loc (fun m ->
            m "Unrecognized model format '%s'" model_ext)
      in
      IRE.value_term (ITypes.term_of_op engine op ty)
    | [ Term _t1; Term _t2 ] -> IRE.reconstruct_term ()
    | _ -> fail_on_unexpected_argument ls

  let read_model_with_abstraction : _ IRE.builtin =
   fun engine ls vl ty ->
    match vl with
    | [
     Term ({ t_node = Tconst (ConstStr svm); _ } as t1);
     Term ({ t_node = Tapp ({ ls_name = { id_string; _ }; _ }, []); _ } as t2);
    ] ->
      let { ITypes.env; cwd; _ } = IRE.user_env engine in
      let op =
        let svm_ext = Stdlib.Filename.extension svm in
        let svm_filename = Stdlib.Filename.concat cwd svm in
        let abstraction =
          match id_string with
          | "Interval" -> Language.Interval
          | "Raf" -> Language.Raf
          | "Hybrid" -> Language.Hybrid
          | _ ->
            Logging.user_error ?loc:t2.t_loc (fun m ->
              m "Unrecognized SVM abstraction '%s'" id_string)
        in
        let svm =
          match String.lowercase svm_ext with
          | ".ovo" -> Language.create_svm env ~abstraction svm_filename
          | _ ->
            Logging.user_error ?loc:t1.t_loc (fun m ->
              m "Unrecognized SVM format '%s'" svm_ext)
        in
        ITypes.Model (SVM svm)
      in
      IRE.value_term (ITypes.term_of_op engine op ty)
    | [ Term _t1; Term _t2 ] -> IRE.reconstruct_term ()
    | _ -> fail_on_unexpected_argument ls

  let apply : _ IRE.builtin =
   fun engine ls vl ty ->
    match vl with
    | [ Term t1; Term t2 ] -> (
      match ITypes.op_of_term engine t1 with
      | Some (Model model, []) ->
        let { ITypes.env; _ } = IRE.user_env engine in
        let ty_elt = (Symbols.Float64.create env).ty in
        let nb_inputs, nb_outputs, filename =
          match model with
          | NN (nn, _) ->
            let nn = Option.value_exn (Language.lookup_nn nn) in
            (nn.nn_nb_inputs, nn.nn_nb_outputs, nn.nn_filename)
          | SVM svm ->
            let svm = Option.value_exn (Language.lookup_svm svm) in
            (svm.svm_nb_inputs, svm.svm_nb_outputs, svm.svm_filename)
        in
        (match ITypes.op_of_term engine t2 with
        | Some (Vector v, tl2) ->
          let length_v =
            match Language.lookup_vector v with
            | None ->
              Logging.code_error ~src:Logging.src_interpret_goal (fun m ->
                m "Cannot find vector from lsymbol %a" Why3.Pretty.print_ls v)
            | Some n ->
              if List.length tl2 <> n
              then
                Logging.code_error ~src:Logging.src_interpret_goal (fun m ->
                  m
                    "Mismatch between (container) vector length and number of \
                     (contained) input variables.");
              n
          in
          if nb_inputs <> length_v
          then
            Logging.user_error ?loc:t2.t_loc (fun m ->
              m
                "Unexpected vector of length %d in input to model '%s',@ which \
                 expects input vectors of length %d"
                length_v filename nb_inputs)
        | _ ->
          ()
          (* Logging.user_error ?loc:t1.t_loc (fun m -> m "Unexpected neural
             network model application: %a" Why3.Pretty.print_term t2) *));
        if false
        then
          let th = Why3.Env.read_theory env [ "caisar"; "types" ] "Vector" in
          let get =
            Why3.Theory.ns_find_ls th.th_export [ Why3.Ident.op_get "" ]
          in
          let t0 = Why3.Term.t_app ls [ t1; t2 ] ty in
          let args =
            List.init nb_outputs ~f:(fun i ->
              ( Why3.Term.fs_app get
                  [
                    t0;
                    Why3.Term.t_const
                      (Why3.Constant.int_const_of_int i)
                      Why3.Ty.ty_int;
                  ]
                  ty_elt,
                ty_elt ))
          in
          let op = ITypes.Vector (Language.create_vector env nb_outputs) in
          IRE.value_term (ITypes.term_of_op ~args engine op ty)
        else IRE.reconstruct_term ()
      | _ -> IRE.reconstruct_term ())
    | _ -> fail_on_unexpected_argument ls

  let builtins : _ IRE.built_in_theories =
    ( [ "caisar"; "model" ],
      "Model",
      [],
      [
        ([ "read_model" ], None, read_model);
        ([ "read_model_with_abstraction" ], None, read_model_with_abstraction);
        ([ Why3.Ident.op_infix "@@" ], None, apply);
      ] )
end

(* -------------------------------------------------------------------------- *)
(* --- CSV Dataset Builtins                                                   *)
(* -------------------------------------------------------------------------- *)

module CSV = struct
  let read_dataset : _ IRE.builtin =
   fun engine ls vl ty ->
    match vl with
    | [ Term { t_node = Tconst (ConstStr dataset); _ } ] ->
      let { ITypes.env; cwd; _ } = IRE.user_env engine in
      let op =
        let filename = Stdlib.Filename.concat cwd dataset in
        let ds = Language.create_dataset_csv env filename in
        (* Ensures that CSV with comments can be parsed. *)
        let dataset =
          ITypes.DS_csv
            (Csv.load filename
            |> List.filter ~f:(function
                 | [] -> false
                 | hd :: _ -> not (String.is_prefix ~prefix:"#" hd)))
        in
        ITypes.Dataset (ds, dataset)
      in
      IRE.value_term (ITypes.term_of_op engine op ty)
    | [ Term _t1 ] -> IRE.reconstruct_term ()
    | _ -> fail_on_unexpected_argument ls

  let infer_min_label : _ IRE.builtin =
   fun engine ls vl _ty ->
    match vl with
    | [ Term ({ t_node = Tapp (ls1, _); _ } as _t1) ] -> (
      match ITypes.op_of_ls engine ls1 with
      | Dataset (_, DS_csv csv) ->
        (* TODO: test for non-empty csv. *)
        let min_label =
          List.fold_left csv ~init:None ~f:(fun min_label elt ->
            let label = Int.of_string (List.hd_exn elt) in
            Some
              (match min_label with
              | None -> label
              | Some min_label -> min min_label label))
        in
        let min_label = Option.value_exn min_label in
        IRE.value_int (Why3.BigInt.of_int min_label)
      | _ -> assert false)
    | [ Term _t1 ] -> IRE.reconstruct_term ()
    | _ -> fail_on_unexpected_argument ls

  let infer_max_label : _ IRE.builtin =
   fun engine ls vl _ty ->
    match vl with
    | [ Term ({ t_node = Tapp (ls1, _); _ } as _t1) ] -> (
      match ITypes.op_of_ls engine ls1 with
      | Dataset (_, DS_csv csv) ->
        (* TODO: test for non-empty csv. *)
        let max_label =
          List.fold_left csv ~init:None ~f:(fun max_label elt ->
            let label = Int.of_string (List.hd_exn elt) in
            Some
              (match max_label with
              | None -> label
              | Some max_label -> max max_label label))
        in
        let max_label = Option.value_exn max_label in
        IRE.value_int (Why3.BigInt.of_int max_label)
      | _ -> assert false)
    | [ Term _t1 ] -> IRE.reconstruct_term ()
    | _ -> fail_on_unexpected_argument ls

  let builtins : _ IRE.built_in_theories =
    ( [ "caisar"; "dataset" ],
      "CSV",
      [],
      [
        ([ "read_dataset" ], None, read_dataset);
        ([ "infer_min_label" ], None, infer_min_label);
        ([ "infer_max_label" ], None, infer_max_label);
      ] )
end

let builtins = [ Vector.builtins; Model.builtins; CSV.builtins ]
