(**************************************************************************)
(*                                                                        *)
(*  This file is part of CAISAR.                                          *)
(*                                                                        *)
(*  Copyright (C) 2025                                                    *)
(*    CEA (Commissariat à l'énergie atomique et aux énergies              *)
(*         alternatives)                                                  *)
(*                                                                        *)
(*  You can redistribute it and/or modify it under the terms of the GNU   *)
(*  Lesser General Public License as published by the Free Software       *)
(*  Foundation, version 2.1.                                              *)
(*                                                                        *)
(*  It is distributed in the hope that it will be useful,                 *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of        *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          *)
(*  GNU Lesser General Public License for more details.                   *)
(*                                                                        *)
(*  See the GNU Lesser General Public License version 2.1                 *)
(*  for more details (enclosed in the file licenses/LGPLv2.1).            *)
(*                                                                        *)
(**************************************************************************)

module IRE = Interpreter_reduction_engine
open Base

type nn_format =
  | NNet
  | ONNX
[@@deriving show]

type model =
  | NN of Why3.Term.lsymbol * nn_format
      [@printer
        fun fmt (ls, nn_format) ->
          Fmt.pf fmt "(%a, %a)" pp_nn_format nn_format
            Fmt.(option Language.pp_nn)
            (Language.lookup_nn ls)]
  | SVM of Why3.Term.lsymbol
      [@printer
        fun fmt ls ->
          Fmt.pf fmt "%a" Fmt.(option Language.pp_svm) (Language.lookup_svm ls)]
[@@deriving show]

type dataset = DS_csv of Csv.t [@printer fun fmt _ -> Fmt.pf fmt "<csv>"]
[@@deriving show]

type data = D_csv of string list [@@deriving show]

type interpreter_op =
  | Model of model
  | Dataset of Why3.Term.lsymbol * dataset
      [@printer
        fun fmt (ls, ds) ->
          Fmt.pf fmt "(%a, %a)"
            Fmt.(option Language.pp_dataset)
            (Language.lookup_dataset_csv ls)
            pp_dataset ds]
  | Data of data
  | Vector of Why3.Term.lsymbol
      [@printer
        fun fmt v -> Fmt.pf fmt "%a" Fmt.(option int) (Language.lookup_vector v)]
[@@deriving show]

type interpreter_op_hls =
  (interpreter_op Why3.Term.Hls.t
  [@printer
    fun fmt hls ->
      Why3.(
        Pp.print_iter2 Term.Hls.iter Pp.newline Pp.comma Pretty.print_ls
          pp_interpreter_op fmt hls)])
[@@deriving show]

type ls_htbl_interpreter_op = (interpreter_op, Why3.Term.lsymbol) Base.Hashtbl.t

type interpreter_env = {
  op_of_ls : interpreter_op_hls;
  ls_of_op : ls_htbl_interpreter_op;
  env : Why3.Env.env;
  cwd : string;
}

let ls_of_op engine interpreter_op ty_args ty =
  let interpreter_env = IRE.user_env engine in
  Hashtbl.find_or_add interpreter_env.ls_of_op interpreter_op
    ~default:(fun () ->
    let id = Why3.Ident.id_fresh "interpreter_op" in
    let ls =
      match interpreter_op with
      | Model (NN (m, _) | SVM m) -> m
      | Vector v -> v
      | Dataset (d, _) -> d
      | _ -> Why3.Term.create_lsymbol id ty_args ty
    in
    Hashtbl.Poly.add_exn interpreter_env.ls_of_op ~key:interpreter_op ~data:ls;
    Why3.Term.Hls.add interpreter_env.op_of_ls ls interpreter_op;
    ls)

let op_of_ls engine ls =
  let interpreter_env = IRE.user_env engine in
  Why3.Term.Hls.find interpreter_env.op_of_ls ls

let term_of_op ?(args = []) engine interpreter_op ty =
  let t_args, ty_args = List.unzip args in
  Why3.Term.t_app_infer (ls_of_op engine interpreter_op ty_args ty) t_args

let op_of_term engine t =
  match t.Why3.Term.t_node with
  | Tapp (ls, args) -> (
    match op_of_ls engine ls with
    | exception Stdlib.Not_found -> None
    | v -> Some (v, args))
  | _ -> None

let interpreter_env ~cwd env =
  {
    ls_of_op = Hashtbl.Poly.create ();
    op_of_ls = Why3.Term.Hls.create 10;
    env;
    cwd;
  }
