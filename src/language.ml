(**************************************************************************)
(*                                                                        *)
(*  This file is part of CAISAR.                                          *)
(*                                                                        *)
(*  Copyright (C) 2025                                                    *)
(*    CEA (Commissariat à l'énergie atomique et aux énergies              *)
(*         alternatives)                                                  *)
(*                                                                        *)
(*  You can redistribute it and/or modify it under the terms of the GNU   *)
(*  Lesser General Public License as published by the Free Software       *)
(*  Foundation, version 2.1.                                              *)
(*                                                                        *)
(*  It is distributed in the hope that it will be useful,                 *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of        *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          *)
(*  GNU Lesser General Public License for more details.                   *)
(*                                                                        *)
(*  See the GNU Lesser General Public License version 2.1                 *)
(*  for more details (enclosed in the file licenses/LGPLv2.1).            *)
(*                                                                        *)
(**************************************************************************)

open Why3
open Base

(* Support for several model formats: *)
(* - NNet and ONNX for neural networks *)
(* - OVO for SVM *)

type nn_shape = {
  nb_inputs : int;
  nb_outputs : int;
  ty_data : Ty.ty;
  filename : string;
  nir : Nir.Ngraph.t option;
}

type svm_shape = {
  nb_inputs : int;
  nb_outputs : int;
  filename : string;
  ovo : Ovo.t option;
}

let loaded_nets = Term.Hls.create 10
let loaded_svms = Term.Hls.create 10
let lookup_loaded_nets = Term.Hls.find_opt loaded_nets
let lookup_loaded_svms = Term.Hls.find_opt loaded_svms

let register_nn_as_tuple env nb_inputs nb_outputs filename ?nir mstr =
  let name = "AsTuple" in
  let th_uc = Pmodule.create_module env (Ident.id_fresh name) in
  let nn = Pmodule.read_module env [ "caisar"; "caisar" ] "NN" in
  let th_uc = Pmodule.use_export th_uc nn in
  let ty_data =
    Ty.ty_app Theory.(ns_find_ts nn.mod_theory.th_export [ "input_type" ]) []
  in
  let ls_nn_apply =
    let f _ = ty_data in
    Term.create_fsymbol
      (Ident.id_fresh "nn_apply")
      (List.init nb_inputs ~f)
      (Ty.ty_tuple (List.init nb_outputs ~f))
  in
  Term.Hls.add loaded_nets ls_nn_apply
    { filename; nb_inputs; nb_outputs; ty_data; nir };
  let th_uc =
    Pmodule.add_pdecl ~vc:false th_uc
      (Pdecl.create_pure_decl (Decl.create_param_decl ls_nn_apply))
  in
  Wstdlib.Mstr.add name (Pmodule.close_module th_uc) mstr

let register_nn_as_array env nb_inputs nb_outputs filename ?nir mstr =
  let name = "AsArray" in
  let th_uc = Pmodule.create_module env (Ident.id_fresh name) in
  let nn =
    Pmodule.read_module env [ "caisar"; "caisar" ] "DatasetClassification"
  in
  let th_uc = Pmodule.use_export th_uc nn in
  let ty_data =
    Ty.ty_app Theory.(ns_find_ts nn.mod_theory.th_export [ "model" ]) []
  in
  let ls_model = Term.create_fsymbol (Ident.id_fresh "model") [] ty_data in
  Term.Hls.add loaded_nets ls_model
    { filename; nb_inputs; nb_outputs; ty_data; nir };
  let th_uc =
    Pmodule.add_pdecl ~vc:false th_uc
      (Pdecl.create_pure_decl (Decl.create_param_decl ls_model))
  in
  Wstdlib.Mstr.add name (Pmodule.close_module th_uc) mstr

let register_svm_as_tuple env nb_inputs nb_outputs filename ?ovo mstr =
  let name = "AsTuple" in
  let th_uc = Pmodule.create_module env (Ident.id_fresh name) in
  let nn = Pmodule.read_module env [ "caisar"; "caisar" ] "NN" in
  let th_uc = Pmodule.use_export th_uc nn in
  let ty_data =
    Ty.ty_app Theory.(ns_find_ts nn.mod_theory.th_export [ "input_type" ]) []
  in
  let ls_svm_apply =
    let f _ = ty_data in
    Term.create_fsymbol
      (Ident.id_fresh "svm_apply")
      (List.init nb_inputs ~f)
      (Ty.ty_tuple (List.init nb_outputs ~f))
  in
  Term.Hls.add loaded_svms ls_svm_apply { filename; nb_inputs; nb_outputs; ovo };
  let th_uc =
    Pmodule.add_pdecl ~vc:false th_uc
      (Pdecl.create_pure_decl (Decl.create_param_decl ls_svm_apply))
  in
  Wstdlib.Mstr.add name (Pmodule.close_module th_uc) mstr

let register_svm_as_array env nb_inputs nb_outputs filename ?ovo mstr =
  let name = "AsArray" in
  let th_uc = Pmodule.create_module env (Ident.id_fresh name) in
  let svm =
    Pmodule.read_module env [ "caisar"; "caisar" ] "DatasetClassification"
  in
  let th_uc = Pmodule.use_export th_uc svm in
  let svm_type =
    Ty.ty_app Theory.(ns_find_ts svm.mod_theory.th_export [ "model" ]) []
  in
  let ls_model = Term.create_fsymbol (Ident.id_fresh "model") [] svm_type in
  Term.Hls.add loaded_svms ls_model { filename; nb_inputs; nb_outputs; ovo };
  let th_uc =
    Pmodule.add_pdecl ~vc:false th_uc
      (Pdecl.create_pure_decl (Decl.create_param_decl ls_model))
  in
  Wstdlib.Mstr.add name (Pmodule.close_module th_uc) mstr

let nnet_parser =
  Env.Wenv.memoize 13 (fun env ->
    let h = Hashtbl.create (module String) in
    Hashtbl.findi_or_add h ~default:(fun filename ->
      let model = Nnet.parse ~permissive:true filename in
      match model with
      | Error s -> Loc.errorm "%s" s
      | Ok { n_inputs; n_outputs; _ } ->
        Wstdlib.Mstr.empty
        |> register_nn_as_tuple env n_inputs n_outputs filename
        |> register_nn_as_array env n_inputs n_outputs filename))

let onnx_parser =
  Env.Wenv.memoize 13 (fun env ->
    let h = Hashtbl.create (module String) in
    Hashtbl.findi_or_add h ~default:(fun filename ->
      let model = Onnx.Reader.from_file filename in
      match model with
      | Error s -> Loc.errorm "%s" s
      | Ok { n_inputs; n_outputs; nir } ->
        let nir =
          match nir with
          | Error msg ->
            Logs.warn (fun m ->
              m "Cannot build network intermediate representation:@ %s" msg);
            None
          | Ok nir -> Some nir
        in
        Wstdlib.Mstr.empty
        |> register_nn_as_tuple env n_inputs n_outputs filename ?nir
        |> register_nn_as_array env n_inputs n_outputs filename ?nir))

let ovo_parser =
  Env.Wenv.memoize 13 (fun env ->
    let h = Hashtbl.create (module String) in
    Hashtbl.findi_or_add h ~default:(fun filename ->
      let model = Ovo.parse filename in
      match model with
      | Error s -> Loc.errorm "%s" s
      | Ok ovo ->
        Wstdlib.Mstr.empty
        |> register_svm_as_tuple env ovo.nb_ins ovo.nb_classes filename ~ovo
        |> register_svm_as_array env ovo.nb_ins ovo.nb_classes filename ~ovo))

let register_nnet_support () =
  Env.register_format ~desc:"NNet format (ReLU only)" Pmodule.mlw_language
    "NNet" [ "nnet" ] (fun env _ filename _ -> nnet_parser env filename)

let register_onnx_support () =
  Env.register_format ~desc:"ONNX format" Pmodule.mlw_language "ONNX" [ "onnx" ]
    (fun env _ filename _ -> onnx_parser env filename)

let register_ovo_support () =
  Env.register_format ~desc:"OVO format" Pmodule.mlw_language "OVO" [ "ovo" ]
    (fun env _ filename _ -> ovo_parser env filename)

(* -------------------------------------------------------------------------- *)
(* --- Vectors                                                                *)
(* -------------------------------------------------------------------------- *)

let vectors = Term.Hls.create 10

let ty_float64_t env =
  let th = Symbols.Float64.create env in
  th.ty

let ty_vector env ty_elt =
  let th = Env.read_theory env [ "caisar"; "types" ] "Vector" in
  Ty.ty_app (Theory.ns_find_ts th.th_export [ "vector" ]) [ ty_elt ]

let create_vector =
  Env.Wenv.memoize 13 (fun env ->
    let h = Hashtbl.create (module Int) in
    let ty_elt = ty_float64_t env in
    let ty = ty_vector env ty_elt in
    Hashtbl.findi_or_add h ~default:(fun length ->
      let ls =
        let id = Ident.id_fresh "vector" in
        Term.create_fsymbol id (List.init length ~f:(fun _ -> ty_elt)) ty
      in
      Term.Hls.add vectors ls length;
      ls))

let lookup_vector = Term.Hls.find_opt vectors
let mem_vector = Term.Hls.mem vectors

(* -------------------------------------------------------------------------- *)
(* --- Neural Networks                                                        *)
(* -------------------------------------------------------------------------- *)

type nn = {
  nn_nb_inputs : int;
  nn_nb_outputs : int;
  nn_ty_elt : Ty.ty; [@printer fun fmt ty -> Fmt.pf fmt "%a" Pretty.print_ty ty]
  nn_filename : string;
  nn_format : nn_format;
}
[@@deriving show]

and nn_format =
  | NNet of Nir.Ngraph.t option [@printer fun fmt _ -> Fmt.pf fmt "<nir>"]
  | ONNX of Nir.Ngraph.t option [@printer fun fmt _ -> Fmt.pf fmt "<nir>"]
[@@deriving show]

let nets = Term.Hls.create 10

let fresh_nn_ls env name =
  let ty_model =
    let th = Env.read_theory env [ "caisar"; "model" ] "Model" in
    Ty.ty_app (Theory.ns_find_ts th.th_export [ "model" ]) []
  in
  let id = Ident.id_fresh name in
  Term.create_fsymbol id [] ty_model

let create_nn_nnet env filename =
  let model = Nnet.parse ~permissive:true filename in
  match model with
  | Error s -> Loc.errorm "%s" s
  | Ok { n_inputs; n_outputs; nir; _ } ->
    {
      nn_nb_inputs = n_inputs;
      nn_nb_outputs = n_outputs;
      nn_ty_elt = ty_float64_t env;
      nn_filename = filename;
      nn_format = NNet (Some nir);
    }

let create_nn_onnx env filename =
  let model = Onnx.Reader.from_file filename in
  match model with
  | Error s -> Loc.errorm "%s" s
  | Ok { n_inputs; n_outputs; nir } ->
    let nir =
      match nir with
      | Error msg ->
        Logs.warn (fun m ->
          m "Cannot build network intermediate representation:@ %s" msg);
        None
      | Ok nir -> Some nir
    in
    {
      nn_nb_inputs = n_inputs;
      nn_nb_outputs = n_outputs;
      nn_ty_elt = ty_float64_t env;
      nn_filename = filename;
      nn_format = ONNX nir;
    }

let create_nn =
  Env.Wenv.memoize 13 (fun env ->
    let h = Hashtbl.Poly.create () in
    Hashtbl.Poly.findi_or_add h ~default:(fun format ->
      let format_nn, create_nn =
        match format with
        | `NNet -> ("nnet", create_nn_nnet)
        | `ONNX -> ("onnx", create_nn_onnx)
      in
      let name_nn = "nn_" ^ format_nn in
      let h = Hashtbl.create (module String) in
      Hashtbl.findi_or_add h ~default:(fun filename ->
        let ls = fresh_nn_ls env name_nn in
        let nn = create_nn env filename in
        Term.Hls.add nets ls nn;
        ls)))

let lookup_nn = Term.Hls.find_opt nets
let mem_nn = Term.Hls.mem nets
let iter_nn f = Term.Hls.iter f nets

let add_meta_nn task ls =
  match lookup_nn ls with
  | None -> task
  | Some { nn_filename; _ } ->
    Task.add_meta task Meta.meta_nn_filename [ MAstr nn_filename ]

(* -------------------------------------------------------------------------- *)
(* --- Support Vector Machines (SVM)                                          *)
(* -------------------------------------------------------------------------- *)

type svm = {
  svm_nb_inputs : int;
  svm_nb_outputs : int;
  svm_abstraction : svm_abstraction;
  svm_filename : string;
  svm_nir : Nir.Ngraph.t;
}
[@@deriving show]

and svm_abstraction =
  | Interval
  | Raf
  | Hybrid
[@@deriving show]

let string_of_svm_abstraction = function
  | Interval -> "interval"
  | Raf -> "raf"
  | Hybrid -> "hybrid"

let svm_abstraction_of_string s =
  match String.lowercase s with
  | "interval" -> Some Interval
  | "raf" -> Some Raf
  | "hybrid" -> Some Hybrid
  | _ -> None

let svms = Term.Hls.create 10

let fresh_svm_ls env name =
  let ty_model =
    let th = Env.read_theory env [ "caisar"; "model" ] "Model" in
    Ty.ty_app (Theory.ns_find_ts th.th_export [ "model" ]) []
  in
  let id = Ident.id_fresh name in
  Term.create_fsymbol id [] ty_model

let create_svm =
  Env.Wenv.memoize 13 (fun env ?(abstraction = Hybrid) ->
    let h = Hashtbl.create (module String) in
    Hashtbl.findi_or_add h ~default:(fun filename ->
      let name = "svm_ovo_" ^ string_of_svm_abstraction abstraction in
      let ls = fresh_svm_ls env name in
      let svm =
        let model = Ovo.parse filename in
        match model with
        | Error s -> Loc.errorm "%s" s
        | Ok ovo ->
          {
            svm_nb_inputs = ovo.nb_ins;
            svm_nb_outputs = ovo.nb_classes;
            svm_abstraction = abstraction;
            svm_filename = filename;
            svm_nir = Ovo.to_nn ovo;
          }
      in
      Term.Hls.add svms ls svm;
      ls))

let lookup_svm = Term.Hls.find_opt svms
let mem_svm = Term.Hls.mem svms
let iter_svm f = Term.Hls.iter f svms

let add_meta_svm task ls =
  match lookup_svm ls with
  | None -> task
  | Some { svm_filename; svm_abstraction; _ } ->
    Task.add_meta task Meta.meta_svm_filename
      [ MAstr svm_filename; MAstr (string_of_svm_abstraction svm_abstraction) ]

(* -------------------------------------------------------------------------- *)
(* --- Datasets                                                               *)
(* -------------------------------------------------------------------------- *)

type dataset = CSV of string [@@deriving show]

let datasets = Term.Hls.create 10

let fresh_dataset_csv_ls env name =
  let ty =
    let th = Env.read_theory env [ "caisar"; "dataset" ] "CSV" in
    Ty.ty_app (Theory.ns_find_ts th.th_export [ "dataset" ]) []
  in
  let id = Ident.id_fresh name in
  Term.create_fsymbol id [] ty

let create_dataset_csv =
  Env.Wenv.memoize 13 (fun env ->
    (* TODO: Should use actual dataset element types. *)
    let h = Hashtbl.create (module String) in
    Hashtbl.findi_or_add h ~default:(fun filename ->
      let ls = fresh_dataset_csv_ls env "dataset_csv" in
      Term.Hls.add datasets ls (CSV filename);
      ls))

let lookup_dataset_csv = Term.Hls.find_opt datasets
let mem_dataset_csv = Term.Hls.mem datasets

let add_meta_dataset_csv task ls =
  match lookup_dataset_csv ls with
  | None -> task
  | Some (CSV filename) ->
    Task.add_meta task Meta.meta_dataset_filename [ MAstr filename ]
