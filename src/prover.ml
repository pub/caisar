(**************************************************************************)
(*                                                                        *)
(*  This file is part of CAISAR.                                          *)
(*                                                                        *)
(*  Copyright (C) 2025                                                    *)
(*    CEA (Commissariat à l'énergie atomique et aux énergies              *)
(*         alternatives)                                                  *)
(*                                                                        *)
(*  You can redistribute it and/or modify it under the terms of the GNU   *)
(*  Lesser General Public License as published by the Free Software       *)
(*  Foundation, version 2.1.                                              *)
(*                                                                        *)
(*  It is distributed in the hope that it will be useful,                 *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of        *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          *)
(*  GNU Lesser General Public License for more details.                   *)
(*                                                                        *)
(*  See the GNU Lesser General Public License version 2.1                 *)
(*  for more details (enclosed in the file licenses/LGPLv2.1).            *)
(*                                                                        *)
(**************************************************************************)

type t =
  | Marabou
  | Maraboupy [@name "maraboupy"]
  | Pyrat [@name "PyRAT"]
  | Saver [@name "SAVer"]
  | Aimos [@name "AIMOS"]
  | CVC5 [@name "cvc5"]
  | AltErgo [@name "alt-ergo"]
  | Z3 [@name "Z3"]
  | Nnenum [@name "nnenum"]
  | ABCrown [@name "alpha-beta-CROWN"]
[@@deriving yojson, show]

let list_available () =
  [
    Marabou; Maraboupy; Pyrat; Saver; Aimos; CVC5; AltErgo; Z3; Nnenum; ABCrown;
  ]

let of_string prover =
  let prover = String.lowercase_ascii prover in
  match prover with
  | "marabou" -> Some Marabou
  | "maraboupy" -> Some Maraboupy
  | "pyrat" -> Some Pyrat
  | "saver" -> Some Saver
  | "aimos" -> Some Aimos
  | "cvc5" -> Some CVC5
  | "nnenum" -> Some Nnenum
  | "abcrown" -> Some ABCrown
  | "alt-ergo" -> Some AltErgo
  | "z3" -> Some Z3
  | _ -> None

let to_string = function
  | Marabou -> "Marabou"
  | Maraboupy -> "maraboupy"
  | Pyrat -> "PyRAT"
  | Saver -> "SAVer"
  | Aimos -> "AIMOS"
  | CVC5 -> "CVC5"
  | AltErgo -> "Alt-Ergo"
  | Z3 -> "Z3"
  | Nnenum -> "nnenum"
  | ABCrown -> "alpha-beta-CROWN"

let has_vnnlib_support = function
  | Pyrat -> true
  | Marabou | Maraboupy | Saver | Aimos | CVC5 | AltErgo | Z3 | Nnenum | ABCrown
    ->
    false
