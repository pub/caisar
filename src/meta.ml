(**************************************************************************)
(*                                                                        *)
(*  This file is part of CAISAR.                                          *)
(*                                                                        *)
(*  Copyright (C) 2025                                                    *)
(*    CEA (Commissariat à l'énergie atomique et aux énergies              *)
(*         alternatives)                                                  *)
(*                                                                        *)
(*  You can redistribute it and/or modify it under the terms of the GNU   *)
(*  Lesser General Public License as published by the Free Software       *)
(*  Foundation, version 2.1.                                              *)
(*                                                                        *)
(*  It is distributed in the hope that it will be useful,                 *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of        *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          *)
(*  GNU Lesser General Public License for more details.                   *)
(*                                                                        *)
(*  See the GNU Lesser General Public License version 2.1                 *)
(*  for more details (enclosed in the file licenses/LGPLv2.1).            *)
(*                                                                        *)
(**************************************************************************)

let meta_input =
  Why3.Theory.(
    register_meta "caisar_input"
      ~desc:"Indicates an input position among the inputs of the neural network"
      [ MTlsymbol; MTint ])

let meta_output =
  Why3.Theory.(
    register_meta "caisar_output"
      ~desc:
        "Indicates an output position among the outputs of the neural network"
      [ MTlsymbol; MTint ])

let meta_nn_filename =
  Why3.Theory.(
    register_meta_excl "caisar_nnet_or_onnx"
      ~desc:"Indicates the filename of the neural network" [ MTstring ])

let meta_svm_filename =
  Why3.Theory.(
    register_meta_excl "caisar_svm"
      ~desc:
        "Indicates the filename and abstraction of the support vector machine"
      [ MTstring; MTstring ])

let meta_dataset_filename =
  Why3.Theory.(
    register_meta_excl "caisar_dataset"
      ~desc:"Indicates the filename of the dataset" [ MTstring ])

let rec collect_meta_input_output ~input_encoding ~output_encoding hls =
  function
  | None -> ()
  | Some { Why3.Task.task_prev; task_decl; _ } -> (
    collect_meta_input_output ~input_encoding ~output_encoding hls task_prev;
    match task_decl.Why3.Theory.td_node with
    | Meta (meta, l) when Why3.Theory.meta_equal meta meta_input -> (
      match l with
      | [ MAls ls; MAint i ] -> Why3.Term.Hls.add hls ls (input_encoding i)
      | _ -> assert false)
    | Meta (meta, l) when Why3.Theory.meta_equal meta meta_output -> (
      match l with
      | [ MAls ls; MAint i ] -> Why3.Term.Hls.add hls ls (output_encoding i)
      | _ -> assert false)
    | Use _ | Clone _ | Decl _ | Meta _ -> ())
