(**************************************************************************)
(*                                                                        *)
(*  This file is part of CAISAR.                                          *)
(*                                                                        *)
(*  Copyright (C) 2025                                                    *)
(*    CEA (Commissariat à l'énergie atomique et aux énergies              *)
(*         alternatives)                                                  *)
(*                                                                        *)
(*  You can redistribute it and/or modify it under the terms of the GNU   *)
(*  Lesser General Public License as published by the Free Software       *)
(*  Foundation, version 2.1.                                              *)
(*                                                                        *)
(*  It is distributed in the hope that it will be useful,                 *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of        *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          *)
(*  GNU Lesser General Public License for more details.                   *)
(*                                                                        *)
(*  See the GNU Lesser General Public License version 2.1                 *)
(*  for more details (enclosed in the file licenses/LGPLv2.1).            *)
(*                                                                        *)
(**************************************************************************)

open Why3
open Base

type eps = float [@@deriving yojson, show]
type threshold = float [@@deriving yojson, show]

let string_of_threshold threshold = Float.to_string threshold

type amplitude_params = {
  start : int;
  stop : int;
  step : int;
}
[@@deriving yojson, show]

type amplitude = amplitude_params option [@@deriving yojson, show]

let string_of_amplitude_params amplitude_params =
  Fmt.str "range(%s, %s, %s)"
    (Int.to_string amplitude_params.start)
    (Int.to_string amplitude_params.stop)
    (Int.to_string amplitude_params.step)

type aimos_params = {
  perturbation : string;
  g_load_fn : string;
  amplitude : amplitude;
}
[@@deriving yojson, show]

type property =
  | Correct
  | Robust of eps
  | CondRobust of eps
  | MetaRobust of threshold * aimos_params
  | MetaRobustMin of threshold * aimos_params
  | MetaRobustDiff of threshold * aimos_params
[@@deriving yojson, show]

type ('a, 'b) predicate = {
  model : 'a;
  dataset : 'b;
  property : property;
}

let find_predicate_ls env p =
  let th =
    Pmodule.read_module env [ "caisar"; "caisar" ] "DatasetClassificationProps"
  in
  Theory.ns_find_ls th.mod_theory.th_export [ p ]

let find_option_ls env p =
  let th = Pmodule.read_module env [ "option" ] "Option" in
  Theory.ns_find_ls th.mod_theory.th_export [ p ]

let fail_on_unsupported_term ?loc t =
  Logging.user_error ?loc (fun m -> m "Unsupported term %a" Pretty.print_term t)

let fail_on_unsupported_ls ?loc ls =
  Logging.user_error ?loc (fun m ->
    m "Unsupported logic symbol %a" Pretty.print_ls ls)

let interpret_predicate env ~on_model ~on_dataset task =
  let task = Trans.apply Introduction.introduce_premises task in
  let term = Task.task_goal_fmla task in
  match term.t_node with
  | Term.Tapp
      ( ls,
        { t_node = Tapp (model, _); _ }
        :: { t_node = Tapp (dataset, _); _ }
        :: tt ) ->
    let property =
      match tt with
      | [] ->
        let correct_predicate = find_predicate_ls env "correct" in
        if Term.ls_equal ls correct_predicate
        then Correct
        else fail_on_unsupported_ls ?loc:term.t_loc ls
      | [ { t_node = Tconst (Constant.ConstReal e); _ } ] ->
        let robust_predicate = find_predicate_ls env "robust" in
        let cond_robust_predicate = find_predicate_ls env "cond_robust" in
        let f = Utils.float_of_real_constant e in
        if Term.ls_equal ls robust_predicate
        then Robust f
        else if Term.ls_equal ls cond_robust_predicate
        then CondRobust f
        else fail_on_unsupported_ls ?loc:term.t_loc ls
      | [
       { t_node = Tconst (Constant.ConstReal e); _ };
       { t_node = Tconst (Constant.ConstStr perturbation); _ };
       { t_node = Tconst (Constant.ConstStr g_load_fn); _ };
       t_amplitude;
      ] ->
        let meta_robust_predicate = find_predicate_ls env "meta_robust" in
        let meta_robust_min_predicate =
          find_predicate_ls env "meta_robust_min"
        in
        let meta_robust_diff_predicate =
          find_predicate_ls env "meta_robust_diff"
        in
        let some_ls = find_option_ls env "Some" in
        let none_ls = find_option_ls env "None" in
        let f = Utils.float_of_real_constant e in
        let amplitude =
          match t_amplitude with
          | {
           t_node =
             Term.Tapp
               ( option_ls,
                 [
                   {
                     t_node =
                       Term.Tapp
                         ( _,
                           [
                             { t_node = Tconst (Constant.ConstInt start); _ };
                             { t_node = Tconst (Constant.ConstInt stop); _ };
                             { t_node = Tconst (Constant.ConstInt step); _ };
                           ] );
                     _;
                   };
                 ] );
           _;
          }
            when Term.ls_equal option_ls some_ls ->
            Some
              {
                start = Number.to_small_integer start;
                stop = Number.to_small_integer stop;
                step = Number.to_small_integer step;
              }
          | { t_node = Term.Tapp (option_ls, _); _ }
            when Term.ls_equal option_ls none_ls ->
            None
          | _ -> fail_on_unsupported_term ?loc:t_amplitude.t_loc t_amplitude
        in
        if Term.ls_equal ls meta_robust_predicate
        then MetaRobust (f, { perturbation; g_load_fn; amplitude })
        else if Term.ls_equal ls meta_robust_min_predicate
        then MetaRobustMin (f, { perturbation; g_load_fn; amplitude })
        else if Term.ls_equal ls meta_robust_diff_predicate
        then MetaRobustDiff (f, { perturbation; g_load_fn; amplitude })
        else fail_on_unsupported_ls ?loc:term.t_loc ls
      | _ -> fail_on_unsupported_term ?loc:term.t_loc term
    in
    let dataset = on_dataset dataset in
    let model = on_model model in
    { model; dataset; property }
  | _ ->
    (* No other term node is supported. *)
    fail_on_unsupported_term ?loc:term.t_loc term
