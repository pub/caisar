(**************************************************************************)
(*                                                                        *)
(*  This file is part of CAISAR.                                          *)
(*                                                                        *)
(*  Copyright (C) 2025                                                    *)
(*    CEA (Commissariat à l'énergie atomique et aux énergies              *)
(*         alternatives)                                                  *)
(*                                                                        *)
(*  You can redistribute it and/or modify it under the terms of the GNU   *)
(*  Lesser General Public License as published by the Free Software       *)
(*  Foundation, version 2.1.                                              *)
(*                                                                        *)
(*  It is distributed in the hope that it will be useful,                 *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of        *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          *)
(*  GNU Lesser General Public License for more details.                   *)
(*                                                                        *)
(*  See the GNU Lesser General Public License version 2.1                 *)
(*  for more details (enclosed in the file licenses/LGPLv2.1).            *)
(*                                                                        *)
(**************************************************************************)

type t = private
  | Marabou
  | Maraboupy [@name "maraboupy"]
  | Pyrat [@name "PyRAT"]
  | Saver [@name "SAVer"]
  | Aimos [@name "AIMOS"]
  | CVC5 [@name "cvc5"]
  | AltErgo [@name "alt-ergo"]
  | Z3 [@name "Z3"]
  | Nnenum [@name "nnenum"]
  | ABCrown [@name "alpha-beta-CROWN"]
[@@deriving yojson, show]

val list_available : unit -> t list
val of_string : string -> t option
val to_string : t -> string
val has_vnnlib_support : t -> bool
