(**************************************************************************)
(*                                                                        *)
(*  This file is part of CAISAR.                                          *)
(*                                                                        *)
(*  Copyright (C) 2025                                                    *)
(*    CEA (Commissariat à l'énergie atomique et aux énergies              *)
(*         alternatives)                                                  *)
(*                                                                        *)
(*  You can redistribute it and/or modify it under the terms of the GNU   *)
(*  Lesser General Public License as published by the Free Software       *)
(*  Foundation, version 2.1.                                              *)
(*                                                                        *)
(*  It is distributed in the hope that it will be useful,                 *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of        *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          *)
(*  GNU Lesser General Public License for more details.                   *)
(*                                                                        *)
(*  See the GNU Lesser General Public License version 2.1                 *)
(*  for more details (enclosed in the file licenses/LGPLv2.1).            *)
(*                                                                        *)
(**************************************************************************)

open Base
open Why3

let sls_model ~lookup sls =
  let rec aux acc term =
    let acc = Term.t_fold aux acc term in
    match term.t_node with
    | Term.Tapp (ls, _) -> (
      match lookup ls with None -> acc | Some _ -> Term.Sls.add ls acc)
    | _ -> acc
  in
  Trans.fold_decl (fun decl acc -> Decl.decl_fold aux acc decl) sls

let do_apply_prover_strategy ~lookup ~trans tasks =
  let sls_model =
    List.fold tasks ~init:Term.Sls.empty ~f:(fun accum task ->
      Trans.apply (sls_model ~lookup accum) task)
  in
  match Term.Sls.cardinal sls_model with
  | 0 -> tasks
  | _ ->
    (* TODO: rework for dealing with SVMs also. *)
    List.map tasks ~f:(Trans.apply trans)

let apply_classic_prover_strategy env task =
  let lookup = Language.lookup_nn in
  let trans = Trans.seq [ Introduction.introduce_premises; Nn2smt.trans env ] in
  do_apply_prover_strategy ~lookup ~trans [ task ]

let split_on_dataset_elts task =
  match Task.on_meta_excl Meta.meta_dataset_filename task with
  | None -> [ task ]
  | Some _ ->
    (* Property verification on a dataset typically involves goals with huge
       toplevel conjunctions, one conjunct for each element of the dataset:
       first thing to do is to destruct such toplevel conjunctions. *)
    Trans.apply Split.split_goal_full_stop_on_quant task

let apply_native_nn_prover_strategy env task =
  let tasks = split_on_dataset_elts task in
  let lookup = Language.lookup_nn in
  let trans =
    Trans.seq [ Introduction.introduce_premises; Native_nn_prover.trans env ]
  in
  do_apply_prover_strategy ~lookup ~trans tasks

let apply_svm_prover_strategy task =
  let tasks = split_on_dataset_elts task in
  let lookup = Language.lookup_svm in
  let trans = Introduction.introduce_premises in
  do_apply_prover_strategy ~lookup ~trans tasks
