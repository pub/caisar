(**************************************************************************)
(*                                                                        *)
(*  This file is part of CAISAR.                                          *)
(*                                                                        *)
(*  Copyright (C) 2025                                                    *)
(*    CEA (Commissariat à l'énergie atomique et aux énergies              *)
(*         alternatives)                                                  *)
(*                                                                        *)
(*  You can redistribute it and/or modify it under the terms of the GNU   *)
(*  Lesser General Public License as published by the Free Software       *)
(*  Foundation, version 2.1.                                              *)
(*                                                                        *)
(*  It is distributed in the hope that it will be useful,                 *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of        *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          *)
(*  GNU Lesser General Public License for more details.                   *)
(*                                                                        *)
(*  See the GNU Lesser General Public License version 2.1                 *)
(*  for more details (enclosed in the file licenses/LGPLv2.1).            *)
(*                                                                        *)
(**************************************************************************)

open Why3

type relops = {
  le : Term.lsymbol;
  ge : Term.lsymbol;
  lt : Term.lsymbol;
  gt : Term.lsymbol;
}

type info = {
  ls_rel_real : relops;
  ls_rel_float : relops;
  info_syn : Printer.syntax_map;
  variables : string Term.Hls.t;
}

let number_format =
  {
    Number.long_int_support = `Default;
    Number.negative_int_support = `Default;
    Number.dec_int_support = `Default;
    Number.hex_int_support = `Unsupported;
    Number.oct_int_support = `Unsupported;
    Number.bin_int_support = `Unsupported;
    Number.negative_real_support = `Custom (fun fmt f -> Fmt.pf fmt "-%t" f);
    Number.dec_real_support = `Default;
    Number.hex_real_support = `Unsupported;
    Number.frac_real_support = `Unsupported (fun _ _ -> assert false);
  }

let rec print_term info fmt t =
  match t.Term.t_node with
  | Tbinop ((Timplies | Tiff | Tor), _, _)
  | Tnot _ | Ttrue | Tfalse | Tvar _ | Tlet _ | Tif _ | Tcase _ | Tquant _
  | Teps _ ->
    Printer.unsupportedTerm t "Marabou: not a base term"
  | Tbinop (Tand, _, _) -> assert false (* Should appear only at top-level. *)
  | Tconst c -> Constant.(print number_format unsupported_escape) fmt c
  | Tapp (ls, [ { t_node = Tapp (ls1, []); _ }; { t_node = Tapp (ls2, []); _ } ])
    -> (
    (* Marabou accepts only constraints of the form 't1 relop t2' where t1 must
       be a sequence of variables, each with a {+,-} symbol upfront, while t2
       must be a single fp constant. If t1 is a single variable and t2 a fp
       constant, t1 may not exhibit a + symbol.

       Whenever t2 is not a fp constant, t2 needs to be aggregated to t1.

       FIXME: This code handles only t1 and t2 single variables w/o {+,-}
       symbols as it prints something of the form '+t1 -t2 relop 0'. That is,
       assumptions on t1 and t2 abound. *)
    match
      ( Term.Hls.find_opt info.variables ls1,
        Term.Hls.find_opt info.variables ls2 )
    with
    | Some s1, Some s2 ->
      if Term.ls_equal ls info.ls_rel_float.le
         || Term.ls_equal ls info.ls_rel_float.lt
         || Term.ls_equal ls info.ls_rel_real.le
         || Term.ls_equal ls info.ls_rel_real.lt
      then Fmt.pf fmt "+%s -%s <= 0" s1 s2
      else if Term.ls_equal ls info.ls_rel_float.ge
              || Term.ls_equal ls info.ls_rel_float.gt
              || Term.ls_equal ls info.ls_rel_real.ge
              || Term.ls_equal ls info.ls_rel_real.gt
      then Fmt.pf fmt "+%s -%s >= 0" s1 s2
      else Printer.unsupportedTerm t "Marabou: unknown relational operator"
    | _ -> Printer.unsupportedTerm t "Marabou: unknown variable(s)")
  | Tapp (ls, l) -> (
    match Printer.query_syntax info.info_syn ls.ls_name with
    | Some s -> Printer.syntax_arguments s (print_term info) fmt l
    | None -> (
      match (Term.Hls.find_opt info.variables ls, l) with
      | Some s, [] -> Fmt.string fmt s
      | _ -> Printer.unsupportedTerm t "Marabou: unknown variable(s)"))

let rec print_top_level_term info fmt t =
  (* We do not print unknown symbols: returns false if not a known syntax or
     variable. *)
  let t_is_known =
    Term.t_s_all
      (fun _ -> true)
      (fun ls ->
        Ident.Mid.mem ls.ls_name info.info_syn || Term.Hls.mem info.variables ls)
  in
  match t.Term.t_node with
  | Tquant _ -> ()
  | Tbinop (Tand, t1, t2) ->
    if t_is_known t1 && t_is_known t2
    then
      Fmt.pf fmt "%a%a"
        (print_top_level_term info)
        t1
        (print_top_level_term info)
        t2
  | _ -> if t_is_known t then Fmt.pf fmt "%a@." (print_term info) t

let rec negate_term info t =
  (* Assumption: conjunctions have been split beforehand, hence cannot appear at
     this stage. *)
  match t.Term.t_node with
  | Tnot t -> t
  | Tbinop (Tor, t1, t2) ->
    Term.t_and (negate_term info t1) (negate_term info t2)
  | Tapp (ls, [ t1; t2 ]) ->
    let tt = [ t1; t2 ] in
    (* Negate float relational symbols. *)
    let ls_neg =
      if Term.ls_equal ls info.ls_rel_float.le
      then info.ls_rel_float.gt
      else if Term.ls_equal ls info.ls_rel_float.lt
      then info.ls_rel_float.ge
      else if Term.ls_equal ls info.ls_rel_float.ge
      then info.ls_rel_float.lt
      else if Term.ls_equal ls info.ls_rel_float.gt
      then info.ls_rel_float.le
      else ls
    in
    (* Negate real relational symbols. *)
    let ls_neg =
      if Term.ls_equal ls info.ls_rel_real.le
      then info.ls_rel_real.gt
      else if Term.ls_equal ls info.ls_rel_real.lt
      then info.ls_rel_real.ge
      else if Term.ls_equal ls info.ls_rel_real.ge
      then info.ls_rel_real.lt
      else if Term.ls_equal ls info.ls_rel_real.gt
      then info.ls_rel_real.le
      else ls_neg
    in
    if Term.ls_equal ls_neg ls
    then Printer.unsupportedTerm t "Marabou: cannot negate term"
    else Term.ps_app ls_neg tt
  | _ -> Printer.unsupportedTerm t "Marabou: cannot negate term"

let print_decl info fmt d =
  match d.Decl.d_node with
  | Dtype _ | Ddata _ | Dparam _ | Dlogic _ | Dind _ -> ()
  | Dprop (Decl.Plemma, _, _) -> assert false
  | Dprop (Decl.Paxiom, _, f) -> print_top_level_term info fmt f
  | Dprop (Decl.Pgoal, _, f) ->
    print_top_level_term info fmt (negate_term info f)

let rec print_tdecl info fmt task =
  match task with
  | None -> ()
  | Some { Task.task_prev; task_decl; _ } -> (
    print_tdecl info fmt task_prev;
    match task_decl.Theory.td_node with
    | Use _ | Clone _ -> ()
    | Meta (_, _) -> ()
    | Decl d -> print_decl info fmt d)

let print_task args ?old:_ fmt task =
  let ls_rel_real =
    let th = Symbols.Real.create args.Printer.env in
    { le = th.le; ge = th.ge; lt = th.lt; gt = th.gt }
  in
  let ls_rel_float =
    let th = Symbols.Float64.create args.Printer.env in
    { le = th.le; ge = th.ge; lt = th.lt; gt = th.gt }
  in
  let info =
    {
      ls_rel_real;
      ls_rel_float;
      info_syn = Discriminate.get_syntax_map task;
      variables = Term.Hls.create 10;
    }
  in
  Printer.print_prelude fmt args.Printer.prelude;
  Meta.collect_meta_input_output info.variables task
    ~input_encoding:(Fmt.str "x%i") ~output_encoding:(Fmt.str "y%i");
  print_tdecl info fmt task

let init () =
  Printer.register_printer ~desc:"Printer@ for@ the@ Marabou@ prover." "marabou"
    print_task
