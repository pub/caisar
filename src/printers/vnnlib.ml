(**************************************************************************)
(*                                                                        *)
(*  This file is part of CAISAR.                                          *)
(*                                                                        *)
(*  Copyright (C) 2025                                                    *)
(*    CEA (Commissariat à l'énergie atomique et aux énergies              *)
(*         alternatives)                                                  *)
(*                                                                        *)
(*  You can redistribute it and/or modify it under the terms of the GNU   *)
(*  Lesser General Public License as published by the Free Software       *)
(*  Foundation, version 2.1.                                              *)
(*                                                                        *)
(*  It is distributed in the hope that it will be useful,                 *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of        *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          *)
(*  GNU Lesser General Public License for more details.                   *)
(*                                                                        *)
(*  See the GNU Lesser General Public License version 2.1                 *)
(*  for more details (enclosed in the file licenses/LGPLv2.1).            *)
(*                                                                        *)
(**************************************************************************)

open Why3

(** SMTLIB tokens taken from CVC4: src/parser/smt2/{Smt2.g,smt2.cpp} *)
let ident_printer () =
  let bls =
    [
      (* Base SMT-LIB commands, see page 43 *)
      "assert";
      "check-sat";
      "check-sat-assuming";
      "declare-const";
      "declare-datatype";
      "declare-datatypes";
      "declare-fun";
      "declare-sort";
      "define-fun";
      "define-fun-rec";
      "define-funs-rec";
      "define-sort";
      "echo";
      "exit";
      "get-assignment";
      "get-assertions";
      "get-info";
      "get-model";
      "get-option";
      "get-proof";
      "get-unsat-assumptions";
      "get-unsat-core";
      "get-value";
      "pop";
      "push";
      "reset";
      "reset-assertions";
      "set-info";
      "set-logic";
      "set-option";
      (* Base SMT-LIB tokens, see page 22*)
      "BINARY";
      "DECIMAL";
      "HEXADECIMAL";
      "NUMERAL";
      "STRING";
      "_";
      "!";
      "as";
      "let";
      "exists";
      "forall";
      "match";
      "par";
      (* extended commands *)
      "assert-rewrite";
      "assert-reduction";
      "assert-propagation";
      "declare-sorts";
      "declare-funs";
      "declare-preds";
      "define";
      "simplify";
      (* operators, including theory symbols *)
      "=";
      "=>";
      "+";
      "-";
      "*";
      "^";
      "<";
      "<=";
      ">";
      ">=";
      "ite";
      "and";
      "distinct";
      "is_int";
      "not";
      "or";
      "select";
      "store";
      "to_int";
      "to_real";
      "xor";
      "/";
      "div";
      "mod";
      "rem";
      "concat";
      "bvnot";
      "bvand";
      "bvor";
      "bvneg";
      "bvadd";
      "bvmul";
      "bvudiv";
      "bvurem";
      "bvshl";
      "bvlshr";
      "bvult";
      "bvnand";
      "bvnor";
      "bvxor";
      "bvxnor";
      "bvcomp";
      "bvsub";
      "bvsdiv";
      "bvsrem";
      "bvsmod";
      "bvashr";
      "bvule";
      "bvugt";
      "bvuge";
      "bvslt";
      "bvsle";
      "bvsgt";
      "bvsge";
      "rotate_left";
      "rotate_right";
      "bvredor";
      "bvredand";
      "bv2nat";
      "sqrt";
      "sin";
      "cos";
      "tan";
      "asin";
      "acos";
      "atan";
      "pi";
      "exp";
      "csc";
      "sec";
      "cot";
      "arcsin";
      "arccos";
      "arctan";
      "arccsc";
      "arcsec";
      "arccot";
      (* the new floating point theory - updated to the 2014-05-27 standard *)
      "FloatingPoint";
      "fp";
      "Float16";
      "Float32";
      "Float64";
      "Float128";
      "RoundingMode";
      "roundNearestTiesToEven";
      "RNE";
      "roundNearestTiesToAway";
      "RNA";
      "roundTowardPositive";
      "RTP";
      "roundTowardNegative";
      "RTN";
      "roundTowardZero";
      "RTZ";
      "NaN";
      "+oo";
      "-oo";
      "+zero";
      "-zero";
      "fp.abs";
      "fp.neg";
      "fp.add";
      "fp.sub";
      "fp.mul";
      "fp.div";
      "fp.fma";
      "fp.sqrt";
      "fp.rem";
      "fp.roundToIntegral";
      "fp.min";
      "fp.max";
      "fp.leq";
      "fp.lt";
      "fp.geq";
      "fp.gt";
      "fp.eq";
      "fp.isNormal";
      "fp.isSubnormal";
      "fp.isZero";
      "fp.isInfinite";
      "fp.isNaN";
      "fp.isNegative";
      "fp.isPositive";
      "to_fp";
      "to_fp_unsigned";
      "fp.to_ubv";
      "fp.to_sbv";
      "fp.to_real";
      (* the new proposed string theory *)
      "String";
      "str.<";
      "str.<=";
      "str.++";
      "str.len";
      "str.substr";
      "str.contains";
      "str.at";
      "str.indexof";
      "str.prefixof";
      "str.suffixof";
      "int.to.str";
      "str.to.int";
      "u16.to.str";
      "str.to.u16";
      "u32.to.str";
      "str.to.u32";
      "str.in.re";
      "str.to.re";
      "str.replace";
      "str.tolower";
      "str.toupper";
      "str.rev";
      "str.from_code";
      "str.is_digit";
      "str.from_int";
      "str.to_int";
      "str.in_re";
      "str.to_code";
      "str.replace_all";
      "int.to.str";
      "str.to.int";
      "str.code";
      "str.replaceall";
      "re.++";
      "re.union";
      "re.inter";
      "re.*";
      "re.+";
      "re.opt";
      "re.range";
      "re.loop";
      "re.comp";
      "re.diff";
      (* the new proposed set theory *)
      "union";
      "intersection";
      "setminus";
      "subset";
      "member";
      "singleton";
      "insert";
      "card";
      "complement";
      "join";
      "product";
      "transpose";
      "tclosure";
      (* built-in sorts *)
      "Bool";
      "Int";
      "Real";
      "BitVec";
      "Array";
      (* Other stuff that Why3 seems to need *)
      "unsat";
      "sat";
      "true";
      "false";
      "const";
      "abs";
      "BitVec";
      "extract";
      "bv2nat";
      "nat2bv";
      (* From Z3 *)
      "map";
      "bv";
      "default";
      "difference";
      (* From CVC4 *)
      "char";
      "choose";
      (* Counterexamples specific keywords *)
      "lambda";
      "LAMBDA";
      "model";
      (* various stuff from "sed -n -e 's/^.*addOperator.*\"\([^\"]*\)\".*/\1/p'
         src/parser/smt2/smt2.cpp" *)
      "inst-closure";
      "dt.size";
      "sep";
      "pto";
      "wand";
      "emp";
      "fmf.card";
      "fmf.card.val";
    ]
  in
  let sanitizer = Ident.(sanitizer char_to_alpha char_to_alnumus) in
  Ident.create_ident_printer bls ~sanitizer

type relops = {
  le : Term.lsymbol;
  ge : Term.lsymbol;
  lt : Term.lsymbol;
  gt : Term.lsymbol;
}

type info = {
  ls_rel_real : relops;
  ls_rel_float : relops;
  info_printer : Ident.ident_printer;
  info_syn : Printer.syntax_map;
  variables : string Term.Hls.t;
}

let number_format =
  {
    Number.long_int_support = `Default;
    Number.negative_int_support = `Default;
    Number.dec_int_support = `Default;
    Number.hex_int_support = `Unsupported;
    Number.oct_int_support = `Unsupported;
    Number.bin_int_support = `Unsupported;
    Number.negative_real_support = `Custom (fun fmt f -> Fmt.pf fmt "-%t" f);
    Number.dec_real_support = `Default;
    Number.hex_real_support = `Unsupported;
    Number.frac_real_support =
      `Custom
        ( (fun fmt i -> Fmt.pf fmt "%s.0" i),
          fun fmt i n -> Fmt.pf fmt "(/ %s.0 %s.0)" i n );
  }

let print_ident info fmt id =
  Fmt.string fmt (Ident.id_unique info.info_printer id)

let rec print_type info fmt ty =
  match ty.Ty.ty_node with
  | Tyvar _ -> Printer.unsupportedType ty "VNN-LIB: unknown type variable"
  | Tyapp (ts, tyl) -> (
    match (Printer.query_syntax info.info_syn ts.ts_name, tyl) with
    | Some s, _ -> Printer.syntax_arguments s (print_type info) fmt tyl
    | None, [] -> print_ident info fmt ts.ts_name
    | None, _ ->
      Fmt.pf fmt "(%a %a)" (print_ident info) ts.ts_name
        (Pp.print_list Pp.space (print_type info))
        tyl)

let print_type_value info fmt = function
  | None -> Fmt.string fmt "Bool"
  | Some ty -> print_type info fmt ty

let rec print_term_axiom info fmt t =
  match t.Term.t_node with
  | Tconst c -> Constant.(print number_format unsupported_escape) fmt c
  | Tapp (ls, tl) -> (
    match Printer.query_syntax info.info_syn ls.ls_name with
    | Some s -> Printer.syntax_arguments s (print_term_axiom info) fmt tl
    | None -> (
      match (Term.Hls.find_opt info.variables ls, tl) with
      | Some s, [] -> Fmt.string fmt s
      | _ -> Printer.unsupportedTerm t "VNN-LIB: unknown variable(s)"))
  | _ -> Printer.unsupportedTerm t "VNN-LIB: not an axiom"

let print_conjunctive_term info fmt tl =
  let rec collect t =
    match t.Term.t_node with
    | Tapp _ -> [ t ]
    | Tbinop (Tand, t1, t2) -> collect t1 @ collect t2
    | _ -> Printer.unsupportedTerm t "VNN-LIB: not a conjunctive term"
  in
  let tl = List.map collect tl in
  List.iter
    (Fmt.pf fmt "@[<v>(and %a)@ @]" (Fmt.list (print_term_axiom info)))
    tl

let print_dnf_term info fmt t =
  let rec collect t =
    match t.Term.t_node with
    | Tapp _ | Tbinop (Tand, _, _) -> [ t ]
    | Tbinop (Tor, t1, t2) -> collect t1 @ collect t2
    | _ -> Printer.unsupportedTerm t "VNN-LIB: not a disjunctive term"
  in
  let tl = collect t in
  Fmt.pf fmt "@[%a@]" (print_conjunctive_term info) tl

let rec print_top_level_term info fmt t =
  match t.Term.t_node with
  | Tapp _ -> Fmt.pf fmt "@[(assert %a)@]" (print_term_axiom info) t
  | Tbinop (Tor, _, _) ->
    Fmt.pf fmt "@[<hov2>(assert@ @[<hv2>(or@ %a)@])@]" (print_dnf_term info) t
  | Tbinop (Tand, t1, t2) ->
    Fmt.pf fmt "@[<v>%a@ %a@]"
      (print_top_level_term info)
      t1
      (print_top_level_term info)
      t2
  | _ -> Printer.unsupportedTerm t "VNN-LIB: not a top-level term"

(* We do not print unknown symbols: returns false if not a known syntax or
   variable. *)
let t_is_known info =
  Term.t_s_all
    (fun _ -> true)
    (fun ls ->
      Ident.Mid.mem ls.ls_name info.info_syn || Term.Hls.mem info.variables ls)

let print_axiom info fmt (pr, t) =
  if t_is_known info t
  then
    Fmt.pf fmt ";; %s@\n@[%a@]@\n\n" pr.Decl.pr_name.id_string
      (print_top_level_term info)
      t

let print_goal info fmt (pr, t) =
  if t_is_known info t
  then
    Fmt.pf fmt ";; Goal %s@\n@[%a@]@\n" pr.Decl.pr_name.id_string
      (print_top_level_term info)
      t

let rec negate_term info t =
  match t.Term.t_node with
  | Tnot t -> t
  | Tbinop (Tand, t1, t2) ->
    Term.t_or (negate_term info t1) (negate_term info t2)
  | Tbinop (Tor, t1, t2) ->
    Term.t_and (negate_term info t1) (negate_term info t2)
  | Tapp (ls, [ t1; t2 ]) ->
    let tt = [ t1; t2 ] in
    (* Negate float relational symbols. *)
    let ls_neg =
      if Term.ls_equal ls info.ls_rel_float.le
      then info.ls_rel_float.gt
      else if Term.ls_equal ls info.ls_rel_float.lt
      then info.ls_rel_float.ge
      else if Term.ls_equal ls info.ls_rel_float.ge
      then info.ls_rel_float.lt
      else if Term.ls_equal ls info.ls_rel_float.gt
      then info.ls_rel_float.le
      else ls
    in
    (* Negate real relational symbols. *)
    let ls_neg =
      if Term.ls_equal ls info.ls_rel_real.le
      then info.ls_rel_real.gt
      else if Term.ls_equal ls info.ls_rel_real.lt
      then info.ls_rel_real.ge
      else if Term.ls_equal ls info.ls_rel_real.ge
      then info.ls_rel_real.lt
      else if Term.ls_equal ls info.ls_rel_real.gt
      then info.ls_rel_real.le
      else ls_neg
    in
    if Term.ls_equal ls_neg ls
    then Printer.unsupportedTerm t "VNN-LIB: cannot negate term"
    else Term.ps_app ls_neg tt
  | _ -> Printer.unsupportedTerm t "VNN-LIB: cannot negate term"

(* A term is printable whenever it is a conjunction of terms, each in
   disjunctive normal form. *)
let rec is_printable_term t =
  let rec is_cnj_only t =
    match t.Term.t_node with
    | Tbinop (Tand, t1, t2) -> is_cnj_only t1 && is_cnj_only t2
    | Tapp _ -> true
    | _ -> false
  in
  let rec is_dnf t =
    match t.Term.t_node with
    | Tbinop (Tor, t1, t2) -> is_dnf t1 && is_dnf t2
    | Tbinop (Tand, t1, t2) -> is_cnj_only t1 && is_cnj_only t2
    | Tapp _ -> true
    | _ -> false
  in
  match t.Term.t_node with
  | Tbinop (Tand, t1, t2) -> is_printable_term t1 && is_printable_term t2
  | Tbinop (Tor, t1, t2) -> is_dnf t1 && is_dnf t2
  | Tapp _ -> true
  | _ -> false

let print_prop_decl info fmt prop_kind pr t =
  match prop_kind with
  | Decl.Plemma -> assert false
  | Decl.Paxiom -> print_axiom info fmt (pr, t)
  | Decl.Pgoal ->
    let neg_t = negate_term info t in
    let neg_t =
      if is_printable_term neg_t
      then neg_t
      else
        (* Make it printable by turning it into conjunctive normal form. *)
        Why3.(Term.t_and_l (Split_goal.split_pos_full neg_t))
    in
    print_goal info fmt (pr, neg_t)

let print_param_decl info fmt ls =
  match Term.Hls.find_opt info.variables ls with
  | None -> ()
  | Some s ->
    Fmt.pf fmt ";; %s@\n" s;
    (* FIXME: The type should not be hardcoded as 'Real', but printed as: *)
    (* Fmt.pf fmt "@[(declare-const %s %a)@]@\n@\n" s (print_type_value info) *)
    (*   ls.ls_value *)
    Fmt.pf fmt "@[(declare-const %s Real)@]@\n@\n" s

let print_decl info fmt d =
  match d.Decl.d_node with
  | Dtype _ | Ddata _ | Dlogic _ | Dind _ -> ()
  | Dparam ls -> print_param_decl info fmt ls
  | Dprop (k, pr, f) -> print_prop_decl info fmt k pr f

let rec print_tdecl info fmt task =
  match task with
  | None -> ()
  | Some { Task.task_prev; task_decl; _ } -> (
    print_tdecl info fmt task_prev;
    match task_decl.Theory.td_node with
    | Use _ | Clone _ -> ()
    | Meta _ -> ()
    | Decl d -> print_decl info fmt d)

let print_task args ?old:_ fmt task =
  let ls_rel_real =
    let th = Symbols.Real.create args.Printer.env in
    { le = th.le; ge = th.ge; lt = th.lt; gt = th.gt }
  in
  let ls_rel_float =
    let th = Symbols.Float64.create args.Printer.env in
    { le = th.le; ge = th.ge; lt = th.lt; gt = th.gt }
  in
  let info =
    {
      ls_rel_real;
      ls_rel_float;
      info_printer = ident_printer ();
      info_syn = Discriminate.get_syntax_map task;
      variables = Term.Hls.create 10;
    }
  in
  Printer.print_prelude fmt args.Printer.prelude;
  Meta.collect_meta_input_output info.variables task
    ~input_encoding:(Fmt.str "X_%i") ~output_encoding:(Fmt.str "Y_%i");
  print_tdecl info fmt task

let init () =
  Printer.register_printer ~desc:"Printer@ for@ the@ VNN-LIB@ format." "vnnlib"
    print_task
