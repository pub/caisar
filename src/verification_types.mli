(**************************************************************************)
(*                                                                        *)
(*  This file is part of CAISAR.                                          *)
(*                                                                        *)
(*  Copyright (C) 2025                                                    *)
(*    CEA (Commissariat à l'énergie atomique et aux énergies              *)
(*         alternatives)                                                  *)
(*                                                                        *)
(*  You can redistribute it and/or modify it under the terms of the GNU   *)
(*  Lesser General Public License as published by the Free Software       *)
(*  Foundation, version 2.1.                                              *)
(*                                                                        *)
(*  It is distributed in the hope that it will be useful,                 *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of        *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          *)
(*  GNU Lesser General Public License for more details.                   *)
(*                                                                        *)
(*  See the GNU Lesser General Public License version 2.1                 *)
(*  for more details (enclosed in the file licenses/LGPLv2.1).            *)
(*                                                                        *)
(**************************************************************************)

open Why3

module File : sig
  type t = private
    | Stdin
    | File of string

  val of_string : string -> (t, string) result
  val pretty : Format.formatter -> t -> unit
end

module Problem : sig
  type t =
    | GenericProblem of {
        filepath : File.t;
        (* Filepath of the specification. *)
        definitions : (string * string) list;
        (* Toplevel definitions for the problem. *)
        theories : string list; (* List of the only theories to verify. *)
        goals : (string * string list) list;
          (* List of the only goals to verify. *)
      }
  [@@deriving of_yojson { strict = false }, show]

  val of_file :
    ?definitions:(string * string) list ->
    ?theories:string list ->
    ?goals:(string * string list) list ->
    File.t ->
    t
  (** [of_file defs goals file] builds a {!Problem.t} to be fed with a query to
      CAISAR.

      @param definitions is a key:value list defining toplevel constants.
      @param theories
        identifies the theories whose goals are the only ones to verify.
      @param goals
        is a theory:goals list each identifying only the goals of a theory to
        verify. *)
end

module Query : sig
  type t = {
    id : string;
    prover : Prover.t;
    prover_altern : string option;
    problem : Problem.t;
    loadpath : string list;
    time_limit : int option;
    memory_limit : int option;
    onnx_out_dir : string option;
    output_file : string option;
    dataset : string option;
  }
  [@@deriving of_yojson { strict = false }, show]

  val pretty : Format.formatter -> t -> unit

  val make :
    loadpath:string list ->
    ?memlimit:string ->
    ?timelimit:string ->
    ?onnx_out_dir:string ->
    ?dataset:string ->
    Prover.t ->
    ?prover_altern:string ->
    Problem.t ->
    t
  (** [make id loadpath problem time_limit memory_limit onnx_out_dir dataset output_file prover prover_altern problem]
      creates a verification query.

      @param loadpath
        is the additional loadpath where CAISAR will look to resolve files.
      @param memlimit is the memory limit granted to the verification.
      @param timelimit is the timeout granted to the verification.
      @param onnx_out_dir
        is the directory in which to write the ONNX files generated from the
        NIR.
      @param prover is the {!Prover.t} for the verification query.
      @param prover_altern
        is an optional alternative [prover] configuration, as specified in the
        file [caisar_detection.conf].
      @param problem is a {!Problem.t} value. *)

  val of_json :
    ?memlimit:string -> ?timelimit:string -> ?outfile:string -> string -> t
  (** Create a query from a JSON file.

      @param memlimit is the memory limit granted to the verification.
      @param timelimit is the timeout granted to the verification.
      @param outfile is the output file to store the result of the query. *)

  val memlimit_of_string : string -> int (* TO BE REMOVED *)
  val timelimit_of_string : string -> int (* TO BE REMOVED *)
end

(** The answer of a verification query by CAISAR. *)
module Answer : sig
  type t = private {
    id : string;
    query_id : string;
    problem_answer : problem_answer;
  }
  [@@deriving to_yojson, show]

  and prover_answer = Call_provers.prover_answer =
    | Valid
    | Invalid
    | Timeout
    | OutOfMemory
    | StepLimitExceeded
    | Unknown of string
    | Failure of string
    | HighFailure
  [@@deriving to_yojson, show]

  and problem_answer =
    | LegacyDatasetAnswer of {
        id : Why3.Decl.prsymbol;
        prover_answer : prover_answer;
        percentage_valid : float option;
        dataset_results : prover_answer list;
        additional_info : string option;
      }
    | GenericProblemAnswer of {
        id : Why3.Decl.prsymbol;
        prover_answer : prover_answer;
        additional_info : string option;
          (** Additional information provided by the prover. *)
      }
  [@@deriving to_yojson, show]

  val make : query:Query.t -> problem_answer -> t
  val pretty : Format.formatter -> t -> unit
end
