(**************************************************************************)
(*                                                                        *)
(*  This file is part of CAISAR.                                          *)
(*                                                                        *)
(*  Copyright (C) 2025                                                    *)
(*    CEA (Commissariat à l'énergie atomique et aux énergies              *)
(*         alternatives)                                                  *)
(*                                                                        *)
(*  You can redistribute it and/or modify it under the terms of the GNU   *)
(*  Lesser General Public License as published by the Free Software       *)
(*  Foundation, version 2.1.                                              *)
(*                                                                        *)
(*  It is distributed in the hope that it will be useful,                 *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of        *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          *)
(*  GNU Lesser General Public License for more details.                   *)
(*                                                                        *)
(*  See the GNU Lesser General Public License version 2.1                 *)
(*  for more details (enclosed in the file licenses/LGPLv2.1).            *)
(*                                                                        *)
(**************************************************************************)

val meta_input : Why3.Theory.meta
(** Indicates an input position among the inputs of the neural network. *)

val meta_output : Why3.Theory.meta
(** Indicates an output position among the outputs of the neural network. *)

val meta_nn_filename : Why3.Theory.meta
(** The filename of the neural network. *)

val meta_svm_filename : Why3.Theory.meta
(** The filename and abstraction of the support vector machine. *)

val meta_dataset_filename : Why3.Theory.meta
(** The filename of the dataset. *)

val collect_meta_input_output :
  input_encoding:(int -> 'a) ->
  output_encoding:(int -> 'a) ->
  'a Why3.Term.Hls.t ->
  Why3.Task.task ->
  unit
(** [collect_meta_input_output ~input_encoding ~output_encoding hls task] adds
    to [hls] all the input and output encoding strings. *)
