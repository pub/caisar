(**************************************************************************)
(*                                                                        *)
(*  This file is part of CAISAR.                                          *)
(*                                                                        *)
(*  Copyright (C) 2025                                                    *)
(*    CEA (Commissariat à l'énergie atomique et aux énergies              *)
(*         alternatives)                                                  *)
(*                                                                        *)
(*  You can redistribute it and/or modify it under the terms of the GNU   *)
(*  Lesser General Public License as published by the Free Software       *)
(*  Foundation, version 2.1.                                              *)
(*                                                                        *)
(*  It is distributed in the hope that it will be useful,                 *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of        *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          *)
(*  GNU Lesser General Public License for more details.                   *)
(*                                                                        *)
(*  See the GNU Lesser General Public License version 2.1                 *)
(*  for more details (enclosed in the file licenses/LGPLv2.1).            *)
(*                                                                        *)
(**************************************************************************)

val apply_classic_prover_strategy :
  Why3.Env.env -> Why3.Task.task -> Why3.Task.task list
(** Detect and translate applications of neural networks into SMT-LIB. *)

val apply_native_nn_prover_strategy :
  Why3.Env.env -> Why3.Task.task -> Why3.Task.task list
(** First detect and split on dataset, if any, intended for neural networks.
    Then, detect and execute applications of neural networks. *)

val apply_svm_prover_strategy : Why3.Task.task -> Why3.Task.task list
(** Detect and split on dataset, if any, intended for support vector machines. *)
