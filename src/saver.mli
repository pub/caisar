(**************************************************************************)
(*                                                                        *)
(*  This file is part of CAISAR.                                          *)
(*                                                                        *)
(*  Copyright (C) 2025                                                    *)
(*    CEA (Commissariat à l'énergie atomique et aux énergies              *)
(*         alternatives)                                                  *)
(*                                                                        *)
(*  You can redistribute it and/or modify it under the terms of the GNU   *)
(*  Lesser General Public License as published by the Free Software       *)
(*  Foundation, version 2.1.                                              *)
(*                                                                        *)
(*  It is distributed in the hope that it will be useful,                 *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of        *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          *)
(*  GNU Lesser General Public License for more details.                   *)
(*                                                                        *)
(*  See the GNU Lesser General Public License version 2.1                 *)
(*  for more details (enclosed in the file licenses/LGPLv2.1).            *)
(*                                                                        *)
(**************************************************************************)

type answer = {
  prover_answer : Why3.Call_provers.prover_answer;
  nb_total : int;  (** Total number of data points. *)
  nb_proved : int;  (** Number of data points verifying the property. *)
}

val call_prover :
  Why3.Call_provers.resource_limit ->
  Why3.Whyconf.main ->
  Why3.Whyconf.config_prover ->
  ?abstraction:Language.svm_abstraction ->
  svm:string ->
  dataset:Csv.t ->
  perturbations:(float * float) list ->
  unit ->
  answer
