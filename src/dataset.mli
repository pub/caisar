(**************************************************************************)
(*                                                                        *)
(*  This file is part of CAISAR.                                          *)
(*                                                                        *)
(*  Copyright (C) 2025                                                    *)
(*    CEA (Commissariat à l'énergie atomique et aux énergies              *)
(*         alternatives)                                                  *)
(*                                                                        *)
(*  You can redistribute it and/or modify it under the terms of the GNU   *)
(*  Lesser General Public License as published by the Free Software       *)
(*  Foundation, version 2.1.                                              *)
(*                                                                        *)
(*  It is distributed in the hope that it will be useful,                 *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of        *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          *)
(*  GNU Lesser General Public License for more details.                   *)
(*                                                                        *)
(*  See the GNU Lesser General Public License version 2.1                 *)
(*  for more details (enclosed in the file licenses/LGPLv2.1).            *)
(*                                                                        *)
(**************************************************************************)

open Why3

type eps [@@deriving yojson, show]
type threshold [@@deriving yojson, show]

val string_of_threshold : threshold -> string

type amplitude_params = {
  start : int;
  stop : int;
  step : int;
}
[@@deriving yojson, show]

val string_of_amplitude_params : amplitude_params -> string

type amplitude = amplitude_params option [@@deriving yojson, show]

type aimos_params = {
  perturbation : string;
  g_load_fn : string;
  amplitude : amplitude;
}
[@@deriving yojson, show]

type property =
  | Correct
  | Robust of eps
  | CondRobust of eps
  | MetaRobust of threshold * aimos_params
  | MetaRobustMin of threshold * aimos_params
  | MetaRobustDiff of threshold * aimos_params
[@@deriving yojson, show]

type ('model, 'dataset) predicate = private {
  model : 'model;
  dataset : 'dataset;
  property : property;
}

val interpret_predicate :
  Env.env ->
  on_model:(Term.lsymbol -> 'model) ->
  on_dataset:(Term.lsymbol -> 'dataset) ->
  Task.task ->
  ('model, 'dataset) predicate
