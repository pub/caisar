(**************************************************************************)
(*                                                                        *)
(*  This file is part of CAISAR.                                          *)
(*                                                                        *)
(*  Copyright (C) 2025                                                    *)
(*    CEA (Commissariat à l'énergie atomique et aux énergies              *)
(*         alternatives)                                                  *)
(*                                                                        *)
(*  You can redistribute it and/or modify it under the terms of the GNU   *)
(*  Lesser General Public License as published by the Free Software       *)
(*  Foundation, version 2.1.                                              *)
(*                                                                        *)
(*  It is distributed in the hope that it will be useful,                 *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of        *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          *)
(*  GNU Lesser General Public License for more details.                   *)
(*                                                                        *)
(*  See the GNU Lesser General Public License version 2.1                 *)
(*  for more details (enclosed in the file licenses/LGPLv2.1).            *)
(*                                                                        *)
(**************************************************************************)

(** {1 Datasets} *)

(** {2 Generic Datasets} *)

theory Dataset

  use caisar.types.Vector

  type a
  type b
  type dataset = vector (a, b)

  function read_dataset (f: string) : dataset

  predicate forall_ (d: dataset) (f: a -> b -> bool) =
    Vector.forall_ d (fun e -> let a, b = e in f a b)

  function foreach (d: dataset) (f: a -> b -> 'c) : vector 'c =
    Vector.foreach d (fun e -> let a, b = e in f a b)

end

(** {2 CSV Datasets}

A dataset in CSV format is such that each element is given as:
- first column is the label,
- rest of the columns are the vector features.

*)

theory CSV

  use caisar.types.IntWithBounds as Label
  use caisar.types.VectorFloat64 as FeatureVector

  type label_ = Label.t
  type features = FeatureVector.t

  clone export Dataset with type a = label_, type b = features

  function infer_min_label (d: dataset) : label_
  function infer_max_label (d: dataset) : label_

end
