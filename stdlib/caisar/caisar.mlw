(**************************************************************************)
(*                                                                        *)
(*  This file is part of CAISAR.                                          *)
(*                                                                        *)
(*  Copyright (C) 2025                                                    *)
(*    CEA (Commissariat à l'énergie atomique et aux énergies              *)
(*         alternatives)                                                  *)
(*                                                                        *)
(*  You can redistribute it and/or modify it under the terms of the GNU   *)
(*  Lesser General Public License as published by the Free Software       *)
(*  Foundation, version 2.1.                                              *)
(*                                                                        *)
(*  It is distributed in the hope that it will be useful,                 *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of        *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          *)
(*  GNU Lesser General Public License for more details.                   *)
(*                                                                        *)
(*  See the GNU Lesser General Public License version 2.1                 *)
(*  for more details (enclosed in the file licenses/LGPLv2.1).            *)
(*                                                                        *)
(**************************************************************************)

theory DatasetClassification
  use ieee_float.Float64
  use int.Int
  use array.Array
  use option.Option

  type features = array t
  type label_ = int
  type record = (features, label_)
  type dataset = array record
  type amplitude_record = { start: int; stop: int; step: int }
  type amplitude = option amplitude_record

  constant dataset: dataset

  type model

  function predict: model -> features -> label_
end

theory DatasetClassificationProps
  use ieee_float.Float64
  use int.Int
  use array.Array
  use DatasetClassification

  predicate meta_robust (m: model) (d: dataset) (threshold: t) (perturbation: string) (g_load_fn: string) (ampli: amplitude)
  predicate meta_robust_min (m: model) (d: dataset) (threshold: t) (perturbation: string) (g_load_fn: string) (ampli: amplitude)
  predicate meta_robust_diff (m: model) (d: dataset) (threshold: t) (perturbation: string) (g_load_fn: string) (ampli: amplitude)
end

theory NN
  (** Module defining commonly-met operations in neural networks. *)
  use ieee_float.Float64

  type input_type = t

  function relu (a: t): t =  if a .> (0.0:t) then a else (0.0:t)
end
