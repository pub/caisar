Test verify
  $ cat - > test_data.csv << EOF
  > 4.000000000000000000e+02,4.000000000000000222e-01,-3.139370431367570990e+00,2.333333333333333428e+02,0.000000000000000000e+00
  > 2.500000000000000000e+02,2.000000000000000111e-01,-3.138814875812015348e+00,2.333333333333333428e+02,0.000000000000000000e+00
  > 2.500000000000000000e+02,2.222222222222222376e-01,-3.138814875812015348e+00,2.333333333333333428e+02,0.000000000000000000e+00
  > EOF

  $ . ./setup_env.sh

  $ bin/aimos --version
  AIMOS 1.0

  $ export TMPDIR=$(pwd)/tmp
  $ mkdir $TMPDIR

  $ caisar verify -L . --format whyml --prover=AIMOS --dataset=test_data.csv --ltag=AIMOS - 2>&1 <<EOF
  > theory META_ROBUST
  >   use TestNetwork.AsArray
  >   use ieee_float.Float64
  >   use caisar.caisar.DatasetClassification
  >   use caisar.caisar.DatasetClassificationProps
  >   use option.Option
  > 
  >   goal G1: meta_robust_min model dataset (1.0:t) "reluplex_rotation" "" None
  >   goal G2: meta_robust_min model dataset (1.0:t) "gaussian_blur" "" (Some { start = 1; stop = 10; step = 2; })
  > end
  > EOF
  [DEBUG]{AIMOS} AIMOS configuration:
  options:
    plot: false
    inputs_path: $TESTCASE_ROOT/test_data.csv
    transformations:
    - name: reluplex_rotation
    custom_t_path: ../../../../../../install/default/share/caisar/config/aimos_custom_transformations.py
  models:
  - defaults:
      models_path: ../../../../../../default/tests/TestNetwork.nnet
      out_mode: classif_min
    customs:
      custom_global_load:
      custom_f_path: ../../../../../../install/default/share/caisar/config/aimos_custom_functions.py
  
  [DEBUG]{AIMOS} AIMOS configuration:
  options:
    plot: false
    inputs_path: $TESTCASE_ROOT/test_data.csv
    transformations:
    - name: gaussian_blur
      fn_range: range(1, 10, 2)
    custom_t_path: ../../../../../../install/default/share/caisar/config/aimos_custom_transformations.py
  models:
  - defaults:
      models_path: ../../../../../../default/tests/TestNetwork.nnet
      out_mode: classif_min
    customs:
      custom_global_load:
      custom_f_path: ../../../../../../install/default/share/caisar/config/aimos_custom_functions.py
  
  Goal G1: Valid
  Goal G2: Valid
