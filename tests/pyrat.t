Test verify
  $ . ./setup_env.sh

  $ bin/pyrat.py --version
  PyRAT 1.1

  $ caisar verify --format whyml --prover=PyRAT --ltag=ProverSpec - <<EOF
  > theory PyRAT
  >   use ieee_float.Float64
  >   use caisar.types.Vector
  >   use caisar.model.Model
  >  
  > 
  >   constant nn: model = read_model "TestNetwork.nnet"
  > 
  >   goal G:
  >     forall i: vector t.
  >       has_length i 5 ->
  >       (0.0:t) .<= i[0] .<= (0.5:t) ->
  >       (0.0:t) .< (nn @@ i)[0] .< (0.5:t)
  > 
  >   goal H:
  >     forall i: vector t.
  >       has_length i 5 ->
  >       ((0.0:t) .<= i[0] .<= (0.5:t) \/ (0.375:t) .<= i[0] .<= (0.75:t)) /\ (0.5:t) .<= i[1] .<= (1.0:t) ->
  >       ((0.0:t) .< (nn @@ i)[0] \/ (0.5:t) .< (nn @@ i)[0]) /\ (0.0:t) .< (nn @@ i)[1] .< (0.5:t)
  > end
  > EOF
  [DEBUG]{ProverSpec} Prover-tailored specification:
  0.0 <= x0
  x0 <= 0.5
  0.0 <  y0 and y0 <  0.5
  
  [DEBUG]{ProverSpec} Prover-tailored specification:
  0.0 <= x0
  x0 <= 0.5
  0.5 <= x1
  x1 <= 1.0
  (0.0 <  y0 or 0.5 <  y0) and 0.0 <  y1 and y1 <  0.5
  
  [DEBUG]{ProverSpec} Prover-tailored specification:
  0.375 <= x0
  x0 <= 0.75
  0.5 <= x1
  x1 <= 1.0
  (0.0 <  y0 or 0.5 <  y0) and 0.0 <  y1 and y1 <  0.5
  
  Goal G: Unknown ()
  Goal H: Unknown ()
