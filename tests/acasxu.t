Test verify on acasxu
  $ . ./setup_env.sh

  $ bin/pyrat.py --version
  PyRAT 1.1

  $ cat > file.mlw <<EOF
  > theory T
  >   use ieee_float.Float64
  >   use int.Int
  >   use caisar.types.Vector
  >   use caisar.model.Model
  >  
  > 
  >   constant nn: model = read_model "TestNetwork.nnet"
  >   let constant nn_onnx: model = read_model "../examples/acasxu/nets/onnx/ACASXU_1_1.onnx"
  >   let constant nn_onnx_1_1: model = read_model "../examples/acasxu/nets/onnx/ACASXU_1_1.onnx"
  >   let constant nn_onnx_1_9: model = read_model "../examples/acasxu/nets/onnx/ACASXU_1_9.onnx"
  > 
  >   type input = vector t
  > 
  >   let constant distance_to_intruder: int = 0
  >   let constant angle_to_intruder: int = 1
  >   let constant intruder_heading: int = 2
  >   let constant speed: int = 3
  >   let constant intruder_speed: int = 4
  > 
  >   type action = int
  > 
  >   let constant clear_of_conflict: action = 0
  >   let constant weak_left: action = 1
  >   let constant weak_right: action = 2
  >   let constant strong_left: action = 3
  >   let constant strong_right: action = 4
  > 
  >   let constant pi: t = 3.141592653589793115997963468544185161590576171875000
  > 
  >   predicate valid_input (i: input) =
  >     (0.0:t) .<= i[distance_to_intruder] .<= (60760.0:t)
  >     /\ .- pi .<= i[angle_to_intruder] .<= pi
  >     /\ .- pi .<= i[intruder_heading] .<= pi
  >     /\ (100.0:t) .<= i[speed] .<= (1200.0:t)
  >     /\ (0.0:t) .<= i[intruder_speed] .<= (1200.0:t)
  > 
  >   predicate valid_action (a: action) = clear_of_conflict <= a <= strong_right
  > 
  >   predicate advises (n: model) (i: input) (a: action) =
  >     valid_action a ->
  >     forall b: action.
  >       valid_action b ->  a <> b -> (n@@i)[a] .< (n@@i)[b]
  > 
  >   predicate intruder_distant_and_slow (i: input) =
  >     i[distance_to_intruder] .>= (55947.6909999999988940544426441192626953125:t)
  >     /\ i[speed] .>= (1145.0:t)
  >     /\ i[intruder_speed] .<= (60.0:t)
  > 
  >   let function denormalize_t (i: t) (mean: t) (range: t) : t = (i .* range) .+ mean
  > 
  >   function denormalize_by_index (idx: int) (t: t) : t =
  >     if idx = distance_to_intruder then denormalize_t t (19791.0:t) (60261.0:t)
  >     else if idx = angle_to_intruder then denormalize_t t (0.0:t) (6.2831853071800001231395071954466402530670166015625:t)
  >     else if idx = intruder_heading then denormalize_t t (0.0:t) (6.2831853071800001231395071954466402530670166015625:t)
  >     else if idx = speed then denormalize_t t (650.0:t) (1100.0:t)
  >     else if idx = intruder_speed then denormalize_t t (600.0:t) (1200.0:t)
  >     else t
  > 
  >   function denormalize_input (i:input) : input =
  >     Vector.mapi i denormalize_by_index
  > 
  >   let function denormalize_output (o: t) : t =
  >     denormalize_t o (7.51888402010059753166615337249822914600372314453125:t) (373.9499200000000200816430151462554931640625:t)
  > 
  >   let function normalize_t (i: t) (mean: t) (range: t) : t = ((i .- mean) ./ range)
  > 
  >   let function normalize_by_index (idx: int) (t: t) : t =
  >     if idx = distance_to_intruder then normalize_t t (19791.0:t) (60261.0:t)
  >     else if idx = angle_to_intruder then normalize_t t (0.0:t) (6.2831853071800001231395071954466402530670166015625:t)
  >     else if idx = intruder_heading then normalize_t t (0.0:t) (6.2831853071800001231395071954466402530670166015625:t)
  >     else if idx = speed then normalize_t t (650.0:t) (1100.0:t)
  >     else if idx = intruder_speed then normalize_t t (600.0:t) (1200.0:t)
  >     else t
  > 
  >   let function normalize_input (i:input) : input =
  >     Vector.mapi i normalize_by_index
  > 
  >   let runP1 (j: input)
  >     requires { has_length j 5 }
  >     requires { valid_input j }
  >     requires { intruder_distant_and_slow j }
  >     ensures { result .<= (1500.0:t) }  =
  >       let i = normalize_input j in
  >       let o = (nn_onnx @@ i)[clear_of_conflict] in
  >       (denormalize_output o)
  > 
  >   let runP1v2 (j: input)
  >     requires { has_length j 5 }
  >     requires { valid_input j }
  >     requires { intruder_distant_and_slow j }
  >     ensures { let o1, o2 = result in o1 .<= (1500.0:t) /\ o2 .<= (1500.0:t) }  =
  >       let i = normalize_input j in
  >       let o1 = (nn_onnx_1_1 @@ i)[clear_of_conflict] in
  >       let o2 = (nn_onnx_1_9 @@ i)[clear_of_conflict] in
  >       (denormalize_output o1), (denormalize_output o2)
  > 
  >   let run2 (j: input)
  >     requires { has_length j 5 }
  >     requires { valid_input j }
  >     requires { intruder_distant_and_slow j }
  >     ensures { let o1, o2 = result in o1 .>= o2 /\ o2 .>= o1 }  =
  >       let i = normalize_input j in
  >       let o1 = (nn_onnx @@ i)[clear_of_conflict] in
  >       let o2 = (nn_onnx @@ i)[weak_left] in
  >       o1, o2
  > 
  >   let run2v2 (i: input) (j: input)
  >     requires { has_length i 5 }
  >     requires { has_length j 5 }
  >     requires { valid_input i }
  >     requires { intruder_distant_and_slow i }
  >     requires { intruder_distant_and_slow j }
  >     ensures { let o1, o2 = result in o1 .>= o2 /\ o2 .>= o1 }  =
  >       let i_ = normalize_input i in
  >       let j_ = normalize_input j in
  >       let o1 = (nn_onnx @@ i_)[clear_of_conflict] in
  >       let o2 = (nn_onnx @@ j_)[clear_of_conflict] in
  >       o1, o2
  > 
  >   let run3 (j: input) (eps:t)
  >     requires { has_length j 5 }
  >     requires { valid_input j }
  >     requires { intruder_distant_and_slow j }
  >     requires { (-1.:t) .<= eps .<= (1.:t) }
  >     ensures { let o1, o2 = result in o1 .>= o2 /\ o2 .>= o1 }  =
  >       let i = normalize_input j in
  >       let j2 = Vector.mapi j (fun i x -> if i = distance_to_intruder then x .+ eps else x) in
  >       let i2 = normalize_input j2 in
  >       let o1 = (nn_onnx @@ i)[clear_of_conflict] in
  >       let o2 = (nn_onnx @@ i2)[clear_of_conflict] in
  >       o1, o2
  > 
  >  let robust_output (j: input) (eps:t)
  >    requires { has_length j 5 }
  >    requires { valid_input j }
  >    requires { (0.0:t) .<= eps .<= (0.375:t) }
  >    ensures { let o1, o2 = result in o1[0] .- o2[0] .- eps .>= (0.0:t) /\ o1[0] .- o2[0] .- eps .<= (0.0:t) } =
  >      let i = normalize_input j in
  >      let o1 = (nn_onnx @@ i) in
  >      let o2 = (nn_onnx @@ i) in
  >      o1, o2
  > 
  >   goal P1:
  >     forall i: input.
  >       has_length i 5 ->
  >       let j = denormalize_input i in
  >       valid_input j /\ intruder_distant_and_slow j ->
  >       let o = (nn @@ i)[clear_of_conflict] in
  >       (denormalize_output o) .<= (1500.0:t)
  > 
  >   predicate directly_ahead (i: input) =
  >     (1500.0:t) .<= i[distance_to_intruder] .<= (1800.0:t)
  >     /\ .- (0.059999999999999997779553950749686919152736663818359375:t) .<= i[angle_to_intruder] .<= (0.059999999999999997779553950749686919152736663818359375:t)
  > 
  >   predicate moving_towards (i: input) =
  >     i[intruder_heading] .>= (3.100000000000000088817841970012523233890533447265625:t)
  >     /\ i[speed] .>= (980.0:t)
  >     /\ i[intruder_speed] .>= (960.0:t)
  > 
  >   goal P3:
  >     forall i: input.
  >       has_length i 5 ->
  >       let j = denormalize_input i in
  >       valid_input j /\ directly_ahead j /\ moving_towards j ->
  >       not (advises nn i clear_of_conflict)
  > end
  > EOF

  $ caisar verify --prover PyRAT --ltag=ProverSpec --ltag=InterpretGoal --ltag=NN-Gen-Term file.mlw
  [DEBUG]{InterpretGoal} Interpreted formula for goal 'runP1'vc':
  forall x:t, x1:t, x2:t, x3:t, x4:t.
   ((le (0.0:t) x /\ le x (60760.0:t)) /\
    (le (neg (3.141592653589793115997963468544185161590576171875:t)) x1 /\
     le x1 (3.141592653589793115997963468544185161590576171875:t)) /\
    (le (neg (3.141592653589793115997963468544185161590576171875:t)) x2 /\
     le x2 (3.141592653589793115997963468544185161590576171875:t)) /\
    (le (100.0:t) x3 /\ le x3 (1200.0:t)) /\ le (0.0:t) x4 /\ le x4 (1200.0:t)) /\
   le (55947.6909999999988940544426441192626953125:t) x /\
   le (1145.0:t) x3 /\ le x4 (60.0:t) ->
   le
   (add RNE
    (mul RNE
     (nn_onnx
      @@ vector (div RNE (sub RNE x (19791.0:t)) (60261.0:t))
         (div RNE (sub RNE x1 (0.0:t))
          (6.2831853071800001231395071954466402530670166015625:t))
         (div RNE (sub RNE x2 (0.0:t))
          (6.2831853071800001231395071954466402530670166015625:t))
         (div RNE (sub RNE x3 (650.0:t)) (1100.0:t))
         (div RNE (sub RNE x4 (600.0:t)) (1200.0:t)))
     [0] (373.9499200000000200816430151462554931640625:t))
    (7.51888402010059753166615337249822914600372314453125:t))
   (1500.0:t)
  nn_onnx,
  (Interpreter_types.Model
     (Interpreter_types.ONNX, { Language.nn_nb_inputs = 5; nn_nb_outputs = 5;
                                nn_ty_elt = t;
                                nn_filename =
                                "./../examples/acasxu/nets/onnx/ACASXU_1_1.onnx";
                                nn_format = <nir> }))
  vector,
  5
  [DEBUG]{NN-Gen-Term} new goal:le y (1500.0:t)
  [DEBUG]{ProverSpec} Prover-tailored specification:
  0.0 <= x0
  x0 <= 60760.0
  -3.141592653589793115997963468544185161590576171875 <= x1
  x1 <= 3.141592653589793115997963468544185161590576171875
  -3.141592653589793115997963468544185161590576171875 <= x2
  x2 <= 3.141592653589793115997963468544185161590576171875
  100.0 <= x3
  x3 <= 1200.0
  0.0 <= x4
  x4 <= 1200.0
  55947.6909999999988940544426441192626953125 <= x0
  1145.0 <= x3
  x4 <= 60.0
  y0 <= 1500.0
  
  [DEBUG]{InterpretGoal} Interpreted formula for goal 'runP1v2'vc':
  forall x:t, x1:t, x2:t, x3:t, x4:t.
   ((le (0.0:t) x /\ le x (60760.0:t)) /\
    (le (neg (3.141592653589793115997963468544185161590576171875:t)) x1 /\
     le x1 (3.141592653589793115997963468544185161590576171875:t)) /\
    (le (neg (3.141592653589793115997963468544185161590576171875:t)) x2 /\
     le x2 (3.141592653589793115997963468544185161590576171875:t)) /\
    (le (100.0:t) x3 /\ le x3 (1200.0:t)) /\ le (0.0:t) x4 /\ le x4 (1200.0:t)) /\
   le (55947.6909999999988940544426441192626953125:t) x /\
   le (1145.0:t) x3 /\ le x4 (60.0:t) ->
   le
   (add RNE
    (mul RNE
     (nn_onnx
      @@ vector (div RNE (sub RNE x (19791.0:t)) (60261.0:t))
         (div RNE (sub RNE x1 (0.0:t))
          (6.2831853071800001231395071954466402530670166015625:t))
         (div RNE (sub RNE x2 (0.0:t))
          (6.2831853071800001231395071954466402530670166015625:t))
         (div RNE (sub RNE x3 (650.0:t)) (1100.0:t))
         (div RNE (sub RNE x4 (600.0:t)) (1200.0:t)))
     [0] (373.9499200000000200816430151462554931640625:t))
    (7.51888402010059753166615337249822914600372314453125:t))
   (1500.0:t) /\
   le
   (add RNE
    (mul RNE
     (nn_onnx1
      @@ vector (div RNE (sub RNE x (19791.0:t)) (60261.0:t))
         (div RNE (sub RNE x1 (0.0:t))
          (6.2831853071800001231395071954466402530670166015625:t))
         (div RNE (sub RNE x2 (0.0:t))
          (6.2831853071800001231395071954466402530670166015625:t))
         (div RNE (sub RNE x3 (650.0:t)) (1100.0:t))
         (div RNE (sub RNE x4 (600.0:t)) (1200.0:t)))
     [0] (373.9499200000000200816430151462554931640625:t))
    (7.51888402010059753166615337249822914600372314453125:t))
   (1500.0:t)
  nn_onnx,
  (Interpreter_types.Model
     (Interpreter_types.ONNX, { Language.nn_nb_inputs = 5; nn_nb_outputs = 5;
                                nn_ty_elt = t;
                                nn_filename =
                                "./../examples/acasxu/nets/onnx/ACASXU_1_1.onnx";
                                nn_format = <nir> }))
  vector, 5
  nn_onnx1,
  (Interpreter_types.Model
     (Interpreter_types.ONNX, { Language.nn_nb_inputs = 5; nn_nb_outputs = 5;
                                nn_ty_elt = t;
                                nn_filename =
                                "./../examples/acasxu/nets/onnx/ACASXU_1_9.onnx";
                                nn_format = <nir> }))
  [DEBUG]{NN-Gen-Term} new goal:le y1 (1500.0:t) /\ le y2 (1500.0:t)
  [DEBUG]{ProverSpec} Prover-tailored specification:
  0.0 <= x0
  x0 <= 60760.0
  -3.141592653589793115997963468544185161590576171875 <= x1
  x1 <= 3.141592653589793115997963468544185161590576171875
  -3.141592653589793115997963468544185161590576171875 <= x2
  x2 <= 3.141592653589793115997963468544185161590576171875
  100.0 <= x3
  x3 <= 1200.0
  0.0 <= x4
  x4 <= 1200.0
  55947.6909999999988940544426441192626953125 <= x0
  1145.0 <= x3
  x4 <= 60.0
  y0 <= 1500.0 and y1 <= 1500.0
  
  [DEBUG]{InterpretGoal} Interpreted formula for goal 'run2'vc':
  forall x:t, x1:t, x2:t, x3:t, x4:t.
   ((le (0.0:t) x /\ le x (60760.0:t)) /\
    (le (neg (3.141592653589793115997963468544185161590576171875:t)) x1 /\
     le x1 (3.141592653589793115997963468544185161590576171875:t)) /\
    (le (neg (3.141592653589793115997963468544185161590576171875:t)) x2 /\
     le x2 (3.141592653589793115997963468544185161590576171875:t)) /\
    (le (100.0:t) x3 /\ le x3 (1200.0:t)) /\ le (0.0:t) x4 /\ le x4 (1200.0:t)) /\
   le (55947.6909999999988940544426441192626953125:t) x /\
   le (1145.0:t) x3 /\ le x4 (60.0:t) ->
   le
   (nn_onnx
    @@ vector (div RNE (sub RNE x (19791.0:t)) (60261.0:t))
       (div RNE (sub RNE x1 (0.0:t))
        (6.2831853071800001231395071954466402530670166015625:t))
       (div RNE (sub RNE x2 (0.0:t))
        (6.2831853071800001231395071954466402530670166015625:t))
       (div RNE (sub RNE x3 (650.0:t)) (1100.0:t))
       (div RNE (sub RNE x4 (600.0:t)) (1200.0:t)))
   [1]
   (nn_onnx
    @@ vector (div RNE (sub RNE x (19791.0:t)) (60261.0:t))
       (div RNE (sub RNE x1 (0.0:t))
        (6.2831853071800001231395071954466402530670166015625:t))
       (div RNE (sub RNE x2 (0.0:t))
        (6.2831853071800001231395071954466402530670166015625:t))
       (div RNE (sub RNE x3 (650.0:t)) (1100.0:t))
       (div RNE (sub RNE x4 (600.0:t)) (1200.0:t)))
   [0] /\
   le
   (nn_onnx
    @@ vector (div RNE (sub RNE x (19791.0:t)) (60261.0:t))
       (div RNE (sub RNE x1 (0.0:t))
        (6.2831853071800001231395071954466402530670166015625:t))
       (div RNE (sub RNE x2 (0.0:t))
        (6.2831853071800001231395071954466402530670166015625:t))
       (div RNE (sub RNE x3 (650.0:t)) (1100.0:t))
       (div RNE (sub RNE x4 (600.0:t)) (1200.0:t)))
   [0]
   (nn_onnx
    @@ vector (div RNE (sub RNE x (19791.0:t)) (60261.0:t))
       (div RNE (sub RNE x1 (0.0:t))
        (6.2831853071800001231395071954466402530670166015625:t))
       (div RNE (sub RNE x2 (0.0:t))
        (6.2831853071800001231395071954466402530670166015625:t))
       (div RNE (sub RNE x3 (650.0:t)) (1100.0:t))
       (div RNE (sub RNE x4 (600.0:t)) (1200.0:t)))
   [1]
  nn_onnx,
  (Interpreter_types.Model
     (Interpreter_types.ONNX, { Language.nn_nb_inputs = 5; nn_nb_outputs = 5;
                                nn_ty_elt = t;
                                nn_filename =
                                "./../examples/acasxu/nets/onnx/ACASXU_1_1.onnx";
                                nn_format = <nir> }))
  vector,
  5
  [DEBUG]{NN-Gen-Term} new goal:le y3 y4 /\ le y4 y3
  [DEBUG]{ProverSpec} Prover-tailored specification:
  0.0 <= x0
  x0 <= 60760.0
  -3.141592653589793115997963468544185161590576171875 <= x1
  x1 <= 3.141592653589793115997963468544185161590576171875
  -3.141592653589793115997963468544185161590576171875 <= x2
  x2 <= 3.141592653589793115997963468544185161590576171875
  100.0 <= x3
  x3 <= 1200.0
  0.0 <= x4
  x4 <= 1200.0
  55947.6909999999988940544426441192626953125 <= x0
  1145.0 <= x3
  x4 <= 60.0
  y1 <= y0 and y0 <= y1
  
  [DEBUG]{InterpretGoal} Interpreted formula for goal 'run2v2'vc':
  forall x:t, x1:t, x2:t, x3:t, x4:t, x5:t, x6:t, x7:t, x8:t, x9:t.
   ((le (0.0:t) x /\ le x (60760.0:t)) /\
    (le (neg (3.141592653589793115997963468544185161590576171875:t)) x1 /\
     le x1 (3.141592653589793115997963468544185161590576171875:t)) /\
    (le (neg (3.141592653589793115997963468544185161590576171875:t)) x2 /\
     le x2 (3.141592653589793115997963468544185161590576171875:t)) /\
    (le (100.0:t) x3 /\ le x3 (1200.0:t)) /\ le (0.0:t) x4 /\ le x4 (1200.0:t)) /\
   (le (55947.6909999999988940544426441192626953125:t) x /\
    le (1145.0:t) x3 /\ le x4 (60.0:t)) /\
   le (55947.6909999999988940544426441192626953125:t) x5 /\
   le (1145.0:t) x8 /\ le x9 (60.0:t) ->
   le
   (nn_onnx
    @@ vector (div RNE (sub RNE x5 (19791.0:t)) (60261.0:t))
       (div RNE (sub RNE x6 (0.0:t))
        (6.2831853071800001231395071954466402530670166015625:t))
       (div RNE (sub RNE x7 (0.0:t))
        (6.2831853071800001231395071954466402530670166015625:t))
       (div RNE (sub RNE x8 (650.0:t)) (1100.0:t))
       (div RNE (sub RNE x9 (600.0:t)) (1200.0:t)))
   [0]
   (nn_onnx
    @@ vector (div RNE (sub RNE x (19791.0:t)) (60261.0:t))
       (div RNE (sub RNE x1 (0.0:t))
        (6.2831853071800001231395071954466402530670166015625:t))
       (div RNE (sub RNE x2 (0.0:t))
        (6.2831853071800001231395071954466402530670166015625:t))
       (div RNE (sub RNE x3 (650.0:t)) (1100.0:t))
       (div RNE (sub RNE x4 (600.0:t)) (1200.0:t)))
   [0] /\
   le
   (nn_onnx
    @@ vector (div RNE (sub RNE x (19791.0:t)) (60261.0:t))
       (div RNE (sub RNE x1 (0.0:t))
        (6.2831853071800001231395071954466402530670166015625:t))
       (div RNE (sub RNE x2 (0.0:t))
        (6.2831853071800001231395071954466402530670166015625:t))
       (div RNE (sub RNE x3 (650.0:t)) (1100.0:t))
       (div RNE (sub RNE x4 (600.0:t)) (1200.0:t)))
   [0]
   (nn_onnx
    @@ vector (div RNE (sub RNE x5 (19791.0:t)) (60261.0:t))
       (div RNE (sub RNE x6 (0.0:t))
        (6.2831853071800001231395071954466402530670166015625:t))
       (div RNE (sub RNE x7 (0.0:t))
        (6.2831853071800001231395071954466402530670166015625:t))
       (div RNE (sub RNE x8 (650.0:t)) (1100.0:t))
       (div RNE (sub RNE x9 (600.0:t)) (1200.0:t)))
   [0]
  nn_onnx,
  (Interpreter_types.Model
     (Interpreter_types.ONNX, { Language.nn_nb_inputs = 5; nn_nb_outputs = 5;
                                nn_ty_elt = t;
                                nn_filename =
                                "./../examples/acasxu/nets/onnx/ACASXU_1_1.onnx";
                                nn_format = <nir> }))
  vector,
  5
  [DEBUG]{NN-Gen-Term} new goal:le y5 y6 /\ le y6 y5
  [DEBUG]{ProverSpec} Prover-tailored specification:
  0.0 <= x0
  x0 <= 60760.0
  -3.141592653589793115997963468544185161590576171875 <= x1
  x1 <= 3.141592653589793115997963468544185161590576171875
  -3.141592653589793115997963468544185161590576171875 <= x2
  x2 <= 3.141592653589793115997963468544185161590576171875
  100.0 <= x3
  x3 <= 1200.0
  0.0 <= x4
  x4 <= 1200.0
  55947.6909999999988940544426441192626953125 <= x0
  1145.0 <= x3
  x4 <= 60.0
  55947.6909999999988940544426441192626953125 <= x5
  1145.0 <= x8
  x9 <= 60.0
  y1 <= y0 and y0 <= y1
  
  [DEBUG]{InterpretGoal} Interpreted formula for goal 'run3'vc':
  forall x:t, x1:t, x2:t, x3:t, x4:t, eps:t.
   ((le (0.0:t) x /\ le x (60760.0:t)) /\
    (le (neg (3.141592653589793115997963468544185161590576171875:t)) x1 /\
     le x1 (3.141592653589793115997963468544185161590576171875:t)) /\
    (le (neg (3.141592653589793115997963468544185161590576171875:t)) x2 /\
     le x2 (3.141592653589793115997963468544185161590576171875:t)) /\
    (le (100.0:t) x3 /\ le x3 (1200.0:t)) /\ le (0.0:t) x4 /\ le x4 (1200.0:t)) /\
   (le (55947.6909999999988940544426441192626953125:t) x /\
    le (1145.0:t) x3 /\ le x4 (60.0:t)) /\
   le ((- 1.0):t) eps /\ le eps (1.0:t) ->
   le
   (nn_onnx
    @@ vector (div RNE (sub RNE (add RNE x eps) (19791.0:t)) (60261.0:t))
       (div RNE (sub RNE x1 (0.0:t))
        (6.2831853071800001231395071954466402530670166015625:t))
       (div RNE (sub RNE x2 (0.0:t))
        (6.2831853071800001231395071954466402530670166015625:t))
       (div RNE (sub RNE x3 (650.0:t)) (1100.0:t))
       (div RNE (sub RNE x4 (600.0:t)) (1200.0:t)))
   [0]
   (nn_onnx
    @@ vector (div RNE (sub RNE x (19791.0:t)) (60261.0:t))
       (div RNE (sub RNE x1 (0.0:t))
        (6.2831853071800001231395071954466402530670166015625:t))
       (div RNE (sub RNE x2 (0.0:t))
        (6.2831853071800001231395071954466402530670166015625:t))
       (div RNE (sub RNE x3 (650.0:t)) (1100.0:t))
       (div RNE (sub RNE x4 (600.0:t)) (1200.0:t)))
   [0] /\
   le
   (nn_onnx
    @@ vector (div RNE (sub RNE x (19791.0:t)) (60261.0:t))
       (div RNE (sub RNE x1 (0.0:t))
        (6.2831853071800001231395071954466402530670166015625:t))
       (div RNE (sub RNE x2 (0.0:t))
        (6.2831853071800001231395071954466402530670166015625:t))
       (div RNE (sub RNE x3 (650.0:t)) (1100.0:t))
       (div RNE (sub RNE x4 (600.0:t)) (1200.0:t)))
   [0]
   (nn_onnx
    @@ vector (div RNE (sub RNE (add RNE x eps) (19791.0:t)) (60261.0:t))
       (div RNE (sub RNE x1 (0.0:t))
        (6.2831853071800001231395071954466402530670166015625:t))
       (div RNE (sub RNE x2 (0.0:t))
        (6.2831853071800001231395071954466402530670166015625:t))
       (div RNE (sub RNE x3 (650.0:t)) (1100.0:t))
       (div RNE (sub RNE x4 (600.0:t)) (1200.0:t)))
   [0]
  nn_onnx,
  (Interpreter_types.Model
     (Interpreter_types.ONNX, { Language.nn_nb_inputs = 5; nn_nb_outputs = 5;
                                nn_ty_elt = t;
                                nn_filename =
                                "./../examples/acasxu/nets/onnx/ACASXU_1_1.onnx";
                                nn_format = <nir> }))
  vector,
  5
  [DEBUG]{NN-Gen-Term} new goal:le y7 y8 /\ le y8 y7
  [DEBUG]{ProverSpec} Prover-tailored specification:
  0.0 <= x0
  x0 <= 60760.0
  -3.141592653589793115997963468544185161590576171875 <= x1
  x1 <= 3.141592653589793115997963468544185161590576171875
  -3.141592653589793115997963468544185161590576171875 <= x2
  x2 <= 3.141592653589793115997963468544185161590576171875
  100.0 <= x3
  x3 <= 1200.0
  0.0 <= x4
  x4 <= 1200.0
  55947.6909999999988940544426441192626953125 <= x0
  1145.0 <= x3
  x4 <= 60.0
  -1.0 <= x5
  x5 <= 1.0
  y0 <= y1 and y1 <= y0
  
  [DEBUG]{InterpretGoal} Interpreted formula for goal 'robust_output'vc':
  forall x:t, x1:t, x2:t, x3:t, x4:t, eps:t.
   ((le (0.0:t) x /\ le x (60760.0:t)) /\
    (le (neg (3.141592653589793115997963468544185161590576171875:t)) x1 /\
     le x1 (3.141592653589793115997963468544185161590576171875:t)) /\
    (le (neg (3.141592653589793115997963468544185161590576171875:t)) x2 /\
     le x2 (3.141592653589793115997963468544185161590576171875:t)) /\
    (le (100.0:t) x3 /\ le x3 (1200.0:t)) /\ le (0.0:t) x4 /\ le x4 (1200.0:t)) /\
   le (0.0:t) eps /\ le eps (0.375:t) ->
   le (0.0:t)
   (sub RNE
    (sub RNE
     (nn_onnx
      @@ vector (div RNE (sub RNE x (19791.0:t)) (60261.0:t))
         (div RNE (sub RNE x1 (0.0:t))
          (6.2831853071800001231395071954466402530670166015625:t))
         (div RNE (sub RNE x2 (0.0:t))
          (6.2831853071800001231395071954466402530670166015625:t))
         (div RNE (sub RNE x3 (650.0:t)) (1100.0:t))
         (div RNE (sub RNE x4 (600.0:t)) (1200.0:t)))
     [0]
     (nn_onnx
      @@ vector (div RNE (sub RNE x (19791.0:t)) (60261.0:t))
         (div RNE (sub RNE x1 (0.0:t))
          (6.2831853071800001231395071954466402530670166015625:t))
         (div RNE (sub RNE x2 (0.0:t))
          (6.2831853071800001231395071954466402530670166015625:t))
         (div RNE (sub RNE x3 (650.0:t)) (1100.0:t))
         (div RNE (sub RNE x4 (600.0:t)) (1200.0:t)))
     [0])
    eps) /\
   le
   (sub RNE
    (sub RNE
     (nn_onnx
      @@ vector (div RNE (sub RNE x (19791.0:t)) (60261.0:t))
         (div RNE (sub RNE x1 (0.0:t))
          (6.2831853071800001231395071954466402530670166015625:t))
         (div RNE (sub RNE x2 (0.0:t))
          (6.2831853071800001231395071954466402530670166015625:t))
         (div RNE (sub RNE x3 (650.0:t)) (1100.0:t))
         (div RNE (sub RNE x4 (600.0:t)) (1200.0:t)))
     [0]
     (nn_onnx
      @@ vector (div RNE (sub RNE x (19791.0:t)) (60261.0:t))
         (div RNE (sub RNE x1 (0.0:t))
          (6.2831853071800001231395071954466402530670166015625:t))
         (div RNE (sub RNE x2 (0.0:t))
          (6.2831853071800001231395071954466402530670166015625:t))
         (div RNE (sub RNE x3 (650.0:t)) (1100.0:t))
         (div RNE (sub RNE x4 (600.0:t)) (1200.0:t)))
     [0])
    eps)
   (0.0:t)
  nn_onnx,
  (Interpreter_types.Model
     (Interpreter_types.ONNX, { Language.nn_nb_inputs = 5; nn_nb_outputs = 5;
                                nn_ty_elt = t;
                                nn_filename =
                                "./../examples/acasxu/nets/onnx/ACASXU_1_1.onnx";
                                nn_format = <nir> }))
  vector,
  5
  [DEBUG]{NN-Gen-Term} new goal:le (0.0:t) y9 /\ le y9 (0.0:t)
  [DEBUG]{ProverSpec} Prover-tailored specification:
  0.0 <= x0
  x0 <= 60760.0
  -3.141592653589793115997963468544185161590576171875 <= x1
  x1 <= 3.141592653589793115997963468544185161590576171875
  -3.141592653589793115997963468544185161590576171875 <= x2
  x2 <= 3.141592653589793115997963468544185161590576171875
  100.0 <= x3
  x3 <= 1200.0
  0.0 <= x4
  x4 <= 1200.0
  0.0 <= x5
  x5 <= 0.375
  0.0 <= y0 and y0 <= 0.0
  
  [DEBUG]{InterpretGoal} Interpreted formula for goal 'P1':
  forall x:t, x1:t, x2:t, x3:t, x4:t.
   ((le (0.0:t) (add RNE (mul RNE x (60261.0:t)) (19791.0:t)) /\
     le (add RNE (mul RNE x (60261.0:t)) (19791.0:t)) (60760.0:t)) /\
    (le (neg (3.141592653589793115997963468544185161590576171875:t))
     (add RNE
      (mul RNE x1 (6.2831853071800001231395071954466402530670166015625:t))
      (0.0:t)) /\
     le
     (add RNE
      (mul RNE x1 (6.2831853071800001231395071954466402530670166015625:t))
      (0.0:t))
     (3.141592653589793115997963468544185161590576171875:t)) /\
    (le (neg (3.141592653589793115997963468544185161590576171875:t))
     (add RNE
      (mul RNE x2 (6.2831853071800001231395071954466402530670166015625:t))
      (0.0:t)) /\
     le
     (add RNE
      (mul RNE x2 (6.2831853071800001231395071954466402530670166015625:t))
      (0.0:t))
     (3.141592653589793115997963468544185161590576171875:t)) /\
    (le (100.0:t) (add RNE (mul RNE x3 (1100.0:t)) (650.0:t)) /\
     le (add RNE (mul RNE x3 (1100.0:t)) (650.0:t)) (1200.0:t)) /\
    le (0.0:t) (add RNE (mul RNE x4 (1200.0:t)) (600.0:t)) /\
    le (add RNE (mul RNE x4 (1200.0:t)) (600.0:t)) (1200.0:t)) /\
   le (55947.6909999999988940544426441192626953125:t)
   (add RNE (mul RNE x (60261.0:t)) (19791.0:t)) /\
   le (1145.0:t) (add RNE (mul RNE x3 (1100.0:t)) (650.0:t)) /\
   le (add RNE (mul RNE x4 (1200.0:t)) (600.0:t)) (60.0:t) ->
   le
   (add RNE
    (mul RNE (nn_nnet @@ vector x x1 x2 x3 x4)[0]
     (373.9499200000000200816430151462554931640625:t))
    (7.51888402010059753166615337249822914600372314453125:t))
   (1500.0:t)
  vector, 5
  nn_nnet,
  (Interpreter_types.Model
     (Interpreter_types.NNet, { Language.nn_nb_inputs = 5; nn_nb_outputs = 5;
                                nn_ty_elt = t;
                                nn_filename = "./TestNetwork.nnet";
                                nn_format = <nir> }))
  [DEBUG]{NN-Gen-Term} new goal:le y10 (1500.0:t)
  [DEBUG]{ProverSpec} Prover-tailored specification:
  -0.328421367053318091766556108268559910356998443603515625 <= x0
  x0 <= 0.67985927880386987087746319957659579813480377197265625
  -0.499999999999967081887319864108576439321041107177734375 <= x1
  x1 <= 0.499999999999967081887319864108576439321041107177734375
  -0.499999999999967081887319864108576439321041107177734375 <= x2
  x2 <= 0.499999999999967081887319864108576439321041107177734375
  -0.5 <= x3
  x3 <= 0.5
  -0.5 <= x4
  x4 <= 0.5
  0.60000151009774149724051994780893437564373016357421875 <= x0
  0.450000000000000011102230246251565404236316680908203125 <= x3
  x4 <= -0.450000000000000011102230246251565404236316680908203125
  y0 <= 1500.0
  
  [DEBUG]{InterpretGoal} Interpreted formula for goal 'P3':
  forall x:t, x1:t, x2:t, x3:t, x4:t.
   ((le (0.0:t) (add RNE (mul RNE x (60261.0:t)) (19791.0:t)) /\
     le (add RNE (mul RNE x (60261.0:t)) (19791.0:t)) (60760.0:t)) /\
    (le (neg (3.141592653589793115997963468544185161590576171875:t))
     (add RNE
      (mul RNE x1 (6.2831853071800001231395071954466402530670166015625:t))
      (0.0:t)) /\
     le
     (add RNE
      (mul RNE x1 (6.2831853071800001231395071954466402530670166015625:t))
      (0.0:t))
     (3.141592653589793115997963468544185161590576171875:t)) /\
    (le (neg (3.141592653589793115997963468544185161590576171875:t))
     (add RNE
      (mul RNE x2 (6.2831853071800001231395071954466402530670166015625:t))
      (0.0:t)) /\
     le
     (add RNE
      (mul RNE x2 (6.2831853071800001231395071954466402530670166015625:t))
      (0.0:t))
     (3.141592653589793115997963468544185161590576171875:t)) /\
    (le (100.0:t) (add RNE (mul RNE x3 (1100.0:t)) (650.0:t)) /\
     le (add RNE (mul RNE x3 (1100.0:t)) (650.0:t)) (1200.0:t)) /\
    le (0.0:t) (add RNE (mul RNE x4 (1200.0:t)) (600.0:t)) /\
    le (add RNE (mul RNE x4 (1200.0:t)) (600.0:t)) (1200.0:t)) /\
   ((le (1500.0:t) (add RNE (mul RNE x (60261.0:t)) (19791.0:t)) /\
     le (add RNE (mul RNE x (60261.0:t)) (19791.0:t)) (1800.0:t)) /\
    le (neg (0.059999999999999997779553950749686919152736663818359375:t))
    (add RNE
     (mul RNE x1 (6.2831853071800001231395071954466402530670166015625:t))
     (0.0:t)) /\
    le
    (add RNE
     (mul RNE x1 (6.2831853071800001231395071954466402530670166015625:t))
     (0.0:t))
    (0.059999999999999997779553950749686919152736663818359375:t)) /\
   le (3.100000000000000088817841970012523233890533447265625:t)
   (add RNE
    (mul RNE x2 (6.2831853071800001231395071954466402530670166015625:t))
    (0.0:t)) /\
   le (980.0:t) (add RNE (mul RNE x3 (1100.0:t)) (650.0:t)) /\
   le (960.0:t) (add RNE (mul RNE x4 (1200.0:t)) (600.0:t)) ->
   not (((lt (nn_nnet @@ vector x x1 x2 x3 x4)[0]
          (nn_nnet @@ vector x x1 x2 x3 x4)[1] /\
          lt (nn_nnet @@ vector x x1 x2 x3 x4)[0]
          (nn_nnet @@ vector x x1 x2 x3 x4)[2]) /\
         lt (nn_nnet @@ vector x x1 x2 x3 x4)[0]
         (nn_nnet @@ vector x x1 x2 x3 x4)[3]) /\
        lt (nn_nnet @@ vector x x1 x2 x3 x4)[0]
        (nn_nnet @@ vector x x1 x2 x3 x4)[4])
  vector, 5
  nn_nnet,
  (Interpreter_types.Model
     (Interpreter_types.NNet, { Language.nn_nb_inputs = 5; nn_nb_outputs = 5;
                                nn_ty_elt = t;
                                nn_filename = "./TestNetwork.nnet";
                                nn_format = <nir> }))
  [DEBUG]{NN-Gen-Term} new goal:not (((lt y11 y12 /\ lt y11 y13) /\ lt y11 y14) /\
                                     lt y11 y15)
  [DEBUG]{ProverSpec} Prover-tailored specification:
  -0.328421367053318091766556108268559910356998443603515625 <= x0
  x0 <= 0.67985927880386987087746319957659579813480377197265625
  -0.499999999999967081887319864108576439321041107177734375 <= x1
  x1 <= 0.499999999999967081887319864108576439321041107177734375
  -0.499999999999967081887319864108576439321041107177734375 <= x2
  x2 <= 0.499999999999967081887319864108576439321041107177734375
  -0.5 <= x3
  x3 <= 0.5
  -0.5 <= x4
  x4 <= 0.5
  -0.303529646039727207806890874053351581096649169921875 <= x0
  x0 <= -0.298551301837009008810497334707179106771945953369140625
  -0.0095492965855130916563719978285007528029382228851318359375 <= x1
  x1 <= 0.0095492965855130916563719978285007528029382228851318359375
  0.493380323584843072382000173092819750308990478515625 <= x2
  0.299999999999999988897769753748434595763683319091796875 <= x3
  0.299999999999999988897769753748434595763683319091796875 <= x4
  (((y0 >= y1 or y0 >= y2) or y0 >= y3) or y0 >= y4)
  
  Goal runP1'vc: Unknown ()
  Goal runP1v2'vc: Unknown ()
  Goal run2'vc: Unknown ()
  Goal run2v2'vc: Unknown ()
  Goal run3'vc: Unknown ()
  Goal robust_output'vc: Unknown ()
  Goal P1: Unknown ()
  Goal P3: Unknown ()

  $ caisar verify --prover PyRAT --prover-altern VNNLIB --ltag=ProverSpec file.mlw
  [DEBUG]{ProverSpec} Prover-tailored specification:
  ;;; produced by PyRAT/VNN-LIB driver
  ;;; produced by VNN-LIB driver
  ;; X_0
  (declare-const X_0 Real)
  
  ;; X_1
  (declare-const X_1 Real)
  
  ;; X_2
  (declare-const X_2 Real)
  
  ;; X_3
  (declare-const X_3 Real)
  
  ;; X_4
  (declare-const X_4 Real)
  
  ;; Requires
  (assert (>= X_0 0.0))
  (assert (<= X_0 60760.0))
  (assert (>= X_1 -3.141592653589793115997963468544185161590576171875))
  (assert (<= X_1 3.141592653589793115997963468544185161590576171875))
  (assert (>= X_2 -3.141592653589793115997963468544185161590576171875))
  (assert (<= X_2 3.141592653589793115997963468544185161590576171875))
  (assert (>= X_3 100.0))
  (assert (<= X_3 1200.0))
  (assert (>= X_4 0.0))
  (assert (<= X_4 1200.0))
  
  ;; Requires
  (assert (>= X_0 55947.6909999999988940544426441192626953125))
  (assert (>= X_3 1145.0))
  (assert (<= X_4 60.0))
  
  ;; Y_0
  (declare-const Y_0 Real)
  
  ;; Goal runP1'vc
  (assert (>= Y_0 1500.0))
  
  [DEBUG]{ProverSpec} Prover-tailored specification:
  ;;; produced by PyRAT/VNN-LIB driver
  ;;; produced by VNN-LIB driver
  ;; X_0
  (declare-const X_0 Real)
  
  ;; X_1
  (declare-const X_1 Real)
  
  ;; X_2
  (declare-const X_2 Real)
  
  ;; X_3
  (declare-const X_3 Real)
  
  ;; X_4
  (declare-const X_4 Real)
  
  ;; Requires
  (assert (>= X_0 0.0))
  (assert (<= X_0 60760.0))
  (assert (>= X_1 -3.141592653589793115997963468544185161590576171875))
  (assert (<= X_1 3.141592653589793115997963468544185161590576171875))
  (assert (>= X_2 -3.141592653589793115997963468544185161590576171875))
  (assert (<= X_2 3.141592653589793115997963468544185161590576171875))
  (assert (>= X_3 100.0))
  (assert (<= X_3 1200.0))
  (assert (>= X_4 0.0))
  (assert (<= X_4 1200.0))
  
  ;; Requires
  (assert (>= X_0 55947.6909999999988940544426441192626953125))
  (assert (>= X_3 1145.0))
  (assert (<= X_4 60.0))
  
  ;; Y_0
  (declare-const Y_0 Real)
  
  ;; Y_1
  (declare-const Y_1 Real)
  
  ;; Goal runP1v2'vc
  (assert (or (and (>= Y_0 1500.0))
              (and (>= Y_1 1500.0))
              ))
  
  [DEBUG]{ProverSpec} Prover-tailored specification:
  ;;; produced by PyRAT/VNN-LIB driver
  ;;; produced by VNN-LIB driver
  ;; X_0
  (declare-const X_0 Real)
  
  ;; X_1
  (declare-const X_1 Real)
  
  ;; X_2
  (declare-const X_2 Real)
  
  ;; X_3
  (declare-const X_3 Real)
  
  ;; X_4
  (declare-const X_4 Real)
  
  ;; Requires
  (assert (>= X_0 0.0))
  (assert (<= X_0 60760.0))
  (assert (>= X_1 -3.141592653589793115997963468544185161590576171875))
  (assert (<= X_1 3.141592653589793115997963468544185161590576171875))
  (assert (>= X_2 -3.141592653589793115997963468544185161590576171875))
  (assert (<= X_2 3.141592653589793115997963468544185161590576171875))
  (assert (>= X_3 100.0))
  (assert (<= X_3 1200.0))
  (assert (>= X_4 0.0))
  (assert (<= X_4 1200.0))
  
  ;; Requires
  (assert (>= X_0 55947.6909999999988940544426441192626953125))
  (assert (>= X_3 1145.0))
  (assert (<= X_4 60.0))
  
  ;; Y_0
  (declare-const Y_0 Real)
  
  ;; Y_1
  (declare-const Y_1 Real)
  
  ;; Goal run2'vc
  (assert (or (and (>= Y_1 Y_0))
              (and (>= Y_0 Y_1))
              ))
  
  [DEBUG]{ProverSpec} Prover-tailored specification:
  ;;; produced by PyRAT/VNN-LIB driver
  ;;; produced by VNN-LIB driver
  ;; X_0
  (declare-const X_0 Real)
  
  ;; X_1
  (declare-const X_1 Real)
  
  ;; X_2
  (declare-const X_2 Real)
  
  ;; X_3
  (declare-const X_3 Real)
  
  ;; X_4
  (declare-const X_4 Real)
  
  ;; X_5
  (declare-const X_5 Real)
  
  ;; X_6
  (declare-const X_6 Real)
  
  ;; X_7
  (declare-const X_7 Real)
  
  ;; X_8
  (declare-const X_8 Real)
  
  ;; X_9
  (declare-const X_9 Real)
  
  ;; Requires
  (assert (>= X_0 0.0))
  (assert (<= X_0 60760.0))
  (assert (>= X_1 -3.141592653589793115997963468544185161590576171875))
  (assert (<= X_1 3.141592653589793115997963468544185161590576171875))
  (assert (>= X_2 -3.141592653589793115997963468544185161590576171875))
  (assert (<= X_2 3.141592653589793115997963468544185161590576171875))
  (assert (>= X_3 100.0))
  (assert (<= X_3 1200.0))
  (assert (>= X_4 0.0))
  (assert (<= X_4 1200.0))
  
  ;; Requires
  (assert (>= X_0 55947.6909999999988940544426441192626953125))
  (assert (>= X_3 1145.0))
  (assert (<= X_4 60.0))
  
  ;; Requires
  (assert (>= X_5 55947.6909999999988940544426441192626953125))
  (assert (>= X_8 1145.0))
  (assert (<= X_9 60.0))
  
  ;; Y_0
  (declare-const Y_0 Real)
  
  ;; Y_1
  (declare-const Y_1 Real)
  
  ;; Goal run2v2'vc
  (assert (or (and (>= Y_1 Y_0))
              (and (>= Y_0 Y_1))
              ))
  
  [DEBUG]{ProverSpec} Prover-tailored specification:
  ;;; produced by PyRAT/VNN-LIB driver
  ;;; produced by VNN-LIB driver
  ;; X_0
  (declare-const X_0 Real)
  
  ;; X_1
  (declare-const X_1 Real)
  
  ;; X_2
  (declare-const X_2 Real)
  
  ;; X_3
  (declare-const X_3 Real)
  
  ;; X_4
  (declare-const X_4 Real)
  
  ;; X_5
  (declare-const X_5 Real)
  
  ;; Requires
  (assert (>= X_0 0.0))
  (assert (<= X_0 60760.0))
  (assert (>= X_1 -3.141592653589793115997963468544185161590576171875))
  (assert (<= X_1 3.141592653589793115997963468544185161590576171875))
  (assert (>= X_2 -3.141592653589793115997963468544185161590576171875))
  (assert (<= X_2 3.141592653589793115997963468544185161590576171875))
  (assert (>= X_3 100.0))
  (assert (<= X_3 1200.0))
  (assert (>= X_4 0.0))
  (assert (<= X_4 1200.0))
  
  ;; Requires
  (assert (>= X_0 55947.6909999999988940544426441192626953125))
  (assert (>= X_3 1145.0))
  (assert (<= X_4 60.0))
  
  ;; H
  (assert (>= X_5 -1.0))
  
  ;; H
  (assert (<= X_5 1.0))
  
  ;; Y_0
  (declare-const Y_0 Real)
  
  ;; Y_1
  (declare-const Y_1 Real)
  
  ;; Goal run3'vc
  (assert (or (and (>= Y_0 Y_1))
              (and (>= Y_1 Y_0))
              ))
  
  [DEBUG]{ProverSpec} Prover-tailored specification:
  ;;; produced by PyRAT/VNN-LIB driver
  ;;; produced by VNN-LIB driver
  ;; X_0
  (declare-const X_0 Real)
  
  ;; X_1
  (declare-const X_1 Real)
  
  ;; X_2
  (declare-const X_2 Real)
  
  ;; X_3
  (declare-const X_3 Real)
  
  ;; X_4
  (declare-const X_4 Real)
  
  ;; X_5
  (declare-const X_5 Real)
  
  ;; Requires
  (assert (>= X_0 0.0))
  (assert (<= X_0 60760.0))
  (assert (>= X_1 -3.141592653589793115997963468544185161590576171875))
  (assert (<= X_1 3.141592653589793115997963468544185161590576171875))
  (assert (>= X_2 -3.141592653589793115997963468544185161590576171875))
  (assert (<= X_2 3.141592653589793115997963468544185161590576171875))
  (assert (>= X_3 100.0))
  (assert (<= X_3 1200.0))
  (assert (>= X_4 0.0))
  (assert (<= X_4 1200.0))
  
  ;; H
  (assert (>= X_5 0.0))
  
  ;; H
  (assert (<= X_5 0.375))
  
  ;; Y_0
  (declare-const Y_0 Real)
  
  ;; Goal robust_output'vc
  (assert (or (and (<= Y_0 0.0))
              (and (>= Y_0 0.0))
              ))
  
  [DEBUG]{ProverSpec} Prover-tailored specification:
  ;;; produced by PyRAT/VNN-LIB driver
  ;;; produced by VNN-LIB driver
  ;; X_0
  (declare-const X_0 Real)
  
  ;; X_1
  (declare-const X_1 Real)
  
  ;; X_2
  (declare-const X_2 Real)
  
  ;; X_3
  (declare-const X_3 Real)
  
  ;; X_4
  (declare-const X_4 Real)
  
  ;; H
  (assert (>= X_0 -0.328421367053318091766556108268559910356998443603515625))
  
  ;; H
  (assert (<= X_0 0.67985927880386987087746319957659579813480377197265625))
  
  ;; H
  (assert (>= X_1 -0.499999999999967081887319864108576439321041107177734375))
  
  ;; H
  (assert (<= X_1 0.499999999999967081887319864108576439321041107177734375))
  
  ;; H
  (assert (>= X_2 -0.499999999999967081887319864108576439321041107177734375))
  
  ;; H
  (assert (<= X_2 0.499999999999967081887319864108576439321041107177734375))
  
  ;; H
  (assert (>= X_3 -0.5))
  
  ;; H
  (assert (<= X_3 0.5))
  
  ;; H
  (assert (>= X_4 -0.5))
  
  ;; H
  (assert (<= X_4 0.5))
  
  ;; H
  (assert (>= X_0 0.60000151009774149724051994780893437564373016357421875))
  
  ;; H
  (assert (>= X_3 0.450000000000000011102230246251565404236316680908203125))
  
  ;; H
  (assert (<= X_4 -0.450000000000000011102230246251565404236316680908203125))
  
  ;; Y_0
  (declare-const Y_0 Real)
  
  ;; Goal P1
  (assert (>= Y_0 1500.0))
  
  [DEBUG]{ProverSpec} Prover-tailored specification:
  ;;; produced by PyRAT/VNN-LIB driver
  ;;; produced by VNN-LIB driver
  ;; X_0
  (declare-const X_0 Real)
  
  ;; X_1
  (declare-const X_1 Real)
  
  ;; X_2
  (declare-const X_2 Real)
  
  ;; X_3
  (declare-const X_3 Real)
  
  ;; X_4
  (declare-const X_4 Real)
  
  ;; H
  (assert (>= X_0 -0.328421367053318091766556108268559910356998443603515625))
  
  ;; H
  (assert (<= X_0 0.67985927880386987087746319957659579813480377197265625))
  
  ;; H
  (assert (>= X_1 -0.499999999999967081887319864108576439321041107177734375))
  
  ;; H
  (assert (<= X_1 0.499999999999967081887319864108576439321041107177734375))
  
  ;; H
  (assert (>= X_2 -0.499999999999967081887319864108576439321041107177734375))
  
  ;; H
  (assert (<= X_2 0.499999999999967081887319864108576439321041107177734375))
  
  ;; H
  (assert (>= X_3 -0.5))
  
  ;; H
  (assert (<= X_3 0.5))
  
  ;; H
  (assert (>= X_4 -0.5))
  
  ;; H
  (assert (<= X_4 0.5))
  
  ;; H
  (assert (>= X_0 -0.303529646039727207806890874053351581096649169921875))
  
  ;; H
  (assert (<= X_0 -0.298551301837009008810497334707179106771945953369140625))
  
  ;; H
  (assert (>= X_1 -0.0095492965855130916563719978285007528029382228851318359375))
  
  ;; H
  (assert (<= X_1 0.0095492965855130916563719978285007528029382228851318359375))
  
  ;; H
  (assert (>= X_2 0.493380323584843072382000173092819750308990478515625))
  
  ;; H
  (assert (>= X_3 0.299999999999999988897769753748434595763683319091796875))
  
  ;; H
  (assert (>= X_4 0.299999999999999988897769753748434595763683319091796875))
  
  ;; Y_0
  (declare-const Y_0 Real)
  
  ;; Y_1
  (declare-const Y_1 Real)
  
  ;; Y_2
  (declare-const Y_2 Real)
  
  ;; Y_3
  (declare-const Y_3 Real)
  
  ;; Y_4
  (declare-const Y_4 Real)
  
  ;; Goal P3
  (assert (<= Y_0 Y_1))
  (assert (<= Y_0 Y_2))
  (assert (<= Y_0 Y_3))
  (assert (<= Y_0 Y_4))
  
  Goal runP1'vc: Unknown ()
  Goal runP1v2'vc: Unknown ()
  Goal run2'vc: Unknown ()
  Goal run2v2'vc: Unknown ()
  Goal run3'vc: Unknown ()
  Goal robust_output'vc: Unknown ()
  Goal P1: Unknown ()
  Goal P3: Unknown ()

  $ caisar verify --prover Marabou --ltag=ProverSpec file.mlw
  [DEBUG]{ProverSpec} Prover-tailored specification:
  x0 >= 0.0
  x0 <= 60760.0
  x1 >= -3.141592653589793115997963468544185161590576171875
  x1 <= 3.141592653589793115997963468544185161590576171875
  x2 >= -3.141592653589793115997963468544185161590576171875
  x2 <= 3.141592653589793115997963468544185161590576171875
  x3 >= 100.0
  x3 <= 1200.0
  x4 >= 0.0
  x4 <= 1200.0
  x0 >= 55947.6909999999988940544426441192626953125
  x3 >= 1145.0
  x4 <= 60.0
  y0 >= 1500.0
  
  [DEBUG]{ProverSpec} Prover-tailored specification:
  x0 >= 0.0
  x0 <= 60760.0
  x1 >= -3.141592653589793115997963468544185161590576171875
  x1 <= 3.141592653589793115997963468544185161590576171875
  x2 >= -3.141592653589793115997963468544185161590576171875
  x2 <= 3.141592653589793115997963468544185161590576171875
  x3 >= 100.0
  x3 <= 1200.0
  x4 >= 0.0
  x4 <= 1200.0
  x0 >= 55947.6909999999988940544426441192626953125
  x3 >= 1145.0
  x4 <= 60.0
  y0 >= 1500.0
  
  [DEBUG]{ProverSpec} Prover-tailored specification:
  x0 >= 0.0
  x0 <= 60760.0
  x1 >= -3.141592653589793115997963468544185161590576171875
  x1 <= 3.141592653589793115997963468544185161590576171875
  x2 >= -3.141592653589793115997963468544185161590576171875
  x2 <= 3.141592653589793115997963468544185161590576171875
  x3 >= 100.0
  x3 <= 1200.0
  x4 >= 0.0
  x4 <= 1200.0
  x0 >= 55947.6909999999988940544426441192626953125
  x3 >= 1145.0
  x4 <= 60.0
  y1 >= 1500.0
  
  [DEBUG]{ProverSpec} Prover-tailored specification:
  x0 >= 0.0
  x0 <= 60760.0
  x1 >= -3.141592653589793115997963468544185161590576171875
  x1 <= 3.141592653589793115997963468544185161590576171875
  x2 >= -3.141592653589793115997963468544185161590576171875
  x2 <= 3.141592653589793115997963468544185161590576171875
  x3 >= 100.0
  x3 <= 1200.0
  x4 >= 0.0
  x4 <= 1200.0
  x0 >= 55947.6909999999988940544426441192626953125
  x3 >= 1145.0
  x4 <= 60.0
  +y1 -y0 >= 0
  
  [DEBUG]{ProverSpec} Prover-tailored specification:
  x0 >= 0.0
  x0 <= 60760.0
  x1 >= -3.141592653589793115997963468544185161590576171875
  x1 <= 3.141592653589793115997963468544185161590576171875
  x2 >= -3.141592653589793115997963468544185161590576171875
  x2 <= 3.141592653589793115997963468544185161590576171875
  x3 >= 100.0
  x3 <= 1200.0
  x4 >= 0.0
  x4 <= 1200.0
  x0 >= 55947.6909999999988940544426441192626953125
  x3 >= 1145.0
  x4 <= 60.0
  +y0 -y1 >= 0
  
  [DEBUG]{ProverSpec} Prover-tailored specification:
  x0 >= 0.0
  x0 <= 60760.0
  x1 >= -3.141592653589793115997963468544185161590576171875
  x1 <= 3.141592653589793115997963468544185161590576171875
  x2 >= -3.141592653589793115997963468544185161590576171875
  x2 <= 3.141592653589793115997963468544185161590576171875
  x3 >= 100.0
  x3 <= 1200.0
  x4 >= 0.0
  x4 <= 1200.0
  x0 >= 55947.6909999999988940544426441192626953125
  x3 >= 1145.0
  x4 <= 60.0
  x5 >= 55947.6909999999988940544426441192626953125
  x8 >= 1145.0
  x9 <= 60.0
  +y1 -y0 >= 0
  
  [DEBUG]{ProverSpec} Prover-tailored specification:
  x0 >= 0.0
  x0 <= 60760.0
  x1 >= -3.141592653589793115997963468544185161590576171875
  x1 <= 3.141592653589793115997963468544185161590576171875
  x2 >= -3.141592653589793115997963468544185161590576171875
  x2 <= 3.141592653589793115997963468544185161590576171875
  x3 >= 100.0
  x3 <= 1200.0
  x4 >= 0.0
  x4 <= 1200.0
  x0 >= 55947.6909999999988940544426441192626953125
  x3 >= 1145.0
  x4 <= 60.0
  x5 >= 55947.6909999999988940544426441192626953125
  x8 >= 1145.0
  x9 <= 60.0
  +y0 -y1 >= 0
  
  [DEBUG]{ProverSpec} Prover-tailored specification:
  x0 >= 0.0
  x0 <= 60760.0
  x1 >= -3.141592653589793115997963468544185161590576171875
  x1 <= 3.141592653589793115997963468544185161590576171875
  x2 >= -3.141592653589793115997963468544185161590576171875
  x2 <= 3.141592653589793115997963468544185161590576171875
  x3 >= 100.0
  x3 <= 1200.0
  x4 >= 0.0
  x4 <= 1200.0
  x0 >= 55947.6909999999988940544426441192626953125
  x3 >= 1145.0
  x4 <= 60.0
  x5 >= -1.0
  x5 <= 1.0
  +y0 -y1 >= 0
  
  [DEBUG]{ProverSpec} Prover-tailored specification:
  x0 >= 0.0
  x0 <= 60760.0
  x1 >= -3.141592653589793115997963468544185161590576171875
  x1 <= 3.141592653589793115997963468544185161590576171875
  x2 >= -3.141592653589793115997963468544185161590576171875
  x2 <= 3.141592653589793115997963468544185161590576171875
  x3 >= 100.0
  x3 <= 1200.0
  x4 >= 0.0
  x4 <= 1200.0
  x0 >= 55947.6909999999988940544426441192626953125
  x3 >= 1145.0
  x4 <= 60.0
  x5 >= -1.0
  x5 <= 1.0
  +y1 -y0 >= 0
  
  [DEBUG]{ProverSpec} Prover-tailored specification:
  x0 >= 0.0
  x0 <= 60760.0
  x1 >= -3.141592653589793115997963468544185161590576171875
  x1 <= 3.141592653589793115997963468544185161590576171875
  x2 >= -3.141592653589793115997963468544185161590576171875
  x2 <= 3.141592653589793115997963468544185161590576171875
  x3 >= 100.0
  x3 <= 1200.0
  x4 >= 0.0
  x4 <= 1200.0
  x5 >= 0.0
  x5 <= 0.375
  y0 <= 0.0
  
  [DEBUG]{ProverSpec} Prover-tailored specification:
  x0 >= 0.0
  x0 <= 60760.0
  x1 >= -3.141592653589793115997963468544185161590576171875
  x1 <= 3.141592653589793115997963468544185161590576171875
  x2 >= -3.141592653589793115997963468544185161590576171875
  x2 <= 3.141592653589793115997963468544185161590576171875
  x3 >= 100.0
  x3 <= 1200.0
  x4 >= 0.0
  x4 <= 1200.0
  x5 >= 0.0
  x5 <= 0.375
  y0 >= 0.0
  
  [DEBUG]{ProverSpec} Prover-tailored specification:
  x0 >= -0.328421367053318091766556108268559910356998443603515625
  x0 <= 0.67985927880386987087746319957659579813480377197265625
  x1 >= -0.499999999999967081887319864108576439321041107177734375
  x1 <= 0.499999999999967081887319864108576439321041107177734375
  x2 >= -0.499999999999967081887319864108576439321041107177734375
  x2 <= 0.499999999999967081887319864108576439321041107177734375
  x3 >= -0.5
  x3 <= 0.5
  x4 >= -0.5
  x4 <= 0.5
  x0 >= 0.60000151009774149724051994780893437564373016357421875
  x3 >= 0.450000000000000011102230246251565404236316680908203125
  x4 <= -0.450000000000000011102230246251565404236316680908203125
  y0 >= 1500.0
  
  [DEBUG]{ProverSpec} Prover-tailored specification:
  x0 >= -0.328421367053318091766556108268559910356998443603515625
  x0 <= 0.67985927880386987087746319957659579813480377197265625
  x1 >= -0.499999999999967081887319864108576439321041107177734375
  x1 <= 0.499999999999967081887319864108576439321041107177734375
  x2 >= -0.499999999999967081887319864108576439321041107177734375
  x2 <= 0.499999999999967081887319864108576439321041107177734375
  x3 >= -0.5
  x3 <= 0.5
  x4 >= -0.5
  x4 <= 0.5
  x0 >= -0.303529646039727207806890874053351581096649169921875
  x0 <= -0.298551301837009008810497334707179106771945953369140625
  x1 >= -0.0095492965855130916563719978285007528029382228851318359375
  x1 <= 0.0095492965855130916563719978285007528029382228851318359375
  x2 >= 0.493380323584843072382000173092819750308990478515625
  x3 >= 0.299999999999999988897769753748434595763683319091796875
  x4 >= 0.299999999999999988897769753748434595763683319091796875
  +y0 -y1 <= 0
  +y0 -y2 <= 0
  +y0 -y3 <= 0
  +y0 -y4 <= 0
  
  Goal runP1'vc: Unknown ()
  Goal runP1v2'vc: Unknown ()
  Goal run2'vc: Unknown ()
  Goal run2v2'vc: Unknown ()
  Goal run3'vc: Unknown ()
  Goal robust_output'vc: Unknown ()
  Goal P1: Unknown ()
  Goal P3: Unknown ()
