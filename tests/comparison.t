  $ . ./setup_env.sh

  $ ls ../examples/
  acasxu
  arithmetic
  mnist
  onnx_rewrite

  $ caisar verify --prover PyRAT --define model_filename_1:../mnist/nets/pruned/FNN_28x28_s42.onnx --define model_filename_2:../mnist/nets/pruned/FNN_28x28_pruned_s42.onnx --define dataset_filename:../mnist/csv/single_image.csv ../examples/onnx_rewrite/comparison.mlw -v
  [INFO] Verification results for theory 'COMPARISON'
  Goal comparison: Unknown ()
