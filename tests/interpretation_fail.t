Test interpret fail
  $ . ./setup_env.sh

  $ cat > file.mlw <<EOF
  > theory T1
  >   use ieee_float.Float64
  >   use caisar.types.Vector
  >   use caisar.model.Model
  >  
  > 
  >   constant nn: model = read_model "TestNetwork.nnet"
  > 
  >   goal G1:
  >     forall i.
  >       i[100] .<= (1.0:t) ->
  >       (nn @@ i)[0] .<= (1.0:t)
  > end
  > EOF

  $ caisar verify --prover nnenum file.mlw
  [ERROR] "file.mlw", line 10, characters 11-12:
          Expecting 'has_length' predicate after universal quantifier on vector 'i'

  $ cat > file.mlw <<EOF
  > theory T2
  >   use ieee_float.Float64
  >   use caisar.types.Vector
  >   use caisar.model.Model
  >  
  > 
  >   constant nn: model = read_model "TestNetwork.nnet"
  > 
  >   predicate off_by_one (i: vector t) = let dummy = i[5] in dummy .<= (1.0:t)
  > 
  >   goal G2:
  >     forall i: vector t.
  >       has_length i 5 -> off_by_one i ->
  >       (nn @@ i)[0] .<= (1.0:t)
  > end
  > EOF

  $ caisar verify --prover nnenum file.mlw
  [ERROR] "file.mlw", line 13, characters 35-36:
          Index constant 5 is out-of-bounds [0,4]

  $ cat > file.mlw <<EOF
  > theory T3
  >   use ieee_float.Float64
  >   use caisar.types.Vector
  >   use caisar.model.Model
  >  
  > 
  >   constant nn: model = read_model "TestNetwork.nnet"
  > 
  >   predicate valid_input (i: vector t) = (0.0:t) .<= i[0] \/ (0.0:t) .>= i[0]
  > 
  >   goal G3:
  >     forall i: vector t.
  >       has_length i 5 -> valid_input i ->
  >       let j = i - i in
  >       (nn @@ j)[0] .<= (1.0:t)
  > end
  > EOF

  $ caisar verify --prover nnenum file.mlw
  [ERROR] "file.mlw", line 14, characters 14-15:
          Invalid substraction operation involving two abstract vectors

  $ cat > file.mlw <<EOF
  > theory T4
  >   use ieee_float.Float64
  >   use caisar.types.Vector
  >   use caisar.model.Model
  >  
  > 
  >   constant nn: model = read_model "nonexistent.nnet"
  > 
  >   goal G4:
  >     forall i: vector t.
  >       has_length i 5 ->
  >       let coc = (nn @@ i)[0] in
  >       coc .<= (1.0:t)
  > end
  > EOF

  $ caisar verify --prover nnenum file.mlw
  [ERROR] ./nonexistent.nnet: No such file or directory

  $ cat > file.mlw <<EOF
  > theory T5
  >   use ieee_float.Float64
  >   use caisar.types.Vector
  >   use caisar.model.Model
  >  
  > 
  >   constant nn: model = read_model "TestNetworkONNX.onnx"
  > 
  >   goal G5:
  >     forall i: vector t.
  >       has_length i 5 ->
  >       let coc = (nn @@ i)[0] in
  >       coc .<= (1.0:t)
  > end
  > EOF

  $ caisar verify --prover nnenum file.mlw
  [ERROR] "file.mlw", line 12, characters 23-24:
          Unexpected vector of length 5 in input to model './TestNetworkONNX.onnx',
          which expects input vectors of length 3

  $ cat > file.mlw <<EOF
  > theory T6
  >   use ieee_float.Float64
  >   use caisar.types.Vector
  >   use caisar.model.Model
  >  
  > 
  >   constant nn: model = read_model "TestNetwork.nnet"
  > 
  >   goal G6:
  >     forall i: vector t.
  >       has_length i 5 ->
  >       let o = (nn @@ i)[-2] in
  >       o .<= (1.0:t)
  > end
  > EOF

  $ caisar verify --prover nnenum file.mlw
  [ERROR] "file.mlw", line 12, characters 24-26: Index constant -2 is negative

  $ cat > file.mlw <<EOF
  > theory T7
  >   use ieee_float.Float64
  >   use caisar.types.Vector
  >   use caisar.model.Model
  >  
  > 
  >   constant nn: model = read_model "TestNetwork.nnet"
  > 
  >   goal G7:
  >     forall i: vector t.
  >       has_length i 5 ->
  >       let o = (nn @@ i)[10] in
  >       o .<= (1.0:t)
  > end
  > EOF

  $ caisar verify --prover nnenum file.mlw
  [ERROR] "file.mlw", line 12, characters 24-26:
          Index constant 10 is out-of-bounds [0,4]

  $ cat > file.mlw <<EOF
  > theory T8
  >   use ieee_float.Float64
  >   use caisar.types.Vector
  >   use caisar.model.Model
  > 
  > 
  >   goal G8:
  >     let svm = read_model "TestSVM.ovo" in
  >     forall x: vector t. has_length x 5 ->
  >       (svm @@ x)[0] .<= (svm @@ x)[1]
  > end
  > EOF

  $ caisar verify --prover SAVer file.mlw
  [ERROR] "file.mlw", line 10, characters 14-15:
          Unexpected vector of length 5 in input to model './TestSVM.ovo',
          which expects input vectors of length 3

  $ cat > file.mlw <<EOF
  > theory T9
  >   use ieee_float.Float64
  >   use caisar.types.Vector
  > 
  > 
  >   goal G9:
  >     let svm = read_model "TestSVM.ovo" in
  >     forall x: vector t. has_length x 3 ->
  >       (svm @@ x)[0] .<= (svm @@ x)[4]
  > end
  > EOF

  $ caisar verify --ltag InterpretGoal --prover SAVer file.mlw
  [ERROR] File "file.mlw", line 7, characters 14-24:
          unbound function or predicate symbol 'read_model'
