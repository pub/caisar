  $ . ./setup_env.sh

  $ ls ../examples/
  acasxu
  arithmetic
  mnist
  onnx_rewrite

  $ caisar verify --prover PyRAT --define model_filename_1:../mnist/nets/splitted/FNN_28x28_pre_s42.onnx --define model_filename_2:../mnist/nets/splitted/FNN_28x28_post_s42.onnx --define dataset_filename:../mnist/csv/single_image.csv ../examples/onnx_rewrite/sequencing.mlw -v
  [INFO] Verification results for theory 'SEQUENCING'
  Goal sequencing: Unknown ()
