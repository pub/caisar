Test verify
  $ . ./setup_env.sh

  $ bin/Marabou --version
  1.0.+

  $ cat > file.mlw <<EOF
  > theory T1
  >   use ieee_float.Float64
  >   use caisar.types.Vector
  >   use caisar.model.Model
  >  
  > 
  >   constant nn: model = read_model "TestNetwork.nnet"
  > 
  >   goal G:
  >     forall i: vector t.
  >       has_length i 5 ->
  >       (0.0:t) .<= i[0] .<= (0.5:t) ->
  >       (0.0:t) .< (nn @@ i)[0] .< (0.5:t)
  > 
  >   goal I:
  >     forall i: vector t.
  >       has_length i 5 ->
  >       (0.0:t) .<= i[0] .<= (0.5:t) /\ (0.5:t) .<= i[1] .<= (1.0:t) ->
  >       (nn @@ i)[1] .< (nn @@ i)[0] \/ (nn @@ i)[0] .< (nn @@ i)[1]
  > 
  >   goal J:
  >     forall i: vector t.
  >       has_length i 5 ->
  >       ((0.0:t) .<= i[0] .<= (0.5:t) \/ (0.75:t) .<= i[0] .<= (1.0:t)) /\ (0.5:t) .<= i[1] .<= (1.0:t) ->
  >       (nn @@ i)[1] .< (nn @@ i)[0] \/ (nn @@ i)[0] .< (nn @@ i)[1]
  > end
  > theory T2
  >   use ieee_float.Float64
  >   use caisar.types.Vector
  >   use caisar.model.Model
  >  
  > 
  >   constant nn: model = read_model "TestNetwork.nnet"
  > 
  >   goal G:
  >     forall i: vector t.
  >       has_length i 5 ->
  >       (0.0:t) .<= i[0] .<= (0.5:t) ->
  >       (0.0:t) .< (nn @@ i)[0] .< (0.5:t)
  > 
  >   goal I:
  >     forall i: vector t.
  >       has_length i 5 ->
  >       (0.0:t) .<= i[0] .<= (0.5:t) /\ (0.5:t) .<= i[1] .<= (1.0:t) ->
  >       (nn @@ i)[1] .< (nn @@ i)[0] \/ (nn @@ i)[0] .< (nn @@ i)[1]
  > 
  >   goal J:
  >     forall i: vector t.
  >       has_length i 5 ->
  >       ((0.0:t) .<= i[0] .<= (0.5:t) \/ (0.75:t) .<= i[0] .<= (1.0:t)) /\ (0.5:t) .<= i[1] .<= (1.0:t) ->
  >       (nn @@ i)[1] .< (nn @@ i)[0] \/ (nn @@ i)[0] .< (nn @@ i)[1]
  > 
  >   goal K:
  >     forall i: vector t.
  >       has_length i 5 ->
  >       (0.0:t) .<= i[0] .<= (0.5:t) ->
  >       (0.0:t) .< (nn @@ i)[0] .< (0.5:t)
  > end
  > EOF

  $ caisar verify --prover=Marabou file.mlw
  Goal G: Unknown ()
  Goal I: Unknown ()
  Goal J: Unknown ()
  Goal G1: Unknown ()
  Goal I1: Unknown ()
  Goal J1: Unknown ()
  Goal K: Unknown ()

  $ caisar verify --prover=Marabou -T T1 file.mlw
  Goal G: Unknown ()
  Goal I: Unknown ()
  Goal J: Unknown ()

  $ caisar verify --prover=Marabou -T T1 -g T2:G file.mlw
  Goal G: Unknown ()
  Goal I: Unknown ()
  Goal J: Unknown ()
  Goal G1: Unknown ()

  $ caisar verify --prover=Marabou -g T1:G -g T2:K file.mlw
  Goal G: Unknown ()
  Goal K: Unknown ()

  $ caisar verify --prover=Marabou -g :I,J file.mlw
  Goal I: Unknown ()
  Goal J: Unknown ()
  Goal I1: Unknown ()
  Goal J1: Unknown ()
