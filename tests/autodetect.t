Test autodetect
  $ . ./setup_env.sh

  $ bin/alt-ergo --version
  2.4.0

  $ bin/pyrat.py --version
  PyRAT 1.1

  $ bin/Marabou --version
  1.0.+

  $ bin/saver --version
  v1.0

  $ bin/aimos --version
  AIMOS 1.0

  $ bin/cvc5 --version
  This is cvc5 version 1.0.2 [git tag 1.0.2 branch HEAD]

  $ bin/z3 --version
  Z3 version 4.8.12 - 64 bits

  $ bin/nnenum.sh --version
  dummy-version

  $ bin/abcrown.sh --version
  dummy-version

  $ bin/runMarabou.py --version
  maraboupy 2.0.0

  $ caisar config -d
  AIMOS 1.0
  Alt-Ergo 2.4.0
  CVC5 1.0.2
  Marabou 1.0.+
  PyRAT 1.1
  PyRAT 1.1 (ACAS)
  PyRAT 1.1 (ACASd)
  PyRAT 1.1 (VNNLIB)
  PyRAT 1.1 (arithmetic)
  SAVer v1.0
  Z3 4.8.12
  alpha-beta-CROWN dummy-version
  alpha-beta-CROWN dummy-version (ACAS)
  maraboupy 2.0.0
  nnenum dummy-version
