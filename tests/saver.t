Test verify
  $ cat - > dataset.csv << EOF
  > 1,0.5,0.5,0.0
  > 0,0.3,0.1,0.8
  > EOF

  $ . ./setup_env.sh

  $ bin/saver --version
  v1.0

  $ cat - > dataset.csv << EOF
  > 1,0.5,0.5,0.0
  > 0,0.3,0.1,0.8
  > EOF

  $ caisar verify --format whyml --prover=SAVer - <<EOF
  > theory T
  >   use ieee_float.Float64
  >   use int.Int
  >   use caisar.types.Float64WithBounds as Feature
  >   use caisar.types.IntWithBounds as Label
  >   use caisar.model.Model
  >   use caisar.dataset.CSV
  >   use caisar.robust.ClassRobustCSV
  > 
  >   constant label_bounds: Label.bounds =
  >     Label.{ lower = 0; upper = 1 }
  > 
  >   constant feature_bounds: Feature.bounds =
  >     Feature.{ lower = (0.0:t); upper = (1.0:t) }
  > 
  >   goal G:
  >     let svm = read_model "TestSVM.ovo" in
  >     let dataset = read_dataset "dataset.csv" in
  >     let eps = (0.0100000000000000002081668171172168513294309377670288085937500000:t) in
  >     robust feature_bounds label_bounds svm dataset eps
  > 
  >   goal H:
  >     let svm = read_model_with_abstraction "TestSVM.ovo" Raf in
  >     let dataset = read_dataset "dataset.csv" in
  >     let eps = (0.0100000000000000002081668171172168513294309377670288085937500000:t) in
  >     robust feature_bounds label_bounds svm dataset eps
  > end
  > EOF
  Goal G: Unknown ()
  Goal H: Unknown ()

  $ cat - > dataset.csv << EOF
  > # 2 3
  > 1,0.5,0.5,0.0
  > 0,0.3,0.1,0.8
  > EOF

  $ caisar verify --format whyml --prover=SAVer - <<EOF
  > theory T
  >   use ieee_float.Float64
  >   use int.Int
  >   use caisar.types.Float64WithBounds as Feature
  >   use caisar.types.IntWithBounds as Label
  >   use caisar.model.Model
  >   use caisar.dataset.CSV
  >   use caisar.robust.ClassRobustCSV
  > 
  >   constant label_bounds: Label.bounds =
  >     Label.{ lower = 0; upper = 1 }
  > 
  >   constant feature_bounds: Feature.bounds =
  >     Feature.{ lower = (0.0:t); upper = (1.0:t) }
  > 
  >   goal G:
  >     let svm = read_model "TestSVM.ovo" in
  >     let dataset = read_dataset "dataset.csv" in
  >     let eps = (0.0100000000000000002081668171172168513294309377670288085937500000:t) in
  >     robust feature_bounds label_bounds svm dataset eps
  > 
  >   goal H:
  >     let svm = read_model_with_abstraction "TestSVM.ovo" Raf in
  >     let dataset = read_dataset "dataset.csv" in
  >     let eps = (0.0100000000000000002081668171172168513294309377670288085937500000:t) in
  >     robust feature_bounds label_bounds svm dataset eps
  > end
  > EOF
  Goal G: Unknown ()
  Goal H: Unknown ()

  $ caisar verify --format whyml --prover=SAVer - <<EOF
  > theory T
  >   use ieee_float.Float64
  >   use int.Int
  >   use caisar.types.Vector
  >   use caisar.types.Float64WithBounds as Feature
  >   use caisar.types.IntWithBounds as Label
  >   use caisar.model.Model
  >   use caisar.dataset.CSV
  >   use caisar.robust.ClassRobustCSV
  > 
  >   goal G:
  >     let svm = read_model "TestSVM.ovo" in
  >     let eps = (0.375:t) in
  >     forall x: vector t. has_length x 3 ->
  >       (.- eps) .<= x[0] .- (0.299999999999999988897769753748434595763683319091796875:t) .<= eps
  >       /\ (.- eps) .<= x[1] .- (0.8000000000000000444089209850062616169452667236328125:t) .<= eps
  >       /\ (.- eps) .<= x[2] .- (0.5:t) .<= eps ->
  >       (svm @@ x)[0] .<= (svm @@ x)[1]
  > end
  > EOF
  Goal G: Unknown ()
