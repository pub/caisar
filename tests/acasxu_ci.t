Test verify on acasxu
  $ . ./setup_env.sh

  $ bin/pyrat.py --version
  PyRAT 1.1

  $ mkdir out
  $ export CAISAR_ONNX_OUTPUT_DIR=out

  $ cat > file.mlw <<EOF
  > theory T
  >   use ieee_float.Float64
  >   use int.Int
  >   use caisar.types.Vector
  >   use caisar.model.Model
  >  
  > 
  >   let constant nn: model = read_model "../examples/acasxu/nets/onnx/ACASXU_1_1.onnx"
  >   let constant nn_: model = read_model "../examples/acasxu/nets/onnx/ACASXU_1_9.onnx"
  > 
  >   type input = vector t
  > 
  >   let constant distance_to_intruder: int = 0
  >   let constant angle_to_intruder: int = 1
  >   let constant intruder_heading: int = 2
  >   let constant speed: int = 3
  >   let constant intruder_speed: int = 4
  > 
  >   type action = int
  > 
  >   let constant clear_of_conflict: action = 0
  >   let constant weak_left: action = 1
  >   let constant weak_right: action = 2
  >   let constant strong_left: action = 3
  >   let constant strong_right: action = 4
  > 
  >   let constant pi: t = 3.141592653589793115997963468544185161590576171875000
  > 
  >   predicate valid_input (i: input) =
  >     (0.0:t) .<= i[distance_to_intruder] .<= (60760.0:t)
  >     /\ .- pi .<= i[angle_to_intruder] .<= pi
  >     /\ .- pi .<= i[intruder_heading] .<= pi
  >     /\ (100.0:t) .<= i[speed] .<= (1200.0:t)
  >     /\ (0.0:t) .<= i[intruder_speed] .<= (1200.0:t)
  > 
  >   predicate valid_action (a: action) = clear_of_conflict <= a <= strong_right
  > 
  >   predicate advises (o: vector t) (a: action) =
  >     valid_action a ->
  >     forall b: action. valid_action b ->  a <> b -> o[a] .< o[b]
  > 
  >   predicate intruder_distant_and_slow (i: input) =
  >     i[distance_to_intruder] .>= (55947.6909999999988940544426441192626953125:t)
  >     /\ i[speed] .>= (1145.0:t)
  >     /\ i[intruder_speed] .<= (60.0:t)
  > 
  >   let function normalize_t (i: t) (mean: t) (range: t) : t = ((i .- mean) ./ range)
  > 
  >   let function normalize_by_index (idx: int) (t: t) : t =
  >     if idx = distance_to_intruder then normalize_t t (19791.0:t) (60261.0:t)
  >     else if idx = angle_to_intruder then normalize_t t (0.0:t) (6.2831853071800001231395071954466402530670166015625:t)
  >     else if idx = intruder_heading then normalize_t t (0.0:t) (6.2831853071800001231395071954466402530670166015625:t)
  >     else if idx = speed then normalize_t t (650.0:t) (1100.0:t)
  >     else if idx = intruder_speed then normalize_t t (600.0:t) (1200.0:t)
  >     else t
  > 
  >   let function normalize_input (i:input) : input =
  >     Vector.mapi i normalize_by_index
  > 
  >   let function denormalize_t (i: t) (mean: t) (range: t) : t = (i .* range) .+ mean
  > 
  >   let function denormalize_output (o: t) : t =
  >     denormalize_t o (7.51888402010059753166615337249822914600372314453125:t) (373.9499200000000200816430151462554931640625:t)
  > 
  >   let run (j:input) : t
  >      requires { has_length j 5 }
  >      requires { valid_input j }
  >      requires { intruder_distant_and_slow j }
  >      ensures { result .<= (1500.0:t) }  =
  >       let i = normalize_input j in
  >       let o = (nn@@i)[clear_of_conflict] in
  >       (denormalize_output o)
  > 
  >   let run1 (j: input)
  >     requires { has_length j 5 }
  >     requires { valid_input j }
  >     requires { intruder_distant_and_slow j }
  >     ensures { let o1, o2 = result in o1 .<= (1500.0:t) /\ o2 .<= (1500.0:t) }  =
  >       let i = normalize_input j in
  >       let o1 = (nn @@ i)[clear_of_conflict] in
  >       let o2 = (nn_ @@ i)[clear_of_conflict] in
  >       (denormalize_output o1), (denormalize_output o2)
  > 
  >   let run2 (j:input)
  >      requires { has_length j 5 }
  >      requires { valid_input j }
  >      requires { intruder_distant_and_slow j }
  >      ensures { let o1, o2 = result in o1 .>= o2 /\ o2 .>= o1 }  =
  >       let i = normalize_input j in
  >       let o1 = (nn@@i)[clear_of_conflict] in
  >       let o2 = (nn@@i)[weak_left] in
  >       o1, o2
  > 
  >     let run3 (j:input) (eps:t)
  >      requires { has_length j 5 }
  >      requires { valid_input j }
  >      requires { intruder_distant_and_slow j }
  >      requires { (-1.:t) .<= eps .<= (1.:t) }
  >      ensures { let o1, o2 = result in o1 .>= o2 /\ o2 .>= o1 }  =
  >       let i = normalize_input j in
  >       let j2 = Vector.mapi j (fun i x -> if i = distance_to_intruder then x .+ eps else x) in
  >       let i2 = normalize_input j2 in
  >       let o1 = (nn@@i)[clear_of_conflict] in
  >       let o2 = (nn@@i2)[clear_of_conflict] in
  >       o1, o2
  > 
  >   predicate directly_ahead (i: input) =
  >     (1500.0:t) .<= i[distance_to_intruder] .<= (1800.0:t)
  >     /\ .- (0.059999999999999997779553950749686919152736663818359375:t) .<= i[angle_to_intruder] .<= (0.059999999999999997779553950749686919152736663818359375:t)
  > 
  >   predicate moving_towards (i: input) =
  >     i[intruder_heading] .>= (3.100000000000000088817841970012523233890533447265625:t)
  >     /\ i[speed] .>= (980.0:t)
  >     /\ i[intruder_speed] .>= (960.0:t)
  > 
  > 
  >   let runP3 (j: input) : vector t
  >      requires { has_length j 5 }
  >      requires { valid_input j }
  >      requires { directly_ahead j /\ moving_towards j }
  >      ensures { not (advises result clear_of_conflict) }  =
  >       let i = normalize_input j in
  >       nn @@ i
  > 
  > end
  > EOF

  $ caisar verify --prover PyRAT --ltag=ProverSpec --ltag=InterpretGoal file.mlw
  [DEBUG]{InterpretGoal} Interpreted formula for goal 'run'vc':
  forall x:t, x1:t, x2:t, x3:t, x4:t.
   ((le (0.0:t) x /\ le x (60760.0:t)) /\
    (le (neg (3.141592653589793115997963468544185161590576171875:t)) x1 /\
     le x1 (3.141592653589793115997963468544185161590576171875:t)) /\
    (le (neg (3.141592653589793115997963468544185161590576171875:t)) x2 /\
     le x2 (3.141592653589793115997963468544185161590576171875:t)) /\
    (le (100.0:t) x3 /\ le x3 (1200.0:t)) /\ le (0.0:t) x4 /\ le x4 (1200.0:t)) /\
   le (55947.6909999999988940544426441192626953125:t) x /\
   le (1145.0:t) x3 /\ le x4 (60.0:t) ->
   le
   (add RNE
    (mul RNE
     (nn_onnx
      @@ vector (div RNE (sub RNE x (19791.0:t)) (60261.0:t))
         (div RNE (sub RNE x1 (0.0:t))
          (6.2831853071800001231395071954466402530670166015625:t))
         (div RNE (sub RNE x2 (0.0:t))
          (6.2831853071800001231395071954466402530670166015625:t))
         (div RNE (sub RNE x3 (650.0:t)) (1100.0:t))
         (div RNE (sub RNE x4 (600.0:t)) (1200.0:t)))
     [0] (373.9499200000000200816430151462554931640625:t))
    (7.51888402010059753166615337249822914600372314453125:t))
   (1500.0:t)
  nn_onnx,
  (Interpreter_types.Model
     (Interpreter_types.ONNX, { Language.nn_nb_inputs = 5; nn_nb_outputs = 5;
                                nn_ty_elt = t;
                                nn_filename =
                                "./../examples/acasxu/nets/onnx/ACASXU_1_1.onnx";
                                nn_format = <nir> }))
  vector,
  5
  [DEBUG]{ProverSpec} Prover-tailored specification:
  0.0 <= x0
  x0 <= 60760.0
  -3.141592653589793115997963468544185161590576171875 <= x1
  x1 <= 3.141592653589793115997963468544185161590576171875
  -3.141592653589793115997963468544185161590576171875 <= x2
  x2 <= 3.141592653589793115997963468544185161590576171875
  100.0 <= x3
  x3 <= 1200.0
  0.0 <= x4
  x4 <= 1200.0
  55947.6909999999988940544426441192626953125 <= x0
  1145.0 <= x3
  x4 <= 60.0
  y0 <= 1500.0
  
  [DEBUG]{InterpretGoal} Interpreted formula for goal 'run1'vc':
  forall x:t, x1:t, x2:t, x3:t, x4:t.
   ((le (0.0:t) x /\ le x (60760.0:t)) /\
    (le (neg (3.141592653589793115997963468544185161590576171875:t)) x1 /\
     le x1 (3.141592653589793115997963468544185161590576171875:t)) /\
    (le (neg (3.141592653589793115997963468544185161590576171875:t)) x2 /\
     le x2 (3.141592653589793115997963468544185161590576171875:t)) /\
    (le (100.0:t) x3 /\ le x3 (1200.0:t)) /\ le (0.0:t) x4 /\ le x4 (1200.0:t)) /\
   le (55947.6909999999988940544426441192626953125:t) x /\
   le (1145.0:t) x3 /\ le x4 (60.0:t) ->
   le
   (add RNE
    (mul RNE
     (nn_onnx
      @@ vector (div RNE (sub RNE x (19791.0:t)) (60261.0:t))
         (div RNE (sub RNE x1 (0.0:t))
          (6.2831853071800001231395071954466402530670166015625:t))
         (div RNE (sub RNE x2 (0.0:t))
          (6.2831853071800001231395071954466402530670166015625:t))
         (div RNE (sub RNE x3 (650.0:t)) (1100.0:t))
         (div RNE (sub RNE x4 (600.0:t)) (1200.0:t)))
     [0] (373.9499200000000200816430151462554931640625:t))
    (7.51888402010059753166615337249822914600372314453125:t))
   (1500.0:t) /\
   le
   (add RNE
    (mul RNE
     (nn_onnx1
      @@ vector (div RNE (sub RNE x (19791.0:t)) (60261.0:t))
         (div RNE (sub RNE x1 (0.0:t))
          (6.2831853071800001231395071954466402530670166015625:t))
         (div RNE (sub RNE x2 (0.0:t))
          (6.2831853071800001231395071954466402530670166015625:t))
         (div RNE (sub RNE x3 (650.0:t)) (1100.0:t))
         (div RNE (sub RNE x4 (600.0:t)) (1200.0:t)))
     [0] (373.9499200000000200816430151462554931640625:t))
    (7.51888402010059753166615337249822914600372314453125:t))
   (1500.0:t)
  nn_onnx,
  (Interpreter_types.Model
     (Interpreter_types.ONNX, { Language.nn_nb_inputs = 5; nn_nb_outputs = 5;
                                nn_ty_elt = t;
                                nn_filename =
                                "./../examples/acasxu/nets/onnx/ACASXU_1_1.onnx";
                                nn_format = <nir> }))
  vector, 5
  nn_onnx1,
  (Interpreter_types.Model
     (Interpreter_types.ONNX, { Language.nn_nb_inputs = 5; nn_nb_outputs = 5;
                                nn_ty_elt = t;
                                nn_filename =
                                "./../examples/acasxu/nets/onnx/ACASXU_1_9.onnx";
                                nn_format = <nir> }))
  [DEBUG]{ProverSpec} Prover-tailored specification:
  0.0 <= x0
  x0 <= 60760.0
  -3.141592653589793115997963468544185161590576171875 <= x1
  x1 <= 3.141592653589793115997963468544185161590576171875
  -3.141592653589793115997963468544185161590576171875 <= x2
  x2 <= 3.141592653589793115997963468544185161590576171875
  100.0 <= x3
  x3 <= 1200.0
  0.0 <= x4
  x4 <= 1200.0
  55947.6909999999988940544426441192626953125 <= x0
  1145.0 <= x3
  x4 <= 60.0
  y0 <= 1500.0 and y1 <= 1500.0
  
  [DEBUG]{InterpretGoal} Interpreted formula for goal 'run2'vc':
  forall x:t, x1:t, x2:t, x3:t, x4:t.
   ((le (0.0:t) x /\ le x (60760.0:t)) /\
    (le (neg (3.141592653589793115997963468544185161590576171875:t)) x1 /\
     le x1 (3.141592653589793115997963468544185161590576171875:t)) /\
    (le (neg (3.141592653589793115997963468544185161590576171875:t)) x2 /\
     le x2 (3.141592653589793115997963468544185161590576171875:t)) /\
    (le (100.0:t) x3 /\ le x3 (1200.0:t)) /\ le (0.0:t) x4 /\ le x4 (1200.0:t)) /\
   le (55947.6909999999988940544426441192626953125:t) x /\
   le (1145.0:t) x3 /\ le x4 (60.0:t) ->
   le
   (nn_onnx
    @@ vector (div RNE (sub RNE x (19791.0:t)) (60261.0:t))
       (div RNE (sub RNE x1 (0.0:t))
        (6.2831853071800001231395071954466402530670166015625:t))
       (div RNE (sub RNE x2 (0.0:t))
        (6.2831853071800001231395071954466402530670166015625:t))
       (div RNE (sub RNE x3 (650.0:t)) (1100.0:t))
       (div RNE (sub RNE x4 (600.0:t)) (1200.0:t)))
   [1]
   (nn_onnx
    @@ vector (div RNE (sub RNE x (19791.0:t)) (60261.0:t))
       (div RNE (sub RNE x1 (0.0:t))
        (6.2831853071800001231395071954466402530670166015625:t))
       (div RNE (sub RNE x2 (0.0:t))
        (6.2831853071800001231395071954466402530670166015625:t))
       (div RNE (sub RNE x3 (650.0:t)) (1100.0:t))
       (div RNE (sub RNE x4 (600.0:t)) (1200.0:t)))
   [0] /\
   le
   (nn_onnx
    @@ vector (div RNE (sub RNE x (19791.0:t)) (60261.0:t))
       (div RNE (sub RNE x1 (0.0:t))
        (6.2831853071800001231395071954466402530670166015625:t))
       (div RNE (sub RNE x2 (0.0:t))
        (6.2831853071800001231395071954466402530670166015625:t))
       (div RNE (sub RNE x3 (650.0:t)) (1100.0:t))
       (div RNE (sub RNE x4 (600.0:t)) (1200.0:t)))
   [0]
   (nn_onnx
    @@ vector (div RNE (sub RNE x (19791.0:t)) (60261.0:t))
       (div RNE (sub RNE x1 (0.0:t))
        (6.2831853071800001231395071954466402530670166015625:t))
       (div RNE (sub RNE x2 (0.0:t))
        (6.2831853071800001231395071954466402530670166015625:t))
       (div RNE (sub RNE x3 (650.0:t)) (1100.0:t))
       (div RNE (sub RNE x4 (600.0:t)) (1200.0:t)))
   [1]
  nn_onnx,
  (Interpreter_types.Model
     (Interpreter_types.ONNX, { Language.nn_nb_inputs = 5; nn_nb_outputs = 5;
                                nn_ty_elt = t;
                                nn_filename =
                                "./../examples/acasxu/nets/onnx/ACASXU_1_1.onnx";
                                nn_format = <nir> }))
  vector,
  5
  [DEBUG]{ProverSpec} Prover-tailored specification:
  0.0 <= x0
  x0 <= 60760.0
  -3.141592653589793115997963468544185161590576171875 <= x1
  x1 <= 3.141592653589793115997963468544185161590576171875
  -3.141592653589793115997963468544185161590576171875 <= x2
  x2 <= 3.141592653589793115997963468544185161590576171875
  100.0 <= x3
  x3 <= 1200.0
  0.0 <= x4
  x4 <= 1200.0
  55947.6909999999988940544426441192626953125 <= x0
  1145.0 <= x3
  x4 <= 60.0
  y1 <= y0 and y0 <= y1
  
  [DEBUG]{InterpretGoal} Interpreted formula for goal 'run3'vc':
  forall x:t, x1:t, x2:t, x3:t, x4:t, eps:t.
   ((le (0.0:t) x /\ le x (60760.0:t)) /\
    (le (neg (3.141592653589793115997963468544185161590576171875:t)) x1 /\
     le x1 (3.141592653589793115997963468544185161590576171875:t)) /\
    (le (neg (3.141592653589793115997963468544185161590576171875:t)) x2 /\
     le x2 (3.141592653589793115997963468544185161590576171875:t)) /\
    (le (100.0:t) x3 /\ le x3 (1200.0:t)) /\ le (0.0:t) x4 /\ le x4 (1200.0:t)) /\
   (le (55947.6909999999988940544426441192626953125:t) x /\
    le (1145.0:t) x3 /\ le x4 (60.0:t)) /\
   le ((- 1.0):t) eps /\ le eps (1.0:t) ->
   le
   (nn_onnx
    @@ vector (div RNE (sub RNE (add RNE x eps) (19791.0:t)) (60261.0:t))
       (div RNE (sub RNE x1 (0.0:t))
        (6.2831853071800001231395071954466402530670166015625:t))
       (div RNE (sub RNE x2 (0.0:t))
        (6.2831853071800001231395071954466402530670166015625:t))
       (div RNE (sub RNE x3 (650.0:t)) (1100.0:t))
       (div RNE (sub RNE x4 (600.0:t)) (1200.0:t)))
   [0]
   (nn_onnx
    @@ vector (div RNE (sub RNE x (19791.0:t)) (60261.0:t))
       (div RNE (sub RNE x1 (0.0:t))
        (6.2831853071800001231395071954466402530670166015625:t))
       (div RNE (sub RNE x2 (0.0:t))
        (6.2831853071800001231395071954466402530670166015625:t))
       (div RNE (sub RNE x3 (650.0:t)) (1100.0:t))
       (div RNE (sub RNE x4 (600.0:t)) (1200.0:t)))
   [0] /\
   le
   (nn_onnx
    @@ vector (div RNE (sub RNE x (19791.0:t)) (60261.0:t))
       (div RNE (sub RNE x1 (0.0:t))
        (6.2831853071800001231395071954466402530670166015625:t))
       (div RNE (sub RNE x2 (0.0:t))
        (6.2831853071800001231395071954466402530670166015625:t))
       (div RNE (sub RNE x3 (650.0:t)) (1100.0:t))
       (div RNE (sub RNE x4 (600.0:t)) (1200.0:t)))
   [0]
   (nn_onnx
    @@ vector (div RNE (sub RNE (add RNE x eps) (19791.0:t)) (60261.0:t))
       (div RNE (sub RNE x1 (0.0:t))
        (6.2831853071800001231395071954466402530670166015625:t))
       (div RNE (sub RNE x2 (0.0:t))
        (6.2831853071800001231395071954466402530670166015625:t))
       (div RNE (sub RNE x3 (650.0:t)) (1100.0:t))
       (div RNE (sub RNE x4 (600.0:t)) (1200.0:t)))
   [0]
  nn_onnx,
  (Interpreter_types.Model
     (Interpreter_types.ONNX, { Language.nn_nb_inputs = 5; nn_nb_outputs = 5;
                                nn_ty_elt = t;
                                nn_filename =
                                "./../examples/acasxu/nets/onnx/ACASXU_1_1.onnx";
                                nn_format = <nir> }))
  vector,
  5
  [DEBUG]{ProverSpec} Prover-tailored specification:
  0.0 <= x0
  x0 <= 60760.0
  -3.141592653589793115997963468544185161590576171875 <= x1
  x1 <= 3.141592653589793115997963468544185161590576171875
  -3.141592653589793115997963468544185161590576171875 <= x2
  x2 <= 3.141592653589793115997963468544185161590576171875
  100.0 <= x3
  x3 <= 1200.0
  0.0 <= x4
  x4 <= 1200.0
  55947.6909999999988940544426441192626953125 <= x0
  1145.0 <= x3
  x4 <= 60.0
  -1.0 <= x5
  x5 <= 1.0
  y0 <= y1 and y1 <= y0
  
  [DEBUG]{InterpretGoal} Interpreted formula for goal 'runP3'vc':
  forall x:t, x1:t, x2:t, x3:t, x4:t.
   ((le (0.0:t) x /\ le x (60760.0:t)) /\
    (le (neg (3.141592653589793115997963468544185161590576171875:t)) x1 /\
     le x1 (3.141592653589793115997963468544185161590576171875:t)) /\
    (le (neg (3.141592653589793115997963468544185161590576171875:t)) x2 /\
     le x2 (3.141592653589793115997963468544185161590576171875:t)) /\
    (le (100.0:t) x3 /\ le x3 (1200.0:t)) /\ le (0.0:t) x4 /\ le x4 (1200.0:t)) /\
   ((le (1500.0:t) x /\ le x (1800.0:t)) /\
    le (neg (0.059999999999999997779553950749686919152736663818359375:t)) x1 /\
    le x1 (0.059999999999999997779553950749686919152736663818359375:t)) /\
   le (3.100000000000000088817841970012523233890533447265625:t) x2 /\
   le (980.0:t) x3 /\ le (960.0:t) x4 ->
   not (((lt
          (nn_onnx
           @@ vector (div RNE (sub RNE x (19791.0:t)) (60261.0:t))
              (div RNE (sub RNE x1 (0.0:t))
               (6.2831853071800001231395071954466402530670166015625:t))
              (div RNE (sub RNE x2 (0.0:t))
               (6.2831853071800001231395071954466402530670166015625:t))
              (div RNE (sub RNE x3 (650.0:t)) (1100.0:t))
              (div RNE (sub RNE x4 (600.0:t)) (1200.0:t)))
          [0]
          (nn_onnx
           @@ vector (div RNE (sub RNE x (19791.0:t)) (60261.0:t))
              (div RNE (sub RNE x1 (0.0:t))
               (6.2831853071800001231395071954466402530670166015625:t))
              (div RNE (sub RNE x2 (0.0:t))
               (6.2831853071800001231395071954466402530670166015625:t))
              (div RNE (sub RNE x3 (650.0:t)) (1100.0:t))
              (div RNE (sub RNE x4 (600.0:t)) (1200.0:t)))
          [1] /\
          lt
          (nn_onnx
           @@ vector (div RNE (sub RNE x (19791.0:t)) (60261.0:t))
              (div RNE (sub RNE x1 (0.0:t))
               (6.2831853071800001231395071954466402530670166015625:t))
              (div RNE (sub RNE x2 (0.0:t))
               (6.2831853071800001231395071954466402530670166015625:t))
              (div RNE (sub RNE x3 (650.0:t)) (1100.0:t))
              (div RNE (sub RNE x4 (600.0:t)) (1200.0:t)))
          [0]
          (nn_onnx
           @@ vector (div RNE (sub RNE x (19791.0:t)) (60261.0:t))
              (div RNE (sub RNE x1 (0.0:t))
               (6.2831853071800001231395071954466402530670166015625:t))
              (div RNE (sub RNE x2 (0.0:t))
               (6.2831853071800001231395071954466402530670166015625:t))
              (div RNE (sub RNE x3 (650.0:t)) (1100.0:t))
              (div RNE (sub RNE x4 (600.0:t)) (1200.0:t)))
          [2]) /\
         lt
         (nn_onnx
          @@ vector (div RNE (sub RNE x (19791.0:t)) (60261.0:t))
             (div RNE (sub RNE x1 (0.0:t))
              (6.2831853071800001231395071954466402530670166015625:t))
             (div RNE (sub RNE x2 (0.0:t))
              (6.2831853071800001231395071954466402530670166015625:t))
             (div RNE (sub RNE x3 (650.0:t)) (1100.0:t))
             (div RNE (sub RNE x4 (600.0:t)) (1200.0:t)))
         [0]
         (nn_onnx
          @@ vector (div RNE (sub RNE x (19791.0:t)) (60261.0:t))
             (div RNE (sub RNE x1 (0.0:t))
              (6.2831853071800001231395071954466402530670166015625:t))
             (div RNE (sub RNE x2 (0.0:t))
              (6.2831853071800001231395071954466402530670166015625:t))
             (div RNE (sub RNE x3 (650.0:t)) (1100.0:t))
             (div RNE (sub RNE x4 (600.0:t)) (1200.0:t)))
         [3]) /\
        lt
        (nn_onnx
         @@ vector (div RNE (sub RNE x (19791.0:t)) (60261.0:t))
            (div RNE (sub RNE x1 (0.0:t))
             (6.2831853071800001231395071954466402530670166015625:t))
            (div RNE (sub RNE x2 (0.0:t))
             (6.2831853071800001231395071954466402530670166015625:t))
            (div RNE (sub RNE x3 (650.0:t)) (1100.0:t))
            (div RNE (sub RNE x4 (600.0:t)) (1200.0:t)))
        [0]
        (nn_onnx
         @@ vector (div RNE (sub RNE x (19791.0:t)) (60261.0:t))
            (div RNE (sub RNE x1 (0.0:t))
             (6.2831853071800001231395071954466402530670166015625:t))
            (div RNE (sub RNE x2 (0.0:t))
             (6.2831853071800001231395071954466402530670166015625:t))
            (div RNE (sub RNE x3 (650.0:t)) (1100.0:t))
            (div RNE (sub RNE x4 (600.0:t)) (1200.0:t)))
        [4])
  nn_onnx,
  (Interpreter_types.Model
     (Interpreter_types.ONNX, { Language.nn_nb_inputs = 5; nn_nb_outputs = 5;
                                nn_ty_elt = t;
                                nn_filename =
                                "./../examples/acasxu/nets/onnx/ACASXU_1_1.onnx";
                                nn_format = <nir> }))
  vector,
  5
  [DEBUG]{ProverSpec} Prover-tailored specification:
  0.0 <= x0
  x0 <= 60760.0
  -3.141592653589793115997963468544185161590576171875 <= x1
  x1 <= 3.141592653589793115997963468544185161590576171875
  -3.141592653589793115997963468544185161590576171875 <= x2
  x2 <= 3.141592653589793115997963468544185161590576171875
  100.0 <= x3
  x3 <= 1200.0
  0.0 <= x4
  x4 <= 1200.0
  1500.0 <= x0
  x0 <= 1800.0
  -0.059999999999999997779553950749686919152736663818359375 <= x1
  x1 <= 0.059999999999999997779553950749686919152736663818359375
  3.100000000000000088817841970012523233890533447265625 <= x2
  980.0 <= x3
  960.0 <= x4
  (((y0 >= y1 or y0 >= y2) or y0 >= y3) or y0 >= y4)
  
  Goal run'vc: Unknown ()
  Goal run1'vc: Unknown ()
  Goal run2'vc: Unknown ()
  Goal run3'vc: Unknown ()
  Goal runP3'vc: Unknown ()

  $ caisar verify --prover PyRAT --prover-altern VNNLIB --ltag=ProverSpec file.mlw
  [DEBUG]{ProverSpec} Prover-tailored specification:
  ;;; produced by PyRAT/VNN-LIB driver
  ;;; produced by VNN-LIB driver
  ;; X_0
  (declare-const X_0 Real)
  
  ;; X_1
  (declare-const X_1 Real)
  
  ;; X_2
  (declare-const X_2 Real)
  
  ;; X_3
  (declare-const X_3 Real)
  
  ;; X_4
  (declare-const X_4 Real)
  
  ;; Requires
  (assert (>= X_0 0.0))
  (assert (<= X_0 60760.0))
  (assert (>= X_1 -3.141592653589793115997963468544185161590576171875))
  (assert (<= X_1 3.141592653589793115997963468544185161590576171875))
  (assert (>= X_2 -3.141592653589793115997963468544185161590576171875))
  (assert (<= X_2 3.141592653589793115997963468544185161590576171875))
  (assert (>= X_3 100.0))
  (assert (<= X_3 1200.0))
  (assert (>= X_4 0.0))
  (assert (<= X_4 1200.0))
  
  ;; Requires
  (assert (>= X_0 55947.6909999999988940544426441192626953125))
  (assert (>= X_3 1145.0))
  (assert (<= X_4 60.0))
  
  ;; Y_0
  (declare-const Y_0 Real)
  
  ;; Goal run'vc
  (assert (>= Y_0 1500.0))
  
  [DEBUG]{ProverSpec} Prover-tailored specification:
  ;;; produced by PyRAT/VNN-LIB driver
  ;;; produced by VNN-LIB driver
  ;; X_0
  (declare-const X_0 Real)
  
  ;; X_1
  (declare-const X_1 Real)
  
  ;; X_2
  (declare-const X_2 Real)
  
  ;; X_3
  (declare-const X_3 Real)
  
  ;; X_4
  (declare-const X_4 Real)
  
  ;; Requires
  (assert (>= X_0 0.0))
  (assert (<= X_0 60760.0))
  (assert (>= X_1 -3.141592653589793115997963468544185161590576171875))
  (assert (<= X_1 3.141592653589793115997963468544185161590576171875))
  (assert (>= X_2 -3.141592653589793115997963468544185161590576171875))
  (assert (<= X_2 3.141592653589793115997963468544185161590576171875))
  (assert (>= X_3 100.0))
  (assert (<= X_3 1200.0))
  (assert (>= X_4 0.0))
  (assert (<= X_4 1200.0))
  
  ;; Requires
  (assert (>= X_0 55947.6909999999988940544426441192626953125))
  (assert (>= X_3 1145.0))
  (assert (<= X_4 60.0))
  
  ;; Y_0
  (declare-const Y_0 Real)
  
  ;; Y_1
  (declare-const Y_1 Real)
  
  ;; Goal run1'vc
  (assert (or (and (>= Y_0 1500.0))
              (and (>= Y_1 1500.0))
              ))
  
  [DEBUG]{ProverSpec} Prover-tailored specification:
  ;;; produced by PyRAT/VNN-LIB driver
  ;;; produced by VNN-LIB driver
  ;; X_0
  (declare-const X_0 Real)
  
  ;; X_1
  (declare-const X_1 Real)
  
  ;; X_2
  (declare-const X_2 Real)
  
  ;; X_3
  (declare-const X_3 Real)
  
  ;; X_4
  (declare-const X_4 Real)
  
  ;; Requires
  (assert (>= X_0 0.0))
  (assert (<= X_0 60760.0))
  (assert (>= X_1 -3.141592653589793115997963468544185161590576171875))
  (assert (<= X_1 3.141592653589793115997963468544185161590576171875))
  (assert (>= X_2 -3.141592653589793115997963468544185161590576171875))
  (assert (<= X_2 3.141592653589793115997963468544185161590576171875))
  (assert (>= X_3 100.0))
  (assert (<= X_3 1200.0))
  (assert (>= X_4 0.0))
  (assert (<= X_4 1200.0))
  
  ;; Requires
  (assert (>= X_0 55947.6909999999988940544426441192626953125))
  (assert (>= X_3 1145.0))
  (assert (<= X_4 60.0))
  
  ;; Y_0
  (declare-const Y_0 Real)
  
  ;; Y_1
  (declare-const Y_1 Real)
  
  ;; Goal run2'vc
  (assert (or (and (>= Y_1 Y_0))
              (and (>= Y_0 Y_1))
              ))
  
  [DEBUG]{ProverSpec} Prover-tailored specification:
  ;;; produced by PyRAT/VNN-LIB driver
  ;;; produced by VNN-LIB driver
  ;; X_0
  (declare-const X_0 Real)
  
  ;; X_1
  (declare-const X_1 Real)
  
  ;; X_2
  (declare-const X_2 Real)
  
  ;; X_3
  (declare-const X_3 Real)
  
  ;; X_4
  (declare-const X_4 Real)
  
  ;; X_5
  (declare-const X_5 Real)
  
  ;; Requires
  (assert (>= X_0 0.0))
  (assert (<= X_0 60760.0))
  (assert (>= X_1 -3.141592653589793115997963468544185161590576171875))
  (assert (<= X_1 3.141592653589793115997963468544185161590576171875))
  (assert (>= X_2 -3.141592653589793115997963468544185161590576171875))
  (assert (<= X_2 3.141592653589793115997963468544185161590576171875))
  (assert (>= X_3 100.0))
  (assert (<= X_3 1200.0))
  (assert (>= X_4 0.0))
  (assert (<= X_4 1200.0))
  
  ;; Requires
  (assert (>= X_0 55947.6909999999988940544426441192626953125))
  (assert (>= X_3 1145.0))
  (assert (<= X_4 60.0))
  
  ;; H
  (assert (>= X_5 -1.0))
  
  ;; H
  (assert (<= X_5 1.0))
  
  ;; Y_0
  (declare-const Y_0 Real)
  
  ;; Y_1
  (declare-const Y_1 Real)
  
  ;; Goal run3'vc
  (assert (or (and (>= Y_0 Y_1))
              (and (>= Y_1 Y_0))
              ))
  
  [DEBUG]{ProverSpec} Prover-tailored specification:
  ;;; produced by PyRAT/VNN-LIB driver
  ;;; produced by VNN-LIB driver
  ;; X_0
  (declare-const X_0 Real)
  
  ;; X_1
  (declare-const X_1 Real)
  
  ;; X_2
  (declare-const X_2 Real)
  
  ;; X_3
  (declare-const X_3 Real)
  
  ;; X_4
  (declare-const X_4 Real)
  
  ;; Requires
  (assert (>= X_0 0.0))
  (assert (<= X_0 60760.0))
  (assert (>= X_1 -3.141592653589793115997963468544185161590576171875))
  (assert (<= X_1 3.141592653589793115997963468544185161590576171875))
  (assert (>= X_2 -3.141592653589793115997963468544185161590576171875))
  (assert (<= X_2 3.141592653589793115997963468544185161590576171875))
  (assert (>= X_3 100.0))
  (assert (<= X_3 1200.0))
  (assert (>= X_4 0.0))
  (assert (<= X_4 1200.0))
  
  ;; H
  (assert (>= X_0 1500.0))
  (assert (<= X_0 1800.0))
  (assert (>= X_1 -0.059999999999999997779553950749686919152736663818359375))
  (assert (<= X_1 0.059999999999999997779553950749686919152736663818359375))
  
  ;; H
  (assert (>= X_2 3.100000000000000088817841970012523233890533447265625))
  (assert (>= X_3 980.0))
  (assert (>= X_4 960.0))
  
  ;; Y_0
  (declare-const Y_0 Real)
  
  ;; Y_1
  (declare-const Y_1 Real)
  
  ;; Y_2
  (declare-const Y_2 Real)
  
  ;; Y_3
  (declare-const Y_3 Real)
  
  ;; Y_4
  (declare-const Y_4 Real)
  
  ;; Goal runP3'vc
  (assert (<= Y_0 Y_1))
  (assert (<= Y_0 Y_2))
  (assert (<= Y_0 Y_3))
  (assert (<= Y_0 Y_4))
  
  Goal run'vc: Unknown ()
  Goal run1'vc: Unknown ()
  Goal run2'vc: Unknown ()
  Goal run3'vc: Unknown ()
  Goal runP3'vc: Unknown ()

  $ caisar verify --prover Marabou --ltag=ProverSpec file.mlw
  [DEBUG]{ProverSpec} Prover-tailored specification:
  x0 >= 0.0
  x0 <= 60760.0
  x1 >= -3.141592653589793115997963468544185161590576171875
  x1 <= 3.141592653589793115997963468544185161590576171875
  x2 >= -3.141592653589793115997963468544185161590576171875
  x2 <= 3.141592653589793115997963468544185161590576171875
  x3 >= 100.0
  x3 <= 1200.0
  x4 >= 0.0
  x4 <= 1200.0
  x0 >= 55947.6909999999988940544426441192626953125
  x3 >= 1145.0
  x4 <= 60.0
  y0 >= 1500.0
  
  [DEBUG]{ProverSpec} Prover-tailored specification:
  x0 >= 0.0
  x0 <= 60760.0
  x1 >= -3.141592653589793115997963468544185161590576171875
  x1 <= 3.141592653589793115997963468544185161590576171875
  x2 >= -3.141592653589793115997963468544185161590576171875
  x2 <= 3.141592653589793115997963468544185161590576171875
  x3 >= 100.0
  x3 <= 1200.0
  x4 >= 0.0
  x4 <= 1200.0
  x0 >= 55947.6909999999988940544426441192626953125
  x3 >= 1145.0
  x4 <= 60.0
  y0 >= 1500.0
  
  [DEBUG]{ProverSpec} Prover-tailored specification:
  x0 >= 0.0
  x0 <= 60760.0
  x1 >= -3.141592653589793115997963468544185161590576171875
  x1 <= 3.141592653589793115997963468544185161590576171875
  x2 >= -3.141592653589793115997963468544185161590576171875
  x2 <= 3.141592653589793115997963468544185161590576171875
  x3 >= 100.0
  x3 <= 1200.0
  x4 >= 0.0
  x4 <= 1200.0
  x0 >= 55947.6909999999988940544426441192626953125
  x3 >= 1145.0
  x4 <= 60.0
  y1 >= 1500.0
  
  [DEBUG]{ProverSpec} Prover-tailored specification:
  x0 >= 0.0
  x0 <= 60760.0
  x1 >= -3.141592653589793115997963468544185161590576171875
  x1 <= 3.141592653589793115997963468544185161590576171875
  x2 >= -3.141592653589793115997963468544185161590576171875
  x2 <= 3.141592653589793115997963468544185161590576171875
  x3 >= 100.0
  x3 <= 1200.0
  x4 >= 0.0
  x4 <= 1200.0
  x0 >= 55947.6909999999988940544426441192626953125
  x3 >= 1145.0
  x4 <= 60.0
  +y1 -y0 >= 0
  
  [DEBUG]{ProverSpec} Prover-tailored specification:
  x0 >= 0.0
  x0 <= 60760.0
  x1 >= -3.141592653589793115997963468544185161590576171875
  x1 <= 3.141592653589793115997963468544185161590576171875
  x2 >= -3.141592653589793115997963468544185161590576171875
  x2 <= 3.141592653589793115997963468544185161590576171875
  x3 >= 100.0
  x3 <= 1200.0
  x4 >= 0.0
  x4 <= 1200.0
  x0 >= 55947.6909999999988940544426441192626953125
  x3 >= 1145.0
  x4 <= 60.0
  +y0 -y1 >= 0
  
  [DEBUG]{ProverSpec} Prover-tailored specification:
  x0 >= 0.0
  x0 <= 60760.0
  x1 >= -3.141592653589793115997963468544185161590576171875
  x1 <= 3.141592653589793115997963468544185161590576171875
  x2 >= -3.141592653589793115997963468544185161590576171875
  x2 <= 3.141592653589793115997963468544185161590576171875
  x3 >= 100.0
  x3 <= 1200.0
  x4 >= 0.0
  x4 <= 1200.0
  x0 >= 55947.6909999999988940544426441192626953125
  x3 >= 1145.0
  x4 <= 60.0
  x5 >= -1.0
  x5 <= 1.0
  +y0 -y1 >= 0
  
  [DEBUG]{ProverSpec} Prover-tailored specification:
  x0 >= 0.0
  x0 <= 60760.0
  x1 >= -3.141592653589793115997963468544185161590576171875
  x1 <= 3.141592653589793115997963468544185161590576171875
  x2 >= -3.141592653589793115997963468544185161590576171875
  x2 <= 3.141592653589793115997963468544185161590576171875
  x3 >= 100.0
  x3 <= 1200.0
  x4 >= 0.0
  x4 <= 1200.0
  x0 >= 55947.6909999999988940544426441192626953125
  x3 >= 1145.0
  x4 <= 60.0
  x5 >= -1.0
  x5 <= 1.0
  +y1 -y0 >= 0
  
  [DEBUG]{ProverSpec} Prover-tailored specification:
  x0 >= 0.0
  x0 <= 60760.0
  x1 >= -3.141592653589793115997963468544185161590576171875
  x1 <= 3.141592653589793115997963468544185161590576171875
  x2 >= -3.141592653589793115997963468544185161590576171875
  x2 <= 3.141592653589793115997963468544185161590576171875
  x3 >= 100.0
  x3 <= 1200.0
  x4 >= 0.0
  x4 <= 1200.0
  x0 >= 1500.0
  x0 <= 1800.0
  x1 >= -0.059999999999999997779553950749686919152736663818359375
  x1 <= 0.059999999999999997779553950749686919152736663818359375
  x2 >= 3.100000000000000088817841970012523233890533447265625
  x3 >= 980.0
  x4 >= 960.0
  +y0 -y1 <= 0
  +y0 -y2 <= 0
  +y0 -y3 <= 0
  +y0 -y4 <= 0
  
  Goal run'vc: Unknown ()
  Goal run1'vc: Unknown ()
  Goal run2'vc: Unknown ()
  Goal run3'vc: Unknown ()
  Goal runP3'vc: Unknown ()

  $ python3 bin/inspect_onnx.py
  out/caisar_0.onnx has 1 input nodes
  {'name': '38', 'type': {'tensorType': {'elemType': 1, 'shape': {'dim': [{'dimValue': '5'}]}}}}
  out/caisar_1.onnx has 1 input nodes
  {'name': '183', 'type': {'tensorType': {'elemType': 1, 'shape': {'dim': [{'dimValue': '5'}]}}}}
  out/caisar_2.onnx has 1 input nodes
  {'name': '325', 'type': {'tensorType': {'elemType': 1, 'shape': {'dim': [{'dimValue': '5'}]}}}}
  out/caisar_3.onnx has 1 input nodes
  {'name': '435', 'type': {'tensorType': {'elemType': 1, 'shape': {'dim': [{'dimValue': '6'}]}}}}
  out/caisar_4.onnx has 1 input nodes
  {'name': '586', 'type': {'tensorType': {'elemType': 1, 'shape': {'dim': [{'dimValue': '5'}]}}}}
  5 files checked
