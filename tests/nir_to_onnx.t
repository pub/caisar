Test verify
  $ . ./setup_env.sh

  $ bin/pyrat.py --version
  PyRAT 1.1

  $ caisar verify --format whyml --prover=PyRAT --ltag=NIR --onnx-out-dir="out" - 2>&1 <<EOF
  > theory NIR_to_ONNX
  >   use ieee_float.Float64
  >   use caisar.types.Vector
  >   use caisar.model.Model
  > 
  >   constant nn: model = read_model "TestNetworkONNX.onnx"
  > 
  >   goal G:
  >     forall i: vector t.
  >       has_length i 3 ->
  >       (0.5:t) .<= i[0] .<= (0.5:t) ->
  >       (0.5:t) .< (nn @@ i)[0] .< (0.5:t)
  > end
  > EOF
  [DEBUG]{NIR} Wrote NIR as ONNX in file 'out/nn_onnx.nir.onnx'
  Goal G: Unknown ()

Input name should be 0

  $ python3 bin/inspect_onnx.py out/nn_onnx.nir.onnx
  out/nn_onnx.nir.onnx has 1 input nodes
  {'name': '0', 'type': {'tensorType': {'elemType': 1, 'shape': {'dim': [{'dimValue': '1'}, {'dimValue': '1'}, {'dimValue': '1'}, {'dimValue': '3'}]}}}}
  1 files checked

  $ caisar verify --format whyml --prover=PyRAT --ltag=NIR --onnx-out-dir="out_nnet" - 2>&1 <<EOF
  > theory NIR_to_ONNX
  >   use ieee_float.Float64
  >   use caisar.types.Vector
  >   use caisar.model.Model
  > 
  >   constant nn: model = read_model "TestNetwork.nnet"
  > 
  >   goal G:
  >     forall i: vector t.
  >       has_length i 5 ->
  >       (0.5:t) .<= i[0] .<= (0.5:t) ->
  >       (0.5:t) .< (nn @@ i)[0] .< (0.5:t)
  > end
  > EOF
  [DEBUG]{NIR} Wrote NIR as ONNX in file 'out_nnet/nn_nnet.nir.onnx'
  Goal G: Unknown ()

Input name should be 0

  $ python3 bin/inspect_onnx.py out_nnet/nn_nnet.nir.onnx
  out_nnet/nn_nnet.nir.onnx has 1 input nodes
  {'name': '0', 'type': {'tensorType': {'elemType': 1, 'shape': {'dim': [{'dimValue': '5'}]}}}}
  1 files checked

Parsing of ovo files

  $ caisar verify --format whyml --prover=PyRAT --ltag=NIR --onnx-out-dir="out_svm" - 2>&1 <<EOF
  > theory OVO_to_ONNX
  >   use ieee_float.Float64
  >   use caisar.types.Vector
  >   use caisar.model.Model
  > 
  >   constant ovo: model = read_model "TestSVM.ovo"
  > 
  >   goal G:
  >     forall i: vector t.
  >       has_length i 3 ->
  >       (0.5:t) .<= i[0] .<= (0.5:t) ->
  >       (0.5:t) .< (ovo @@ i)[0] .< (0.5:t)
  > end
  > EOF
  [DEBUG]{NIR} Wrote SVM as ONNX in file 'out_svm/svm_ovo_hybrid.ovo.onnx'
  [DEBUG]{NIR} Wrote SVM as ONNX in file 'out_svm/svm_ovo_hybrid1.ovo.onnx'
  Goal G: Unknown ()

  $ python3 bin/inspect_onnx.py out_svm/svm_ovo_hybrid.ovo.onnx
  out_svm/svm_ovo_hybrid.ovo.onnx has 1 input nodes
  {'name': '0', 'type': {'tensorType': {'elemType': 1, 'shape': {'dim': [{'dimValue': '3'}]}}}}
  1 files checked
