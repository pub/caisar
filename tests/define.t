Test interpretation of on-the-fly definition of toplevel constants
  $ . ./setup_env.sh

  $ bin/pyrat.py --version
  PyRAT 1.1

  $ cat > file.mlw <<EOF
  > theory T
  >   use int.Int
  > 
  >   constant n: int
  > 
  >   goal G: 1 = n
  > end
  > EOF

  $ caisar verify --prover PyRAT file.mlw -D n:1 --ltag=InterpretGoal
  [DEBUG]{InterpretGoal} Interpreted formula for goal 'G':
  true
  
  [ERROR] No neural network nor SVM model found in task.

  $ caisar verify --prover PyRAT file.mlw -Dn:2
  [ERROR] No neural network nor SVM model found in task.

  $ cat > file.mlw <<EOF
  > theory T
  >   use int.Int
  > 
  >   constant n: string
  > 
  >   goal G: n = "foo"
  > end
  > EOF

  $ caisar verify --prover PyRAT file.mlw -D n:foo --ltag=InterpretGoal
  [DEBUG]{InterpretGoal} Interpreted formula for goal 'G':
  true
  
  [ERROR] No neural network nor SVM model found in task.

  $ caisar verify --prover PyRAT file.mlw -D n:bar
  [ERROR] No neural network nor SVM model found in task.

  $ cat > file.mlw <<EOF
  > theory T
  >   constant n: real
  > 
  >   goal G: n = 1.0
  > end
  > EOF

  $ caisar verify --prover PyRAT file.mlw -D n:1.0 --ltag=InterpretGoal
  [DEBUG]{InterpretGoal} Interpreted formula for goal 'G':
  true
  
  [ERROR] No neural network nor SVM model found in task.

  $ caisar verify --prover PyRAT file.mlw -D n:foo
  [ERROR] 'n' expects a numerical constant, got 'foo' instead

  $ cat > file.mlw <<EOF
  > theory T
  >   use ieee_float.Float64
  >   constant n: t
  > 
  >   goal G: n = (0.5:t)
  > end
  > EOF

  $ caisar verify --prover PyRAT file.mlw -D n:0.5 --ltag=InterpretGoal
  [DEBUG]{InterpretGoal} Interpreted formula for goal 'G':
  true
  
  [ERROR] No neural network nor SVM model found in task.

  $ cat > file.mlw <<EOF
  > theory T
  >   predicate n
  > 
  >   goal G: n
  > end
  > EOF

  $ caisar verify --prover PyRAT file.mlw -D n:true --ltag=InterpretGoal
  [DEBUG]{InterpretGoal} Interpreted formula for goal 'G':
  true
  
  [ERROR] No neural network nor SVM model found in task.

  $ caisar verify --prover PyRAT file.mlw -D n:false
  [ERROR] No neural network nor SVM model found in task.

  $ cat > file.mlw <<EOF
  > theory T
  >   constant n: bool
  > 
  >   goal G: n = true
  > end
  > EOF

  $ caisar verify --prover PyRAT file.mlw -D n:true --ltag=InterpretGoal
  [DEBUG]{InterpretGoal} Interpreted formula for goal 'G':
  true
  
  [ERROR] No neural network nor SVM model found in task.

  $ caisar verify --prover PyRAT file.mlw -D n:false --ltag=InterpretGoal
  [DEBUG]{InterpretGoal} Interpreted formula for goal 'G':
  false
  
  [ERROR] No neural network nor SVM model found in task.

  $ caisar verify --prover PyRAT file.mlw -D n:boh --ltag=InterpretGoal
  [ERROR] 'n' expects 'true' or 'false', got 'boh' instead
