from onnx import load as load_onnx
from onnx.reference import ReferenceEvaluator
import numpy
import sys

onnx_filename = sys.argv[1]
test_filename = sys.argv[2]

def read_onnx(filename):
    with open(filename, "rb") as f:
        return load_onnx(f)

def read_inputs(filename):
    with open(filename, 'rb') as f:
        import re
        result = []
        for line in f:
            line = repr(line)
            reg = r"[-+]?(?:\d*\.*\d+)"
            floats = [float(x) for x in re.findall(reg, line)]
            result.append((floats[0], floats[1:]))
        return result

def verify_onnx(onnx, inputs):
    sess = ReferenceEvaluator(onnx)
    for y,X in inputs:
        X = numpy.array([numpy.float32(x) for x in X])
        feeds = {'0': X}
        res = sess.run(None, feeds)[0]
        print(f"Output res+1 {res+1} equals to {y}")
        assert res+1 == y

onnx = read_onnx(onnx_filename)
inputs = read_inputs(test_filename)
verify_onnx(onnx, inputs)
