#!/usr/bin/env python3

import onnx
from google.protobuf.json_format import MessageToDict
import os, sys

l = []
if len(sys.argv) > 1:
    l = sys.argv[1:]
else:
    l = os.listdir("out")
    l.sort()
    l = [os.path.join("out", file) for file in  l]

for file in l:
    m = onnx.load(file)
    print (f"{file} has {len(m.graph.input)} input nodes")
    onnx.checker.check_model(m)
    onnx.shape_inference.infer_shapes(m,check_type = True, strict_mode = True, data_prop = True)
    for _input in m.graph.input:
        print(MessageToDict(_input))
    
print(len(l),"files checked")
