#!/bin/sh -e

case $1 in
    --version)
        echo "maraboupy 2.0.0"
        ;;
    *)
        echo "PWD: $(pwd)"
        echo "NN: $1"
        test -e $1 || (echo "Cannot find the NN file" && exit 1)
        echo "Goal:"
        cat $2
        echo "Unknown"
esac
