Test verify
  $ . ./setup_env.sh

  $ bin/pyrat.py --version
  PyRAT 1.1

  $ caisar verify --format whyml -p PyRAT --prover-altern VNNLIB --ltag=ProverSpec - <<EOF
  > theory PyRAT_ONNX
  >   use ieee_float.Float64
  >   use caisar.types.Vector
  >   use caisar.model.Model
  >  
  > 
  >   constant nn: model = read_model "TestNetworkONNX.onnx"
  > 
  >   goal G:
  >     forall i: vector t.
  >       has_length i 3 ->
  >       (0.0:t) .<= i[0] .<= (0.5:t) ->
  >       (0.0:t) .< (nn @@ i)[0] .< (0.5:t)
  >       /\ ((0.0:t) .< (nn @@ i)[1] .< (0.375:t)
  >       \/ ((0.375:t) .< (nn @@ i)[0] .< (0.5:t)
  >       \/ (0.5:t) .< (nn @@ i)[1] .< (1.5:t))
  >       /\ (0.5:t) .< (nn @@ i)[0] .< (2.5:t))
  > end
  > EOF
  [DEBUG]{ProverSpec} Prover-tailored specification:
  ;;; produced by PyRAT/VNN-LIB driver
  ;;; produced by VNN-LIB driver
  ;; X_0
  (declare-const X_0 Real)
  
  ;; X_1
  (declare-const X_1 Real)
  
  ;; X_2
  (declare-const X_2 Real)
  
  ;; H
  (assert (>= X_0 0.0))
  
  ;; H
  (assert (<= X_0 0.5))
  
  ;; Y_0
  (declare-const Y_0 Real)
  
  ;; Y_1
  (declare-const Y_1 Real)
  
  ;; Goal G
  (assert
    (or
      (and (<= Y_0 0.0))
      (and (>= Y_0 0.5))
      (and (<= Y_1 0.0))
      (and (>= Y_1 0.375))
      ))
  (assert
    (or
      (and (<= Y_0 0.0))
      (and (>= Y_0 0.5))
      (and (<= Y_0 0.375))
      (and (>= Y_0 0.5))
      (and (<= Y_0 0.5))
      (and (>= Y_0 2.5))
      ))
  (assert
    (or
      (and (<= Y_0 0.0))
      (and (>= Y_0 0.5))
      (and (<= Y_1 0.5))
      (and (>= Y_1 1.5))
      (and (<= Y_0 0.5))
      (and (>= Y_0 2.5))
      ))
  
  Goal G: Unknown ()
