Test verify
  $ . ./setup_env.sh

  $ bin/alt-ergo --version
  2.4.0

  $ caisar verify --format whyml --prover Alt-Ergo --ltag=ProverSpec --ltag=StackTrace - <<EOF
  > theory AltErgo
  >   use ieee_float.Float64
  >   use caisar.types.Vector
  >   use caisar.model.Model
  > 
  >   constant nn: model = read_model "TestNetworkONNX.onnx"
  > 
  >   goal G:
  >     forall i: vector t.
  >       has_length i 3 ->
  >       (0.0:t) .<= i[0] .<= (0.5:t) ->
  >       (0.0:t) .< (nn @@ i)[0] .< (0.5:t)
  >       /\ (0.0:t) .< (nn @@ i)[1] .< (1.0:t)
  > end
  > EOF
  [DEBUG]{ProverSpec} Prover-tailored specification:
  (* this is the prelude for Alt-Ergo, version >= 2.4.0 *)
  (* this is a prelude for Alt-Ergo integer arithmetic *)
  (* this is a prelude for Alt-Ergo real arithmetic *)
  axiom CompatOrderMult :
    (forall x:int. forall y:int. forall z:int. ((x <= y) -> ((0 <= z) ->
    ((x * z) <= (y * z)))))
  
  axiom add_div :
    (forall x:real. forall y:real. forall z:real. ((not (z = 0.0)) ->
    (((x + y) / z) = ((x / z) + (y / z)))))
  
  axiom sub_div :
    (forall x:real. forall y:real. forall z:real. ((not (z = 0.0)) ->
    (((x - y) / z) = ((x / z) - (y / z)))))
  
  axiom neg_div :
    (forall x:real. forall y:real. ((not (y = 0.0)) ->
    (((-x) / y) = (-(x / y)))))
  
  axiom assoc_mul_div :
    (forall x:real. forall y:real. forall z:real. ((not (z = 0.0)) ->
    (((x * y) / z) = (x * (y / z)))))
  
  axiom assoc_div_mul :
    (forall x:real. forall y:real. forall z:real. (((not (y = 0.0)) and
    (not (z = 0.0))) -> (((x / y) / z) = (x / (y * z)))))
  
  axiom assoc_div_div :
    (forall x:real. forall y:real. forall z:real. (((not (y = 0.0)) and
    (not (z = 0.0))) -> ((x / (y / z)) = ((x * z) / y))))
  
  axiom CompatOrderMult1 :
    (forall x:real. forall y:real. forall z:real. ((x <= y) -> ((0.0 <= z) ->
    ((x * z) <= (y * z)))))
  
  type t
  
  logic tqtreal : t -> real
  
  logic tqtisFinite : t -> prop
  
  axiom tqtaxiom :
    (forall x:t. (tqtisFinite(x) ->
    ((-179769313486231570814527423731704356798070567525844996598917476803157260780028538760589558632766878171540458953514382464234321326889464182768467546703537516986049910576551282076245490090389328944075868508455133942304583236903222948165808559332123348274797826204144723168738177180919299881250404026184124858368.0) <= tqtreal(x))))
  
  axiom tqtaxiom1 :
    (forall x:t. (tqtisFinite(x) ->
    (tqtreal(x) <= 179769313486231570814527423731704356798070567525844996598917476803157260780028538760589558632766878171540458953514382464234321326889464182768467546703537516986049910576551282076245490090389328944075868508455133942304583236903222948165808559332123348274797826204144723168738177180919299881250404026184124858368.0)))
  
  logic pow2 : int -> int
  
  axiom Power_0 : (pow2(0) = 1)
  
  axiom Power_s : (forall n:int. ((0 <= n) -> (pow2((n + 1)) = (2 * pow2(n)))))
  
  axiom Power_1 : (pow2(1) = 2)
  
  axiom Power_sum :
    (forall n:int. forall m:int. (((0 <= n) and (0 <= m)) ->
    (pow2((n + m)) = (pow2(n) * pow2(m)))))
  
  axiom pow2pos : (forall i:int. ((0 <= i) -> (0 <  pow2(i))))
  
  axiom pow2_0 : (pow2(0) = 1)
  
  axiom pow2_1 : (pow2(1) = 2)
  
  axiom pow2_2 : (pow2(2) = 4)
  
  axiom pow2_3 : (pow2(3) = 8)
  
  axiom pow2_4 : (pow2(4) = 16)
  
  axiom pow2_5 : (pow2(5) = 32)
  
  axiom pow2_6 : (pow2(6) = 64)
  
  axiom pow2_7 : (pow2(7) = 128)
  
  axiom pow2_8 : (pow2(8) = 256)
  
  axiom pow2_9 : (pow2(9) = 512)
  
  axiom pow2_10 : (pow2(10) = 1024)
  
  axiom pow2_11 : (pow2(11) = 2048)
  
  axiom pow2_12 : (pow2(12) = 4096)
  
  axiom pow2_13 : (pow2(13) = 8192)
  
  axiom pow2_14 : (pow2(14) = 16384)
  
  axiom pow2_15 : (pow2(15) = 32768)
  
  axiom pow2_16 : (pow2(16) = 65536)
  
  axiom pow2_17 : (pow2(17) = 131072)
  
  axiom pow2_18 : (pow2(18) = 262144)
  
  axiom pow2_19 : (pow2(19) = 524288)
  
  axiom pow2_20 : (pow2(20) = 1048576)
  
  axiom pow2_21 : (pow2(21) = 2097152)
  
  axiom pow2_22 : (pow2(22) = 4194304)
  
  axiom pow2_23 : (pow2(23) = 8388608)
  
  axiom pow2_24 : (pow2(24) = 16777216)
  
  axiom pow2_25 : (pow2(25) = 33554432)
  
  axiom pow2_26 : (pow2(26) = 67108864)
  
  axiom pow2_27 : (pow2(27) = 134217728)
  
  axiom pow2_28 : (pow2(28) = 268435456)
  
  axiom pow2_29 : (pow2(29) = 536870912)
  
  axiom pow2_30 : (pow2(30) = 1073741824)
  
  axiom pow2_31 : (pow2(31) = 2147483648)
  
  axiom pow2_32 : (pow2(32) = 4294967296)
  
  axiom pow2_33 : (pow2(33) = 8589934592)
  
  axiom pow2_34 : (pow2(34) = 17179869184)
  
  axiom pow2_35 : (pow2(35) = 34359738368)
  
  axiom pow2_36 : (pow2(36) = 68719476736)
  
  axiom pow2_37 : (pow2(37) = 137438953472)
  
  axiom pow2_38 : (pow2(38) = 274877906944)
  
  axiom pow2_39 : (pow2(39) = 549755813888)
  
  axiom pow2_40 : (pow2(40) = 1099511627776)
  
  axiom pow2_41 : (pow2(41) = 2199023255552)
  
  axiom pow2_42 : (pow2(42) = 4398046511104)
  
  axiom pow2_43 : (pow2(43) = 8796093022208)
  
  axiom pow2_44 : (pow2(44) = 17592186044416)
  
  axiom pow2_45 : (pow2(45) = 35184372088832)
  
  axiom pow2_46 : (pow2(46) = 70368744177664)
  
  axiom pow2_47 : (pow2(47) = 140737488355328)
  
  axiom pow2_48 : (pow2(48) = 281474976710656)
  
  axiom pow2_49 : (pow2(49) = 562949953421312)
  
  axiom pow2_50 : (pow2(50) = 1125899906842624)
  
  axiom pow2_51 : (pow2(51) = 2251799813685248)
  
  axiom pow2_52 : (pow2(52) = 4503599627370496)
  
  axiom pow2_53 : (pow2(53) = 9007199254740992)
  
  axiom pow2_54 : (pow2(54) = 18014398509481984)
  
  axiom pow2_55 : (pow2(55) = 36028797018963968)
  
  axiom pow2_56 : (pow2(56) = 72057594037927936)
  
  axiom pow2_57 : (pow2(57) = 144115188075855872)
  
  axiom pow2_58 : (pow2(58) = 288230376151711744)
  
  axiom pow2_59 : (pow2(59) = 576460752303423488)
  
  axiom pow2_60 : (pow2(60) = 1152921504606846976)
  
  axiom pow2_61 : (pow2(61) = 2305843009213693952)
  
  axiom pow2_62 : (pow2(62) = 4611686018427387904)
  
  axiom pow2_63 : (pow2(63) = 9223372036854775808)
  
  axiom pow2_64 : (pow2(64) = 18446744073709551616)
  
  function abs(x: real) : real = (if (0.0 <= x) then x else (-x))
  
  axiom Abs_le : (forall x:real. forall y:real. ((abs(x) <= y) -> ((-y) <= x)))
  
  axiom Abs_le1 : (forall x:real. forall y:real. ((abs(x) <= y) -> (x <= y)))
  
  axiom Abs_le2 :
    (forall x:real. forall y:real. ((((-y) <= x) and (x <= y)) ->
    (abs(x) <= y)))
  
  axiom Abs_pos : (forall x:real. (0.0 <= abs(x)))
  
  axiom Abs_sum :
    (forall x:real. forall y:real. (abs((x + y)) <= (abs(x) + abs(y))))
  
  axiom Abs_prod :
    (forall x:real. forall y:real. (abs((x * y)) = (abs(x) * abs(y))))
  
  axiom triangular_inequality :
    (forall x:real. forall y:real. forall z:real.
    (abs((x - z)) <= (abs((x - y)) + abs((y - z)))))
  
  logic from_int : int -> real
  
  axiom Zero : (from_int(0) = 0.0)
  
  axiom One : (from_int(1) = 1.0)
  
  axiom Add :
    (forall x:int. forall y:int.
    (from_int((x + y)) = (from_int(x) + from_int(y))))
  
  axiom Sub :
    (forall x:int. forall y:int.
    (from_int((x - y)) = (from_int(x) - from_int(y))))
  
  axiom Mul :
    (forall x:int. forall y:int.
    (from_int((x * y)) = (from_int(x) * from_int(y))))
  
  axiom Neg : (forall x:int. (from_int((-x)) = (-from_int(x))))
  
  axiom Injective :
    (forall x:int. forall y:int. ((from_int(x) = from_int(y)) -> (x = y)))
  
  axiom Monotonic :
    (forall x:int. forall y:int. ((x <= y) -> (from_int(x) <= from_int(y))))
  
  logic truncate : real -> int
  
  axiom Truncate_int : (forall i:int. (truncate(from_int(i)) = i))
  
  axiom Truncate_down_pos :
    (forall x:real. ((0.0 <= x) -> (from_int(truncate(x)) <= x)))
  
  axiom Truncate_down_pos1 :
    (forall x:real. ((0.0 <= x) -> (x <  from_int((truncate(x) + 1)))))
  
  axiom Truncate_up_neg :
    (forall x:real. ((x <= 0.0) -> (from_int((truncate(x) - 1)) <  x)))
  
  axiom Truncate_up_neg1 :
    (forall x:real. ((x <= 0.0) -> (x <= from_int(truncate(x)))))
  
  axiom Real_of_truncate :
    (forall x:real. ((x - 1.0) <= from_int(truncate(x))))
  
  axiom Real_of_truncate1 :
    (forall x:real. (from_int(truncate(x)) <= (x + 1.0)))
  
  axiom Truncate_monotonic :
    (forall x:real. forall y:real. ((x <= y) -> (truncate(x) <= truncate(y))))
  
  axiom Truncate_monotonic_int1 :
    (forall x:real. forall i:int. ((x <= from_int(i)) -> (truncate(x) <= i)))
  
  axiom Truncate_monotonic_int2 :
    (forall x:real. forall i:int. ((from_int(i) <= x) -> (i <= truncate(x))))
  
  logic floor : real -> int
  
  logic ceil : real -> int
  
  axiom Floor_int : (forall i:int. (floor(from_int(i)) = i))
  
  axiom Ceil_int : (forall i:int. (ceil(from_int(i)) = i))
  
  axiom Floor_down : (forall x:real. (from_int(floor(x)) <= x))
  
  axiom Floor_down1 : (forall x:real. (x <  from_int((floor(x) + 1))))
  
  axiom Ceil_up : (forall x:real. (from_int((ceil(x) - 1)) <  x))
  
  axiom Ceil_up1 : (forall x:real. (x <= from_int(ceil(x))))
  
  axiom Floor_monotonic :
    (forall x:real. forall y:real. ((x <= y) -> (floor(x) <= floor(y))))
  
  axiom Ceil_monotonic :
    (forall x:real. forall y:real. ((x <= y) -> (ceil(x) <= ceil(y))))
  
  type mode = RNE | RNA | RTP | RTN | RTZ
  
  logic match_mode : mode, 'a, 'a, 'a, 'a, 'a -> 'a
  
  axiom match_mode_RNE :
    (forall z:'a. forall z1:'a. forall z2:'a. forall z3:'a. forall z4:'a.
    (match_mode(RNE, z, z1, z2, z3, z4) = z))
  
  axiom match_mode_RNA :
    (forall z:'a. forall z1:'a. forall z2:'a. forall z3:'a. forall z4:'a.
    (match_mode(RNA, z, z1, z2, z3, z4) = z1))
  
  axiom match_mode_RTP :
    (forall z:'a. forall z1:'a. forall z2:'a. forall z3:'a. forall z4:'a.
    (match_mode(RTP, z, z1, z2, z3, z4) = z2))
  
  axiom match_mode_RTN :
    (forall z:'a. forall z1:'a. forall z2:'a. forall z3:'a. forall z4:'a.
    (match_mode(RTN, z, z1, z2, z3, z4) = z3))
  
  axiom match_mode_RTZ :
    (forall z:'a. forall z1:'a. forall z2:'a. forall z3:'a. forall z4:'a.
    (match_mode(RTZ, z, z1, z2, z3, z4) = z4))
  
  predicate to_nearest(m: mode) = ((m = RNE) or (m = RNA))
  
  axiom eb_gt_1 : (1 <  11)
  
  axiom sb_gt_1 : (1 <  53)
  
  logic zeroF : t
  
  logic add : mode, t, t -> t
  
  logic sub : mode, t, t -> t
  
  logic mul : mode, t, t -> t
  
  logic div : mode, t, t -> t
  
  logic abs1 : t -> t
  
  logic neg : t -> t
  
  logic fma : mode, t, t, t -> t
  
  logic sqrt : mode, t -> t
  
  logic roundToIntegral : mode, t -> t
  
  logic min : t, t -> t
  
  logic max : t, t -> t
  
  logic le : t, t -> prop
  
  logic lt : t, t -> prop
  
  logic eq : t, t -> prop
  
  logic is_zero : t -> prop
  
  logic is_infinite : t -> prop
  
  logic is_nan : t -> prop
  
  logic is_positive : t -> prop
  
  logic is_negative : t -> prop
  
  predicate is_plus_infinity(x: t) = (is_infinite(x) and is_positive(x))
  
  predicate is_minus_infinity(x: t) = (is_infinite(x) and is_negative(x))
  
  predicate is_not_nan(x: t) = (tqtisFinite(x) or is_infinite(x))
  
  axiom is_not_nan1 : (forall x:t. (is_not_nan(x) -> (not is_nan(x))))
  
  axiom is_not_nan2 : (forall x:t. ((not is_nan(x)) -> is_not_nan(x)))
  
  axiom is_not_finite :
    (forall x:t. ((not tqtisFinite(x)) -> (is_infinite(x) or is_nan(x))))
  
  axiom is_not_finite1 :
    (forall x:t. ((is_infinite(x) or is_nan(x)) -> (not tqtisFinite(x))))
  
  axiom zeroF_is_positive : is_positive(zeroF)
  
  axiom zeroF_is_zero : is_zero(zeroF)
  
  axiom zero_to_real :
    (forall x:t [is_zero(x)]. (is_zero(x) -> tqtisFinite(x)))
  
  axiom zero_to_real1 :
    (forall x:t [is_zero(x)]. (is_zero(x) -> (tqtreal(x) = 0.0)))
  
  axiom zero_to_real2 :
    (forall x:t [is_zero(x)]. ((tqtisFinite(x) and (tqtreal(x) = 0.0)) ->
    is_zero(x)))
  
  logic of_int : mode, int -> t
  
  logic to_int : mode, t -> int
  
  axiom zero_of_int : (forall m:mode. (zeroF = of_int(m, 0)))
  
  logic round : mode, real -> real
  
  axiom max_int_spec :
    (179769313486231570814527423731704356798070567525844996598917476803157260780028538760589558632766878171540458953514382464234321326889464182768467546703537516986049910576551282076245490090389328944075868508455133942304583236903222948165808559332123348274797826204144723168738177180919299881250404026184124858368 = (pow2(pow2((11 - 1))) - pow2((pow2((11 - 1)) - 53))))
  
  axiom max_real_int :
    (0x1.FFFFFFFFFFFFFp1023 = from_int(179769313486231570814527423731704356798070567525844996598917476803157260780028538760589558632766878171540458953514382464234321326889464182768467546703537516986049910576551282076245490090389328944075868508455133942304583236903222948165808559332123348274797826204144723168738177180919299881250404026184124858368))
  
  predicate in_range(x: real) = (((-0x1.FFFFFFFFFFFFFp1023) <= x) and
    (x <= 0x1.FFFFFFFFFFFFFp1023))
  
  predicate in_int_range(i: int) =
    (((-179769313486231570814527423731704356798070567525844996598917476803157260780028538760589558632766878171540458953514382464234321326889464182768467546703537516986049910576551282076245490090389328944075868508455133942304583236903222948165808559332123348274797826204144723168738177180919299881250404026184124858368) <= i) and
    (i <= 179769313486231570814527423731704356798070567525844996598917476803157260780028538760589558632766878171540458953514382464234321326889464182768467546703537516986049910576551282076245490090389328944075868508455133942304583236903222948165808559332123348274797826204144723168738177180919299881250404026184124858368))
  
  axiom is_finite : (forall x:t. (tqtisFinite(x) -> in_range(tqtreal(x))))
  
  predicate no_overflow(m: mode, x: real) = in_range(round(m, x))
  
  axiom Bounded_real_no_overflow :
    (forall m:mode. forall x:real. (in_range(x) -> no_overflow(m, x)))
  
  axiom Round_monotonic :
    (forall m:mode. forall x:real. forall y:real. ((x <= y) -> (round(m,
    x) <= round(m, y))))
  
  axiom Round_idempotent :
    (forall m1:mode. forall m2:mode. forall x:real. (round(m1, round(m2,
    x)) = round(m2, x)))
  
  axiom Round_to_real :
    (forall m:mode. forall x:t. (tqtisFinite(x) -> (round(m,
    tqtreal(x)) = tqtreal(x))))
  
  axiom Round_down_le : (forall x:real. (round(RTN, x) <= x))
  
  axiom Round_up_ge : (forall x:real. (x <= round(RTP, x)))
  
  axiom Round_down_neg : (forall x:real. (round(RTN, (-x)) = (-round(RTP, x))))
  
  axiom Round_up_neg : (forall x:real. (round(RTP, (-x)) = (-round(RTN, x))))
  
  axiom pow2sb : (9007199254740992 = pow2(53))
  
  predicate in_safe_int_range(i: int) = (((-9007199254740992) <= i) and
    (i <= 9007199254740992))
  
  axiom Exact_rounding_for_integers :
    (forall m:mode. forall i:int. (in_safe_int_range(i) -> (round(m,
    from_int(i)) = from_int(i))))
  
  predicate same_sign(x: t, y: t) = ((is_positive(x) and is_positive(y)) or
    (is_negative(x) and is_negative(y)))
  
  predicate diff_sign(x: t, y: t) = ((is_positive(x) and is_negative(y)) or
    (is_negative(x) and is_positive(y)))
  
  axiom feq_eq :
    (forall x:t. forall y:t. (tqtisFinite(x) -> (tqtisFinite(y) ->
    ((not is_zero(x)) -> (eq(x, y) -> (x = y))))))
  
  axiom eq_feq :
    (forall x:t. forall y:t. (tqtisFinite(x) -> (tqtisFinite(y) -> ((x = y) ->
    eq(x, y)))))
  
  axiom eq_refl : (forall x:t. (tqtisFinite(x) -> eq(x, x)))
  
  axiom eq_sym : (forall x:t. forall y:t. (eq(x, y) -> eq(y, x)))
  
  axiom eq_trans :
    (forall x:t. forall y:t. forall z:t. (eq(x, y) -> (eq(y, z) -> eq(x, z))))
  
  axiom eq_zero : eq(zeroF, neg(zeroF))
  
  axiom eq_to_real_finite :
    (forall x:t. forall y:t. ((tqtisFinite(x) and tqtisFinite(y)) -> (eq(x,
    y) -> (tqtreal(x) = tqtreal(y)))))
  
  axiom eq_to_real_finite1 :
    (forall x:t. forall y:t. ((tqtisFinite(x) and tqtisFinite(y)) ->
    ((tqtreal(x) = tqtreal(y)) -> eq(x, y))))
  
  axiom eq_special : (forall x:t. forall y:t. (eq(x, y) -> is_not_nan(x)))
  
  axiom eq_special1 : (forall x:t. forall y:t. (eq(x, y) -> is_not_nan(y)))
  
  axiom eq_special2 :
    (forall x:t. forall y:t. (eq(x, y) -> ((tqtisFinite(x) and
    tqtisFinite(y)) or (is_infinite(x) and (is_infinite(y) and same_sign(x,
    y))))))
  
  axiom lt_finite :
    (forall x:t. forall y:t [lt(x, y)]. ((tqtisFinite(x) and tqtisFinite(y)) ->
    (lt(x, y) -> (tqtreal(x) <  tqtreal(y)))))
  
  axiom lt_finite1 :
    (forall x:t. forall y:t [lt(x, y)]. ((tqtisFinite(x) and tqtisFinite(y)) ->
    ((tqtreal(x) <  tqtreal(y)) -> lt(x, y))))
  
  axiom le_finite :
    (forall x:t. forall y:t [le(x, y)]. ((tqtisFinite(x) and tqtisFinite(y)) ->
    (le(x, y) -> (tqtreal(x) <= tqtreal(y)))))
  
  axiom le_finite1 :
    (forall x:t. forall y:t [le(x, y)]. ((tqtisFinite(x) and tqtisFinite(y)) ->
    ((tqtreal(x) <= tqtreal(y)) -> le(x, y))))
  
  axiom le_lt_trans :
    (forall x:t. forall y:t. forall z:t. ((le(x, y) and lt(y, z)) -> lt(x, z)))
  
  axiom lt_le_trans :
    (forall x:t. forall y:t. forall z:t. ((lt(x, y) and le(y, z)) -> lt(x, z)))
  
  axiom le_ge_asym :
    (forall x:t. forall y:t. ((le(x, y) and le(y, x)) -> eq(x, y)))
  
  axiom not_lt_ge :
    (forall x:t. forall y:t. (((not lt(x, y)) and (is_not_nan(x) and
    is_not_nan(y))) -> le(y, x)))
  
  axiom not_gt_le :
    (forall x:t. forall y:t. (((not lt(y, x)) and (is_not_nan(x) and
    is_not_nan(y))) -> le(x, y)))
  
  axiom le_special :
    (forall x:t. forall y:t [le(x, y)]. (le(x, y) -> ((tqtisFinite(x) and
    tqtisFinite(y)) or ((is_minus_infinity(x) and is_not_nan(y)) or
    (is_not_nan(x) and is_plus_infinity(y))))))
  
  axiom lt_special :
    (forall x:t. forall y:t [lt(x, y)]. (lt(x, y) -> ((tqtisFinite(x) and
    tqtisFinite(y)) or ((is_minus_infinity(x) and (is_not_nan(y) and
    (not is_minus_infinity(y)))) or (is_not_nan(x) and
    ((not is_plus_infinity(x)) and is_plus_infinity(y)))))))
  
  axiom lt_lt_finite :
    (forall x:t. forall y:t. forall z:t. (lt(x, y) -> (lt(y, z) ->
    tqtisFinite(y))))
  
  axiom positive_to_real :
    (forall x:t [is_positive(x)]. (tqtisFinite(x) -> (is_positive(x) ->
    (0.0 <= tqtreal(x)))))
  
  axiom to_real_positive :
    (forall x:t [is_positive(x)]. (tqtisFinite(x) -> ((0.0 <  tqtreal(x)) ->
    is_positive(x))))
  
  axiom negative_to_real :
    (forall x:t [is_negative(x)]. (tqtisFinite(x) -> (is_negative(x) ->
    (tqtreal(x) <= 0.0))))
  
  axiom to_real_negative :
    (forall x:t [is_negative(x)]. (tqtisFinite(x) -> ((tqtreal(x) <  0.0) ->
    is_negative(x))))
  
  axiom negative_xor_positive :
    (forall x:t. (not (is_positive(x) and is_negative(x))))
  
  axiom negative_or_positive :
    (forall x:t. (is_not_nan(x) -> (is_positive(x) or is_negative(x))))
  
  axiom diff_sign_trans :
    (forall x:t. forall y:t. forall z:t. ((diff_sign(x, y) and diff_sign(y,
    z)) -> same_sign(x, z)))
  
  axiom diff_sign_product :
    (forall x:t. forall y:t. ((tqtisFinite(x) and (tqtisFinite(y) and
    ((tqtreal(x) * tqtreal(y)) <  0.0))) -> diff_sign(x, y)))
  
  axiom same_sign_product :
    (forall x:t. forall y:t. ((tqtisFinite(x) and (tqtisFinite(y) and
    same_sign(x, y))) -> (0.0 <= (tqtreal(x) * tqtreal(y)))))
  
  predicate product_sign(z: t, x: t, y: t) = ((same_sign(x, y) ->
    is_positive(z)) and (diff_sign(x, y) -> is_negative(z)))
  
  predicate overflow_value(m: mode, x: t) = ((((((m = RNE) ->
    is_infinite(x)) and ((m = RNA) -> is_infinite(x))) and ((m = RTP) ->
    (if is_positive(x) then is_infinite(x) else (tqtisFinite(x) and
    (tqtreal(x) = (-0x1.FFFFFFFFFFFFFp1023)))))) and ((m = RTN) ->
    (if is_positive(x) then (tqtisFinite(x) and
    (tqtreal(x) = 0x1.FFFFFFFFFFFFFp1023)) else is_infinite(x)))) and
    ((m = RTZ) -> (if is_positive(x) then (tqtisFinite(x) and
    (tqtreal(x) = 0x1.FFFFFFFFFFFFFp1023)) else (tqtisFinite(x) and
    (tqtreal(x) = (-0x1.FFFFFFFFFFFFFp1023))))))
  
  predicate sign_zero_result(m: mode, x: t) = (is_zero(x) -> ((((((m = RNE) ->
    is_positive(x)) and ((m = RNA) -> is_positive(x))) and ((m = RTP) ->
    is_positive(x))) and ((m = RTN) -> is_negative(x))) and ((m = RTZ) ->
    is_positive(x))))
  
  axiom add_finite :
    (forall m:mode. forall x:t. forall y:t [add(m, x, y)]. (tqtisFinite(x) ->
    (tqtisFinite(y) -> (no_overflow(m, (tqtreal(x) + tqtreal(y))) ->
    tqtisFinite(add(m, x, y))))))
  
  axiom add_finite1 :
    (forall m:mode. forall x:t. forall y:t [add(m, x, y)]. (tqtisFinite(x) ->
    (tqtisFinite(y) -> (no_overflow(m, (tqtreal(x) + tqtreal(y))) ->
    (tqtreal(add(m, x, y)) = round(m, (tqtreal(x) + tqtreal(y))))))))
  
  axiom add_finite_rev :
    (forall m:mode. forall x:t. forall y:t [add(m, x, y)]. (tqtisFinite(add(m,
    x, y)) -> tqtisFinite(x)))
  
  axiom add_finite_rev1 :
    (forall m:mode. forall x:t. forall y:t [add(m, x, y)]. (tqtisFinite(add(m,
    x, y)) -> tqtisFinite(y)))
  
  axiom add_finite_rev_n :
    (forall m:mode. forall x:t. forall y:t [add(m, x, y)]. (to_nearest(m) ->
    (tqtisFinite(add(m, x, y)) -> no_overflow(m, (tqtreal(x) + tqtreal(y))))))
  
  axiom add_finite_rev_n1 :
    (forall m:mode. forall x:t. forall y:t [add(m, x, y)]. (to_nearest(m) ->
    (tqtisFinite(add(m, x, y)) -> (tqtreal(add(m, x, y)) = round(m,
    (tqtreal(x) + tqtreal(y)))))))
  
  axiom sub_finite :
    (forall m:mode. forall x:t. forall y:t [sub(m, x, y)]. (tqtisFinite(x) ->
    (tqtisFinite(y) -> (no_overflow(m, (tqtreal(x) - tqtreal(y))) ->
    tqtisFinite(sub(m, x, y))))))
  
  axiom sub_finite1 :
    (forall m:mode. forall x:t. forall y:t [sub(m, x, y)]. (tqtisFinite(x) ->
    (tqtisFinite(y) -> (no_overflow(m, (tqtreal(x) - tqtreal(y))) ->
    (tqtreal(sub(m, x, y)) = round(m, (tqtreal(x) - tqtreal(y))))))))
  
  axiom sub_finite_rev :
    (forall m:mode. forall x:t. forall y:t [sub(m, x, y)]. (tqtisFinite(sub(m,
    x, y)) -> tqtisFinite(x)))
  
  axiom sub_finite_rev1 :
    (forall m:mode. forall x:t. forall y:t [sub(m, x, y)]. (tqtisFinite(sub(m,
    x, y)) -> tqtisFinite(y)))
  
  axiom sub_finite_rev_n :
    (forall m:mode. forall x:t. forall y:t [sub(m, x, y)]. (to_nearest(m) ->
    (tqtisFinite(sub(m, x, y)) -> no_overflow(m, (tqtreal(x) - tqtreal(y))))))
  
  axiom sub_finite_rev_n1 :
    (forall m:mode. forall x:t. forall y:t [sub(m, x, y)]. (to_nearest(m) ->
    (tqtisFinite(sub(m, x, y)) -> (tqtreal(sub(m, x, y)) = round(m,
    (tqtreal(x) - tqtreal(y)))))))
  
  axiom mul_finite :
    (forall m:mode. forall x:t. forall y:t [mul(m, x, y)]. (tqtisFinite(x) ->
    (tqtisFinite(y) -> (no_overflow(m, (tqtreal(x) * tqtreal(y))) ->
    tqtisFinite(mul(m, x, y))))))
  
  axiom mul_finite1 :
    (forall m:mode. forall x:t. forall y:t [mul(m, x, y)]. (tqtisFinite(x) ->
    (tqtisFinite(y) -> (no_overflow(m, (tqtreal(x) * tqtreal(y))) ->
    (tqtreal(mul(m, x, y)) = round(m, (tqtreal(x) * tqtreal(y))))))))
  
  axiom mul_finite_rev :
    (forall m:mode. forall x:t. forall y:t [mul(m, x, y)]. (tqtisFinite(mul(m,
    x, y)) -> tqtisFinite(x)))
  
  axiom mul_finite_rev1 :
    (forall m:mode. forall x:t. forall y:t [mul(m, x, y)]. (tqtisFinite(mul(m,
    x, y)) -> tqtisFinite(y)))
  
  axiom mul_finite_rev_n :
    (forall m:mode. forall x:t. forall y:t [mul(m, x, y)]. (to_nearest(m) ->
    (tqtisFinite(mul(m, x, y)) -> no_overflow(m, (tqtreal(x) * tqtreal(y))))))
  
  axiom mul_finite_rev_n1 :
    (forall m:mode. forall x:t. forall y:t [mul(m, x, y)]. (to_nearest(m) ->
    (tqtisFinite(mul(m, x, y)) -> (tqtreal(mul(m, x, y)) = round(m,
    (tqtreal(x) * tqtreal(y)))))))
  
  axiom div_finite :
    (forall m:mode. forall x:t. forall y:t [div(m, x, y)]. (tqtisFinite(x) ->
    (tqtisFinite(y) -> ((not is_zero(y)) -> (no_overflow(m,
    (tqtreal(x) / tqtreal(y))) -> tqtisFinite(div(m, x, y)))))))
  
  axiom div_finite1 :
    (forall m:mode. forall x:t. forall y:t [div(m, x, y)]. (tqtisFinite(x) ->
    (tqtisFinite(y) -> ((not is_zero(y)) -> (no_overflow(m,
    (tqtreal(x) / tqtreal(y))) -> (tqtreal(div(m, x, y)) = round(m,
    (tqtreal(x) / tqtreal(y)))))))))
  
  axiom div_finite_rev :
    (forall m:mode. forall x:t. forall y:t [div(m, x, y)]. (tqtisFinite(div(m,
    x, y)) -> ((tqtisFinite(x) and (tqtisFinite(y) and (not is_zero(y)))) or
    (tqtisFinite(x) and (is_infinite(y) and (tqtreal(div(m, x, y)) = 0.0))))))
  
  axiom div_finite_rev_n :
    (forall m:mode. forall x:t. forall y:t [div(m, x, y)]. (to_nearest(m) ->
    (tqtisFinite(div(m, x, y)) -> (tqtisFinite(y) -> no_overflow(m,
    (tqtreal(x) / tqtreal(y)))))))
  
  axiom div_finite_rev_n1 :
    (forall m:mode. forall x:t. forall y:t [div(m, x, y)]. (to_nearest(m) ->
    (tqtisFinite(div(m, x, y)) -> (tqtisFinite(y) -> (tqtreal(div(m, x,
    y)) = round(m, (tqtreal(x) / tqtreal(y))))))))
  
  axiom neg_finite :
    (forall x:t [neg(x)]. (tqtisFinite(x) -> tqtisFinite(neg(x))))
  
  axiom neg_finite1 :
    (forall x:t [neg(x)]. (tqtisFinite(x) ->
    (tqtreal(neg(x)) = (-tqtreal(x)))))
  
  axiom neg_finite_rev :
    (forall x:t [neg(x)]. (tqtisFinite(neg(x)) -> tqtisFinite(x)))
  
  axiom neg_finite_rev1 :
    (forall x:t [neg(x)]. (tqtisFinite(neg(x)) ->
    (tqtreal(neg(x)) = (-tqtreal(x)))))
  
  axiom abs_finite :
    (forall x:t [abs1(x)]. (tqtisFinite(x) -> tqtisFinite(abs1(x))))
  
  axiom abs_finite1 :
    (forall x:t [abs1(x)]. (tqtisFinite(x) ->
    (tqtreal(abs1(x)) = abs(tqtreal(x)))))
  
  axiom abs_finite2 :
    (forall x:t [abs1(x)]. (tqtisFinite(x) -> is_positive(abs1(x))))
  
  axiom abs_finite_rev :
    (forall x:t [abs1(x)]. (tqtisFinite(abs1(x)) -> tqtisFinite(x)))
  
  axiom abs_finite_rev1 :
    (forall x:t [abs1(x)]. (tqtisFinite(abs1(x)) ->
    (tqtreal(abs1(x)) = abs(tqtreal(x)))))
  
  axiom abs_universal : (forall x:t [abs1(x)]. (not is_negative(abs1(x))))
  
  axiom fma_finite :
    (forall m:mode. forall x:t. forall y:t. forall z:t [fma(m, x, y, z)].
    (tqtisFinite(x) -> (tqtisFinite(y) -> (tqtisFinite(z) -> (no_overflow(m,
    ((tqtreal(x) * tqtreal(y)) + tqtreal(z))) -> tqtisFinite(fma(m, x, y,
    z)))))))
  
  axiom fma_finite1 :
    (forall m:mode. forall x:t. forall y:t. forall z:t [fma(m, x, y, z)].
    (tqtisFinite(x) -> (tqtisFinite(y) -> (tqtisFinite(z) -> (no_overflow(m,
    ((tqtreal(x) * tqtreal(y)) + tqtreal(z))) -> (tqtreal(fma(m, x, y,
    z)) = round(m, ((tqtreal(x) * tqtreal(y)) + tqtreal(z)))))))))
  
  axiom fma_finite_rev :
    (forall m:mode. forall x:t. forall y:t. forall z:t [fma(m, x, y, z)].
    (tqtisFinite(fma(m, x, y, z)) -> tqtisFinite(x)))
  
  axiom fma_finite_rev1 :
    (forall m:mode. forall x:t. forall y:t. forall z:t [fma(m, x, y, z)].
    (tqtisFinite(fma(m, x, y, z)) -> tqtisFinite(y)))
  
  axiom fma_finite_rev2 :
    (forall m:mode. forall x:t. forall y:t. forall z:t [fma(m, x, y, z)].
    (tqtisFinite(fma(m, x, y, z)) -> tqtisFinite(z)))
  
  axiom fma_finite_rev_n :
    (forall m:mode. forall x:t. forall y:t. forall z:t [fma(m, x, y, z)].
    (to_nearest(m) -> (tqtisFinite(fma(m, x, y, z)) -> no_overflow(m,
    ((tqtreal(x) * tqtreal(y)) + tqtreal(z))))))
  
  axiom fma_finite_rev_n1 :
    (forall m:mode. forall x:t. forall y:t. forall z:t [fma(m, x, y, z)].
    (to_nearest(m) -> (tqtisFinite(fma(m, x, y, z)) -> (tqtreal(fma(m, x, y,
    z)) = round(m, ((tqtreal(x) * tqtreal(y)) + tqtreal(z)))))))
  
  function sqr(x: real) : real = (x * x)
  
  logic sqrt1 : real -> real
  
  axiom Sqrt_positive : (forall x:real. ((0.0 <= x) -> (0.0 <= sqrt1(x))))
  
  axiom Sqrt_square : (forall x:real. ((0.0 <= x) -> (sqr(sqrt1(x)) = x)))
  
  axiom Square_sqrt : (forall x:real. ((0.0 <= x) -> (sqrt1((x * x)) = x)))
  
  axiom Sqrt_mul :
    (forall x:real. forall y:real. (((0.0 <= x) and (0.0 <= y)) ->
    (sqrt1((x * y)) = (sqrt1(x) * sqrt1(y)))))
  
  axiom Sqrt_le :
    (forall x:real. forall y:real. (((0.0 <= x) and (x <= y)) ->
    (sqrt1(x) <= sqrt1(y))))
  
  axiom sqrt_finite :
    (forall m:mode. forall x:t [sqrt(m, x)]. (tqtisFinite(x) ->
    ((0.0 <= tqtreal(x)) -> tqtisFinite(sqrt(m, x)))))
  
  axiom sqrt_finite1 :
    (forall m:mode. forall x:t [sqrt(m, x)]. (tqtisFinite(x) ->
    ((0.0 <= tqtreal(x)) -> (tqtreal(sqrt(m, x)) = round(m,
    sqrt1(tqtreal(x)))))))
  
  axiom sqrt_finite_rev :
    (forall m:mode. forall x:t [sqrt(m, x)]. (tqtisFinite(sqrt(m, x)) ->
    tqtisFinite(x)))
  
  axiom sqrt_finite_rev1 :
    (forall m:mode. forall x:t [sqrt(m, x)]. (tqtisFinite(sqrt(m, x)) ->
    (0.0 <= tqtreal(x))))
  
  axiom sqrt_finite_rev2 :
    (forall m:mode. forall x:t [sqrt(m, x)]. (tqtisFinite(sqrt(m, x)) ->
    (tqtreal(sqrt(m, x)) = round(m, sqrt1(tqtreal(x))))))
  
  predicate same_sign_real(x: t, r: real) = ((is_positive(x) and (0.0 <  r)) or
    (is_negative(x) and (r <  0.0)))
  
  axiom add_special :
    (forall m:mode. forall x:t. forall y:t [add(m, x, y)]. ((is_nan(x) or
    is_nan(y)) -> is_nan(add(m, x, y))))
  
  axiom add_special1 :
    (forall m:mode. forall x:t. forall y:t [add(m, x, y)]. ((tqtisFinite(x) and
    is_infinite(y)) -> is_infinite(add(m, x, y))))
  
  axiom add_special2 :
    (forall m:mode. forall x:t. forall y:t [add(m, x, y)]. ((tqtisFinite(x) and
    is_infinite(y)) -> same_sign(add(m, x, y), y)))
  
  axiom add_special3 :
    (forall m:mode. forall x:t. forall y:t [add(m, x, y)]. ((is_infinite(x) and
    tqtisFinite(y)) -> is_infinite(add(m, x, y))))
  
  axiom add_special4 :
    (forall m:mode. forall x:t. forall y:t [add(m, x, y)]. ((is_infinite(x) and
    tqtisFinite(y)) -> same_sign(add(m, x, y), x)))
  
  axiom add_special5 :
    (forall m:mode. forall x:t. forall y:t [add(m, x, y)]. ((is_infinite(x) and
    (is_infinite(y) and same_sign(x, y))) -> is_infinite(add(m, x, y))))
  
  axiom add_special6 :
    (forall m:mode. forall x:t. forall y:t [add(m, x, y)]. ((is_infinite(x) and
    (is_infinite(y) and same_sign(x, y))) -> same_sign(add(m, x, y), x)))
  
  axiom add_special7 :
    (forall m:mode. forall x:t. forall y:t [add(m, x, y)]. ((is_infinite(x) and
    (is_infinite(y) and diff_sign(x, y))) -> is_nan(add(m, x, y))))
  
  axiom add_special8 :
    (forall m:mode. forall x:t. forall y:t [add(m, x, y)]. ((tqtisFinite(x) and
    (tqtisFinite(y) and (not no_overflow(m, (tqtreal(x) + tqtreal(y)))))) ->
    same_sign_real(add(m, x, y), (tqtreal(x) + tqtreal(y)))))
  
  axiom add_special9 :
    (forall m:mode. forall x:t. forall y:t [add(m, x, y)]. ((tqtisFinite(x) and
    (tqtisFinite(y) and (not no_overflow(m, (tqtreal(x) + tqtreal(y)))))) ->
    overflow_value(m, add(m, x, y))))
  
  axiom add_special10 :
    (forall m:mode. forall x:t. forall y:t [add(m, x, y)]. ((tqtisFinite(x) and
    tqtisFinite(y)) -> (same_sign(x, y) -> same_sign(add(m, x, y), x))))
  
  axiom add_special11 :
    (forall m:mode. forall x:t. forall y:t [add(m, x, y)]. ((tqtisFinite(x) and
    tqtisFinite(y)) -> ((not same_sign(x, y)) -> sign_zero_result(m, add(m, x,
    y)))))
  
  axiom sub_special :
    (forall m:mode. forall x:t. forall y:t [sub(m, x, y)]. ((is_nan(x) or
    is_nan(y)) -> is_nan(sub(m, x, y))))
  
  axiom sub_special1 :
    (forall m:mode. forall x:t. forall y:t [sub(m, x, y)]. ((tqtisFinite(x) and
    is_infinite(y)) -> is_infinite(sub(m, x, y))))
  
  axiom sub_special2 :
    (forall m:mode. forall x:t. forall y:t [sub(m, x, y)]. ((tqtisFinite(x) and
    is_infinite(y)) -> diff_sign(sub(m, x, y), y)))
  
  axiom sub_special3 :
    (forall m:mode. forall x:t. forall y:t [sub(m, x, y)]. ((is_infinite(x) and
    tqtisFinite(y)) -> is_infinite(sub(m, x, y))))
  
  axiom sub_special4 :
    (forall m:mode. forall x:t. forall y:t [sub(m, x, y)]. ((is_infinite(x) and
    tqtisFinite(y)) -> same_sign(sub(m, x, y), x)))
  
  axiom sub_special5 :
    (forall m:mode. forall x:t. forall y:t [sub(m, x, y)]. ((is_infinite(x) and
    (is_infinite(y) and same_sign(x, y))) -> is_nan(sub(m, x, y))))
  
  axiom sub_special6 :
    (forall m:mode. forall x:t. forall y:t [sub(m, x, y)]. ((is_infinite(x) and
    (is_infinite(y) and diff_sign(x, y))) -> is_infinite(sub(m, x, y))))
  
  axiom sub_special7 :
    (forall m:mode. forall x:t. forall y:t [sub(m, x, y)]. ((is_infinite(x) and
    (is_infinite(y) and diff_sign(x, y))) -> same_sign(sub(m, x, y), x)))
  
  axiom sub_special8 :
    (forall m:mode. forall x:t. forall y:t [sub(m, x, y)]. ((tqtisFinite(x) and
    (tqtisFinite(y) and (not no_overflow(m, (tqtreal(x) - tqtreal(y)))))) ->
    same_sign_real(sub(m, x, y), (tqtreal(x) - tqtreal(y)))))
  
  axiom sub_special9 :
    (forall m:mode. forall x:t. forall y:t [sub(m, x, y)]. ((tqtisFinite(x) and
    (tqtisFinite(y) and (not no_overflow(m, (tqtreal(x) - tqtreal(y)))))) ->
    overflow_value(m, sub(m, x, y))))
  
  axiom sub_special10 :
    (forall m:mode. forall x:t. forall y:t [sub(m, x, y)]. ((tqtisFinite(x) and
    tqtisFinite(y)) -> (diff_sign(x, y) -> same_sign(sub(m, x, y), x))))
  
  axiom sub_special11 :
    (forall m:mode. forall x:t. forall y:t [sub(m, x, y)]. ((tqtisFinite(x) and
    tqtisFinite(y)) -> ((not diff_sign(x, y)) -> sign_zero_result(m, sub(m, x,
    y)))))
  
  axiom mul_special :
    (forall m:mode. forall x:t. forall y:t [mul(m, x, y)]. ((is_nan(x) or
    is_nan(y)) -> is_nan(mul(m, x, y))))
  
  axiom mul_special1 :
    (forall m:mode. forall x:t. forall y:t [mul(m, x, y)]. ((is_zero(x) and
    is_infinite(y)) -> is_nan(mul(m, x, y))))
  
  axiom mul_special2 :
    (forall m:mode. forall x:t. forall y:t [mul(m, x, y)]. ((tqtisFinite(x) and
    (is_infinite(y) and (not is_zero(x)))) -> is_infinite(mul(m, x, y))))
  
  axiom mul_special3 :
    (forall m:mode. forall x:t. forall y:t [mul(m, x, y)]. ((is_infinite(x) and
    is_zero(y)) -> is_nan(mul(m, x, y))))
  
  axiom mul_special4 :
    (forall m:mode. forall x:t. forall y:t [mul(m, x, y)]. ((is_infinite(x) and
    (tqtisFinite(y) and (not is_zero(y)))) -> is_infinite(mul(m, x, y))))
  
  axiom mul_special5 :
    (forall m:mode. forall x:t. forall y:t [mul(m, x, y)]. ((is_infinite(x) and
    is_infinite(y)) -> is_infinite(mul(m, x, y))))
  
  axiom mul_special6 :
    (forall m:mode. forall x:t. forall y:t [mul(m, x, y)]. ((tqtisFinite(x) and
    (tqtisFinite(y) and (not no_overflow(m, (tqtreal(x) * tqtreal(y)))))) ->
    overflow_value(m, mul(m, x, y))))
  
  axiom mul_special7 :
    (forall m:mode. forall x:t. forall y:t [mul(m, x, y)]. (let r = mul(m, x,
    y) : t in ((not is_nan(r)) -> product_sign(r, x, y))))
  
  axiom div_special :
    (forall m:mode. forall x:t. forall y:t [div(m, x, y)]. ((is_nan(x) or
    is_nan(y)) -> is_nan(div(m, x, y))))
  
  axiom div_special1 :
    (forall m:mode. forall x:t. forall y:t [div(m, x, y)]. ((tqtisFinite(x) and
    is_infinite(y)) -> is_zero(div(m, x, y))))
  
  axiom div_special2 :
    (forall m:mode. forall x:t. forall y:t [div(m, x, y)]. ((is_infinite(x) and
    tqtisFinite(y)) -> is_infinite(div(m, x, y))))
  
  axiom div_special3 :
    (forall m:mode. forall x:t. forall y:t [div(m, x, y)]. ((is_infinite(x) and
    is_infinite(y)) -> is_nan(div(m, x, y))))
  
  axiom div_special4 :
    (forall m:mode. forall x:t. forall y:t [div(m, x, y)]. ((tqtisFinite(x) and
    (tqtisFinite(y) and ((not is_zero(y)) and (not no_overflow(m,
    (tqtreal(x) / tqtreal(y))))))) -> overflow_value(m, div(m, x, y))))
  
  axiom div_special5 :
    (forall m:mode. forall x:t. forall y:t [div(m, x, y)]. ((tqtisFinite(x) and
    (is_zero(y) and (not is_zero(x)))) -> is_infinite(div(m, x, y))))
  
  axiom div_special6 :
    (forall m:mode. forall x:t. forall y:t [div(m, x, y)]. ((is_zero(x) and
    is_zero(y)) -> is_nan(div(m, x, y))))
  
  axiom div_special7 :
    (forall m:mode. forall x:t. forall y:t [div(m, x, y)]. (let r = div(m, x,
    y) : t in ((not is_nan(r)) -> product_sign(r, x, y))))
  
  axiom neg_special : (forall x:t [neg(x)]. (is_nan(x) -> is_nan(neg(x))))
  
  axiom neg_special1 :
    (forall x:t [neg(x)]. (is_infinite(x) -> is_infinite(neg(x))))
  
  axiom neg_special2 :
    (forall x:t [neg(x)]. ((not is_nan(x)) -> diff_sign(x, neg(x))))
  
  axiom abs_special : (forall x:t [abs1(x)]. (is_nan(x) -> is_nan(abs1(x))))
  
  axiom abs_special1 :
    (forall x:t [abs1(x)]. (is_infinite(x) -> is_infinite(abs1(x))))
  
  axiom abs_special2 :
    (forall x:t [abs1(x)]. ((not is_nan(x)) -> is_positive(abs1(x))))
  
  axiom fma_special :
    (forall m:mode. forall x:t. forall y:t. forall z:t [fma(m, x, y, z)].
    ((is_nan(x) or (is_nan(y) or is_nan(z))) -> is_nan(fma(m, x, y, z))))
  
  axiom fma_special1 :
    (forall m:mode. forall x:t. forall y:t. forall z:t [fma(m, x, y, z)].
    ((is_zero(x) and is_infinite(y)) -> is_nan(fma(m, x, y, z))))
  
  axiom fma_special2 :
    (forall m:mode. forall x:t. forall y:t. forall z:t [fma(m, x, y, z)].
    ((is_infinite(x) and is_zero(y)) -> is_nan(fma(m, x, y, z))))
  
  axiom fma_special3 :
    (forall m:mode. forall x:t. forall y:t. forall z:t [fma(m, x, y, z)].
    ((tqtisFinite(x) and ((not is_zero(x)) and (is_infinite(y) and
    tqtisFinite(z)))) -> is_infinite(fma(m, x, y, z))))
  
  axiom fma_special4 :
    (forall m:mode. forall x:t. forall y:t. forall z:t [fma(m, x, y, z)].
    ((tqtisFinite(x) and ((not is_zero(x)) and (is_infinite(y) and
    tqtisFinite(z)))) -> product_sign(fma(m, x, y, z), x, y)))
  
  axiom fma_special5 :
    (forall m:mode. forall x:t. forall y:t. forall z:t [fma(m, x, y, z)].
    ((tqtisFinite(x) and ((not is_zero(x)) and (is_infinite(y) and
    is_infinite(z)))) -> (product_sign(z, x, y) -> is_infinite(fma(m, x, y,
    z)))))
  
  axiom fma_special6 :
    (forall m:mode. forall x:t. forall y:t. forall z:t [fma(m, x, y, z)].
    ((tqtisFinite(x) and ((not is_zero(x)) and (is_infinite(y) and
    is_infinite(z)))) -> (product_sign(z, x, y) -> same_sign(fma(m, x, y, z),
    z))))
  
  axiom fma_special7 :
    (forall m:mode. forall x:t. forall y:t. forall z:t [fma(m, x, y, z)].
    ((tqtisFinite(x) and ((not is_zero(x)) and (is_infinite(y) and
    is_infinite(z)))) -> ((not product_sign(z, x, y)) -> is_nan(fma(m, x, y,
    z)))))
  
  axiom fma_special8 :
    (forall m:mode. forall x:t. forall y:t. forall z:t [fma(m, x, y, z)].
    ((is_infinite(x) and (tqtisFinite(y) and ((not is_zero(y)) and
    tqtisFinite(z)))) -> is_infinite(fma(m, x, y, z))))
  
  axiom fma_special9 :
    (forall m:mode. forall x:t. forall y:t. forall z:t [fma(m, x, y, z)].
    ((is_infinite(x) and (tqtisFinite(y) and ((not is_zero(y)) and
    tqtisFinite(z)))) -> product_sign(fma(m, x, y, z), x, y)))
  
  axiom fma_special10 :
    (forall m:mode. forall x:t. forall y:t. forall z:t [fma(m, x, y, z)].
    ((is_infinite(x) and (tqtisFinite(y) and ((not is_zero(y)) and
    is_infinite(z)))) -> (product_sign(z, x, y) -> is_infinite(fma(m, x, y,
    z)))))
  
  axiom fma_special11 :
    (forall m:mode. forall x:t. forall y:t. forall z:t [fma(m, x, y, z)].
    ((is_infinite(x) and (tqtisFinite(y) and ((not is_zero(y)) and
    is_infinite(z)))) -> (product_sign(z, x, y) -> same_sign(fma(m, x, y, z),
    z))))
  
  axiom fma_special12 :
    (forall m:mode. forall x:t. forall y:t. forall z:t [fma(m, x, y, z)].
    ((is_infinite(x) and (tqtisFinite(y) and ((not is_zero(y)) and
    is_infinite(z)))) -> ((not product_sign(z, x, y)) -> is_nan(fma(m, x, y,
    z)))))
  
  axiom fma_special13 :
    (forall m:mode. forall x:t. forall y:t. forall z:t [fma(m, x, y, z)].
    ((is_infinite(x) and (is_infinite(y) and tqtisFinite(z))) ->
    is_infinite(fma(m, x, y, z))))
  
  axiom fma_special14 :
    (forall m:mode. forall x:t. forall y:t. forall z:t [fma(m, x, y, z)].
    ((is_infinite(x) and (is_infinite(y) and tqtisFinite(z))) ->
    product_sign(fma(m, x, y, z), x, y)))
  
  axiom fma_special15 :
    (forall m:mode. forall x:t. forall y:t. forall z:t [fma(m, x, y, z)].
    ((tqtisFinite(x) and (tqtisFinite(y) and is_infinite(z))) ->
    is_infinite(fma(m, x, y, z))))
  
  axiom fma_special16 :
    (forall m:mode. forall x:t. forall y:t. forall z:t [fma(m, x, y, z)].
    ((tqtisFinite(x) and (tqtisFinite(y) and is_infinite(z))) ->
    same_sign(fma(m, x, y, z), z)))
  
  axiom fma_special17 :
    (forall m:mode. forall x:t. forall y:t. forall z:t [fma(m, x, y, z)].
    ((is_infinite(x) and (is_infinite(y) and is_infinite(z))) ->
    (product_sign(z, x, y) -> is_infinite(fma(m, x, y, z)))))
  
  axiom fma_special18 :
    (forall m:mode. forall x:t. forall y:t. forall z:t [fma(m, x, y, z)].
    ((is_infinite(x) and (is_infinite(y) and is_infinite(z))) ->
    (product_sign(z, x, y) -> same_sign(fma(m, x, y, z), z))))
  
  axiom fma_special19 :
    (forall m:mode. forall x:t. forall y:t. forall z:t [fma(m, x, y, z)].
    ((is_infinite(x) and (is_infinite(y) and is_infinite(z))) ->
    ((not product_sign(z, x, y)) -> is_nan(fma(m, x, y, z)))))
  
  axiom fma_special20 :
    (forall m:mode. forall x:t. forall y:t. forall z:t [fma(m, x, y, z)].
    ((tqtisFinite(x) and (tqtisFinite(y) and (tqtisFinite(z) and
    (not no_overflow(m, ((tqtreal(x) * tqtreal(y)) + tqtreal(z))))))) ->
    same_sign_real(fma(m, x, y, z), ((tqtreal(x) * tqtreal(y)) + tqtreal(z)))))
  
  axiom fma_special21 :
    (forall m:mode. forall x:t. forall y:t. forall z:t [fma(m, x, y, z)].
    ((tqtisFinite(x) and (tqtisFinite(y) and (tqtisFinite(z) and
    (not no_overflow(m, ((tqtreal(x) * tqtreal(y)) + tqtreal(z))))))) ->
    overflow_value(m, fma(m, x, y, z))))
  
  axiom fma_special22 :
    (forall m:mode. forall x:t. forall y:t. forall z:t [fma(m, x, y, z)].
    ((tqtisFinite(x) and (tqtisFinite(y) and tqtisFinite(z))) ->
    (product_sign(z, x, y) -> same_sign(fma(m, x, y, z), z))))
  
  axiom fma_special23 :
    (forall m:mode. forall x:t. forall y:t. forall z:t [fma(m, x, y, z)].
    ((tqtisFinite(x) and (tqtisFinite(y) and tqtisFinite(z))) ->
    ((not product_sign(z, x, y)) ->
    ((((tqtreal(x) * tqtreal(y)) + tqtreal(z)) = 0.0) -> ((m = RTN) ->
    is_negative(fma(m, x, y, z)))))))
  
  axiom fma_special24 :
    (forall m:mode. forall x:t. forall y:t. forall z:t [fma(m, x, y, z)].
    ((tqtisFinite(x) and (tqtisFinite(y) and tqtisFinite(z))) ->
    ((not product_sign(z, x, y)) ->
    ((((tqtreal(x) * tqtreal(y)) + tqtreal(z)) = 0.0) -> ((not (m = RTN)) ->
    is_positive(fma(m, x, y, z)))))))
  
  axiom sqrt_special :
    (forall m:mode. forall x:t [sqrt(m, x)]. (is_nan(x) -> is_nan(sqrt(m, x))))
  
  axiom sqrt_special1 :
    (forall m:mode. forall x:t [sqrt(m, x)]. (is_plus_infinity(x) ->
    is_plus_infinity(sqrt(m, x))))
  
  axiom sqrt_special2 :
    (forall m:mode. forall x:t [sqrt(m, x)]. (is_minus_infinity(x) ->
    is_nan(sqrt(m, x))))
  
  axiom sqrt_special3 :
    (forall m:mode. forall x:t [sqrt(m, x)]. ((tqtisFinite(x) and
    (tqtreal(x) <  0.0)) -> is_nan(sqrt(m, x))))
  
  axiom sqrt_special4 :
    (forall m:mode. forall x:t [sqrt(m, x)]. (is_zero(x) -> same_sign(sqrt(m,
    x), x)))
  
  axiom sqrt_special5 :
    (forall m:mode. forall x:t [sqrt(m, x)]. ((tqtisFinite(x) and
    (0.0 <  tqtreal(x))) -> is_positive(sqrt(m, x))))
  
  axiom of_int_add_exact :
    (forall m:mode. forall n:mode. forall i:int. forall j:int.
    (in_safe_int_range(i) -> (in_safe_int_range(j) ->
    (in_safe_int_range((i + j)) -> eq(of_int(m, (i + j)), add(n, of_int(m, i),
    of_int(m, j)))))))
  
  axiom of_int_sub_exact :
    (forall m:mode. forall n:mode. forall i:int. forall j:int.
    (in_safe_int_range(i) -> (in_safe_int_range(j) ->
    (in_safe_int_range((i - j)) -> eq(of_int(m, (i - j)), sub(n, of_int(m, i),
    of_int(m, j)))))))
  
  axiom of_int_mul_exact :
    (forall m:mode. forall n:mode. forall i:int. forall j:int.
    (in_safe_int_range(i) -> (in_safe_int_range(j) ->
    (in_safe_int_range((i * j)) -> eq(of_int(m, (i * j)), mul(n, of_int(m, i),
    of_int(m, j)))))))
  
  axiom Min_r : (forall x:t. forall y:t. (le(y, x) -> eq(min(x, y), y)))
  
  axiom Min_l : (forall x:t. forall y:t. (le(x, y) -> eq(min(x, y), x)))
  
  axiom Max_r : (forall x:t. forall y:t. (le(y, x) -> eq(max(x, y), x)))
  
  axiom Max_l : (forall x:t. forall y:t. (le(x, y) -> eq(max(x, y), y)))
  
  logic is_int : t -> prop
  
  axiom zeroF_is_int : is_int(zeroF)
  
  axiom of_int_is_int :
    (forall m:mode. forall x:int. (in_int_range(x) -> is_int(of_int(m, x))))
  
  axiom big_float_is_int :
    (forall m:mode. forall i:t. (tqtisFinite(i) -> ((le(i, neg(of_int(m,
    9007199254740992))) or le(of_int(m, 9007199254740992), i)) -> is_int(i))))
  
  axiom roundToIntegral_is_int :
    (forall m:mode. forall x:t. (tqtisFinite(x) -> is_int(roundToIntegral(m,
    x))))
  
  axiom eq_is_int :
    (forall x:t. forall y:t. (eq(x, y) -> (is_int(x) -> is_int(y))))
  
  axiom add_int :
    (forall x:t. forall y:t. forall m:mode. (is_int(x) -> (is_int(y) ->
    (tqtisFinite(add(m, x, y)) -> is_int(add(m, x, y))))))
  
  axiom sub_int :
    (forall x:t. forall y:t. forall m:mode. (is_int(x) -> (is_int(y) ->
    (tqtisFinite(sub(m, x, y)) -> is_int(sub(m, x, y))))))
  
  axiom mul_int :
    (forall x:t. forall y:t. forall m:mode. (is_int(x) -> (is_int(y) ->
    (tqtisFinite(mul(m, x, y)) -> is_int(mul(m, x, y))))))
  
  axiom fma_int :
    (forall x:t. forall y:t. forall z:t. forall m:mode. (is_int(x) ->
    (is_int(y) -> (is_int(z) -> (tqtisFinite(fma(m, x, y, z)) -> is_int(fma(m,
    x, y, z)))))))
  
  axiom neg_int : (forall x:t. (is_int(x) -> is_int(neg(x))))
  
  axiom abs_int1 : (forall x:t. (is_int(x) -> is_int(abs1(x))))
  
  axiom is_int_of_int :
    (forall x:t. forall m:mode. forall mqt:mode. (is_int(x) -> eq(x,
    of_int(mqt, to_int(m, x)))))
  
  axiom is_int_to_int :
    (forall m:mode. forall x:t. (is_int(x) -> in_int_range(to_int(m, x))))
  
  axiom is_int_is_finite : (forall x:t. (is_int(x) -> tqtisFinite(x)))
  
  axiom int_to_real :
    (forall m:mode. forall x:t. (is_int(x) -> (tqtreal(x) = from_int(to_int(m,
    x)))))
  
  axiom truncate_int :
    (forall m:mode. forall i:t. (is_int(i) -> eq(roundToIntegral(m, i), i)))
  
  axiom truncate_neg :
    (forall x:t. (tqtisFinite(x) -> (is_negative(x) -> (roundToIntegral(RTZ,
    x) = roundToIntegral(RTP, x)))))
  
  axiom truncate_pos :
    (forall x:t. (tqtisFinite(x) -> (is_positive(x) -> (roundToIntegral(RTZ,
    x) = roundToIntegral(RTN, x)))))
  
  axiom ceil_le :
    (forall x:t. (tqtisFinite(x) -> le(x, roundToIntegral(RTP, x))))
  
  axiom ceil_lest :
    (forall x:t. forall y:t. ((le(x, y) and is_int(y)) ->
    le(roundToIntegral(RTP, x), y)))
  
  axiom ceil_to_real :
    (forall x:t. (tqtisFinite(x) -> (tqtreal(roundToIntegral(RTP,
    x)) = from_int(ceil(tqtreal(x))))))
  
  axiom ceil_to_int :
    (forall m:mode. forall x:t. (tqtisFinite(x) -> (to_int(m,
    roundToIntegral(RTP, x)) = ceil(tqtreal(x)))))
  
  axiom floor_le :
    (forall x:t. (tqtisFinite(x) -> le(roundToIntegral(RTN, x), x)))
  
  axiom floor_lest :
    (forall x:t. forall y:t. ((le(y, x) and is_int(y)) -> le(y,
    roundToIntegral(RTN, x))))
  
  axiom floor_to_real :
    (forall x:t. (tqtisFinite(x) -> (tqtreal(roundToIntegral(RTN,
    x)) = from_int(floor(tqtreal(x))))))
  
  axiom floor_to_int :
    (forall m:mode. forall x:t. (tqtisFinite(x) -> (to_int(m,
    roundToIntegral(RTN, x)) = floor(tqtreal(x)))))
  
  axiom RNA_down :
    (forall x:t. (lt(sub(RNE, x, roundToIntegral(RTN, x)), sub(RNE,
    roundToIntegral(RTP, x), x)) -> (roundToIntegral(RNA,
    x) = roundToIntegral(RTN, x))))
  
  axiom RNA_up :
    (forall x:t. (lt(sub(RNE, roundToIntegral(RTP, x), x), sub(RNE, x,
    roundToIntegral(RTN, x))) -> (roundToIntegral(RNA,
    x) = roundToIntegral(RTP, x))))
  
  axiom RNA_down_tie :
    (forall x:t. (eq(sub(RNE, x, roundToIntegral(RTN, x)), sub(RNE,
    roundToIntegral(RTP, x), x)) -> (is_negative(x) -> (roundToIntegral(RNA,
    x) = roundToIntegral(RTN, x)))))
  
  axiom RNA_up_tie :
    (forall x:t. (eq(sub(RNE, roundToIntegral(RTP, x), x), sub(RNE, x,
    roundToIntegral(RTN, x))) -> (is_positive(x) -> (roundToIntegral(RNA,
    x) = roundToIntegral(RTP, x)))))
  
  axiom to_int_roundToIntegral :
    (forall m:mode. forall x:t. (to_int(m, x) = to_int(m, roundToIntegral(m,
    x))))
  
  axiom to_int_monotonic :
    (forall m:mode. forall x:t. forall y:t. (tqtisFinite(x) ->
    (tqtisFinite(y) -> (le(x, y) -> (to_int(m, x) <= to_int(m, y))))))
  
  axiom to_int_of_int :
    (forall m:mode. forall i:int. (in_safe_int_range(i) -> (to_int(m, of_int(m,
    i)) = i)))
  
  axiom eq_to_int :
    (forall m:mode. forall x:t. forall y:t. (tqtisFinite(x) -> (eq(x, y) ->
    (to_int(m, x) = to_int(m, y)))))
  
  axiom neg_to_int :
    (forall m:mode. forall x:t. (is_int(x) -> (to_int(m, neg(x)) = (-to_int(m,
    x)))))
  
  axiom roundToIntegral_is_finite :
    (forall m:mode. forall x:t. (tqtisFinite(x) ->
    tqtisFinite(roundToIntegral(m, x))))
  
  axiom round_bound_ne :
    (forall x:real [round(RNE, x)]. (no_overflow(RNE, x) ->
    (((x - (0x1.0p-53 * abs(x))) - 0x1.0p-1075) <= round(RNE, x))))
  
  axiom round_bound_ne1 :
    (forall x:real [round(RNE, x)]. (no_overflow(RNE, x) -> (round(RNE,
    x) <= ((x + (0x1.0p-53 * abs(x))) + 0x1.0p-1075))))
  
  axiom round_bound :
    (forall m:mode. forall x:real [round(m, x)]. (no_overflow(m, x) ->
    (((x - (0x1.0p-52 * abs(x))) - 0x1.0p-1074) <= round(m, x))))
  
  axiom round_bound1 :
    (forall m:mode. forall x:real [round(m, x)]. (no_overflow(m, x) ->
    (round(m, x) <= ((x + (0x1.0p-52 * abs(x))) + 0x1.0p-1074))))
  
  logic fliteral : t
  
  axiom fliteral_axiom : tqtisFinite(fliteral)
  
  axiom fliteral_axiom1 : (tqtreal(fliteral) = 0.0)
  
  function relu(a: t) : t = (if lt(fliteral, a) then a else fliteral)
  
  type model
  
  logic nn_onnx : model
  
  logic x : t
  
  logic x1 : t
  
  logic x2 : t
  
  axiom H : le(fliteral, x)
  
  logic fliteral1 : t
  
  axiom fliteral_axiom2 : tqtisFinite(fliteral1)
  
  axiom fliteral_axiom3 : (tqtreal(fliteral1) = 0.5)
  
  axiom H1 : le(x, fliteral1)
  
  function n0_0() : t = x
  
  logic fliteral2 : t
  
  axiom fliteral_axiom4 : tqtisFinite(fliteral2)
  
  axiom fliteral_axiom5 :
    (tqtreal(fliteral2) = (- 0.14730601012706756591796875))
  
  function n4_0() : t = fliteral2
  
  function n0_1() : t = x1
  
  logic fliteral3 : t
  
  axiom fliteral_axiom6 : tqtisFinite(fliteral3)
  
  axiom fliteral_axiom7 : (tqtreal(fliteral3) = 0.13545693457126617431640625)
  
  function n4_2() : t = fliteral3
  
  function n0_2() : t = x2
  
  logic fliteral4 : t
  
  axiom fliteral_axiom8 : tqtisFinite(fliteral4)
  
  axiom fliteral_axiom9 :
    (tqtreal(fliteral4) = (- 0.13880468904972076416015625))
  
  function n4_4() : t = fliteral4
  
  function n5_0() : t = add(RNE, add(RNE, mul(RNE, n0_0, n4_0), mul(RNE, n0_1,
    n4_2)), mul(RNE, n0_2, n4_4))
  
  logic fliteral5 : t
  
  axiom fliteral_axiom10 : tqtisFinite(fliteral5)
  
  axiom fliteral_axiom11 : (tqtreal(fliteral5) = 0.3481832444667816162109375)
  
  function n3_0() : t = fliteral5
  
  function n6_0() : t = add(RNE, n5_0, n3_0)
  
  function n7_0() : t = relu(n6_0)
  
  logic fliteral6 : t
  
  axiom fliteral_axiom12 : tqtisFinite(fliteral6)
  
  axiom fliteral_axiom13 : (tqtreal(fliteral6) = 0.45817434787750244140625)
  
  function n2_1() : t = fliteral6
  
  logic fliteral7 : t
  
  axiom fliteral_axiom14 : tqtisFinite(fliteral7)
  
  axiom fliteral_axiom15 : (tqtreal(fliteral7) = 0.2798371016979217529296875)
  
  function n4_1() : t = fliteral7
  
  logic fliteral8 : t
  
  axiom fliteral_axiom16 : tqtisFinite(fliteral8)
  
  axiom fliteral_axiom17 :
    (tqtreal(fliteral8) = (- 0.3427034318447113037109375))
  
  function n4_3() : t = fliteral8
  
  logic fliteral9 : t
  
  axiom fliteral_axiom18 : tqtisFinite(fliteral9)
  
  axiom fliteral_axiom19 :
    (tqtreal(fliteral9) = (- 0.1886940896511077880859375))
  
  function n4_5() : t = fliteral9
  
  function n5_1() : t = add(RNE, add(RNE, mul(RNE, n0_0, n4_1), mul(RNE, n0_1,
    n4_3)), mul(RNE, n0_2, n4_5))
  
  logic fliteral10 : t
  
  axiom fliteral_axiom20 : tqtisFinite(fliteral10)
  
  axiom fliteral_axiom21 :
    (tqtreal(fliteral10) = (- 0.544907033443450927734375))
  
  function n3_1() : t = fliteral10
  
  function n6_1() : t = add(RNE, n5_1, n3_1)
  
  function n7_1() : t = relu(n6_1)
  
  logic fliteral11 : t
  
  axiom fliteral_axiom22 : tqtisFinite(fliteral11)
  
  axiom fliteral_axiom23 : (tqtreal(fliteral11) = 0.22944785654544830322265625)
  
  function n2_3() : t = fliteral11
  
  function n8_1() : t = add(RNE, mul(RNE, n7_0, n2_1), mul(RNE, n7_1, n2_3))
  
  logic fliteral12 : t
  
  axiom fliteral_axiom24 : tqtisFinite(fliteral12)
  
  axiom fliteral_axiom25 :
    (tqtreal(fliteral12) = (- 0.615869700908660888671875))
  
  function n1_1() : t = fliteral12
  
  function n9_1() : t = add(RNE, n8_1, n1_1)
  
  logic fliteral13 : t
  
  axiom fliteral_axiom26 : tqtisFinite(fliteral13)
  
  axiom fliteral_axiom27 :
    (tqtreal(fliteral13) = (- 0.37202036380767822265625))
  
  function n2_0() : t = fliteral13
  
  logic fliteral14 : t
  
  axiom fliteral_axiom28 : tqtisFinite(fliteral14)
  
  axiom fliteral_axiom29 : (tqtreal(fliteral14) = 0.6372568607330322265625)
  
  function n2_2() : t = fliteral14
  
  function n8_0() : t = add(RNE, mul(RNE, n7_0, n2_0), mul(RNE, n7_1, n2_2))
  
  logic fliteral15 : t
  
  axiom fliteral_axiom30 : tqtisFinite(fliteral15)
  
  axiom fliteral_axiom31 : (tqtreal(fliteral15) = 0.59453976154327392578125)
  
  function n1_0() : t = fliteral15
  
  function n9_0() : t = add(RNE, n8_0, n1_0)
  
  logic fliteral16 : t
  
  axiom fliteral_axiom32 : tqtisFinite(fliteral16)
  
  axiom fliteral_axiom33 : (tqtreal(fliteral16) = 1.0)
  
  goal G : ((lt(fliteral, n9_0) and lt(n9_0, fliteral1)) and (lt(fliteral,
    n9_1) and lt(n9_1, fliteral16)))
  
  Goal G: Unknown (unrecognized prover answer)
