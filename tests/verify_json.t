Test verify-json

  $ . ./setup_env.sh

  $ cat - > test_data.csv << EOF
  > 1,0.0,1.0,0.784313725,0.019607843,0.776470588
  > EOF

  $ cat - > config.json << EOF
  > {"id":"test_json",
  > "prover":["PyRAT"],
  > "dataset": null,
  > "prover_altern":null,
  > "time_limit":null,
  > "memory_limit":null,
  > "output_file":"out.json",
  > "format":"whyml",
  > "onnx_out_dir":null,
  > "loadpath":["."],
  > "problem":["GenericProblem",
  >    {"filepath":["File","../examples/arithmetic/arithmetic.why"],
  >     "definitions":[["model_filename", "FNN_s42.onnx"]],
  >     "goals":[],
  >     "theories":[]}]
  > }
  > EOF

  $ . ./setup_env.sh

  $ bin/pyrat.py --version
  PyRAT 1.1

  $ caisar verify-json --ltag=ProverSpec config.json
  [DEBUG]{ProverSpec} Prover-tailored specification:
  -5.0 <= x0
  x0 <= 5.0
  -5.0 <= x1
  x1 <= 5.0
  -5.0 <= x2
  x2 <= 5.0
  y0 <= 0.5
  

  $ jq '.problem_answer[1].prover_answer[0]' out.json
  "Unknown"
