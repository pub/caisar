Test verify
  $ . ./setup_env.sh

  $ bin/Marabou --version
  1.0.+

  $ cat > file.mlw <<EOF
  > theory T
  >   use ieee_float.Float64
  >   use caisar.types.Vector
  >   use caisar.model.Model
  >  
  > 
  >   constant nn: model = read_model "TestNetwork.nnet"
  > 
  >   goal G:
  >     forall i: vector t.
  >       has_length i 5 ->
  >       (0.0:t) .<= i[0] .<= (0.5:t) ->
  >       (0.0:t) .< (nn @@ i)[0] .< (0.5:t)
  > 
  >   goal H:
  >     forall i: vector t.
  >       has_length i 5 ->
  >       (0.0:t) .<= i[0] .<= (0.5:t) /\ (0.5:t) .<= i[1] .<= (1.0:t) ->
  >       ((0.0:t) .< (nn @@ i)[0] \/ (0.5:t) .< (nn @@ i)[0]) /\ (0.0:t) .< (nn @@ i)[1] .< (0.5:t)
  > 
  >   goal I:
  >     forall i: vector t.
  >       has_length i 5 ->
  >       (0.0:t) .<= i[0] .<= (0.5:t) /\ (0.5:t) .<= i[1] .<= (1.0:t) ->
  >       (nn @@ i)[1] .< (nn @@ i)[0] \/ (nn @@ i)[0] .< (nn @@ i)[1]
  > 
  >   goal J:
  >     forall i: vector t.
  >       has_length i 5 ->
  >       ((0.0:t) .<= i[0] .<= (0.5:t) \/ (0.75:t) .<= i[0] .<= (1.0:t)) /\ (0.5:t) .<= i[1] .<= (1.0:t) ->
  >       (nn @@ i)[1] .< (nn @@ i)[0] \/ (nn @@ i)[0] .< (nn @@ i)[1]
  > end
  > EOF

  $ caisar verify --prover=Marabou --ltag=ProverSpec file.mlw
  [DEBUG]{ProverSpec} Prover-tailored specification:
  x0 >= 0.0
  x0 <= 0.5
  y0 <= 0.0
  
  [DEBUG]{ProverSpec} Prover-tailored specification:
  x0 >= 0.0
  x0 <= 0.5
  y0 >= 0.5
  
  [DEBUG]{ProverSpec} Prover-tailored specification:
  x0 >= 0.0
  x0 <= 0.5
  x1 >= 0.5
  x1 <= 1.0
  y0 <= 0.0
  y0 <= 0.5
  
  [DEBUG]{ProverSpec} Prover-tailored specification:
  x0 >= 0.0
  x0 <= 0.5
  x1 >= 0.5
  x1 <= 1.0
  y1 <= 0.0
  
  [DEBUG]{ProverSpec} Prover-tailored specification:
  x0 >= 0.0
  x0 <= 0.5
  x1 >= 0.5
  x1 <= 1.0
  y1 >= 0.5
  
  [DEBUG]{ProverSpec} Prover-tailored specification:
  x0 >= 0.0
  x0 <= 0.5
  x1 >= 0.5
  x1 <= 1.0
  +y1 -y0 >= 0
  +y0 -y1 >= 0
  
  [DEBUG]{ProverSpec} Prover-tailored specification:
  x0 >= 0.0
  x0 <= 0.5
  x1 >= 0.5
  x1 <= 1.0
  +y1 -y0 >= 0
  +y0 -y1 >= 0
  
  [DEBUG]{ProverSpec} Prover-tailored specification:
  x0 >= 0.75
  x0 <= 1.0
  x1 >= 0.5
  x1 <= 1.0
  +y1 -y0 >= 0
  +y0 -y1 >= 0
  
  Goal G: Unknown ()
  Goal H: Unknown ()
  Goal I: Unknown ()
  Goal J: Unknown ()

  $ caisar verify --prover=maraboupy --ltag=ProverSpec file.mlw
  [DEBUG]{ProverSpec} Prover-tailored specification:
  x0 >= 0.0
  x0 <= 0.5
  y0 <= 0.0
  
  [DEBUG]{ProverSpec} Prover-tailored specification:
  x0 >= 0.0
  x0 <= 0.5
  y0 >= 0.5
  
  [DEBUG]{ProverSpec} Prover-tailored specification:
  x0 >= 0.0
  x0 <= 0.5
  x1 >= 0.5
  x1 <= 1.0
  y0 <= 0.0
  y0 <= 0.5
  
  [DEBUG]{ProverSpec} Prover-tailored specification:
  x0 >= 0.0
  x0 <= 0.5
  x1 >= 0.5
  x1 <= 1.0
  y1 <= 0.0
  
  [DEBUG]{ProverSpec} Prover-tailored specification:
  x0 >= 0.0
  x0 <= 0.5
  x1 >= 0.5
  x1 <= 1.0
  y1 >= 0.5
  
  [DEBUG]{ProverSpec} Prover-tailored specification:
  x0 >= 0.0
  x0 <= 0.5
  x1 >= 0.5
  x1 <= 1.0
  +y1 -y0 >= 0
  +y0 -y1 >= 0
  
  [DEBUG]{ProverSpec} Prover-tailored specification:
  x0 >= 0.0
  x0 <= 0.5
  x1 >= 0.5
  x1 <= 1.0
  +y1 -y0 >= 0
  +y0 -y1 >= 0
  
  [DEBUG]{ProverSpec} Prover-tailored specification:
  x0 >= 0.75
  x0 <= 1.0
  x1 >= 0.5
  x1 <= 1.0
  +y1 -y0 >= 0
  +y0 -y1 >= 0
  
  Goal G: Unknown ()
  Goal H: Unknown ()
  Goal I: Unknown ()
  Goal J: Unknown ()
