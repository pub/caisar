{
  description = "CAISAR Nix flake.";
  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs";
    flake-utils.url = "github:numtide/flake-utils";
    nix-filter.url = "github:numtide/nix-filter";
    saver = {
        url = "./vendor/saver";
        inputs.nixpkgs.follows = "nixpkgs";
    };
    marabou = {
        url = "./vendor/marabou";
        inputs.nixpkgs.follows = "nixpkgs";
    };
    nnenum = {
        url = "./vendor/nnenum";
        inputs.nixpkgs.follows = "nixpkgs";
    };
    abcrown = {
        url = "./vendor/abcrown";
        inputs.nixpkgs.follows = "nixpkgs";
    };

    pyrat_compiled = {
        url = "./vendor/pyrat_compiled";
        inputs.nixpkgs.follows = "nixpkgs";
    };
    caisar-version = { url = "./VERSION"; flake = false; };
  };
  outputs =
    { self
    , flake-utils
    , nixpkgs
    , nix-filter
    , caisar-version
    , saver
    , marabou
    , nnenum
    , abcrown
    #, pyrat_compiled dependencies to be fixed
    , ...
    }:
    flake-utils.lib.eachDefaultSystem (system:
    let
      pkgs = nixpkgs.legacyPackages.${system};
      lib = pkgs.lib;
      sources = {
        ocaml = nix-filter.lib {
          root = ./.;
          include = [
            ".ocamlformat"
            "dune-project"
            "caisar.opam"
            "Makefile"
            "dune"
            "configurator.ml"
            "VERSION"
            (nix-filter.lib.inDirectory "src")
            (nix-filter.lib.inDirectory "caisarpy")
            (nix-filter.lib.inDirectory "examples")
            (nix-filter.lib.inDirectory "lib")
            (nix-filter.lib.inDirectory "tests")
            (nix-filter.lib.inDirectory "config")
            (nix-filter.lib.inDirectory "stdlib")
            (nix-filter.lib.inDirectory "doc")
            (nix-filter.lib.inDirectory "utils")
          ];
        };
      };
      buildCaisarWith = {p, withSolvers ? false}:
      let
        commonNativeBuildInputs = [
          p.ocaml-protoc-plugin
          pkgs.protobuf
          pkgs.sphinxHook
          pkgs.which
          p.utop
          (pkgs.why3.override (oldAttrs: { ocamlPackages = p; }))
        ];
      in p.buildDunePackage {
        pname = "caisar";
        version = builtins.readFile caisar-version;
        duneVersion = "3";
        minimalOCamlVersion = "4.14";
        installTargets = "all doc";
        src = sources.ocaml;
        nativeBuildInputs = if withSolvers then
          commonNativeBuildInputs ++ [
            # Provers
            saver.packages.${system}.default
            marabou.packages.${system}.default
            nnenum.packages.${system}.default
            abcrown.packages.${system}.default
            # pyrat_compiled.packages.${system}.default
            pkgs.cvc5
            pkgs.z3
          ]
        else
          commonNativeBuildInputs;
        sphinxBuilders = [
          "html"
          "latex"
          # TODO: generate pdf automatically
        ];
        buildInputs = with p;
          [
            csv
            ocplib-endian
            zarith
            ocamlgraph
            ppx_deriving
            ppx_deriving_yojson
            ppx_inline_test
            ppx_deriving_yaml
            ocaml-protoc-plugin
            re
            fpath
            dune-site
            dune-configurator
            dune-release
            fmt
            logs
            cmdliner
            yaml
            pkgs.z3
            pkgs.cvc5
            (pkgs.why3.override
              (oldAttrs: {
                ocamlPackages = p;
              }))
          ];
      };
    in
    {
      packages =
        {
          default = self.packages.${system}.caisar414;
          # CAISAR package; includes binary and documentation
          caisarLatest = buildCaisarWith {p = pkgs.ocaml-ng.ocamlPackages;};
          caisarSolver = buildCaisarWith {p = pkgs.ocaml-ng.ocamlPackages; withSolvers=true;};
          caisar414 = buildCaisarWith {p = pkgs.ocaml-ng.ocamlPackages_4_14;};
          caisar51 = buildCaisarWith {p = pkgs.ocaml-ng.ocamlPackages_5_1;};
        };
      checks = {
        default =
          self.packages.${system}.default.overrideAttrs
            (oldAttrs: {
              name = "check-${oldAttrs.name}";
              dontBuild = true;
              dontInstall = true;
              doCheck = true;
              checkPhase = ''
                make test-ci
              '';
              buildInputs = oldAttrs.buildInputs ++
              (with pkgs;
                [
                  python3
                  python3Packages.onnx
                  python3Packages.scikit-learn
                ]);
              nativeBuildInputs = oldAttrs.nativeBuildInputs ++
              (with pkgs;
                [
                  python3
                  python3Packages.onnx
                  python3Packages.scikit-learn
                  jq
                ]);

            });
        lint =
          self.packages.${system}.default.overrideAttrs
            (oldAttrs: {
              name = "check-${oldAttrs.name}";
              dontBuild = true;
              dontInstall = true;
              doCheck = true;
              checkPhase = ''
                make lint
              '';
              nativeBuildInputs = oldAttrs.nativeBuildInputs ++
                [
                  pkgs.ocaml-ng.ocamlPackages_4_14.ocamlformat_0_25_1
                ];
            });
        pythonBindings =
          self.packages.${system}.default.overrideAttrs
            (oldAttrs: {
              name = "check-${oldAttrs.name}";
              dontBuild = true;
              dontInstall = true;
              doCheck = true;
              checkPhase = ''
                cd caisarpy
                python3 -m unittest tests.loading.TestImports
                python3 -m unittest tests.loading.TestSolversDetected
                cd ../
              '';
                # TODO: add verification tests with Marabou once it is made
                # available inside of CI's environment
              nativeBuildInputs = oldAttrs.nativeBuildInputs ++
                [
                  pkgs.python3
                  pkgs.python3Packages.termcolor
                  pkgs.opam
                  self.packages.${system}.default
                  pkgs.alt-ergo
                  pkgs.cvc5
                ];
            });
      };
      apps = {
        default = {
          type = "app";
          program =
            "${self.packages.${system}.default}/bin/caisar";
        };
      };
      devShells = {
        default = pkgs.mkShell {
          name = "CAISAR development shell environment.";
          inputsFrom = [ self.packages.${system}.default ];
          packages = with pkgs.ocaml-ng.ocamlPackages_4_14;
            [ ocamlformat_0_25_1 ocaml-lsp ];
          shellHook = ''
            echo "Welcome in the development shell for CAISAR."
          '';
        };
        bundled = pkgs.mkShell {
          name = "CAISAR development shell environment with all provers.";
          inputsFrom = [ self.packages.${system}.caisar_solver ];
          packages = with pkgs.ocaml-ng.ocamlPackages_4_14;
            [ ocamlformat_0_25_1 ocaml-lsp ];
          shellHook = ''
            export PYTHONPATH=${nnenum.packages.${system}.default}/src:$PYTHONPATH
            export PYTHONPATH=${abcrown.packages.${system}.default}/complete_verifier:$PYTHONPATH
            export PYTHONPATH=${abcrown.packages.${system}.default}/auto_LiRPA:$PYTHONPATH

            echo "Welcome in the development shell for CAISAR, batteries included."
          '';
        };
      };
    });
}

