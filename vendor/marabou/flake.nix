{
  description = "Marabou Nix flake.";
  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/nixos-unstable";
    flake-utils.url = "github:numtide/flake-utils";
  };

  outputs =
    { self
    , nixpkgs
    , flake-utils
    , ...
    }:
    flake-utils.lib.eachDefaultSystem (system:
    let
      pkgs = nixpkgs.legacyPackages.${system};
      pkgsStatic = nixpkgs.legacyPackages.${system}.pkgsStatic;
      onnx-protofile = (with pkgs; stdenv.mkDerivation {
        name = "onnx-protofile";
        src = fetchFromGitHub
          {
            owner = "onnx";
            repo = "onnx";
            rev = "v1.15.0";
            sparseCheckout = [
              "onnx/"
            ];
            hash = "sha256-sRHeYo/3GWkzeHxq31Lshk5EpFl/HZOnLmtl/PXONtU=";
          };
        installPhase = ''
          mkdir $out
          mv onnx/onnx.proto3 $out/onnx.proto3
        '';
      });
      marabou = (with pkgs; stdenv.mkDerivation {
        pname = "Marabou";
        version = "2.0";
        src = fetchgit {
          url = "https://github.com/NeuralNetworkVerification/Marabou.git";
          rev = "v2.0.0";
          sha256 = "sha256-Bplaq1T+3KPA9Cykzk9Zxje4KQscYDTrDY2rdYlO2E0=";
          fetchSubmodules = true;
        };
        # Protobuf version here 3.20.3, whereas it is 3.18.12 upstream
        nativeBuildInputs = with pkgs; [
          cmake
          pkgsStatic.openblas
          boost186
          python3
          python3Packages.pybind11
          python3Packages.pythonImportsCheckHook
          pkgsStatic.protobuf3_21
          wget
          bash
          gzip
          onnx-protofile
        ];
        cmakeFlags = [
          "-DBUILD_PYTHON=OFF"
          "-DRUN_UNIT_TEST=OFF"
          "-DOPENBLAS_DIR=${pkgsStatic.openblas}"
          "-DPROTOBUF_DIR=${pkgsStatic.protobuf3_21}"
        ];
        # Generating ourselves the onnx proto header, preempting CMake
        # Assuming onnx revision is vX.YY.Z
        prePatch = ''
          export ONNX_VERSION=$(echo "${onnx-protofile.src.rev}" | tr -d v)
          mkdir -p tools/onnx-$ONNX_VERSION
          ${pkgsStatic.protobuf3_21}/bin/protoc --proto_path=${onnx-protofile} --cpp_out=tools/onnx-$ONNX_VERSION ${onnx-protofile}/onnx.proto3
        '';
        # Patch the tests to use python3 constructs and change some paths in
        # cmake to comply with nix installed paths
        patches = [ ./edit-cmake-path.patch ./python3-cxx-tests.patch ];
        installPhase = ''
          mkdir -p $out/bin
          mv Marabou $out/bin/
        '';
      }
      );
    in
    rec {
      packages = rec {
        default = marabou;
      };
      apps = rec {
        default = {
          type = "app";
          program = "${self.packages.${system}.default}/bin/Marabou";
        };
      };
      devShells = rec {
        default = pkgs.mkShell {
          buildInputs = [
            marabou
          ];
        };
      };
    });
}

