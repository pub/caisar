{
  description = "saver Nix flake.";
  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/nixos-unstable";
    flake-utils.url = "github:numtide/flake-utils";
  };

  outputs =
    { self
    , nixpkgs
    , flake-utils
    , ...
    }:
    flake-utils.lib.eachDefaultSystem (system:
    let
      pkgs = nixpkgs.legacyPackages.${system};
      saver = (with pkgs; stdenv.mkDerivation {
        pname = "saver";
        version = "1.0";
        src = fetchgit {
          url = "https://github.com/svm-abstract-verifier/saver.git";
          rev = "de7a6d6";
          sha256 = "sha256-HLJxjlAk2qui0jsUQHQw7+lX2jBkrbjAjSrfkmD2VGg=";
          fetchSubmodules = true;
        };
        buildInputs = [ boost186 openblas ];
        nativeBuildInputs = [
          clang
          python3
          python311Packages.pybind11
        ];
        buildPhase = ''
          cd src/
          make -j $NIX_BUILD_CORES
        '';
        installPhase = ''
          mkdir -p $out/bin
          mv saver $out/bin/
        '';
      }
      );
    in
    rec {
      packages = rec {
        default = saver;
      };
      apps = rec {
        default = {
          type = "app";
          program = "${self.packages.${system}.default}/bin/saver";
        };
      };
      devShells = rec{
        default = pkgs.mkShell {
          buildInputs = [
            saver
          ];
        };
      };
    });
}

