{
  description = "nnenum Nix flake.";
  inputs = {
    flake-utils.url = "github:numtide/flake-utils";
    nix-filter.url = "github:numtide/nix-filter";
    nixpkgs.url = "nixpkgs";
  };
  outputs =
    { self
    , flake-utils
    , nixpkgs
    , nix-filter
    , ...
    }:
    flake-utils.lib.eachDefaultSystem (system:
    let
      pkgs = nixpkgs.legacyPackages.${system};
      pythonPkgs = pkgs.python311Packages;
      lib = pkgs.lib;
      swiglpk = (with pkgs; pythonPkgs.buildPythonPackage rec {
        pname = "swiglpk";
        version = "5.0.10";
        src = fetchPypi {
          inherit pname version;
          hash = "sha256-V6w0rTNNqV3RaBFL/bUK4Qoqaj3e8h5JQfRv5DDFp+E=";
        };
        buildInputs = with pkgs; [ glpk ];
        nativeBuildInputs = with pkgs; [ glpk swig ];
      });
    in
    rec {
      packages = rec {
        pname = "nnenum";
        version = "0.1";
        default = self.packages.${system}.nnenum;
        # Since nnenum does not provide a binary, we need to use it as a package
        nnenum = pythonPkgs.buildPythonPackage
          {
            inherit pname version;
            src = pkgs.fetchFromGitHub {
              inherit pname version;
              owner = "stanleybak";
              repo = "nnenum";
              rev = "cf7c0e72c13543011a7ac3fbe0f5c59c3aafa77e";
              hash = "sha256-/EoGMklTYKaK0UqX/MKbUBWXrCtQQSC2c9ZRV/L6QLI=";
            };
            format = "other";
            propagatedBuildInputs = with pythonPkgs; [
              numpy
              scipy
              onnx
              onnxruntime
              skl2onnx
              termcolor
              swiglpk
            ];
            # nnenum does not provide a setup.py build, so we only need the
            # dependencies and add the `src/` folder to the nix path
            buildPhase = ''
              mkdir $out
            '';
            installPhase = ''
              export OPENBLAS_NUM_THREADS=1
              export OMP_NUM_THREADS=1
              cp -r src/ $out/
            '';
            # TODO: add the PYTHONPATH to the src folder, and set the two
            # environment variables in the derivation
            checkPhase = ''
              export PYTHONPATH=src/:$PYTHONPATH
              bash run_tests.sh
            '';
          };
      };
    });
}





