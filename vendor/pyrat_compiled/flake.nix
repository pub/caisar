{
  description = "PyRAT compiled Nix flake.";
  inputs = {
    flake-utils.url = "github:numtide/flake-utils";
    nix-filter.url = "github:numtide/nix-filter";
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-22.11";
  };
  outputs =
    { self
    , flake-utils
    , nixpkgs
    , nix-filter
    , ...
    }:
    flake-utils.lib.eachDefaultSystem (system:
    let
      pkgs = nixpkgs.legacyPackages.${system};
      pythonPkgs = pkgs.python39Packages;
      lib = pkgs.lib;
    in
    rec {
      packages = rec {
        pname = "pyratCompiled";
        version = "0.1";
        default = self.packages.${system}.pyratCompiled;
        # Since pyrat does not provide a binary, we need to use it as a package
        pyratCompiled = pythonPkgs.buildPythonPackage
          {
            inherit pname version;
            src = pkgs.fetchFromGitLab {
              inherit pname version;
              domain = "git.frama-c.com";
              owner = "pub";
              repo = "pyrat";
              rev = "be7352ca5628669ae6a2ae5149c52a99fe86ed6a";
              hash = "sha256-MojR1pghMIIbG1UaeKu9+w3hd+hLuAItwBrN8qcKxIY=";
            };
            format = "other";
            propagatedBuildInputs = with pythonPkgs; [
              numpy
              scipy
              onnx
              onnxruntime
              skl2onnx
              torch
              termcolor
              loguru
            ];
            # pyrat does not provide a setup.py build, so we only need the
            # dependencies and add the `src/` folder to the nix path
            buildPhase = ''
              mkdir $out
            '';
            installPhase = ''
              cp -r src/ $out/
            '';
            # TODO: add the PYTHONPATH to the src folder, and set the two
            # environment variables in the derivation
            checkPhase = ''
              export PYTHONPATH=src/:$PYTHONPATH
              python3 -c "import pyrat"
            '';
          };
      };
    });
}





