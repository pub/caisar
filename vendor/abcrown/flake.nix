{
  description = "alpha-beta-crown Nix flake.";
  inputs = {
    flake-utils.url = "github:numtide/flake-utils";
    nix-filter.url = "github:numtide/nix-filter";
    nixpkgs.url = "nixpkgs";
  };
  outputs =
    {
      self,
      flake-utils,
      nixpkgs,
      nix-filter,
      ...
    }:
    flake-utils.lib.eachDefaultSystem (
      system:
      let
        pkgs = nixpkgs.legacyPackages.${system};
        pythonPkgs = pkgs.python311Packages;
        lib = pkgs.lib;
        onnx2pytorch = (
          with pkgs;
          pythonPkgs.buildPythonPackage rec {
            pname = "onnx2pytorch";
            version = "0.4.1";
            src = fetchPypi {
              inherit version pname;
              hash = "sha256-+TX2sWL8LbQRG44pSNyiP15pjaCopQoXcSmcCHVL7PM=";
            };
            format = "setuptools";
          }
        );
        autoLirpa = (
          with pkgs;
          pythonPkgs.buildPythonPackage rec {
            pname = "auto_lirpa";
            version = "1.0";
            src = fetchFromGitHub {
              inherit pname;
              owner = "Verified-Intelligence";
              repo = "auto_LiRPA";
              hash = "sha256-vaIISxe+SdJXB1f9tGv3iR1H4nh4O5JEA2jzvJW4Jeo=";
              rev = "bfb7997115c5e66327de81b43c42b23c8a0ffb1e";
            };
            format = "setuptools";
          }
        );
        # TODO: cmake build input seems to mess around with this one
        # I should look setup.py shenanigans
        onnxoptimizer = (
          with pkgs;
          pythonPkgs.buildPythonPackage rec {
            pname = "onnxoptimizer";
            version = "0.3.13";
            format = "setuptools";
            src = fetchPypi {
              inherit version pname;
              hash = "sha256-4Itybg1Fd+UeUp82vDJL8Rt8/xKFLPPu4IHwXIuMbzM=";
            };
            # buildInputs = with pkgs; [ cmake ];
            # nativeBuildInputs = with pkgs; [ cmake ];
            cmakeFlags = [
              "-DBUILD_ONNX_PYTHON=OFF"
              "-DONNX_GEN_PB_TYPE_STUBS=OFF"
            ];
            nativeBuildInputs = with pythonPkgs; [
              pkgs.cmake
              pkgs.bash
              pkgs.protobuf
              pytest-runner
              pybind11
            ];
            # no need for onnx type stubs, and they cause compilation failure
            patches = [ ./dont-build-onnx-types-stubs.patch ];
            # circumventing cmake hook as described here
            # https://discourse.nixos.org/t/python-package-fails-to-find-setup-py-with-cmake-out-of-source-build/17821/2
            buildPhase = ''
              cd ../
              python3 setup.py bdist_wheel
            '';
          }
        );
      in
      {
        packages = rec {
          pname = "alpha-beta-crown";
          version = "1.0";
          default = self.packages.${system}.abcrown;
          # abcrown does not provide a binary either, so similarly
          # to nnenum, we expose it as a package
          abcrown = pythonPkgs.buildPythonPackage {
            inherit pname version;
            src = pkgs.fetchFromGitHub {
              owner = "Verified-Intelligence";
              repo = "alpha-beta-CROWN";
              rev = "dc32df038440a9726e97547b88f9913743773e7f";
              hash = "sha256-dolbomJSVsPTwVG/rzC4hnLcTIfoK5NMA1xOa4C+QQ0=";
              fetchSubmodules = true;
            };
            format = "other";
            propagatedBuildInputs = with pythonPkgs; [
              numpy
              scipy
              onnx
              onnxruntime
              torch
              skl2onnx
              appdirs
              pkgs.protobuf3_21
              onnx2pytorch
              torchvision
              onnxoptimizer
              autoLirpa
              installer
              pandas
              sortedcontainers
            ];
            # abcrown does not provide a setup.py build, so we only need the
            # dependencies and add the `complete_verifier` folder to the nix path
            buildPhase = "  mkdir $out\n";
            installPhase = ''
              cp -r complete_verifier/ $out
              cp -r auto_LiRPA/ $out
            '';
            # TODO: add the PYTHONPATH to the src folder, and set the two
            # environment variables in the derivation
          };
        };
      }
    );
}
